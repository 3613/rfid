package com.redbean.rfid;

/**
 * Created by nampham on 3/7/18.
 */

public enum  RFIDStatus {
    IS_DETECTING,
    RECOGNIZED,
    UNRECOGNIZED,
    PLUGOUT,
}
