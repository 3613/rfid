//package com.redbean.rfid;
//
///**
// * Created by nampham on 3/7/18.
// */
//
//public interface RFIDModule {
//
//    interface Controller{
//        void open();
//
//        void close();
//
//        void startScan();
//
//        void stopScan();
//
//        void setListener(Listener listener);
//
//        void clear();
//    }
//
//    interface Listener{
//        void onNewDevice(String cardId);
//
//        void onConnect();
//
//        void onDisconnect();
//
//        void onChangeStatus(RFIDStatus status);
//
//        void onStopScanSuccess();
//
//        void onStartScanSuccess();
//
//        void onStartScanFailed();
//    }
//}
