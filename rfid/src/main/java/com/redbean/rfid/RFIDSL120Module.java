//package com.redbean.rfid;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.Log;
//
//import com.ivrjack.ru01.IvrJackAdapter;
//import com.ivrjack.ru01.IvrJackService;
//import com.ivrjack.ru01.IvrJackStatus;
//
//import java.lang.ref.WeakReference;
//import java.util.HashMap;
//import java.util.Map;
//
//
///**
// * Created by nampham on 3/7/18.
// */
//
//public class RFIDSL120Module implements RFIDModule.Controller, IvrJackAdapter {
//    private static final String TAG = RFIDSL120Module.class.getSimpleName();
//    private Context mContext;
//    public IvrJackService service;
//    private static RFIDModule.Listener mListener;
//
//    public RFIDSL120Module(Context context) {
//        mContext = context;
//    }
//
//    public void setListener(RFIDModule.Listener mListener) {
//        this.mListener = mListener;
//    }
//
//    @Override
//    public void open() {
//        service = new IvrJackService(mContext, this, 1);
//        service.open();
//    }
//
//    @Override
//    public void close() {
//        service.close();
//        service = null;
//    }
//
//    @Override
//    public void startScan() {
//        new Thread(new StartTask()).start();
//    }
//
//    @Override
//    public void stopScan() {
//        new Thread(new StopTask()).start();
//    }
//
//    @Override
//    public void clear() {
//        countList.clear();
//    }
//
//    @Override
//    public void onConnect(String s) {
//        if (mListener != null) {
//            mListener.onConnect();
//        }
//    }
//
//    @Override
//    public void onDisconnect() {
//        if (mListener != null) {
//            mListener.onDisconnect();
//        }
//    }
//
//    @Override
//    public void onStatusChange(IvrJackStatus ivrJackStatus) {
//        if (mListener != null) {
//            switch (ivrJackStatus) {
//                case ijsDetecting:
//                    mListener.onChangeStatus(RFIDStatus.IS_DETECTING);
//                    break;
//                case ijsRecognized:
//                    mListener.onChangeStatus(RFIDStatus.RECOGNIZED);
//                    break;
//                case ijsUnRecognized:
//                    mListener.onChangeStatus(RFIDStatus.UNRECOGNIZED);
//                    break;
//                case ijsPlugout:
//                    mListener.onChangeStatus(RFIDStatus.PLUGOUT);
//                    break;
//            }
//        }
//    }
//
//    @Override
//    public void onInventory(byte[] epc) {
//        StringBuilder builder = new StringBuilder();
//        for (int i = 0; i < epc.length; i++) {
//            builder.append(String.format("%02X", epc[i]));
//            if ((i + 1) % 4 == 0) builder.append(" ");
//        }
//        String id = builder.toString();
//        addCardList(id);
//    }
//
//    private volatile Map<String, Integer> countList = new HashMap<>();
//
//
//    private synchronized void addCardList(String epc){
//        Integer count = countList.get(epc);
//        if (count == null){
//            //add new
//            countList.put(epc, 1);
//            //star timer
//            Message message = new Message();
//            Bundle data = new Bundle();
//            data.putString("epc", epc);
//            message.setData(data);
//            message.what = 1;
//            mHandler.sendMessageDelayed(message, 2000);
//        } else if (count == 8){
//            countList.remove(epc);
//        } else {
//            countList.put(epc, count + 1);
//        }
//    }
//
//    class StartTask implements Runnable {
//
//        @Override
//        public void run() {
//            int ret = service.setReadEpcStatus((byte) 1);
//            mHandler.obtainMessage(2, ret).sendToTarget();
//        }
//    }
//
//    class StopTask implements Runnable {
//
//        @Override
//        public void run() {
//            int ret = service.setReadEpcStatus((byte) 0);
//            mHandler.obtainMessage(3, ret).sendToTarget();
//        }
//    }
//
//    private MyHandler mHandler = new MyHandler(this);
//
//    private static class MyHandler extends Handler {
//        private final WeakReference<RFIDSL120Module> module;
//
//        public MyHandler(RFIDSL120Module module){
//            this.module = new WeakReference<RFIDSL120Module>(module);
//        }
//
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            RFIDSL120Module parent = module.get();
//            switch (msg.what){
//                case 1:
//                    Bundle data = msg.getData();
//                    String epc = data.getString("epc");
//                    Integer count = parent.countList.get(epc);
//                    if (count >= 3 ){
//                        Log.e(TAG, ">>>>>>> new card <<<<<<<");
//                        if (parent.mListener != null){
//                            parent.mListener.onNewDevice(epc);
//                        }
//                    } else {
//
//                        parent.countList.remove(epc);
//                    }
//                    break;
//                case 2:
//                    //start scan callback
//                    int ret = (Integer) msg.obj;
//                    if (ret == 0) {
//                        //device start read epc
//                        if (parent.mListener != null) {
//                            parent.mListener.onStartScanSuccess();
//                        }
//                    } else {
//                        if (parent.mListener != null) {
//                            parent.mListener.onStartScanFailed();
//                        }
//                    }
//                    break;
//                case 3:
//                    //stop scan callback
//                    if (parent.mListener != null) {
//                        parent.mListener.onStopScanSuccess();
//                    }
//                    break;
//            }
//        }
//    }
//
//
//
//}
