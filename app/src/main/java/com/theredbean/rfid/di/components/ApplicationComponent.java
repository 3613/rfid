package com.theredbean.rfid.di.components;

import android.content.Context;

import com.google.gson.Gson;
//import com.redbean.rfid.RFIDModule;
import com.theredbean.rfid.app.MyApp;
import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.incomemodule.EzParkingInCome;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.data.network.RxService;
import com.theredbean.rfid.data.sharedpreference.ISharedPreferences;
import com.theredbean.rfid.data.sharedpreference.MySharedPref;
import com.theredbean.rfid.di.modules.ApplicationModule;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.Navigator;
import com.theredbean.rfid.rxbus.RxBus;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by nampham on 9/5/17.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(MyApp app);

    void inject(BaseActivity baseActivity);

    void inject(MySharedPref sharedPref);


    Context context();

    RxService rxService();

    Gson gson();

    Navigator navigator();

    @Named("realm")
    IDatabase realmDatabase();

//    RFIDModule.Controller rfidControler();

    ISharedPreferences sharedPreference();

    EzParkManager ezParkManager();

    EzParkingInCome ezParkInCome();

    RxBus rxBus();
    MyApp myApp();
}

