package com.theredbean.rfid.di.components;

import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.di.modules.DatabaseModule;
import com.theredbean.rfid.presentation.firstconfig.FirstConfigActivity;
import com.theredbean.rfid.presentation.splash.IntroActivity;

import dagger.Component;

/**
 * Created by hoang on 19/03/2018.
 */
@Component(dependencies = ApplicationComponent.class, modules = DatabaseModule.class)

@ActivityScope
public interface DatabaseComponent {

    //void inject(HomeActivity activity);

//    void inject(ConfigFragment fragment);

    void inject(IntroActivity activity);


    void inject(FirstConfigActivity firstConfigActivity);
}
