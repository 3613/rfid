package com.theredbean.rfid.di.modules;


import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.presentation.employee.camera.fragmentcheckin.CheckinContract;
import com.theredbean.rfid.presentation.employee.camera.fragmentcheckin.CheckinPresenter;
import com.theredbean.rfid.presentation.menu.config.ConfigContract;
import com.theredbean.rfid.presentation.menu.config.ConfigPresenter;
import com.theredbean.rfid.presentation.firstconfig.FirstConfigContract;
import com.theredbean.rfid.presentation.firstconfig.FirstConfigPresenter;
import com.theredbean.rfid.presentation.splash.IntroContract;
import com.theredbean.rfid.presentation.splash.IntroPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hoang on 19/03/2018 nhe.
 */

@Module
public class DatabaseModule {

//    @Provides
//    @ActivityScope
//    HomeContract.Presenter provideCamerepresenter(HomePresenter presenter) {
//        return presenter;
//    }

    @Provides
    @ActivityScope
    public CheckinContract.Presenter provideCameResultPresenter(CheckinPresenter presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    public ConfigContract.Presenter provideConfigContract(ConfigPresenter presenter) {
        return presenter;
    }


    @Provides
    @ActivityScope
    public IntroContract.Presenter provideIntro(IntroPresenter introPresenter) {
        return introPresenter;
    }

    @Provides
    @ActivityScope
    public FirstConfigContract.Presenter provideFirstConfig(FirstConfigPresenter p) {
        return p;
    }


//    @Provides
//    @ActivityScope
//    public LoginUsecase loginUsecaseprovideLoginUsercase(LoginUsecaseImpl loginUsecase){
//        return loginUsecase;
//    }
}
