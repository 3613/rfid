package com.theredbean.rfid.di.components;

import com.theredbean.rfid.data.services.EzUploadImageService;
import com.theredbean.rfid.data.services.EzUploadImageServiceV2;
import com.theredbean.rfid.data.services.PendingService;
import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.presentation.common.userProfile.UserProfileFragment;
import com.theredbean.rfid.presentation.employee.camera.HomeActivity;
import com.theredbean.rfid.presentation.employee.camera.fragmentcheckin.CheckinFragment;
import com.theredbean.rfid.presentation.employee.camera.fragmentcheckout.CheckoutFragment;
import com.theredbean.rfid.presentation.menu.MenuActivity;
import com.theredbean.rfid.presentation.menu.config.ConfigFragment;
import com.theredbean.rfid.presentation.employee.menu.history.FilterFragment;
import com.theredbean.rfid.presentation.employee.menu.history.HistoryFragment;
import com.theredbean.rfid.presentation.menu.history.childfragment.ui.CurrentParkingFragment;
import com.theredbean.rfid.presentation.menu.history.childfragment.ui.TotalParkedFragment;
import com.theredbean.rfid.presentation.menu.revenue.RevenueFragment;
import com.theredbean.rfid.presentation.login.LoginActivity;
import com.theredbean.rfid.presentation.login.addemployee.RegisterEmployeeFragment;
import com.theredbean.rfid.presentation.login.errorlogin.LoginErrorFragment;
import com.theredbean.rfid.presentation.login.ownerlist.PickOwnerFragment;
import com.theredbean.rfid.presentation.login.register.RegisterOwnerFragment;
import com.theredbean.rfid.presentation.login.addemployee.selectOwner.ChooseOwnerDialog;
import com.theredbean.rfid.presentation.login.signin.LoginFragment;
import com.theredbean.rfid.presentation.owner.OwnerActivity;
import com.theredbean.rfid.presentation.menu.addParking.AddParkingFragment;
import com.theredbean.rfid.presentation.menu.employeeinfo.EmployeeInfoFragment;
import com.theredbean.rfid.presentation.menu.employeelist.EmployeeListFragment;
import com.theredbean.rfid.presentation.owner.incom.IncomeFragment;
import com.theredbean.rfid.presentation.menu.incomedetail.IncomeDetailFragment;
import com.theredbean.rfid.presentation.menu.parkinglist.ParkingFragment;
import com.theredbean.rfid.presentation.menu.summary.SummaryFragment;

import dagger.Component;

/**
 * Created by nampham on 9/26/17.
 */
@Component(dependencies = ApplicationComponent.class, modules = EzParkingModule.class)
@ActivityScope
public interface EzParkingComponent {
    void inject(HomeActivity activity);

    void inject(CheckinFragment fragment);

    void inject(CheckoutFragment fragment);

    void inject(RevenueFragment fragment);

    void inject(SummaryFragment fragment);

    void inject(MenuActivity activity);

    void inject(TotalParkedFragment totalParkedFragment);

    void inject(CurrentParkingFragment totalBikeParkedFragment);

    void inject(HistoryFragment historyFragment);

    void inject(LoginActivity activity);

    void inject(RegisterOwnerFragment registerOwnerFragment);

    void inject(RegisterEmployeeFragment registerEmployeeFragment);

    void inject(LoginFragment loginFragment);

    void inject(EmployeeListFragment employeeListFragment);

    void inject(PickOwnerFragment pickOwnerFragment);

    void inject(OwnerActivity ownerActivity);

    void inject(ParkingFragment parkingFragment);

    void inject(EmployeeInfoFragment employeeInfoFragment);

    void inject(AddParkingFragment addParkingFragment);

    void inject(IncomeFragment incomeFragment);

    void inject(UserProfileFragment userProfileFragment);

    void inject(LoginErrorFragment loginErrorFragment);

    void inject(EzUploadImageService service);

    void inject(ConfigFragment configFragment);

    void inject(ChooseOwnerDialog chooseOwnerDialog);

    void inject(EzUploadImageServiceV2 ezUploadImageServiceV2);

    void inject(PendingService pendingService);

    void inject(FilterFragment filterFragment);

    void inject(IncomeDetailFragment incomeDetailFragment);
}
