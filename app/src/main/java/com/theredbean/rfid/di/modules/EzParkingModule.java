package com.theredbean.rfid.di.modules;


import com.theredbean.rfid.business.checkcard.CheckCardUsecase;
import com.theredbean.rfid.business.checkcard.CheckCardUsecaseImpl;
import com.theredbean.rfid.business.employee.EmployeeUsecase;
import com.theredbean.rfid.business.employee.EmployeeUsecaseImpl;
import com.theredbean.rfid.business.history.HistoryUsecase;
import com.theredbean.rfid.business.history.HistoryUsecaseImpl;
import com.theredbean.rfid.business.image.ImageUsecase;
import com.theredbean.rfid.business.image.ImageUsecaseImpl;
import com.theredbean.rfid.business.imageV2.UploadUsecase;
import com.theredbean.rfid.business.imageV2.UploadUsecaseImpl;
import com.theredbean.rfid.business.income.IncomeUsecase;
import com.theredbean.rfid.business.income.IncomeUsecaseImpl;
import com.theredbean.rfid.business.login.LoginUsecase;
import com.theredbean.rfid.business.login.LoginUsecaseImpl;
import com.theredbean.rfid.business.owner.OwnerUsecase;
import com.theredbean.rfid.business.owner.OwnerUsecaseImpl;
import com.theredbean.rfid.business.parking.ParkingUsercase;
import com.theredbean.rfid.business.parking.ParkingUsercaseImpl;
import com.theredbean.rfid.business.pricing.PriceUsecase;
import com.theredbean.rfid.business.pricing.PriceUsecaseImpl;
import com.theredbean.rfid.business.register.RegisterUsecase;
import com.theredbean.rfid.business.register.RegisterUsecaseImpl;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.data.db.RealmDatabase;
import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.presentation.common.userProfile.UserProfileContract;
import com.theredbean.rfid.presentation.common.userProfile.UserProfilePresenter;
import com.theredbean.rfid.presentation.employee.camera.HomeContract;
import com.theredbean.rfid.presentation.employee.camera.HomePresenter;
import com.theredbean.rfid.presentation.employee.camera.fragmentcheckin.CheckinContract;
import com.theredbean.rfid.presentation.employee.camera.fragmentcheckin.CheckinPresenter;
import com.theredbean.rfid.presentation.employee.camera.fragmentcheckout.CheckoutContract;
import com.theredbean.rfid.presentation.employee.camera.fragmentcheckout.CheckoutPresenter;
import com.theredbean.rfid.presentation.menu.MenuContract;
import com.theredbean.rfid.presentation.menu.MenuPresenter;
import com.theredbean.rfid.presentation.menu.config.ConfigContract;
import com.theredbean.rfid.presentation.menu.config.ConfigPresenter;
import com.theredbean.rfid.presentation.menu.history.HistoryContract;
import com.theredbean.rfid.presentation.menu.history.HistoryPresenter;
import com.theredbean.rfid.presentation.menu.revenue.RevenueContract;
import com.theredbean.rfid.presentation.menu.revenue.RevenuePresenter;
import com.theredbean.rfid.presentation.login.addemployee.selectOwner.ChooseOwnerContract;
import com.theredbean.rfid.presentation.login.addemployee.selectOwner.ChooseOwnerPrenter;
import com.theredbean.rfid.presentation.owner.incom.IncomeContract;
import com.theredbean.rfid.presentation.owner.incom.IncomePresenter;
import com.theredbean.rfid.presentation.menu.incomedetail.IncomeDetailContract;
import com.theredbean.rfid.presentation.menu.incomedetail.IncomeDetailPresenter;
import com.theredbean.rfid.presentation.menu.summary.SummaryContract;
import com.theredbean.rfid.presentation.menu.summary.SummaryPresenter;
import com.theredbean.rfid.presentation.login.LoginContract;
import com.theredbean.rfid.presentation.login.LoginPresenter;
import com.theredbean.rfid.presentation.login.addemployee.RegisterEmployeeContract;
import com.theredbean.rfid.presentation.login.addemployee.RegisterEmployeePresenter;
import com.theredbean.rfid.presentation.login.ownerlist.PickOwnerContract;
import com.theredbean.rfid.presentation.login.ownerlist.PickOwnerPresenter;
import com.theredbean.rfid.presentation.login.register.RegisterOwnerContract;
import com.theredbean.rfid.presentation.login.register.RegisterOwnerPresenter;
import com.theredbean.rfid.presentation.login.signin.LoginFragmentContract;
import com.theredbean.rfid.presentation.login.signin.LoginFragmentPresenter;
import com.theredbean.rfid.presentation.owner.OwnerContract;
import com.theredbean.rfid.presentation.owner.OwnerPresenter;
import com.theredbean.rfid.presentation.menu.addParking.AddParkingContract;
import com.theredbean.rfid.presentation.menu.addParking.AddParkingPresenter;
import com.theredbean.rfid.presentation.menu.employeeinfo.EmployeeInfoContract;
import com.theredbean.rfid.presentation.menu.employeeinfo.EmployeeInfoPresenter;
import com.theredbean.rfid.presentation.menu.employeelist.EmployeeListContract;
import com.theredbean.rfid.presentation.menu.employeelist.EmployeeListPresenter;
import com.theredbean.rfid.presentation.menu.parkinglist.ParkingFragmentContract;
import com.theredbean.rfid.presentation.menu.parkinglist.ParkingFragmentPresenter;

import dagger.Module;
import dagger.Provides;
import utils.PriceCalculator;

/**
 * Created by nampham on 11/17/17.
 */
@Module
public class EzParkingModule {

    @Provides
    @ActivityScope
    CheckoutContract.Presenter provideFragCheckOutPresenter(CheckoutPresenter presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    CheckinContract.Presenter provideFragCheckInPresenter(CheckinPresenter presenter) {
        return presenter;
    }


    @Provides
    @ActivityScope
    HomeContract.Presenter providecameContractPresenter(HomePresenter presenter) {
        return presenter;
    }


    @Provides
    @ActivityScope
    MenuContract.Presenter provideMenuPresenter(MenuPresenter presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    HistoryContract.Presenter provideHistoryPresenter(HistoryPresenter presenter) {
        return presenter;
    }


    @Provides
    @ActivityScope
    RevenueContract.Presenter provideRevenuePresenter(RevenuePresenter presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    SummaryContract.Presenter provideSummaryPresenter(SummaryPresenter presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    LoginContract.Presenter provideLoginPresetener(LoginPresenter presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    RegisterOwnerContract.Presenter presenterRegisterPresenter(RegisterOwnerPresenter registerOwnerPresenter) {
        return registerOwnerPresenter;
    }

    @Provides
    @ActivityScope
    RegisterEmployeeContract.Presenter providePresenter(RegisterEmployeePresenter presenter) {
        return presenter;
    }


    @Provides
    @ActivityScope
    LoginFragmentContract.Presenter provideLoginreFragmentContract(LoginFragmentPresenter presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope
    EmployeeListContract.Presenter provideEmployeeListContract(EmployeeListPresenter employeeListPresenter) {
        return employeeListPresenter;
    }

    @Provides
    @ActivityScope
    LoginUsecase provideLoginUsecase(LoginUsecaseImpl loginUsecase) {
        return loginUsecase;
    }

    @Provides
    @ActivityScope
    RegisterUsecase provideRegisterUsecase(RegisterUsecaseImpl registerUsecase) {
        return registerUsecase;
    }

    @Provides
    @ActivityScope
    EmployeeUsecase proEmployeeUsecase(EmployeeUsecaseImpl employeeUsecase) {
        return employeeUsecase;
    }

    @Provides
    @ActivityScope
    OwnerUsecase provideOwnerUsecase(OwnerUsecaseImpl usecase) {
        return usecase;
    }

    @Provides
    @ActivityScope
    PickOwnerContract.Presenter providePickownerPresenter(PickOwnerPresenter pickOwnerPresenter) {
        return pickOwnerPresenter;
    }

    @Provides
    @ActivityScope
    ChooseOwnerContract.Presenter providePickownerPresenterhaihai(ChooseOwnerPrenter pickOwnerPresenter) {
        return pickOwnerPresenter;
    }


    @Provides
    @ActivityScope
    ParkingUsercase provideParkingUsercase(ParkingUsercaseImpl parkingUsercase) {
        return parkingUsercase;
    }

    @Provides
    @ActivityScope
    ParkingFragmentContract.Presenter providePackingListPresenter(ParkingFragmentPresenter parkingFragmentPresenter) {
        return parkingFragmentPresenter;
    }

    @Provides
    @ActivityScope
    public ImageUsecase imageUsecaseProvider(ImageUsecaseImpl imageUsecase) {
        return imageUsecase;
    }

    @Provides
    @ActivityScope
    public UploadUsecase uploadUsecaseProvider(UploadUsecaseImpl usecase) {
        return usecase;
    }

    @Provides
    @ActivityScope
    public IDatabase imageUsecaseProvideridatabasez(RealmDatabase imageUsecase) {
        return imageUsecase;
    }

    @Provides
    @ActivityScope
    public CheckCardUsecase checkCardUsecaseProvider(CheckCardUsecaseImpl checkCardUsecase) {
        return checkCardUsecase;
    }

    @Provides
    @ActivityScope
    public EmployeeInfoContract.Presenter provideEmployeeInfoContract(EmployeeInfoPresenter p) {
        return p;
    }


    @Provides
    @ActivityScope
    public OwnerContract.Presenter providethisContactActivity(OwnerPresenter ownerPresenter) {
        return ownerPresenter;
    }

    @Provides
    @ActivityScope
    public IncomeUsecase IncomeUsecaseProvide(IncomeUsecaseImpl usecase) {
        return usecase;
    }


    @Provides
    @ActivityScope
    public AddParkingContract.Presenter provideAddParkingContractPresenter(AddParkingPresenter p) {
        return p;
    }

    @Provides
    @ActivityScope
    public IncomeContract.Presenter provideIncomeContractPresenter(IncomePresenter p) {
        return p;
    }


    @Provides
    @ActivityScope
    public HistoryUsecase HistoryUsecaseprovider(HistoryUsecaseImpl usecase) {
        return usecase;
    }

    @Provides
    @ActivityScope
    public UserProfileContract.Presenter provideUserProfileContractPresenter(UserProfilePresenter p) {
        return p;
    }


    @Provides
    @ActivityScope
    public PriceUsecase PriceUsecaseprovider(PriceUsecaseImpl usecase) {
        return usecase;
    }


    @Provides
    @ActivityScope
    public ConfigContract.Presenter provideConfigContractPresenter(ConfigPresenter p) {
        return p;
    }

    @Provides
    @ActivityScope
    PriceCalculator providePriceCalculator(PriceCalculator priceCalculator) {
        return priceCalculator;
    }

    @Provides
    @ActivityScope
    public IncomeDetailContract.Presenter provideIncomeDetailContractPresenter(IncomeDetailPresenter p) {
        return p;
    }
}
