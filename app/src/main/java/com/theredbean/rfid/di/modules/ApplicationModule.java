package com.theredbean.rfid.di.modules;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
//import com.redbean.rfid.RFIDModule;
//import com.redbean.rfid.RFIDSL120Module;
import com.theredbean.rfid.app.MyApp;
import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.incomemodule.EzParkingInCome;
import com.theredbean.rfid.business.incomemodule.EzParkingInComeImpl;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.data.db.RealmDatabase;
import com.theredbean.rfid.data.sharedpreference.ISharedPreferences;
import com.theredbean.rfid.data.sharedpreference.MySharedPref;
import com.theredbean.rfid.rxbus.RxBus;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import timber.log.Timber;


/**
 * Created by nampham on 9/5/17.
 */

@Module
public class ApplicationModule {
    private static final String TAG = ApplicationModule.class.getSimpleName();

    private final Application app;
    private final Gson gson;
    private final IDatabase database;

//    private final RFIDModule.Controller mIRFIDController;
    private final ISharedPreferences mSharePrefercence;

    private final EzParkManager mManager;

    private final EzParkingInCome ezParkingInCome;

    private final RxBus mBus;
    private final MyApp myApp;


    public ApplicationModule(Application app) {
        this.app = app;
        this.gson = new Gson();
        this.database = new RealmDatabase(this.app);
//        mIRFIDController = new RFIDSL120Module(this.app);
        this.mSharePrefercence = new MySharedPref(this.app);
        this.mManager = new EzParkManager();
        this.ezParkingInCome = new EzParkingInComeImpl();
        this.myApp = (MyApp) app;

        this.mBus = new RxBus();


    }


    @Provides
    @Singleton
    public Context provideContext() {
        Timber.tag(TAG).e("appcontextInstance 5 %s", app.hashCode());

        return app;
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return this.gson;
    }

    @Provides
    @Singleton
    @Named("realm")
    public IDatabase provideDatabase() {
        return this.database;
    }

//    @Provides
//    @Singleton
//    public RFIDModule.Controller provideController() {
//        return this.mIRFIDController;
//    }

    @Provides
    @Singleton
    public ISharedPreferences provideSharePreference(Context context, Gson gson) {
        return new MySharedPref(context, gson);
    }

    @Provides
    @Singleton
    public EzParkManager provideEzParkManager() {
        return mManager;
    }

    @Provides
    @Singleton
    public EzParkingInCome provideEzParkIncome() {
        return ezParkingInCome;
    }

    @Provides
    @Singleton
    public RxBus provideBus() {
        return mBus;
    }

    @Provides
    @Singleton
    public MyApp provideMyApp() {
        return myApp;
    }


}
