package com.theredbean.rfid.rxbus;

import android.os.Bundle;

/**
 * Created by nampham on 4/15/18.
 */
public class RxBusEvent {
    public enum Req{
        UPLOAD_IMAGE,
        INTERNET_STATUS,
    }

    public Req what;
    public Bundle data;

    public RxBusEvent(Req what) {
        this.what = what;
    }

    public RxBusEvent(Req what, Bundle data) {
        this.what = what;
        this.data = data;
    }


}
