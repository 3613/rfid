package com.theredbean.rfid.rxbus.penz;

import io.realm.RealmObject;

/**
 * Created by hoang on 14/05/2018 nhe.
 */
public class PendingObject extends RealmObject {
    String localPath;
    long checkinTime;
    long checkouttime;
    String cardId;

    String employeeUuid;
    String parkingUuid;
    String ownerUuid;
    String path;

    @Override
    public String toString() {
        return "PendingObject{" +
                "localPath='" + localPath + '\'' +
                ", checkinTime=" + checkinTime +
                ", checkouttime=" + checkouttime +
                ", cardId='" + cardId + '\'' +
                ", employeeUuid='" + employeeUuid + '\'' +
                ", parkingUuid='" + parkingUuid + '\'' +
                ", ownerUuid='" + ownerUuid + '\'' +
                ", path='" + path + '\'' +
                '}';
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public long getCheckinTime() {
        return checkinTime;
    }

    public void setCheckinTime(long checkinTime) {
        this.checkinTime = checkinTime;
    }

    public long getCheckouttime() {
        return checkouttime;
    }


    public boolean isCurrentRecord() {
        return checkouttime == 0;
    }

    public void setCheckouttime(long checkouttime) {
        this.checkouttime = checkouttime;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getEmployeeUuid() {
        return employeeUuid;
    }

    public void setEmployeeUuid(String employeeUuid) {
        this.employeeUuid = employeeUuid;
    }

    public String getParkingUuid() {
        return parkingUuid;
    }

    public void setParkingUuid(String parkingUuid) {
        this.parkingUuid = parkingUuid;
    }

    public String getOwnerUuid() {
        return ownerUuid;
    }

    public void setOwnerUuid(String ownerUuid) {
        this.ownerUuid = ownerUuid;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public PendingObject() {
        //empty
    }

}
