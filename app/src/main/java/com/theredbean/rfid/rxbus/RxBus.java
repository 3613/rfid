package com.theredbean.rfid.rxbus;


import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by nampham on 4/15/18.
 */
public class RxBus {

    public RxBus(){
        //empty
    }

    public PublishSubject bus = PublishSubject.create();

    public void send(Object o){
        bus.onNext(o);
    }

    public Observable<Object> toObservable(){
        return bus;
    }

    public boolean hasObservers() {
        return bus.hasObservers();
    }
}
