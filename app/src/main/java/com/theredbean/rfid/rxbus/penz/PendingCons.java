package com.theredbean.rfid.rxbus.penz;

/**
 * Created by hoang on 14/05/2018 nhe.
 */
public class PendingCons {
    public static final int CURRENT = 1;
    public static final int HISTORY = 2;

    //key
    public static final String KEY_CACHE_PATH = "cachePath";
    public static final String KEY_LOCAL_PATH = "localPath";
    public static final String KEY_TARGET_IMG_LINK = "onlineImageTarget";
    public static final String KEY_TYPE = "type";
    public static final String KEY_CHECKIN_TIME = "checkinTime";
    public static final String KEY_CHECKOUT_TIME = "checkouttime";
    public static final String KEY_CARD_ID = "cardId";
}
