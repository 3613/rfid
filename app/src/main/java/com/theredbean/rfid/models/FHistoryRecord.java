package com.theredbean.rfid.models;

/**
 * Created by nampham on 4/5/18.
 */
public class FHistoryRecord {
    private String cardId;
    private long timestampCheckIn;
    private String employeeKeyCheckIn;
    private String pathImage;
    private int type;

    private long timestampCheckOut;
    private String employeeKeyCheckOut;
    private int price;

    public FHistoryRecord() {
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public long getTimestampCheckIn() {
        return timestampCheckIn;
    }

    public void setTimestampCheckIn(long timestampCheckIn) {
        this.timestampCheckIn = timestampCheckIn;
    }

    public String getEmployeeKeyCheckIn() {
        return employeeKeyCheckIn;
    }

    public void setEmployeeKeyCheckIn(String employeeKeyCheckIn) {
        this.employeeKeyCheckIn = employeeKeyCheckIn;
    }

    public String getEmployeeKeyCheckOut() {
        return employeeKeyCheckOut;
    }

    public void setEmployeeKeyCheckOut(String employeeKeyCheckOut) {
        this.employeeKeyCheckOut = employeeKeyCheckOut;
    }

    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTimestampCheckOut() {
        return timestampCheckOut;
    }

    public void setTimestampCheckOut(long timestampCheckOut) {
        this.timestampCheckOut = timestampCheckOut;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "HistoryRecord{" +
                "cardId='" + cardId + '\'' +
                ", timestampCheckIn=" + timestampCheckIn +
                ", timestampCheckOut=" + timestampCheckOut +
                ", employeeKeyCheckIn='" + employeeKeyCheckIn + '\'' +
                ", employeeKeyCheckOut='" + employeeKeyCheckOut + '\'' +
                ", pathImage='" + pathImage + '\'' +
                ", type=" + type +
                ", price=" + price +
                '}';
    }
}
