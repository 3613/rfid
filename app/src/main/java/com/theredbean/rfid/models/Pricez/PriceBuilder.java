package com.theredbean.rfid.models.Pricez;

/**
 * Created by hoang on 19/04/2018 nhe.
 */
public class PriceBuilder {
    PriceXedap dap;
    PriceXeGa ga;
    PriceXesSo so;


    public PriceXedap getDap() {
        return dap;
    }

    public PriceBuilder setDap(PriceXedap dap) {
        this.dap = dap;
        return this;
    }

    public PriceXeGa getGa() {
        return ga;
    }

    public PriceBuilder setGa(PriceXeGa ga) {
        this.ga = ga;
        return this;
    }

    public PriceXesSo getSo() {
        return so;

    }

    public PriceBuilder setSo(PriceXesSo so) {
        this.so = so;
        return this;
    }

    public FPrice build() {
        return new FPrice(dap, ga, so);
    }
}
