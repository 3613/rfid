package com.theredbean.rfid.models;

import com.google.firebase.database.Exclude;

import org.parceler.Parcel;

/**
 * Created by nampham on 4/5/18.
 */
@Parcel
public class Employee {

    public String name;
    public String ownerKey;
    public String pass;
    public String description;
    public String parkingKey;

    @Exclude
    public String uuid;

    public Employee() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParkingKey() {
        return parkingKey;
    }

    public void setParkingKey(String parkingKey) {
        this.parkingKey = parkingKey;
    }



    public Employee(String name, String ownerKey, String pass, String description, String parkingKey) {
        this.name = name;
        this.ownerKey = ownerKey;
        this.pass = pass;
        this.description = description;
        this.parkingKey = parkingKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerKey() {
        return ownerKey;
    }

    public void setOwnerKey(String ownerKey) {
        this.ownerKey = ownerKey;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", ownerKey='" + ownerKey + '\'' +
                ", pass='" + pass + '\'' +
                ", description='" + description + '\'' +
                ", parkingKey='" + parkingKey + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}

