package com.theredbean.rfid.models;

import com.google.firebase.database.Exclude;
import com.theredbean.rfid.models.Pricez.FPrice;

import java.util.Map;

/**
 * Created by nampham on 4/5/18.
 */
public class Parking {

    private String name;
    private String address;
    private String description;
    private Map<String,FPrice> prices;

    public Map<String, FPrice> getPrices() {
        return prices;
    }

    public void setPrices(Map<String, FPrice> prices) {
        this.prices = prices;
    }

    @Exclude
    private String uuid;


    public Parking(){
        //empty constructor
    }

    public Parking(String name, String address, String description) {
        this.name = name;
        this.address = address;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }


    @Override
    public String toString() {
        return "Parking{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", description='" + description + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
