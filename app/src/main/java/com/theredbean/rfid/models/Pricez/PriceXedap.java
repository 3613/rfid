package com.theredbean.rfid.models.Pricez;

/**
 * Created by hoang on 19/04/2018 nhe.
 */
public class PriceXedap {
    private int day;
    private int night;



    public PriceXedap(int day, int night) {
        this.day = day;
        this.night = night;
    }

    public PriceXedap() {
    }

    public int getDay() {
        return day;
    }


    public int getNight() {
        return night;
    }


    public static class Builder {
        private int day;
        private int night;

        public Builder setDay(int day) {
            this.day = day;
            return this;
        }

        public Builder setNight(int night) {
            this.night = night;
            return this;
        }

        public PriceXedap build() {
            return new PriceXedap(day, night);
        }
    }
}
