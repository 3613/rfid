package com.theredbean.rfid.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;

/**
 * Created by hoang on 06/04/2018 nhe.
 */
public class Owner {
    private String name;
    private String description;
    private boolean activated;
    private String email;
    private HashMap<String, Parking> parkings;
    @Exclude
    private String uuid;


    public Owner() {
    }

    public HashMap<String, Parking> getParkings() {
        return parkings;
    }

    public void setParkings(HashMap<String, Parking> parkings) {
        this.parkings = parkings;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Owner(String name, String description, boolean activated, String email) {
        this.name = name;
        this.description = description;
        this.activated = activated;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }



    @Override
    public String toString() {
        return "Owner{" + "name='" + name + '\'' + ", description='" + description + '\'' + ", activated=" + activated + '}';
    }
}
