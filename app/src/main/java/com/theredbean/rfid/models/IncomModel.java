package com.theredbean.rfid.models;

/**
 * Created by hoang on 29/05/2018 nhe.
 */
public class IncomModel {
    int price;
    String parkingName;
    String parkingUUid;

    public String getParkingUUid() {
        return parkingUUid;
    }

    public void setParkingUUid(String parkingUUid) {
        this.parkingUUid = parkingUUid;
    }

    public IncomModel(int price, String parkingName, String parkingUUid) {
        this.price = price;
        this.parkingName = parkingName;
        this.parkingUUid = parkingUUid;
    }

    public IncomModel() {
    }

    public IncomModel(int price, String parkingUUid) {
        this.price = price;
        this.parkingUUid = parkingUUid;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getParkingName() {
        return parkingName;
    }

    public void setParkingName(String parkingName) {
        this.parkingName = parkingName;
    }
    @Override
    public String toString() {
        return "IncomModel{" +
                "price=" + price +
                ", parkingName='" + parkingName + '\'' +
                '}';
    }
}
