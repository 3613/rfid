package com.theredbean.rfid.models.Pricez;

/**
 * Created by hoang on 19/04/2018 nhe.
 */
public class PriceXeGa {
    private int day;
    private int night;

    public PriceXeGa() {

    }

    public PriceXeGa(int day, int night) {
        this.day = day;
        this.night = night;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getNight() {
        return night;
    }

    public void setNight(int night) {
        this.night = night;
    }
}
