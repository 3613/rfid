package com.theredbean.rfid.models;

import org.parceler.Parcel;

import java.io.File;

import timber.log.Timber;

/**
 * Created by nampham on 4/5/18.
 */
@Parcel
public class FCurrentRecord {
    public String cardId;
    public long timestampCheckIn;
    public String employeeKey;
    public String pathImage;
    public int type;
    public String localPath;
    public String cachePath;

    public int price;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCachePath() {
        return cachePath;
    }

    public void setCachePath(String cachePath) {
        this.cachePath = cachePath;
    }

    public static String getTAG() {
        return TAG;
    }

    public FCurrentRecord() {
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public long getTimestampCheckIn() {
        return timestampCheckIn;
    }

    public void setTimestampCheckIn(long timestampCheckIn) {
        this.timestampCheckIn = timestampCheckIn;
    }

    public String getEmployeeKey() {
        return employeeKey;
    }

    public void setEmployeeKey(String employeeKey) {
        this.employeeKey = employeeKey;
    }

    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }


    public boolean checkLocal() {
        if (localPath == null || localPath.isEmpty()) {
            Timber.tag(TAG).e("checklocal == null");

            return false;
        }
        return new File(localPath).exists();
    }

    public static final String TAG = FCurrentRecord.class.getSimpleName();


    @Override
    public String toString() {
        return "FCurrentRecord{" +
                "cardId='" + cardId + '\'' +
                ", timestampCheckIn=" + timestampCheckIn +
                ", employeeKey='" + employeeKey + '\'' +
                ", pathImage='" + pathImage + '\'' +
                ", type=" + type +
                ", localPath='" + localPath + '\'' +
                '}';
    }
}
