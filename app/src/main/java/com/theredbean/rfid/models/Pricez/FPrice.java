package com.theredbean.rfid.models.Pricez;

/**
 * Created by hoang on 19/04/2018 nhe.
 */
public class FPrice {
    private PriceXedap xe_dap;
    private PriceXeGa xe_ga;
    private PriceXesSo xe_so;

    public FPrice() {
    }

    FPrice(PriceXedap xe_dap, PriceXeGa xe_ga, PriceXesSo xe_so) {
        this.xe_dap = xe_dap;
        this.xe_ga = xe_ga;
        this.xe_so = xe_so;
    }

    public PriceXedap getXe_dap() {
        return xe_dap;
    }

    public void setXe_dap(PriceXedap xe_dap) {
        this.xe_dap = xe_dap;
    }

    public PriceXeGa getXe_ga() {
        return xe_ga;
    }

    public void setXe_ga(PriceXeGa xe_ga) {
        this.xe_ga = xe_ga;
    }

    public PriceXesSo getXe_so() {
        return xe_so;
    }

    public void setXe_so(PriceXesSo xe_so) {
        this.xe_so = xe_so;
    }

    @Override
    public String toString() {
        return "FPrice{" +
                "dap=" + xe_dap +
                ", ga=" + xe_ga +
                ", so=" + xe_so +
                '}';
    }


    /// 3 casi classes


}
