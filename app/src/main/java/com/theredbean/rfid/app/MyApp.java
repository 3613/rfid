package com.theredbean.rfid.app;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.redbean.rfid.BuildConfig;
//import com.redbean.rfid.RFIDModule;
import com.squareup.leakcanary.LeakCanary;
import com.theredbean.rfid.data.services.EzUploadImageService;
import com.theredbean.rfid.data.services.EzUploadImageServiceV2;
import com.theredbean.rfid.data.services.PendingService;
import com.theredbean.rfid.di.components.ApplicationComponent;
import com.theredbean.rfid.di.components.DaggerApplicationComponent;
import com.theredbean.rfid.di.modules.ApplicationModule;
import com.theredbean.rfid.rxbus.RxBus;
import com.theredbean.rfid.rxbus.RxBusEvent;

import net.danlew.android.joda.JodaTimeAndroid;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;
import utils.EzConstants;
import utils.InternetStatusHelper;


/**
 * Created by nampham on 9/5/17.
 */

public class MyApp extends Application {
    private static final String TAG = MyApp.class.getSimpleName();
    private ApplicationComponent component;
    public boolean isInternet = false;
    private InternetStatusHelper internetelper;

//    @Inject
//    RFIDModule.Controller mRFIDControler;

    @Inject
    RxBus rxBus;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        FirebaseApp.initializeApp(getApplicationContext());

        // init firebase app before setPersictanceEnable required for avoid canary leak
        // todo: Take a look  at Live360 repository ---> good way initialize later nha :D
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return;
//        }
//        LeakCanary.install(this);

        initializeComponent();

        JodaTimeAndroid.init(this);
//        Fabric.with(this, new Crashlytics());
//        mRFIDControler.open();

        //start service
        Intent i = new Intent(this, EzUploadImageService.class);
        startService(i);

        internetelper = new InternetStatusHelper();
        internetelper.setListener(new InternetStatusHelper.InternetStatusHelperListener() {
            @Override
            public void onOnline() {
                Timber.tag(TAG).e("online");

                isInternet = true;
                Bundle data = new Bundle();
                data.putBoolean(EzConstants.STATUS_INTERNET, true);
                RxBusEvent event = new RxBusEvent(RxBusEvent.Req.INTERNET_STATUS, data);
                rxBus.send(event);
                //start intent
                Intent pendingService = new Intent(MyApp.this, EzUploadImageServiceV2.class);
                startService(pendingService);

            }

            @Override
            public void onOffline() {
                Timber.tag(TAG).e("offlinezzz");

                isInternet = false;
                Bundle data = new Bundle();
                data.putBoolean(EzConstants.STATUS_INTERNET, false);
                RxBusEvent event = new RxBusEvent(RxBusEvent.Req.INTERNET_STATUS, data);
                rxBus.send(event);
            }
        });

        internetelper.rcheck();
    }

    @Override
    public void onTerminate() {
//        mRFIDControler.close();
        super.onTerminate();
    }

    public void initializeComponent() {
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        component.inject(this);
    }


    public ApplicationComponent getComponent() {
        return component;
    }

}
