package com.theredbean.rfid.configure;

/**
 * Created by hoang on 06/04/2018 nhe.
 */
public class FirebaseConstant {
    public static final String KEY_OWNER = "Owners";
    public static final String KEY_EMPLOYEES = "Employees";
    public static final String KEY_PARKINGS = "parkings";

    public static final String PARAM_EMPLOYEE_OWNER_UUID = "ownerKey";
    public static final String PARAM_PARKING_UUID = "parkingKey";
    public static final String PARAM_PATH_IMAGE = "pathImage";
    public static final String PARAM_PATH_LOCAL = "localPath";

    public static final String KEY_CURRENT = "Current";
    public static final String KEY_HISTORY = "History";
    public static final String KEY_INCOME = "Income";

    public static final String KEY_TIME_CHECKOUT = "timestampCheckOut";
    public static final String KEY_TIME_CHECIN = "timestampCheckIn";


    public static final String KEY_PRICES = "prices";

    public static final String KEY_ONLINE_STATE = "onlinestatus";
}
