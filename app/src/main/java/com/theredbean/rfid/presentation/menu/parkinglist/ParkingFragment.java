package com.theredbean.rfid.presentation.menu.parkinglist;

import android.app.Fragment;
import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.hnam.uibutton.MyTextView;
import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.Parking;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;
import com.theredbean.rfid.presentation.menu.parkinglist.viewholder.ParkingAdapter;
import com.theredbean.rfid.presentation.menu.parkinglist.viewholder.ParkingViewHolder;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class ParkingFragment extends BaseFragment
        implements ParkingFragmentContract.View
        , ParkingViewHolder.OnParkingViewHolderListener {
    public static final String TAG = ParkingFragment.class.getSimpleName();


    List<Parking> mdata;


    ParkingAdapter adapter2;
    Context context;

    @BindView(R.id.rvParking)
    RecyclerView rvParking;

    @Inject
    ParkingFragmentContract.Presenter presenter;

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.txtNoParking)
    MyTextView txtNoParking;

    @BindView(R.id.flAddParking)
    FloatingActionButton flAddParking;

    public static Fragment getInstance() {
        return new ParkingFragment();
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        // TODO: inflate a fragment view
//        View rootView = super.onCreateView(inflater, container, savedInstanceState);
//        unbinder = ButterKnife.bind(this, rootView);
//        return rootView;
//    }

    public interface MyListener {


        void onItemPackingSelected(int position, String parkingUUID);

        void onAddNewPacking();
    }

    MyListener mListener;

    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof MyListener) {
            mListener = (MyListener) context;
        } else {
            throw new ClassCastException(TAG + " canot cast Mylistener");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_parking_list;
    }

    @Override
    protected void onMyCreateView(View view) {
        initInjection();
        presenter.takeView(this);
        setupRV2();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.dropView();
    }

    private void setupRV2() {
        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        rvParking.setLayoutManager(lm);
        rvParking.setHasFixedSize(true);
        adapter2 = new ParkingAdapter(getActivity(), this);
        rvParking.setAdapter(adapter2);
    }

    public void initInjection() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build()
                .inject(this);

    }

    @Override
    public void onStart() {
        super.onStart();
        Timber.tag(TAG).e("onStart--->");

        presenter.getParkingList();

    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.tag(TAG).e("onResume--->");


    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {
//        pbloading.setVisibility(View.VISIBLE);
        RB_Loading_MSg("");

    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }

    @Override
    public void showAlertDialog(String msg) {

    }

    @Override
    public void ongetListParkingSuccessful(List<Parking> parkings) {
        Timber.tag(TAG).e("ongetListParkingSuccessful %s", parkings.get(0).toString());

        mdata = parkings;
        adapter2.setData(mdata);

    }

    @Override
    public void ongetListParkingFalied(String msg) {
        RB_Error_MSg(msg);
    }

    @Override
    public void displayNoparking() {
        rvParking.setVisibility(View.INVISIBLE);
        txtNoParking.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(int position) {
        Timber.tag(TAG).e("onClick %d", position);
        String parkingUUID = mdata.get(position).getUuid();
        mListener.onItemPackingSelected(position, parkingUUID);


    }

    @Override
    public void onLongClick(int position) {
        Timber.tag(TAG).e("onLongClick %d", position);
        String parkingUUID = mdata.get(position).getUuid();
        mListener.onItemPackingSelected(position, parkingUUID);


    }

    @OnClick(R.id.flAddParking)
    public void onAddParkingButtonClick() {
        // todo open activity
        mListener.onAddNewPacking();
    }
}
