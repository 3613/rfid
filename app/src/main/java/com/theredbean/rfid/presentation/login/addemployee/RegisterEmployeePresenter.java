package com.theredbean.rfid.presentation.login.addemployee;


import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.employee.EmployeeUsecase;
import com.theredbean.rfid.business.register.RegisterUsecase;

import javax.inject.Inject;

import timber.log.Timber;

public class RegisterEmployeePresenter implements RegisterEmployeeContract.Presenter {

    private static final String TAG = RegisterEmployeePresenter.class.getSimpleName();
    RegisterEmployeeContract.View view;

    RegisterUsecase registerUsecase;

    EmployeeUsecase employeeUsecase;

    @Inject

    public RegisterEmployeePresenter(RegisterUsecase registerUsecase, EmployeeUsecase employeeUsecase) {
        this.registerUsecase = registerUsecase;
        this.employeeUsecase = employeeUsecase;
    }


    @Override
    public void takeView(RegisterEmployeeContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        view = null;
    }


    @Override
    public void registerEmployee(String email, String password, String description) {
        view.showLoading();
        registerUsecase.register(email, password, new EzParkingCallback.RegisterCallback() {
            @Override
            public void onRegisterSuccess() {
                view.hideLoading();
                view.showRegisterEmployeeSuccess();
            }

            @Override
            public void onRegisterFailed(String msg) {
                view.hideLoading();
                view.showRegisterEmployeeFailed(msg);
            }

            @Override
            public void onExceptionError(String msg) {
                view.hideLoading();
                view.showRegisterEmployeeFailed(msg);

            }

            @Override
            public void onExceptionAuthentication() {
                view.hideLoading();
                view.showRegisterEmployeeFailed("onExceptionAuthentication");

            }
        });
    }


    @Override
    public void registerEmployeeV2(String email, String password, String name, String description, String ownerUUID) {
        view.showLoading();
        registerUsecase.registerEmployee(email, password, name, description, ownerUUID, new EzParkingCallback.RegisterEmployeeCallback() {
            @Override
            public void onRegisterSuccess() {
                view.hideLoading();
                view.showRegisterEmployeeSuccess();
                Timber.tag(TAG).e("onRegisterSuccess");

            }

            @Override
            public void onRegisterFailed(String msg) {
                view.hideLoading();
                Timber.tag(TAG).e("onRegisterFailed %s,", msg);
                view.showRegisterEmployeeFailed(msg);

            }

            @Override
            public void onExceptionError(String msg) {
                view.hideLoading();
                Timber.tag(TAG).e("onExceptionError %s,", msg);
                view.showRegisterEmployeeFailed(msg);


            }

            @Override
            public void onExceptionAuthentication() {
                view.hideLoading();
                Timber.tag(TAG).e("onExceptionAuthentication %s,", "ok");
                view.showRegisterEmployeeFailed("onExceptionAuthentication");


            }
        });
    }


}
