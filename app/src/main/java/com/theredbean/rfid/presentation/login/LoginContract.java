package com.theredbean.rfid.presentation.login;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface LoginContract {


    interface View extends BaseView {

        void openActivityEmployeeee();

        void openActivityOwner();

        void openLoginScreen();

        void onSignoutSuccess();

        void onSignOutFalied(String msg);

        void showErrorUserScreen();

        void showErrorUserScreen(String msg);
    }

    interface Presenter extends BasePresenter<LoginContract.View> {

        void checkUserLogin();

        void signOut();
    }
}
