package com.theredbean.rfid.presentation.login;

import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.login.LoginUsecase;
import com.theredbean.rfid.business.model.OwnerModel;
import com.theredbean.rfid.business.model.UserProfile;
import com.theredbean.rfid.business.pricing.PriceUsecase;
import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Pricez.FPrice;

import javax.inject.Inject;

import timber.log.Timber;

@ActivityScope
public class LoginPresenter implements LoginContract.Presenter {

    public static final String TAG = LoginPresenter.class.getSimpleName();
    private LoginContract.View mView;
    private LoginUsecase mUsecase;
    PriceUsecase priceUsecase;

    @Inject
    EzParkManager ezParkManager;

    @Inject
    public LoginPresenter(LoginUsecase usecase, PriceUsecase priceUsecase) {
        this.mUsecase = usecase;
        this.priceUsecase = priceUsecase;
    }


    @Override

    public void takeView(LoginContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }


    @Override
    public void checkUserLogin() {
        mView.showLoading();
        mUsecase.checkLogged(new EzParkingCallback.CheckLoggedCallback() {
            @Override
            public void onEmployeeLogged(UserProfile profile, Employee employee) {
                mView.hideLoading();
                Timber.tag(TAG).e("onEmployeeLogged");
                Timber.tag(TAG).e("manager + %s");
                Timber.tag(TAG).e("ezpmnm: %s", ezParkManager.toString());
                // todo check if null ownerKey thi hien thong bao chua phai la nhan vien


                if (employee.getOwnerKey() == null || employee.getParkingKey() == null) {
                    mView.showErrorUserScreen();
                } else {
                    Timber.tag(TAG).e("else roi ne");

                    mView.showLoading();
                    ezParkManager.setEmployee(employee);
                    priceUsecase.loadPriceConfig(employee, new EzParkingCallback.CheckPriceCallBack() {
                        @Override
                        public void onExceptionError(String msg) {
                            Timber.tag(TAG).e("onExceptionError");


                            mView.hideLoading();
                            mView.showMessage("loadPriceConfig %s" + msg);

                        }

                        @Override
                        public void onExceptionAuthentication() {
                            Timber.tag(TAG).e("onExceptionAuthentication");

                            mView.hideLoading();
                            mView.showMessage("onExceptionAuthentication %s");
                        }

                        @Override
                        public void onPriceCheckSuccess(FPrice fPrice) {
                            Timber.tag(TAG).e("onPriceCheckSuccess");

                            mView.hideLoading();

                            ezParkManager.setPrices(fPrice);
                            Timber.tag(TAG).e("pricene %d", fPrice.getXe_dap().getNight());
                            mView.openActivityEmployeeee();


                        }

                        @Override
                        public void onPriceCheckFalied(String msg) {
                            Timber.tag(TAG).e("onPriceCheckFalied %s", msg);
                            // todo handle  lỗi không có price ở đây
                            mView.hideLoading();
                            mView.showAlertDialog(msg);
                            mView.showErrorUserScreen(msg);


                        }
                    });
                }

            }

            @Override
            public void onOwnerLogged(UserProfile profile, OwnerModel owner) {
                Timber.tag(TAG).e("onOwnerLogged");
                ezParkManager.setOwner(owner.getOwner());
                ezParkManager.setParks(owner.getParks());

                mView.hideLoading();
                mView.openActivityOwner();
            }


            @Override
            public void onNoUserLogged() {
                Timber.tag(TAG).e("onNoUserLogged");
                mView.hideLoading();
                mView.openLoginScreen();
            }

            @Override
            public void onExceptionError(String msg) {
                Timber.tag(TAG).e("onExceptionError %s", msg);
                mView.hideLoading();
                mView.showMessage(msg);
            }

            @Override
            public void onExceptionAuthentication() {
                Timber.tag(TAG).e("onExceptionAuthentication %s");

                mView.hideLoading();
                mView.showMessage("onExceptionAuthentication");

            }
        });
    }

    @Override
    public void signOut() {
        mUsecase.signOut(new EzParkingCallback.SignOutCallback() {
            @Override
            public void onSignOutSuccess() {
                mView.onSignoutSuccess();
            }

            @Override
            public void onExceptionError(String msg) {
                mView.onSignOutFalied(msg);
            }

            @Override
            public void onExceptionAuthentication() {
                mView.onSignOutFalied("onExceptionAuthentication");

            }
        });
    }
}
