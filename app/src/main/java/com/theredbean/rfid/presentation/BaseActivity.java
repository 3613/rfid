package com.theredbean.rfid.presentation;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.theredbean.rfid.R;
import com.theredbean.rfid.app.MyApp;
import com.theredbean.rfid.di.components.ApplicationComponent;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;
import utils.AlertDialogListener;

/**
 * Created by hnam on 10/31/2016.
 */

public abstract class BaseActivity
        extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();

    private Unbinder mUnbinder;
    boolean isRegister = false;
    SweetAlertDialog mSweetAlertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        getApplicationContext();
        setUpInternetHelper();
    }


    private void setUpInternetHelper() {
        Timber.tag(TAG).e("start call cai ham");

    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideAlertDialog();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();
        mUnbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        mUnbinder.unbind();
        super.onDestroy();
    }

    protected abstract int getLayoutResId();

    /**
     * show toast message
     *
     * @param message: content
     */
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    /**
     * show alert dialog
     *
     * @param message content which you want to show
     */
    private AlertDialog dialog = null;

    public void showAlertDialog(String message) {
        hideAlertDialog();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name).setMessage(message).setCancelable(true).setPositiveButton("OK", (dialog, i) -> {
            dialog.dismiss();
        });

        dialog = builder.create();
        dialog.show();
    }

    public void showAlertDialog(String title, String message) {
        hideAlertDialog();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(message).setCancelable(true).setPositiveButton("OK", (dialog, i) -> {
            dialog.dismiss();
        });

        dialog = builder.create();
        dialog.show();
    }

    public void hideAlertDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.hide();
            dialog = null;
        }
    }

    private ProgressDialog mProgressDialog = null;

    public void showLoadingDialog(String message) {
        hideLoadingDialog();
        mProgressDialog = ProgressDialog.show(this, null, message, false, false);

    }

    public void hideLoadingDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    /**
     * Get the Main Application component for dependency injection.
     *
     * @return application component
     */
    public ApplicationComponent getApplicationComponent() {
        return ((MyApp) getApplication()).getComponent();
    }


    // hide keyboard
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //show keyboard
    public void showKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }


    public void alertError(String message, int type_error, String title) {
        try {

            mSweetAlertDialog = new SweetAlertDialog(this, type_error);

            mSweetAlertDialog.setTitleText(title).setCustomImage(null).setContentText(message).show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void dissmissSweetDialog() {
        try {
            if (mSweetAlertDialog != null && mSweetAlertDialog.isShowing()) {
                mSweetAlertDialog.dismissWithAnimation();
            }
        } catch (Exception e) {
            Timber.tag(TAG).e("dissmissSweetDialog : %s", e.getCause());
        }
    }

    //theredbean

    public void customToast(String msg) {
        // Get your custom_toast.xml ayout
        LayoutInflater inflater = getLayoutInflater();

        View layout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

// set a message
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(msg);

// Toast...
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public void RB_Succeess_MSg(String message) {
        try {
            if (mSweetAlertDialog != null) {
                mSweetAlertDialog.dismiss();
                mSweetAlertDialog = null;
            }
            mSweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);

            mSweetAlertDialog
                    .setTitleText(getResources()
                            .getString(R.string.success))
                    .setCustomImage(null)
                    .setContentText(message).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void RB_Error_MSg(String message) {
        try {
            if (mSweetAlertDialog != null) {
                mSweetAlertDialog.dismiss();
                mSweetAlertDialog = null;
            }
            mSweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);

            mSweetAlertDialog
                    .setTitleText(getResources()
                            .getString(R.string.have_error))
                    .setCustomImage(null)
                    .setContentText(message).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void RB_Loading_MSg(String message) {
        try {
            if (mSweetAlertDialog != null) {
                mSweetAlertDialog.dismiss();
                mSweetAlertDialog = null;
            }
            mSweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

            mSweetAlertDialog
                    .setTitleText(getResources()
                            .getString(R.string.loading))
                    .setCustomImage(null)
                    .setContentText(message).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void RB_Warning_MSg(String message) {
        try {
            if (mSweetAlertDialog != null) {
                mSweetAlertDialog.dismiss();
                mSweetAlertDialog = null;
            }
            mSweetAlertDialog =
                    new SweetAlertDialog(
                            this,
                            SweetAlertDialog.WARNING_TYPE);

            mSweetAlertDialog
                    .setTitleText(getResources()
                            .getString(R.string.info))
                    .setCustomImage(null)
                    .setContentText(message).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void RB_Confirm_Dialog(String title,
                                  String contentText,
                                  String cancelText,
                                  String confirmtext,
                                  AlertDialogListener alertListener) {
        try {


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    create(title, contentText, cancelText, confirmtext, alertListener);
                }
            }, 200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void create(String title,
                        String contentText,
                        String cancelText,
                        String confirmtext,
                        AlertDialogListener alertListener) {

        if (mSweetAlertDialog != null) {
            mSweetAlertDialog.dismiss();
            mSweetAlertDialog = null;
            Timber.tag(TAG).e("mSweetAlertDialog if ");

        }
        Timber.tag(TAG).e("mSweetAlertDialog without if ");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                init(title, contentText, cancelText, confirmtext, alertListener);
            }
        }, 200);

    }

    private void init(String title, String contentText, String cancelText, String confirmtext, AlertDialogListener alertListener) {
        mSweetAlertDialog = new SweetAlertDialog(
                this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(contentText)
                .setConfirmText(confirmtext)
                .setCancelText(cancelText)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        alertListener.onAlertConfirmClick();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        alertListener.onAlertCancelClick();
                    }
                });
        mSweetAlertDialog.show();
    }

}
