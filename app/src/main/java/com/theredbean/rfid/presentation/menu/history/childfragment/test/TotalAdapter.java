package com.theredbean.rfid.presentation.menu.history.childfragment.test;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theredbean.rfid.R;
import com.theredbean.rfid.models.FHistoryRecord;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by hoang on 02/04/2018 nhe.
 */
public class TotalAdapter extends RecyclerView.Adapter<com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.TotalViewholder> {
    private static final String TAG = TotalAdapter.class.getSimpleName();
    private Activity mActivity;
    private List<FHistoryRecord> mData;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.TotalViewholder.OnTTViewholderListener mListener;

    public TotalAdapter(Context context, com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.TotalViewholder.OnTTViewholderListener listener) {
        mLayoutInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mListener = listener;
        mData = null;
    }

    public void setData(List<FHistoryRecord> data) {
        if (mData == null) {
            mData = new ArrayList<>();
        }
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.TotalViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_history_parked_bike, parent, false);
        return new com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.TotalViewholder(view, listener);
    }

    @Override
    public void onBindViewHolder(com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.TotalViewholder holder, int position) {
        FHistoryRecord entity = mData.get(position);
        holder.renderUi(mContext,position, entity);
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    private final com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.TotalViewholder.OnTTViewholderListener listener = new com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.TotalViewholder.OnTTViewholderListener() {
        @Override
        public void onClick(int position) {
            mListener.onClick(position);

        }

        @Override
        public void onLongClick(int position) {
            mListener.onLongClick(position);
        }
    };
}
