package com.theredbean.rfid.presentation.login.errorlogin;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.hnam.uibutton.MyButton;
import com.hnam.uibutton.MyTextView;
import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

public class LoginErrorFragment extends BaseFragment implements LoginErrorContract.View {

    public static final String TAG = LoginErrorFragment.class.getSimpleName();


    //    @Inject
    LoginErrorContract.Presenter presenter;

    Context context;

    Mylistener mLMylistener;

    @BindView(R.id.tvErrorMsg)
    MyTextView tvErrorMsg;
    @BindView(R.id.btnLogut)
    MyButton btnLogut;
    Unbinder unbinder;

    public static Fragment getInstance() {
        LoginErrorFragment loginErrorFragment = new LoginErrorFragment();

        if (loginErrorFragment == null) {
            loginErrorFragment = new
                    LoginErrorFragment();
        }
        return loginErrorFragment;
    }

    public static Fragment getInstance(String msg) {
        LoginErrorFragment loginErrorFragment = new LoginErrorFragment();

        if (loginErrorFragment == null) {
            loginErrorFragment = new
                    LoginErrorFragment();
        }
        Bundle data = new Bundle();
        data.putString("ERROR_MSG", msg);
        loginErrorFragment.setArguments(data);
        return loginErrorFragment;
    }

    public interface Mylistener {
        void onSignOut();
    }


    @OnClick(R.id.btnLogut)
    public void showLoginScreen() {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.signOut();
        mLMylistener.onSignOut();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_login_error;
    }


    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof Mylistener) {
            mLMylistener = (Mylistener) context;
        } else {
            throw new ClassCastException(TAG + " -- can not cast Mylistener interface");
        }
    }

    String errorMsg = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInjection();
        /// get path from Activity camera
        Bundle bundle = getArguments();
        if (bundle != null) {

            errorMsg = bundle.getString("ERROR_MSG");
            Timber.tag(TAG).e("----------bulldle %s", errorMsg);

        } else {
            Timber.tag(TAG).e("----nulllllllllll bundle");

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, super.onCreateView(inflater, container, savedInstanceState));
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    protected void onMyCreateView(View view) {
        tvErrorMsg.setText(errorMsg);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        presenter.dropView();
        unbinder.unbind();
    }

    @Override
    public void showMessage(String message) {
        RB_Succeess_MSg(message);
    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {
        RB_Loading_MSg(msg);

    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }

    @Override
    public void showAlertDialog(String msg) {
        RB_Warning_MSg(msg);
    }

    private void initInjection() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build()
                .inject(this);

    }
}
