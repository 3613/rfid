package com.theredbean.rfid.presentation.login.ownerlist;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;
import com.theredbean.rfid.presentation.Navigator;
import com.theredbean.rfid.presentation.login.ownerlist.viewholder.OwnerViewHolder;
import com.theredbean.rfid.presentation.login.ownerlist.viewholder.PickOwnerAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

public class PickOwnerFragment extends BaseFragment
        implements PickOwnerContract.View
        , OwnerViewHolder.OnOwnerListener {
    public static final String TAG = PickOwnerFragment.class.getSimpleName();

    @Inject
    Navigator navigator;
    // rv

    List<Owner> mdata;
    PickOwnerAdapter ownerListAdapter;

    PickOwnerAdapter adapter;

    @BindView(R.id.rvOwner)
    RecyclerView rvOwner;
//    rv

    @Inject
    PickOwnerContract.Presenter presenter;
    Context context;
    PickOwnerFragmentListener mListener;

    @Override
    public void onItemClick(int position) {
        Timber.tag(TAG).e("onitem clicked  %d", position);
        String owner_UID = mdata.get(position).getUuid();
//        mListener.onSelectedOwnerId(owner_UID);
        presenter.saveEmployee(owner_UID);
    }

    @Override
    public void onItemLongClick(int position) {
        Timber.tag(TAG).e("onitem Longclicked  %d", position);

    }


    public static Fragment getInstance() {
        return new PickOwnerFragment();
    }

    public interface PickOwnerFragmentListener {
        void gotoLoginActivity();

        void onRegisterEmployeeSucess();
    }

    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof PickOwnerFragment.PickOwnerFragmentListener) {
            mListener = (PickOwnerFragment.PickOwnerFragmentListener) context;
        } else {
            throw new ClassCastException(TAG + " theredbean Canot cast" + TAG);
        }

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInjection();
        Bundle bundle = getArguments();
        if (bundle != null) {
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_pick_list_owner;
    }

    @Override
    protected void onMyCreateView(View view) {
        presenter.takeView(this);

    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.getListOwner();

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }

    @Override
    public void showAlertDialog(String msg) {

    }

    @Override
    public void onGetlistOwnerSuccess(List<Owner> ownerList) {
        mdata = ownerList;
        setupRV2();
    }

    @Override
    public void onGetlistOwnerFalied() {

    }


    @Override
    public void onSaveEmployeeSuccess() {
        presenter.signOut();
//        RB_Succeess_MSg("");
        mListener.gotoLoginActivity();
        mListener.onRegisterEmployeeSucess();


    }

    @Override
    public void onSaveEmployeeFailed(String msg) {
        RB_Error_MSg(msg);
    }

    public void initInjection() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build()
                .inject(this);

    }

    private void setupRV2() {
        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        rvOwner.setLayoutManager(lm);
        rvOwner.setHasFixedSize(true);
        ownerListAdapter = new PickOwnerAdapter(getActivity(), this);
        ownerListAdapter.setData(mdata);
        rvOwner.setAdapter(ownerListAdapter);


    }
}
