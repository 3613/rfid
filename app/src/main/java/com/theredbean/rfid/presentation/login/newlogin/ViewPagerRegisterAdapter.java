package com.theredbean.rfid.presentation.login.newlogin;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.theredbean.rfid.presentation.login.addemployee.RegisterEmployeeFragment;
import com.theredbean.rfid.presentation.login.register.RegisterOwnerFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoang on 29/03/2018 nhe.
 */

public class ViewPagerRegisterAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragmentList;
    private List<String> fragmentTitleList;
    Context context;

    public ViewPagerRegisterAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;

        fragmentList = new ArrayList<>();
        fragmentList.add(RegisterOwnerFragment.getInstance());
        fragmentList.add(new RegisterEmployeeFragment());


        fragmentTitleList = new ArrayList<>();

        fragmentTitleList.add("Thêm chủ bãi xe");
        fragmentTitleList.add("Thêm nhân viên");
    }


    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitleList.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }


}
