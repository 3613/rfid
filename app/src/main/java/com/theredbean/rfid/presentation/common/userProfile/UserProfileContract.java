package com.theredbean.rfid.presentation.common.userProfile;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface UserProfileContract {

    interface View extends BaseView {
        void onLoadCurrentProfileSuccess();

        void onLoadCurrentProfileFailed(String whatHappend);


        void onUpdateProfileSuccess();

        void onUpdateProfileFailed(String whatHappend);
    }

    interface Presenter extends BasePresenter<UserProfileContract.View> {

        void loadCurrentProfile();

        void updateProfile();
    }
}
