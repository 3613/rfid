package com.theredbean.rfid.presentation.owner;


import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.login.LoginUsecase;

import javax.inject.Inject;

public class OwnerPresenter implements OwnerContract.Presenter {


    LoginUsecase loginUsecase;
    OwnerContract.View mView;
    EzParkManager userManager;

    @Inject
    public OwnerPresenter(LoginUsecase loginUsecase, EzParkManager userManager) {
        this.loginUsecase = loginUsecase;
        this.userManager = userManager;


    }


    @Override
    public void takeView(OwnerContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void signOut() {
        loginUsecase.signOut(new EzParkingCallback.SignOutCallback() {
            @Override
            public void onSignOutSuccess() {
                mView.hideLoading();
                mView.onSignoutSuccess();
            }

            @Override
            public void onExceptionError(String msg) {
                mView.hideLoading();

                mView.onSignoutFalied(msg);

            }

            @Override
            public void onExceptionAuthentication() {
                mView.hideLoading();
                mView.onSignoutFalied("onExceptionAuthentication");

            }
        });
    }

    @Override
    public void setLeftProfile() {
        mView.setLeftProfiles(userManager);
    }
}
