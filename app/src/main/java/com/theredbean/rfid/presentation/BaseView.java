package com.theredbean.rfid.presentation;

/**
 * Created by nampham on 9/5/17.
 */

public interface BaseView {

    void showMessage(String message);

    void showLoading();

    void showLoading(String msg);

    void hideLoading();

    void showAlertDialog(String msg);



}
