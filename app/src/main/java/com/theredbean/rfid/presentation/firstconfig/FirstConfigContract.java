package com.theredbean.rfid.presentation.firstconfig;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface FirstConfigContract {

    interface View extends BaseView {

        void onWriteSuccess();

        void onWritepriceFalied();
    }

    interface Presenter extends BasePresenter<FirstConfigContract.View> {

        void writeAllPriceConfig(int a, int b, int c, int d, int e, int f);
    }
}
