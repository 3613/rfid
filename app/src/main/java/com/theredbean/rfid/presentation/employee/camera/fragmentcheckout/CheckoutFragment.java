package com.theredbean.rfid.presentation.employee.camera.fragmentcheckout;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hnam.uibutton.MyButton;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;

import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;
import utils.AlertDialogListener;
import utils.GlideApp;
import utils.ProgressBarUtils;

/**
 * Created by hoang on 20/03/2018.
 */

public class CheckoutFragment extends BaseFragment
        implements CheckoutContract.View, AlertDialogListener {

    FCurrentRecord currentRecord;

    public FragmetCheckOutListener mListener;


    // alerdialog listener
    @Override
    public void onAlertConfirmClick() {
        Timber.tag(TAG).e("V2: onAlertConfirmClick");

        mListener.onCancelCheckOutButton();
        hideLoading();

    }

    // alerdialog listener
    @Override
    public void onAlertCancelClick() {
        Timber.tag(TAG).e("V2: onAlertCancelClick");
        hideLoading();
        mListener.onCancelCheckOutButton();

    }

    public interface FragmetCheckOutListener {
        void onCancelCheckOutButton();

        void onCheckOutSuccess();

        void onCheckoutFaled();

        void onCheckOutSuccessV2(String localpath, String onlineTarget);
    }

    @Inject
    CheckoutContract.Presenter presenter;

    Context context;

    @BindView(R.id.ivResult)
    ImageView ivResult;
    @BindView(R.id.btnConfirm)
    MyButton btnconfirm;
    @BindView(R.id.btnCancel)
    MyButton btnCancel;

    @BindView(R.id.txtFragTitle)
    TextView txtFragTitle;


    public static final String TAG = CheckoutFragment.class.getSimpleName();

    public static CheckoutFragment getInstance(String deviceId, String path, FCurrentRecord record) {
        CheckoutFragment fragment = new CheckoutFragment();
        Bundle data = new Bundle();
        data.putString("DEVICE_ID", deviceId);
        data.putString("PATH", path);

        data.putParcelable("CRRC", Parcels.wrap(record));

        fragment.setArguments(data);
        return fragment;
    }

    String path;
    String deviceId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInjection();
        Bundle bundle = getArguments();
        if (bundle != null) {
            deviceId = bundle.getString("DEVICE_ID");
            path = bundle.getString("PATH");
            currentRecord = Parcels.unwrap(bundle.getParcelable("CRRC"));
            Timber.tag(TAG).e(currentRecord.toString());


        }
        Timber.tag(TAG).e(deviceId);
        Timber.tag(TAG).e(path);
    }


    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof FragmetCheckOutListener) {
            mListener = (FragmetCheckOutListener) context;
        } else {
            throw new ClassCastException(getActivity().getLocalClassName() + "is not cast CameraResult listener");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_check_out;
    }

    @Override
    protected void onMyCreateView(View view) {
        presenter.takeView(this);
        Timber.tag(TAG).e("onMyCreateView :%s", currentRecord.getPathImage());

//        Picasso.get().load(new File(currentRecord.getPathImage())).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).placeholder(R.drawable.ic_adb_black_24dp).error(R.drawable.ic_error_outline_black_24dp).into(ivResult);


        if (currentRecord.checkLocal()) {
//            Glide.with(this).load(currentRecord.localPath).into(ivResult);
            GlideApp.with(this)
                    .load(currentRecord.localPath)
                    .placeholder(ProgressBarUtils.getInstanceLoadingDrawable(context))
                    .fallback(R.drawable.error_image)
                    .error(R.drawable.error_image)
                    .into(ivResult);
            Timber.tag(TAG).e("abcdef if %s", currentRecord.getLocalPath());

        } else {
            if (currentRecord.getPathImage().equals("x")) {
                RB_Confirm_Dialog(getString(R.string.notice),
                        getString(R.string.error_online_path_notify),
                        null,
                        null,
                        this);
            } else {
//                Glide.with(this).load(currentRecord.getPathImage()).into(ivResult);
                Timber.tag(TAG).e("abcdef else %s, ", currentRecord.getLocalPath());
                GlideApp.with(this)
                        .load(currentRecord.getPathImage())
                        .placeholder(ProgressBarUtils.getInstanceLoadingDrawable(context))
                        .fallback(R.drawable.error_image)
                        .error(R.drawable.error_image)
                        .into(ivResult);
            }


        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void showFWarningMessage(String message) {
        super.showFWarningMessage(message);
    }

    @Override
    public void showFLoadingView(String msg) {
        super.showFLoadingView(msg);
    }

    @Override
    public void hideFLoadingView() {
        super.hideFLoadingView();
    }

    @Override
    public void showFWarningDialog(String msg) {
        super.showFWarningDialog(msg);
    }

    @OnClick(R.id.btnConfirm)
    public void onConfirmCheckOut() {
        Timber.tag(TAG).e("onConfirmCheckOut     %s", deviceId);
        presenter.processCheckOutOnline(deviceId, currentRecord);
    }

    @OnClick(R.id.btnCancel)
    public void onCancelCheckOut() {
        Timber.tag(TAG).e("onXacNhanonXacNhan");
        mListener.onCancelCheckOutButton();
    }


    private void initInjection() {
        DaggerEzParkingComponent.builder().applicationComponent(((BaseActivity) getActivity())
                .getApplicationComponent()).ezParkingModule(new EzParkingModule()).build().inject(this);
    }

    @Override
    public void showMessage(String message) {
        alertErrors(message, message, SweetAlertDialog.SUCCESS_TYPE);
    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");

    }

    @Override
    public void showLoading(String msg) {
        RB_Loading_MSg(msg);
    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }

    @Override
    public void showAlertDialog(String msg) {

    }


    @Override
    public void onCheckoutSuccess() {
        mListener.onCheckOutSuccess();
    }

    @Override
    public void onCheckoutFalied() {
        mListener.onCheckoutFaled();
    }

    @Override
    public void onShowCheckoutDetails(int totalPrice, String photoUrl) {
        Timber.tag(TAG).e("checkout %d --- %s", totalPrice, photoUrl);
        txtFragTitle.setText(
                context.getResources().getString(R.string.total_bike, String.valueOf(totalPrice)));
    }

    @Override
    public void onCheckoutSuccessV2(String localpath, String onlineTarget) {
        mListener.onCheckOutSuccessV2(localpath, onlineTarget);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.takeView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.dropView();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
        presenter.getCurrentCheckoutRecord(deviceId);

    }
}
