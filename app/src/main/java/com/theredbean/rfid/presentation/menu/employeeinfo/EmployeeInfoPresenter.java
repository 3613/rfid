package com.theredbean.rfid.presentation.menu.employeeinfo;


import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.model.OwnerModel;
import com.theredbean.rfid.business.parking.ParkingUsercase;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Parking;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class EmployeeInfoPresenter implements EmployeeInfoContract.Presenter {
    public static final String TAG = EmployeeInfoPresenter.class.getSimpleName();


    ParkingUsercase parkingUsercase;
    EmployeeInfoContract.View mView;
    @Inject
    EzParkManager userManager;


    @Inject
    public EmployeeInfoPresenter(ParkingUsercase parkingUsercase) {
        this.parkingUsercase = parkingUsercase;
    }

    @Override
    public void takeView(EmployeeInfoContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }


    @Override
    public void getListEmployeee() {
        mView.showLoading();
        parkingUsercase.getParkings(new EzParkingCallback.GetParkingCallback() {
            @Override
            public void onGetParkings(List<Parking> parkings) {
                mView.hideLoading();
                mView.onGetlistParkingSuccess(parkings);
            }

            @Override
            public void onGetParkingsEmpty() {
                mView.hideLoading();
                Timber.tag(TAG).e("onGetParkingsEmpty");
                mView.ongetListFailed("Chưa có bãi xe nào, vui lòng thêm bãi xe trước");

            }

            @Override
            public void onExceptionError(String msg) {
                mView.hideLoading();
                mView.ongetListFailed("onExceptionError");

            }

            @Override
            public void onExceptionAuthentication() {
                mView.hideLoading();
                mView.ongetListFailed("onExceptionAuthentication");

            }
        });
    }

    @Override
    public void updateParkingForEmployee(Employee employee, Parking parking) {

        mView.showLoading();

        parkingUsercase.updateParkingForEmployee(employee, parking, new EzParkingCallback.UpdateParkingForEmployeeCallback() {
            @Override
            public void onUpdateParkingSuccess() {
                Timber.tag(TAG).e("onUpdateParkingSuccess");
                mView.hideLoading();
                mView.displayUpdateParkingForEmployeeSuccess();
            }

            @Override
            public void onUpdateParkingFailed(String msg) {
                Timber.tag(TAG).e("onUpdateParkingFailed" + msg);

                mView.hideLoading();
                mView.onUpdateParkingForEmployeeFailed();
            }

            @Override
            public void onUpdateParkingSuccess(OwnerModel map) {
                mView.hideLoading();
                userManager.setParks(map.getParks());
                mView.displayUpdateParkingForEmployeeSuccess();

            }

            @Override
            public void onExceptionError(String msg) {
                Timber.tag(TAG).e("onExceptionError: " + msg);
                mView.hideLoading();
                mView.onUpdateParkingForEmployeeFailed();

            }

            @Override
            public void onExceptionAuthentication() {
                Timber.tag(TAG).e("onExceptionAuthentication");
                mView.hideLoading();
                mView.onUpdateParkingForEmployeeFailed();

            }
        });
    }
}
