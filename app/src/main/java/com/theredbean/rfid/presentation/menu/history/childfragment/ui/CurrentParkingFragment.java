package com.theredbean.rfid.presentation.menu.history.childfragment.ui;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.dialog.ImageDialogFragment;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.models.FHistoryRecord;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;
import com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.CurrentViewHolder;
import com.theredbean.rfid.presentation.menu.history.HistoryContract;
import com.theredbean.rfid.presentation.menu.history.childfragment.test.CurrentAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;
import utils.ProgressBarUtils;

public class CurrentParkingFragment extends BaseFragment
        implements HistoryContract.View, CurrentViewHolder.OnCurrentViewHolderListener {
    public static final String TAG = CurrentParkingFragment.class.getSimpleName();
    Context context;
    @Inject
    HistoryContract.Presenter presenter;

    CurrentAdapter adapter2;

    List<FCurrentRecord> mdata;

    @BindView(R.id.rvHistory)
    RecyclerView rvHistory;
    ShimmerFrameLayout shimmerViewContainer;
    Unbinder unbinder;


    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {
        Timber.tag(TAG).e("----showloading");

//        ProgressBarUtils.showWaitingDialog(context);
    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {
        Timber.tag(TAG).e("----hideLoading");

//        ProgressBarUtils.hideWaitingDialog();

    }

    @Override
    public void showAlertDialog(String msg) {

    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_history_list_recycler;
    }


    private void initDependency() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(((BaseActivity) getActivity())
                        .getApplicationComponent()).ezParkingModule(new EzParkingModule()).build().inject(this);
    }

    @Override
    protected void onMyCreateView(View view) {
//        showLoading();
        shimmerViewContainer =
                (ShimmerFrameLayout) view.findViewById(R.id.shimmer_view_container);


        mdata = new ArrayList<>();
        setupRV2();
    }

    private void setupRV2() {

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        rvHistory.setLayoutManager(lm);
        rvHistory.setHasFixedSize(true);
        adapter2 = new CurrentAdapter(context.getApplicationContext(), this);
        adapter2.setData(mdata);
        rvHistory.setAdapter(adapter2);
        adapter2.notifyDataSetChanged();


    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDependency();
        showLoading();
    }

    @Override
    public void onStart() {
        super.onStart();
        shimmerViewContainer.startShimmerAnimation();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                presenter.getListCurrent();

            }
        }, 3000);

    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.dropView();
    }

    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;

    }


    @Override
    public void onClick(int position) {
        Bundle args = new Bundle();
        args.putString("imgPath", mdata.get(position).getPathImage());
        DialogFragment newFragment = new ImageDialogFragment();
        newFragment.setArguments(args);
        newFragment.show(getFragmentManager(), TAG);
        Timber.tag(TAG).e(args.toString());

        //TODO call dialog and put path to the imageView
    }

    @Override
    public void onLongClick(int position) {

    }

    @Override
    public void onGotListCurrent(List<FCurrentRecord> currentRecords) {
        Timber.tag(TAG).e("onGotListCurrent");
        
        mdata = currentRecords;
        adapter2.setData(mdata);
        Timber.tag(TAG).e(mdata.get(0).getPathImage());
        shimmerViewContainer.stopShimmerAnimation();


    }

    @Override
    public void onGotListHistory(List<FHistoryRecord> historyRecords) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
