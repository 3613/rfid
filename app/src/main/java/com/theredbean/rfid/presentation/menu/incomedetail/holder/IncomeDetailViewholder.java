package com.theredbean.rfid.presentation.menu.incomedetail.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.theredbean.rfid.R;
import com.theredbean.rfid.models.IncomModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import utils.MyUtils;


public class IncomeDetailViewholder extends RecyclerView.ViewHolder implements View.OnClickListener,
        View.OnLongClickListener {

    private int position;
    private IncomModel item;

    private OnIncomeDetailViewholderListener mListener;

    public void setName(Integer integer) {

        // todo

        if (integer != null) {
            Integer price = integer.intValue();

            tvIncome.setText(MyUtils.fomatVND(price));
        }


    }

    public interface OnIncomeDetailViewholderListener {
        void onClick(int position);

        void onLongClick(int position);
    }

    public IncomeDetailViewholder(View view, OnIncomeDetailViewholderListener listener) {
        super(view);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        mListener = listener;
        ButterKnife.bind(this, view);


    }

    @BindView(R.id.txParkingName)
    TextView parkingName;
    @BindView(R.id.tvIncome)
    TextView tvIncome;

    public void renderUi(int position, IncomModel entity) {
        this.position = position;
        parkingName.setText(entity.getParkingName());
        tvIncome.setText(MyUtils.fomatVND(entity.getPrice()));

    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onClick(position);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mListener != null) {
            mListener.onLongClick(position);
        }
        return false;
    }
}