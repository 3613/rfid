package com.theredbean.rfid.presentation.login.errorlogin;


public class LoginErrorPresenter implements LoginErrorContract.Presenter {

    LoginErrorContract.View mView;


//    @Inject
    public LoginErrorPresenter() {
    }


    @Override
    public void takeView(LoginErrorContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }
}
