package com.theredbean.rfid.presentation.menu.history;


import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.history.HistoryUsecase;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.data.db.record.CurrentRecord;
import com.theredbean.rfid.data.db.record.HistoryRecord;
import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.models.FHistoryRecord;
import com.theredbean.rfid.models.Owner;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import timber.log.Timber;

@ActivityScope

public class HistoryPresenter implements HistoryContract.Presenter {
    public static final String TAG = HistoryPresenter.class.getSimpleName();

    IDatabase iDatabase;
    HistoryContract.View view;

    HistoryUsecase historyUsecase;
    EzParkManager userManager;

    @Inject
    public HistoryPresenter(@Named("realm") IDatabase iDatabase, HistoryUsecase historyusecase, EzParkManager userManager) {
        this.iDatabase = iDatabase;
        this.historyUsecase = historyusecase;
        this.userManager = userManager;
    }

    @Override
    public void takeView(HistoryContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        view = null;
    }

    @Override
    public List<CurrentRecord> getCurrentParkingRecord() {
        Timber.tag(TAG).e(iDatabase.getCurrentRecord().toString());
        return iDatabase.getCurrentRecord();
    }

    @Override
    public List<HistoryRecord> getTotalParckedRecord() {
        Timber.tag(TAG).e(iDatabase.getCurrentRecord().toString());
        return iDatabase.getHistoryRecord();
    }

    @Override
    public void getListCurrent() {
//        view.showLoading();
        Timber.tag(TAG).e("start -- > getListCurrent");

        Owner owner = userManager.getOwner();
        Employee employee = userManager.getEmployee();
        historyUsecase.getCurrentList(owner, employee, new EzParkingCallback.HistoryCallBack() {
            @Override
            public void success(List<FHistoryRecord> fHistoryRecords) {
                Timber.tag(TAG).e("success");

            }

            @Override
            public void onGotListCurrent(List<FCurrentRecord> fCurrentRecords) {
                if (view == null) {
                    return;
                }
                view.hideLoading();
                Timber.tag(TAG).e("onGotListCurrent" + fCurrentRecords.get(0).getPathImage());

                view.onGotListCurrent(fCurrentRecords);

            }

            @Override
            public void onExceptionError(String msg) {
                view.hideLoading();
                Timber.tag(TAG).e(msg + "X");

            }

            @Override
            public void onExceptionAuthentication() {
                view.hideLoading();
                Timber.tag(TAG).e("onExceptionError");

            }
        });
    }

    @Override
    public void getlistHistory() {
//        view.showLoading();
        Timber.tag(TAG).e("start get history");

        historyUsecase.getHistoryList(userManager.getOwner(), userManager.getEmployee(), new EzParkingCallback.HistoryCallBack() {
            @Override
            public void success(List<FHistoryRecord> fHistoryRecords) {
                Timber.tag(TAG).e("get ok");

                view.onGotListHistory(fHistoryRecords);
            }

            @Override
            public void onGotListCurrent(List<FCurrentRecord> fCurrentRecords) {
                Timber.tag(TAG).e(" onGotListCurrent khong the vo day dc");

            }

            @Override
            public void onExceptionError(String msg) {
                Timber.tag(TAG).e(msg);

            }

            @Override
            public void onExceptionAuthentication() {
                Timber.tag(TAG).e("onExceptionAuthentication");

            }
        });
    }

    @Override
    public void getTotalParckedRecord(long start, long end) {
        view.showLoading();
        Timber.tag(TAG).e("start get history");

        historyUsecase.getHistoryListWithFilter(userManager.getOwner(),
                userManager.getEmployee(),
                start, end,
                new EzParkingCallback.HistoryCallBack() {
                    @Override
                    public void success(List<FHistoryRecord> fHistoryRecords) {
                        Timber.tag(TAG).e("get ok");

                        view.onGotListHistory(fHistoryRecords);
                    }

                    @Override
                    public void onGotListCurrent(List<FCurrentRecord> fCurrentRecords) {
                        Timber.tag(TAG).e(" onGotListCurrent khong the vo day dc");

                    }

                    @Override
                    public void onExceptionError(String msg) {
                        Timber.tag(TAG).e(msg);

                    }

                    @Override
                    public void onExceptionAuthentication() {
                        Timber.tag(TAG).e("onExceptionAuthentication");

                    }
                });
    }
}
