package com.theredbean.rfid.presentation.menu.summary;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;
import com.theredbean.rfid.presentation.employee.menu.history.childfragment.adapter.ViewPagerHistoryAdapter;

import javax.inject.Inject;

import butterknife.BindView;


public class SummaryFragment extends BaseFragment implements SummaryContract.View {
    public static final String TAG = SummaryFragment.class.getSimpleName();
    @Inject
    SummaryContract.Presenter presenter;
    Context context;

    @BindView(R.id.vpPager)
    ViewPager viewPager;
    ViewPagerHistoryAdapter mViewPagerAdapter;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showAlertDialog(String msg) {

    }

    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_summary;
    }

    @Override
    protected void onMyCreateView(View view) {
        setUpTablayout();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDependency();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.takeView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void initDependency() {
        DaggerEzParkingComponent.builder().applicationComponent(((BaseActivity) getActivity()).getApplicationComponent()).ezParkingModule(new EzParkingModule()).build().inject(this);


    }

    private void setUpTablayout() {
        mViewPagerAdapter = new ViewPagerHistoryAdapter(getFragmentManager(), context);
        viewPager.setAdapter(mViewPagerAdapter);
        viewPager.setOffscreenPageLimit(4);
        tabLayout.setupWithViewPager(viewPager);


    }


    public static Fragment getInstance() {

        return new SummaryFragment();
    }
}

