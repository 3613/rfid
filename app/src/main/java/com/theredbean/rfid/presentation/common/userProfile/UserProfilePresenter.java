package com.theredbean.rfid.presentation.common.userProfile;


import javax.inject.Inject;

public class UserProfilePresenter implements UserProfileContract.Presenter {

    UserProfileContract.View mView;

    @Inject
    public UserProfilePresenter() {
    }


    @Override
    public void takeView(UserProfileContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void loadCurrentProfile() {

    }

    @Override
    public void updateProfile() {

    }
}
