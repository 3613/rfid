package com.theredbean.rfid.presentation.menu.employeeinfo;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Parking;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;
import com.theredbean.rfid.presentation.menu.parkinglist.viewholder.ParkingAdapter;
import com.theredbean.rfid.presentation.menu.parkinglist.viewholder.ParkingViewHolder;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;
import utils.AlertDialogListener;

public class EmployeeInfoFragment extends BaseFragment
        implements EmployeeInfoContract.View,
        ParkingViewHolder.OnParkingViewHolderListener, AlertDialogListener {
    public static final String TAG = EmployeeInfoFragment.class.getSimpleName();


    public Mylistener mLlistener;

    @Override
    public void onAlertConfirmClick() {
        hideLoading();
        Timber.tag(TAG).e("onAlertConfirmClick");
        presenter.updateParkingForEmployee(mEmployee, selectedKParking);

    }

    @Override
    public void onAlertCancelClick() {
        hideLoading();
        Timber.tag(TAG).e("onAlertCancelClick");

    }

    public interface Mylistener {
        void onAddparkingForEmployee(Employee employee, Parking parking);

        void restartApp();

        void restartFragment(Employee e);
    }

    Employee mEmployee;
    Parking mParking;

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvParkingName)
    TextView tvParkingName;

    Context context;

    List<Parking> parkingList;
    Parking selectedKParking;


    ParkingAdapter parkingAdapter;

    @BindView(R.id.rvParking)
    RecyclerView rvParking;
    @Inject
    EmployeeInfoContract.Presenter presenter;


    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof EmployeeInfoFragment.Mylistener) {
            mLlistener = (Mylistener) context;
        } else {
            throw new ClassCastException(TAG + " -  can not cast Mylistener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle data = getArguments();
        if (data != null) {
            try {
                Employee e = (Employee) Parcels.unwrap(data.getParcelable("EMP"));
                mEmployee = e;
                Timber.tag(TAG).e(e.toString());

                Timber.tag(TAG).e(e.getDescription());

            } catch (Exception e) {
                Timber.tag(TAG).e(e.getLocalizedMessage());

            }
        }
    }

    private void setUserInfomations(Employee e) {
        employeeName = e.getName();
        tvName.setText(e.getName());

        // todo cần lấy thông tin bãi xe đang làm việc của user này
        if (e.getParkingKey() == null || e.getParkingKey().equals("")) {
            tvParkingName.setText("Chưa được gán bãi xe, vui lòng chọn bãi xe làm việc cho nhân viên này");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_employee_detail_info;
    }

    @Override
    protected void onMyCreateView(View view) {
        initInjection();
        presenter.takeView(this);
        setUserInfomations(mEmployee);
        setupRV2();

    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
        presenter.getListEmployeee();

    }

    @Override
    public void showMessage(String message) {
        RB_Warning_MSg(message);

    }

    @Override
    public void showLoading() {

        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {
        RB_Loading_MSg(msg);

    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }


    @Override
    public void showAlertDialog(String msg) {

    }


    public void initInjection() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build()
                .inject(this);

    }


    private void setupRV2() {
        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        rvParking.setLayoutManager(lm);
        rvParking.setHasFixedSize(true);
        parkingAdapter = new ParkingAdapter(getActivity(), this);
        rvParking.setAdapter(parkingAdapter);
    }


    @Override
    public void onGetlistParkingSuccess(List<Parking> parkings) {

        parkingList = parkings;
        parkingAdapter.setData(parkingList);

        parkingAdapter.setWorkingAt(mEmployee);


    }

    @Override
    public void ongetListFailed(String msg) {
        RB_Error_MSg(msg);
    }


    String parkingName = "", employeeName = "";

    @Override
    public void displayUpdateParkingForEmployeeSuccess() {


        // todo [bug] ko chịu chuyển cái sticker trong trang profile :v
        parkingList.clear();
        parkingAdapter.notifyDataSetChanged();

        rvParking.removeAllViewsInLayout();
        parkingAdapter = null;


        setupRV2();

        RB_Succeess_MSg("Đã gán thành công bãi xe cho nhân viên " + employeeName);
//        mLlistener.restartApp();
        tvParkingName.setText(parkingName);
        parkingList.clear();

        presenter.getListEmployeee();
//        mLlistener.restartFragment(mEmployee);

    }

    @Override
    public void onUpdateParkingForEmployeeFailed() {

    }

    public static Fragment getInstance(Employee e) {
        EmployeeInfoFragment fragment = new EmployeeInfoFragment();
        Bundle data = new Bundle();
        data.putParcelable("EMP", Parcels.wrap(e));
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    public void onClick(int position) {
        Timber.tag(TAG).e("clicked %d", position);
        parkingName = parkingList.get(position).getName();

        selectedKParking = parkingList.get(position);
        RB_Confirm_Dialog("Vui lòng xác nhận",
                "Bạn có muốn gán nhân viên: " + employeeName + "\n cho bãi xe :" + selectedKParking.getName(),
                "Bỏ qua",
                "Xác nhận",
                this);
        Timber.tag(TAG).e(parkingList.get(position).toString());
        Timber.tag(TAG).e(mEmployee.toString());


    }

    @Override
    public void onLongClick(int position) {
        Timber.tag(TAG).e("Long clicked %d", position);

    }
}
