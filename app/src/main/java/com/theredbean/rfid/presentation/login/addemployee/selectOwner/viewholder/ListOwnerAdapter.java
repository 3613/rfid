package com.theredbean.rfid.presentation.login.addemployee.selectOwner.viewholder;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theredbean.rfid.R;
import com.theredbean.rfid.models.Owner;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by hoang on 09/04/2018 nhe.
 */
public class ListOwnerAdapter extends RecyclerView.Adapter<ListOwnerViewHolder> {
    private static final String TAG = ListOwnerAdapter.class.getSimpleName();
    private Activity mActivity;
    private List<Owner> mData;
    private LayoutInflater mLayoutInflater;
    ListOwnerViewHolder.OnOwnerListener mListener;


    public ListOwnerAdapter(Activity activity, ListOwnerViewHolder.OnOwnerListener mListener) {
        mLayoutInflater = LayoutInflater.from(activity);
        mActivity = activity;
        mData = null;
        this.mListener = mListener;
    }



    public void setData(List<Owner> data) {
        if (mData == null) {
            mData = new ArrayList<>();
        }
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ListOwnerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_list_owner, parent, false);
        return new ListOwnerViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(ListOwnerViewHolder holder, int position) {
        Owner entity = mData.get(position);
        holder.renderUi(position, entity);
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }


    private final ListOwnerViewHolder.OnOwnerListener listener = new ListOwnerViewHolder.OnOwnerListener() {
        @Override
        public void onItemClick(int position) {
            mListener.onItemClick(position);
        }

        @Override
        public void onItemLongClick(int position) {
            mListener.onItemLongClick(position);
        }

    };
}
