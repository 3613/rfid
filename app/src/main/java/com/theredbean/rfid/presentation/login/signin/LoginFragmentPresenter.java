package com.theredbean.rfid.presentation.login.signin;


import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.login.LoginUsecase;
import com.theredbean.rfid.business.model.OwnerModel;
import com.theredbean.rfid.business.pricing.PriceUsecase;
import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Pricez.FPrice;

import javax.inject.Inject;

import timber.log.Timber;

@ActivityScope
public class LoginFragmentPresenter implements LoginFragmentContract.Presenter {

    private static final String TAG = LoginFragmentPresenter.class.getSimpleName();
    LoginFragmentContract.View mView;
    LoginUsecase mUsecase;
    EzParkManager userManager;
    PriceUsecase priceUsecase;

    @Inject
    public LoginFragmentPresenter(LoginUsecase mUsecase,
                                  EzParkManager userManager,
                                  PriceUsecase priceUsecase) {
        this.mUsecase = mUsecase;
        this.userManager = userManager;
        this.priceUsecase = priceUsecase;
    }


    @Override
    public void takeView(LoginFragmentContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        this.mView = null;
    }

    @Override
    public void login(String email, String password) {
        Timber.tag(TAG).e(">>>> login");
        mView.showLoading();
        mUsecase.loginWithEmailAndPass(email, password, new EzParkingCallback.LoginCallback() {


            @Override
            public void onOwnerLogged(OwnerModel owner) {
                Timber.tag(TAG).e("onOwnerLogged");
                userManager.setOwner(owner.getOwner());
                userManager.setParks(owner.getParks());
                mView.hideLoading();
                mView.showOwnerScreen();
            }

            @Override
            public void onEmployeeLogged(Employee employee) {
                userManager.setEmployee(employee);
                Timber.tag(TAG).e(employee.toString());
                if (employee.getOwnerKey() == null || employee.getParkingKey() == null) {
                    mView.hideLoading();
                    String msg = "Bạn chưa được chỉ định làm việc ở bãi xe nào \n vui lòng liên hệ chủ bãi xe";
                    Timber.tag(TAG).e(msg);

                    mView.showErrorUserScreen(msg);
                    return;
                }
                priceUsecase.loadPriceConfig(employee, new EzParkingCallback.CheckPriceCallBack() {
                    @Override
                    public void onPriceCheckSuccess(FPrice fPrice) {
                        userManager.setPrices(fPrice);
                        mView.hideLoading();
                        mView.showEmployeeScreen();
                    }

                    @Override
                    public void onPriceCheckFalied(String msg) {
                        mView.hideLoading();
                        mView.showErrorUserScreen(msg);

                    }

                    @Override
                    public void onExceptionError(String msg) {
                        mView.hideLoading();
                        mView.showErrorUserScreen(msg);
                    }

                    @Override
                    public void onExceptionAuthentication() {

                    }
                });


                Timber.tag(TAG).e("onEmployeeLogged");

            }

            @Override
            public void onNoUserExisting() {
                Timber.tag(TAG).e("onNoUserExisting");
                mView.hideLoading();
                mView.showAlertNoExistingUser();
            }

            @Override
            public void onLoginSuccessed() {
                Timber.tag(TAG).e("onLoginSuccessed");

            }

            @Override
            public void onOwnerFirstLogin() {
                mView.hideLoading();
                Timber.tag(TAG).e("onOwnerFirstLogin");
                mUsecase.saveOwnerProfileToDatabase(new EzParkingCallback.SaveOwnerCallBack() {
                    @Override
                    public void onExceptionError(String msg) {
                        Timber.tag(TAG).e("onExceptionError");

                        mView.hideLoading();
                        mView.showAlertError(msg);
                    }

                    @Override
                    public void onExceptionAuthentication() {
                        Timber.tag(TAG).e("onExceptionAuthentication");
                    }

                    @Override
                    public void onSaveOwnerSuccess() {
                        Timber.tag(TAG).e("onSaveOwnerSuccess");


                        mView.hideLoading();
                        mView.showOwnerScreen();

                    }

                    @Override
                    public void onSaveOwnerFailed(String msg) {
                        Timber.tag(TAG).e("onSaveOwnerFailed");

                        mView.showAlertError(msg);
                    }


                });


            }

            @Override
            public void onExceptionError(String msg) {
                mView.hideLoading();
                mView.showAlertError("login errors");
                mView.onLoginFailed(msg);
                Timber.tag(TAG).e("onExceptionError" + msg);

            }

            @Override
            public void onExceptionAuthentication() {
                mView.hideLoading();
                Timber.tag(TAG).e("Exception authentication");

            }
        });
    }

}
