package com.theredbean.rfid.presentation.menu.employeelist;


import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.employee.EmployeeUsecase;
import com.theredbean.rfid.models.Employee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

public class EmployeeListPresenter implements EmployeeListContract.Presenter {
    public static final String TAG = EmployeeListPresenter.class.getSimpleName();

    private List<Employee> employees;

    EmployeeListContract.View mView;

    EmployeeUsecase employeeUsecase;
    EzParkManager userManager;

    @Inject
    public EmployeeListPresenter(EmployeeUsecase employeeUsecase, EzParkManager ezParkManager) {
        this.employeeUsecase = employeeUsecase;
        userManager = ezParkManager;
    }

    @Override
    public void takeView(EmployeeListContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }


    @Override
    public void getListEmployeee(String uid) {
        mView.showLoading();
        employeeUsecase.getEmployees(new EzParkingCallback.EmployeeCallback() {
            @Override
            public void onGetEmployeeSuccess(List<Employee> employees) {
                mView.hideLoading();
                if (employees.isEmpty()) {
                    mView.displayEmployee();
                }
                mView.onGetListEmployeesOfOwnerSuccess(employees);
            }

            @Override
            public void onGetEmployeeFailed(String msg) {
                mView.hideLoading();
                mView.onGetListEmployeesOfOwnerFalied(msg);
            }

            @Override
            public void onExceptionError(String msg) {
                mView.hideLoading();
                mView.onGetListEmployeesOfOwnerFalied(msg);
            }

            @Override
            public void onExceptionAuthentication() {
                mView.hideLoading();
                mView.onGetListEmployeesOfOwnerFalied(TAG + "-- onExceptionAuthentication");
            }
        });
    }

    @Override
    public void getParkingsKey() {
        HashMap<String, String> newMaps = new HashMap<>();

        List<String> keyList = new ArrayList<>();
        List<String> valueList = new ArrayList<>();

//        for (Map.Entry<String, Parking> entry : userManager.getOwner().getParkings().entrySet()) {
//            keyList.add(entry.getKey());
//            valueList.add(entry.getValue().getName());
//            newMaps.put(entry.getKey(), entry.getValue().getName());
//            Timber.tag(TAG).e("  \n keygot:  %s   %s ", entry.getKey(), entry.getValue().getName());
//
//        }

//        mView.gotParkings(newMaps);

        mView.gotParkings(userManager.getParks());


    }
}
