package com.theredbean.rfid.presentation.owner.incom;


import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.income.IncomeUsecase;
import com.theredbean.rfid.business.incomemodule.EzParkingInCome;
import com.theredbean.rfid.business.incomemodule.EzParkingInComeCallback;
import com.theredbean.rfid.business.model.CurrentIncome;
import com.theredbean.rfid.business.parking.ParkingUsercase;
import com.theredbean.rfid.models.Parking;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import timber.log.Timber;

public class IncomePresenter implements IncomeContract.Presenter {
    public static final String TAG = IncomePresenter.class.getSimpleName();

    IncomeContract.View mView;
    IncomeUsecase incomeUsecase;
    EzParkManager userManager;
    EzParkingInCome ezParkingInCome;


    ParkingUsercase parkingUsercase;
    Map<String, Long> mIncomeHash;

    @Inject
    public IncomePresenter(ParkingUsercase parkingUsercase, IncomeUsecase incomeUsecase, EzParkingInCome ezParkingInCome, EzParkManager userManager) {
        this.incomeUsecase = incomeUsecase;
        this.ezParkingInCome = ezParkingInCome;
        this.userManager = userManager;
        this.parkingUsercase = parkingUsercase;
        mIncomeHash = new HashMap<>();
    }


    @Override
    public void takeView(IncomeContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }


    @Override
    public void getParkings() {
        List<Parking> parkingList = userManager.getParks();
        if (parkingList == null || parkingList.isEmpty()) {
            mView.disPlayNoParking();
            mView.hideLoading();

            Timber.tag(TAG).e("----no data");

            return;
        }
        mView.ongetParkingsSuccess(userManager.getParks());
        mView.displayHaveData();
        mView.hideLoading();
        Timber.tag(TAG).e("----have data");


    }

    @Override
    public void registerIncomeListener() {
        mView.showLoading();
        ezParkingInCome.registerCallback(new EzParkingInComeCallback() {
            @Override
            public void onInComeToday(CurrentIncome currentIncome) {
                //todo
                mIncomeHash.put(currentIncome.getParkingUuid(), currentIncome.getIncome());
                long result = 0;
                for (Map.Entry<String, Long> entry : mIncomeHash.entrySet()) {
                    result += entry.getValue();
                }
                mView.hideLoading();

                mView.onGetIncomeSuccess(currentIncome, result);
            }

            @Override
            public void onExeptionError(String msg) {
                mView.hideLoading();
                Timber.tag(TAG).e("onExeptionError");

            }
        });

        ezParkingInCome.registerListenerChangeFromParks(userManager.getOwner(),
                userManager.getParks());
        mView.hideLoading();
    }

    @Override
    public void unregisterIncomListener() {
        ezParkingInCome.unregisterListenerChangeFromParks();
    }


}
