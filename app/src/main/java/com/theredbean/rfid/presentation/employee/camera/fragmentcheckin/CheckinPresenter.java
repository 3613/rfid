package com.theredbean.rfid.presentation.employee.camera.fragmentcheckin;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.checkcard.CheckCardUsecase;
import com.theredbean.rfid.business.image.ImageModel;
import com.theredbean.rfid.business.image.ImageUsecase;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.rxbus.RxBus;
import com.theredbean.rfid.rxbus.RxBusConstants;
import com.theredbean.rfid.rxbus.RxBusEvent;

import org.parceler.Parcels;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;
import utils.EzConstants;
import utils.RxUltils;
import utils.TimeUtils;

/**
 * Created by hoang on 20/03/2018.
 */

public class CheckinPresenter implements CheckinContract.Presenter {
    public static final String TAG = CheckinPresenter.class.getSimpleName();

    private CheckinContract.View mView;

    IDatabase database;
    ImageUsecase imageUsecase;
    CheckCardUsecase checkCardUsecase;
    RxBus rxBus;

    @Inject
    EzParkManager userManager;

    @Inject
    public CheckinPresenter(@Named("realm") IDatabase database,
                            ImageUsecase imageUsecase,
                            CheckCardUsecase checkCardUsecase,
                            RxBus rxBus) {
        this.database = database;
        this.checkCardUsecase = checkCardUsecase;
        this.imageUsecase = imageUsecase;
        this.rxBus = rxBus;
    }


    @Override
    public void takeView(CheckinContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void showImage(String url) {
        mView.loadImage(url);
    }

    @Override
    public void processCheckin(String id, String path, int bikeType) {
        new CheckInTask(id, new File(path), bikeType).execute();
    }

    @Override
    public void onlineCheckin(File file, String fileName) {
        Timber.tag(TAG + " timing").e("\n\n\t\t \t START-CHECK - IN \t\t\t\t\t %s", TimeUtils.get_HH_MM_SS());
        Employee employee2 = userManager.getEmployee();


        Timber.tag(TAG).e("File = %s, \n fileName %s", file, fileName);
    }

    @Override
    public void onlineCheckin(Bitmap bitmap, String card_NFC_id) {
        //
    }


    @Override
    public void onlineCheckin(String path, String carid) {
        if (mView != null) {
            mView.showLoading("Vui lòng đợi");
        }

        Timber.tag(TAG + " timing").e("\n\n\t\t \t START-CHECK - IN \t\t\t\t\t %s", TimeUtils.get_HH_MM_SS());
        Employee employee2 = userManager.getEmployee();
        Timber.tag(TAG).e("path of  %s", path);

        checkCardUsecase.checkIn(userManager.getPrices(), employee2, carid, path, new EzParkingCallback.CheckInCardCallback() {
            @Override
            public void onCheckInCardSuccess() {

            }

            @Override
            public void onCheckInCardFailed(String msg) {
                Timber.tag(TAG).e("onCheckInCardFailed %s", msg);
                mView.onCheckInFalied(msg);

            }

            @Override
            public void onCheckInCardSuccessV2(FCurrentRecord currentRecord) {
                if (mView != null) {
                    mView.hideLoading();
                    mView.oncheckInSuccess();
                }

                //todo
                ImageModel imageModel = new ImageModel();
                imageModel.setCardId(carid);
                imageModel.setEmployeeUuid(employee2.getUuid());
                imageModel.setOwnerUuid(employee2.getOwnerKey());
                imageModel.setParkingUuid(employee2.getParkingKey());
                imageModel.setPath(path);
                imageModel.setTimeIn(currentRecord.getTimestampCheckIn());
                Timber.tag(TAG).e("onCheckInCardSuccess");
                Timber.tag(TAG + " timing").e("\t\t\t WRITE-DATABASE-SUCCESS---\t\t\t %s %s", TimeUtils.get_HH_MM_SS(), "-----end----");

                Bundle data = new Bundle();
                data.putParcelable(RxBusConstants.DATA, Parcels.wrap(imageModel));
                RxBusEvent event = new RxBusEvent(RxBusEvent.Req.UPLOAD_IMAGE, data);
                rxBus.send(event);
            }


            @Override
            public void onExceptionError(String msg) {
                Timber.tag(TAG).e("onExceptionError %s", msg);
                mView.onCheckInFalied(msg);

            }

            @Override
            public void onExceptionAuthentication() {
                Timber.tag(TAG).e("onExceptionAuthentication");
                mView.onCheckInFalied("onExceptionAuthentication");

            }
        });

    }

    private CompositeDisposable compositeDisposable;


    @Override
    public void startCounter() {
        compositeDisposable = new CompositeDisposable();
        RxUltils.getTimerObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Long>() {

                    @Override
                    public void onSubscribe(Disposable disposable) {
                        compositeDisposable.add(disposable);
                        Timber.tag(TAG).e("composite onSubscribe %s", compositeDisposable.isDisposed());
                    }

                    @Override
                    public void onNext(Long aLong) {
                        Timber.tag(TAG).e("composite onNext %s", compositeDisposable.isDisposed());

                        Timber.tag(TAG).e("onNext %s", aLong);
                        // disable click button if couterstrike == 1 to fix null
                        if (aLong == 1) {
                            if (mView == null) return;
                            mView.disableButton();
                        }
                        if (mView == null) return;
                        mView.onNextTimer(aLong);
                    }


                    @Override
                    public void onError(Throwable e) {
                        Timber.tag(TAG).e("onError %s", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Timber.tag(TAG).e("onComplete");
                        if (mView == null)return;
                        mView.onCompleteTimer();
                            Timber.tag(TAG).e("composite onComplete tren %s", compositeDisposable.isDisposed());

                        if (!compositeDisposable.isDisposed()) {
                            compositeDisposable.dispose();
                            compositeDisposable.clear();
                        }

                        Timber.tag(TAG).e("composite onComplete duoi %s", compositeDisposable.isDisposed());

                    }
                });
    }

    @SuppressLint("StaticFieldLeak")
    private class CheckInTask extends AsyncTask<Void, Void, Boolean> {
        File src;
        String epc;
        int carType;

        public CheckInTask(String epc, File src, int carType) {
            this.src = src;
            this.epc = epc;
            this.carType = carType;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            //todo copy file
            //todo write to database

            File result = getFilesInRedbeanFolderSetting();
            try {
                copy(src, result); // copy
                database.saveCurrentRecord(epc, result.getPath(), carType); // write db
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                //todo success
                Log.e(TAG, "copy success");
                mView.oncheckInSuccess();

            } else {
                Log.e(TAG, "copy failed");
                mView.onCheckInFalied("copy failed");

            }
        }


        public void copy(File src, File dst) throws IOException {
            InputStream in = new FileInputStream(src);
            try {
                OutputStream out = new FileOutputStream(dst);
                try {
                    // Transfer bytes from in to out
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                } finally {
                    out.close();

                }
            } finally {
                in.close();
            }
        }

    }

    public File getFilesInRedbeanFolderSetting() {
        // Tạo thư mục chứa hình
        File folder = new File(Environment.getExternalStorageDirectory() + EzConstants.IMAGE_FOLDER_NAME);
        if (!folder.exists()) {
            @SuppressLint("SdCardPath") File wallpaperDirectory = new File(EzConstants.IMAGE_PATH);
            wallpaperDirectory.mkdirs();
        }


        // tên tên hình là thời gian
        @SuppressLint("SdCardPath") File file = new File(new File(EzConstants.IMAGE_PATH), Calendar.getInstance().getTimeInMillis() + ".jpg");
        if (file.exists()) {
            file.delete();
        }

        return file;
    }
}
