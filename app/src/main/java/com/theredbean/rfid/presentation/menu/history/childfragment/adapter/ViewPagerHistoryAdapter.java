package com.theredbean.rfid.presentation.employee.menu.history.childfragment.adapter;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.theredbean.rfid.R;
import com.theredbean.rfid.presentation.menu.history.childfragment.ui.CurrentParkingFragment;
import com.theredbean.rfid.presentation.menu.history.childfragment.ui.TotalParkedFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoang on 29/03/2018 nhe.
 */

public class ViewPagerHistoryAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragmentList;
    private List<String> fragmentTitleList;
    Context context;

    public ViewPagerHistoryAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;

        fragmentList = new ArrayList<>();
        fragmentList.add(new CurrentParkingFragment());
        fragmentList.add(new TotalParkedFragment());




        fragmentTitleList = new ArrayList<>();
        fragmentTitleList.add(context.getResources().getString(R.string.bike_in_parking));
        fragmentTitleList.add(context.getResources().getString(R.string.bike_had_out));
    }


    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitleList.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }



}
