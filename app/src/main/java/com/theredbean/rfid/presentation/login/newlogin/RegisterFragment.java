package com.theredbean.rfid.presentation.login.newlogin;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.theredbean.rfid.R;
import com.theredbean.rfid.presentation.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;

public class RegisterFragment extends BaseFragment implements RegisterBaseContract.View {

    public static final String TAG = RegisterFragment.class.getSimpleName();


    @Inject
    RegisterBaseContract.Presenter presenter;

    Context context;

    @BindView(R.id.vpPager)
    ViewPager viewPager;
    ViewPagerRegisterAdapter mViewPagerAdapter;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    Mylistener mLMylistener;

    public static Fragment getInstance() {
        RegisterFragment registerFragment = new RegisterFragment();
        if (registerFragment == null) {
            registerFragment = new RegisterFragment();
        }
        return registerFragment;
    }

    public interface Mylistener {
        void ontest();
    }


    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof Mylistener) {
            mLMylistener = (Mylistener) context;
        } else {
            throw new ClassCastException(TAG + " -- can not cast Mylistener interface");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_history_base;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInjection();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    protected void onMyCreateView(View view) {
        setUpTablayout();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        presenter.dropView();
    }

    @Override
    public void showMessage(String message) {
        RB_Succeess_MSg(message);
    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {
        RB_Loading_MSg(msg);

    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }

    @Override
    public void showAlertDialog(String msg) {
        RB_Warning_MSg(msg);
    }

    private void initInjection() {
//        DaggerEzParkingComponent.builder()
//                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
//                .ezParkingModule(new EzParkingModule())
//                .build()
//                .inject(this);

    }

    private void setUpTablayout() {
        mViewPagerAdapter = new ViewPagerRegisterAdapter(getChildFragmentManager(), context);
        viewPager.setAdapter(mViewPagerAdapter);
        viewPager.setOffscreenPageLimit(4);
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            RelativeLayout relativeLayout = (RelativeLayout)
                    LayoutInflater.from(context).inflate(R.layout.tab_layout, tabLayout, false);

            TextView tabTextView = (TextView) relativeLayout.findViewById(R.id.tab_title);
            if (i == 0) {
                View tabline = (View) relativeLayout.findViewById(R.id.doc);
                tabline.setVisibility(View.GONE);

            }
            tabTextView.setText(tab.getText());
            tab.setCustomView(relativeLayout);
//            tab.select();
        }


    }

}
