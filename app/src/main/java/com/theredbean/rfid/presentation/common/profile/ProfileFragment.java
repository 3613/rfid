package com.theredbean.rfid.presentation.common.profile;

import android.content.Context;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.theredbean.rfid.R;
import com.theredbean.rfid.presentation.BaseFragment;
import com.theredbean.rfid.presentation.login.signin.LoginFragmentContract;

public class ProfileFragment extends BaseFragment implements ProfileFragmentContract.View {
    public static final String TAG = ProfileFragment.class.getSimpleName();

    LoginFragmentContract.Presenter presenter;
    FirebaseAuth mAuth;

    Context context;
    ProfileListener mListenner;

    public interface ProfileListener {
        void onGotoRegister();

        void onGotoForgotPassWord();
    }




    @Override
    protected void onAttachToContext(Context context) {
//        this.context = context;
//        if (context instanceof ProfileFragment.ProfileListener) {
//            mListenner = (ProfileFragment.ProfileListener) context;
//        } else {
//            throw new ClassCastException(getActivity().getLocalClassName() + "is not cast CameraResult listener");
//        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_profile;
    }

    @Override
    protected void onMyCreateView(View view) {
        mAuth = FirebaseAuth.getInstance();


    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showAlertDialog(String msg) {

    }


    public static ProfileFragment getInstance(String deviceId, String path) {
        ProfileFragment fragment = new ProfileFragment();
//        Bundle data = new Bundle();
//        data.putString("DEVICE_ID", deviceId);
//        data.putString("PATH", path);
//        fragment.setArguments(data);
        return fragment;
    }
}
