package com.theredbean.rfid.presentation.owner;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.theredbean.rfid.R;
import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.Navigator;
import com.theredbean.rfid.presentation.menu.MenuActivity;
import com.theredbean.rfid.presentation.menu.revenue.RevenueFragment;
import com.theredbean.rfid.presentation.login.LoginActivity;
import com.theredbean.rfid.presentation.owner.incom.IncomeFragment;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;
import utils.EzConstants;
import utils.FragmentConstants;

public class OwnerActivity extends BaseActivity
        implements
        OwnerContract.View,
        IncomeFragment.Mylistener{
    public static final String TAG = OwnerActivity.class.getSimpleName();


    private FragmentManager fm = getFragmentManager();
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    TextView leftname, leftDescription;


    @Inject
    Navigator navigator;

    @Inject
    OwnerContract.Presenter presenter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializingInjection();
        presenter.takeView(this);

        findViews();
        setupToolbar();
        setUpDrawer();

        // set first fragment
        addFragment(new IncomeFragment(), IncomeFragment.TAG);


    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.setLeftProfile();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        setAppTitle(getString(R.string.my_app_name));

    }

    private void setAppTitle(String title) {
        try {
            getSupportActionBar().setTitle("");

        } catch (Exception e) {
            Timber.tag(TAG).e(e.getMessage());
        }
    }

    private void setUpDrawer() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_close, R.string.drawer_open);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        actionBarDrawerToggle.syncState();

        // menu listener
        navigationView.setNavigationItemSelectedListener(itemSelectedListener);

        // findview view of header
        View header = navigationView.getHeaderView(0);
        leftDescription = header.findViewById(R.id.txtNavAdress);
        leftname = header.findViewById(R.id.txtNavName);


    }

    private void findViews() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_owner;
    }

    @Override
    public void showMessage(String message) {
        showAlertDialog("abc");
    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {
        dissmissSweetDialog();
    }

    private void initializingInjection() {
        DaggerEzParkingComponent.builder().applicationComponent(getApplicationComponent()).ezParkingModule(new EzParkingModule()).build().inject(this);
    }

    void clearMenu() {
        if (menu != null) {
            //nothing
        }
    }


    /*******************************************
     *  Start Fragment navigator controll      *
     *                                         *
     * ****************************************/
    public void addFragment(Fragment fragment, String tag) {
        if (fm == null) {
            fm = getFragmentManager();

        }


        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction
                .setCustomAnimations(R.animator.slide_enter, R.animator.slide_exit, R.animator.slide_pop_enter, R.animator.slide_pop_exit);
        fragmentTransaction
                .add(R.id.content_fragment_owner, fragment, tag)
                .addToBackStack(tag).commit();
    }


    public void hideFragment(int resId) {
        if (fm == null) {
            return;
        } else {

            fm.beginTransaction().setCustomAnimations(R.animator.slide_enter, R.animator.slide_exit, R.animator.slide_enter, R.animator.slide_exit).remove(fm.findFragmentById(resId)).commit();
        }

    }


    @SuppressLint("ResourceType")
    private void replaceFragment(Fragment fragment, String tag, String title) {
        if (fm == null) {
            fm = getFragmentManager();
            Timber.tag(TAG).e("replaceFragment null");

        }
        Timber.tag(TAG).e("replaceFragment Ok");
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
//        fragmentTransaction.setCustomAnimations(R.animator.slide_enter, R.animator.slide_exit, R.animator.slide_enter, R.animator.slide_exit);
        fragmentTransaction.replace(R.id.content_fragment_owner, fragment, tag).commit();
        if (title != null) {
            setAppTitle(title);
        }

    }

    /*******************************************
     *     END Fragment navigator controll     *
     *                                         *
     * ****************************************/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    Menu menu;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        return true;
    }

    Fragment fragment = null;
    Bundle bundle = null;
    private NavigationView.OnNavigationItemSelectedListener itemSelectedListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if (item.getItemId() == R.id.nav_logout) {
                presenter.signOut();
            }
            Bundle bundle = new Bundle();
// todo duplicate code nhieu qua clean sau hen :D
            switch (item.getItemId()) {

                case R.id.nav_parking_list:
                    clearMenu();
                    // todo change new with singleton nhe
                    bundle = new Bundle();
                    bundle.putInt(EzConstants.KEY_BUNDLE_FRAGMENT_TAG, FragmentConstants.FRAGMENT_PARKING_LIST);
                    navigator.startActivity(OwnerActivity.this, MenuActivity.class, bundle);
                    break;
                case R.id.nav_employee_list:
                    clearMenu();
                    fragment = null;
                    bundle = new Bundle();
                    bundle.putInt(EzConstants.KEY_BUNDLE_FRAGMENT_TAG, FragmentConstants.FRAGMENT_EMPLOYEE_LIST);
                    navigator.startActivity(OwnerActivity.this, MenuActivity.class, bundle);
                    break;
                case R.id.nav_revenue_owner:
                    clearMenu();
                    fragment = RevenueFragment.getInstance();
                    break;
                case R.id.nav_income:
                    clearMenu();
                    fragment = IncomeFragment.getIntance();
                    break;
                case R.id.nav_income_detail:
                    clearMenu();
                    fragment = null;
                    // todo naviga to menuActivity and
                    bundle = new Bundle();
                    bundle.putInt(EzConstants.KEY_BUNDLE_FRAGMENT_TAG, FragmentConstants.FRAGMENT_INCOME_DETAIL);
                    navigator.startActivity(OwnerActivity.this, MenuActivity.class, bundle);
                    break;
                default:

                    return false;
            }

            if (fragment == null) {
                drawerLayout.closeDrawer(Gravity.LEFT);
                return true;
            } else {
                replaceFragment(fragment, fragment.getTag(), "");
                drawerLayout.closeDrawer(Gravity.LEFT);
            }
            return true;
        }
    };

    @Override
    public void onSignoutSuccess() {
        this.finish();
        navigator.startActivity(OwnerActivity.this, LoginActivity.class, null);
    }

    @Override
    public void onSignoutFalied(String msg) {
        RB_Error_MSg(msg);

    }

    @Override
    public void setLeftProfiles(EzParkManager userManager) {
        if (userManager == null || userManager.getOwner() == null || userManager.getEmployee() == null) {
            Timber.tag(TAG).e("null usermanager");

        } else {
            leftname.setText(userManager.getOwner().getName());
            leftDescription.setText(userManager.getOwner().getDescription());
        }
    }


}
