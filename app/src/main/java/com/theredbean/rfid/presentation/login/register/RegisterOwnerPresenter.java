package com.theredbean.rfid.presentation.login.register;


import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.model.UserProfile;
import com.theredbean.rfid.business.register.RegisterUsecase;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.models.Owner;

import javax.inject.Inject;

public class RegisterOwnerPresenter implements RegisterOwnerContract.Presenter {
    public static final String TAG = RegisterOwnerPresenter.class.getSimpleName();
    RegisterOwnerContract.View mView;
    IDatabase realm;

    RegisterUsecase registerUsecase;

    @Inject
    public RegisterOwnerPresenter(RegisterUsecase registerUsecase) {
        this.registerUsecase = registerUsecase;
    }

//
//    @Inject
//    public RegisterOwnerPresenter(@Named("realm") IDatabase realm, IOnlineDatabase firebase) {
//        this.realm = realm;
//        this.firebase = firebase;
//        Timber.tag(TAG).e(">>>>> realm: %s", realm.toString());
//        Timber.tag(TAG).e(">>>>> firebase: %s", firebase.toString());
//    }

    @Override
    public void takeView(RegisterOwnerContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        this.mView = null;

    }


    @Override
    public void register(String user, String pass, String name, String description) {
        mView.showLoading();
        registerUsecase.registerV1(
                user,
                pass,
                name,
                description, new EzParkingCallback.RegisterCallbackV1() {
                    @Override
                    public void onRegisterSuccess(UserProfile profile) {
                        mView.hideLoading();
                        mView.onRegisterSuccess();
                    }

                    @Override
                    public void onRegisterFailed(String msg) {
                        mView.hideLoading();
                        mView.onRegisterFalied(msg);

                    }

                    @Override
                    public void onExceptionError(String msg) {
                        mView.hideLoading();
                        mView.onRegisterFalied(msg);
                    }

                    @Override
                    public void onExceptionAuthentication() {
                        mView.hideLoading();
                        mView.onRegisterFalied("onExceptionAuthentication");
                    }
                });
    }

    @Override
    public void writeOnwerUserRegistered(Owner owner) {

    }
}
