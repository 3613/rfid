package com.theredbean.rfid.presentation.menu.config;

import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.Pricez.FPrice;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;
import utils.AlertDialogListener;
import utils.EzConstants;
import utils.ProgressBarUtils;

public class ConfigFragment extends BaseFragment implements ConfigContract.View, AlertDialogListener {
    public static final String TAG = ConfigFragment.class.getSimpleName();
    Context context;
    int a, b, c, d, e, f;
    int position = -1;

    @Override
    public void onAlertConfirmClick() {
        hideLoading();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mylistener.onBackButton();
            }
        }, 500);
    }

    @Override
    public void onAlertCancelClick() {

    }

    public interface Mylistener {
        void onBackButton();

        void showProgressbarUtils();

        void hideProgressBarUtils();
    }

    public Mylistener mylistener;

    @Inject
    ConfigContract.Presenter presenter;

    @BindView(R.id.edtA)
    EditText edtA;
    @BindView(R.id.edtB)
    EditText edtB;

    @BindView(R.id.edtC)
    EditText edtC;

    @BindView(R.id.edtD)
    EditText edtD;

    @BindView(R.id.edtE)
    EditText edtE;

    @BindView(R.id.edtF)
    EditText edtF;


    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {
        ProgressBarUtils.showWaitingDialog(context.getApplicationContext());

    }

    @Override
    public void showLoading(String msg) {
//        RB_Loading_MSg(msg);

    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }

    @Override
    public void showAlertDialog(String msg) {
        RB_Warning_MSg(msg);
    }


    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (this.context instanceof ConfigFragment.Mylistener) {
            mylistener = (Mylistener) context;
        } else {
            throw new ClassCastException(getActivity().getLocalClassName() + "is not cast CameraResult listener");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_config;
    }


    @Override
    protected void onMyCreateView(View view) {
        Timber.tag(TAG).e("appcontextInstance4 %s", view.getContext().hashCode());


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onStart() {
        super.onStart();
        presenter.takeView(this);
        Timber.tag(TAG).e("appcontextInstance1 %s", context.getApplicationContext().hashCode());
        Timber.tag(TAG).e("appcontextInstance2 %s", getContext().hashCode());
        Timber.tag(TAG).e("appcontextInstance3 %s", context.hashCode());
        presenter.takeView(this);
        presenter.loadConfig(parkingKey);
    }

    @Override
    public void onResume() {
        super.onResume();
        // todo: move on save button event

    }

    @OnClick(R.id.btnSaveConfig)
    public void saveConfig() {

        String a = edtA.getText().toString().trim();
        String b = edtB.getText().toString().trim();
        String c = edtC.getText().toString().trim();
        String d = edtD.getText().toString().trim();
        String e = edtE.getText().toString().trim();
        String f = edtF.getText().toString().trim();
        if (TextUtils.isEmpty(a) || TextUtils.isEmpty(b) || TextUtils.isEmpty(c)
                || TextUtils.isEmpty(d) || TextUtils.isEmpty(e) || TextUtils.isEmpty(f)) {
            showAlertEditextEmpty(context.getResources().getString(R.string.please_fill_all));
        } else {
//            ProgressBarUtils.showWaitingDialog(context.getApplicationContext());

            mylistener.showProgressbarUtils();
            presenter.savePriceConfig(parkingKey, a, b, c, d, e, f);
        }


    }


    private void initDependency() {
        DaggerEzParkingComponent.builder().applicationComponent(((BaseActivity) getActivity())
                .getApplicationComponent()).ezParkingModule(new EzParkingModule()).build().inject(this);
    }


    String parkingKey = EzConstants.BUNDLE_DEFAULT;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDependency();
        setHasOptionsMenu(true);
        Bundle data = getArguments();
        int p = -2;
        if (data != null) {
            p = data.getInt(EzConstants.KEY_BUNDLE_POSITION_ITEM, -1);
            parkingKey = data.getString(EzConstants.KEY_BUNDLE_PARKING_UUID, EzConstants.BUNDLE_DEFAULT);
            Timber.tag(TAG).e("my position got: %d", p);
            Timber.tag(TAG).e("myParkingKey %s", parkingKey);


        }
        Timber.tag(TAG).e("my position got: %d", p);

    }

    @Override
    public void onWriteSuccess() {
        mylistener.hideProgressBarUtils();
        RB_Confirm_Dialog("Thêm bãi xe thành công",
                "Bạn đã cấu hình thành công \n Bấm xác nhận để quay lại màn hình danh sách bãi xe",
                null,
                "Quay lại màn hình chính",
                this);

    }

    @Override
    public void onWritepriceFalied(String msg) {
        RB_Warning_MSg(msg);
    }

    @Override
    public void showAlertEditextEmpty(String msg) {
        RB_Warning_MSg(msg);

    }

    @Override
    public void showLoadConfigSuccess(FPrice price) {
        edtA.setText(String.valueOf(price.getXe_ga().getDay()));
        edtB.setText(String.valueOf(price.getXe_so().getDay()));
        edtC.setText(String.valueOf(price.getXe_dap().getDay()));
        edtD.setText(String.valueOf(price.getXe_ga().getNight()));
        edtE.setText(String.valueOf(price.getXe_so().getNight()));
        edtF.setText(String.valueOf(price.getXe_dap().getNight()));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.mnSaveConfig) {
            saveConfig();
        }
        ;
        return super.onOptionsItemSelected(item);

    }

    public static Fragment getInstance(int position, String parkingUUID) {

        ConfigFragment fragment = new ConfigFragment();
        Bundle data = new Bundle();
        data.putInt(EzConstants.KEY_BUNDLE_POSITION_ITEM, position);
        data.putString(EzConstants.KEY_BUNDLE_PARKING_UUID, parkingUUID);

        fragment.setArguments(data);
        return fragment;

    }
}
