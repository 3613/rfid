package com.theredbean.rfid.presentation.menu.employeelist.holderAdapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theredbean.rfid.R;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Parking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by hoang on 07/04/2018 nhe.
 */
public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeViewHolder> {
    private static final String TAG = EmployeeAdapter.class.getSimpleName();
    private Activity mActivity;
    private List<Employee> mData;
    private LayoutInflater mLayoutInflater;
    private HashMap<String, String> mWorkingAt; //parking key, income
    private List<Parking> parkings;

    EmployeeViewHolder.OnEmployeeViewHolderListener mListener;

    public EmployeeAdapter(Activity activity, EmployeeViewHolder.OnEmployeeViewHolderListener mListener) {
        mLayoutInflater = LayoutInflater.from(activity);
        mActivity = activity;
        mData = null;
        this.mListener = mListener;
        mData = new ArrayList<>();
        mWorkingAt = new HashMap<>();
        parkings = new ArrayList<>();

    }

    public void setData(List<Employee> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_list_employee, parent, false);
        return new EmployeeViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(EmployeeViewHolder holder, int position) {
        Employee entity = mData.get(position);
        holder.renderUi(position, entity);
        holder.setWorking(mWorkingAt.get(mData.get(position).getParkingKey()));


    }

    public void setParkingLustKey(List<Parking> parkings) {
        this.parkings = parkings;
        for(Parking p : parkings){
            mWorkingAt.put(p.getUuid(), p.getName());
        }
       notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    private final EmployeeViewHolder.OnEmployeeViewHolderListener listener = new EmployeeViewHolder.OnEmployeeViewHolderListener() {
        @Override
        public void onClick(int position) {
            mListener.onClick(position);
        }

        @Override
        public void onLongClick(int position) {
            mListener.onClick(position);
        }
    };
}
