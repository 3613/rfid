package com.theredbean.rfid.presentation.login.ownerlist;

import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

import java.util.List;

public interface PickOwnerContract {

    interface View extends BaseView {
        void onGetlistOwnerSuccess(List<Owner> ownerList);

        void onGetlistOwnerFalied();

        void onSaveEmployeeSuccess();

        void onSaveEmployeeFailed(String msg);
    }

    interface Presenter extends BasePresenter<PickOwnerContract.View> {
        void getListOwner();

        void saveEmployee(String owner_UID);

        void signOut();
    }
}
