package com.theredbean.rfid.presentation.owner.incom.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.theredbean.rfid.R;
import com.theredbean.rfid.business.model.CurrentIncome;
import com.theredbean.rfid.models.Parking;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;
import utils.MyUtils;


public class IncomeViewholder extends RecyclerView.ViewHolder implements View.OnClickListener,
        View.OnLongClickListener {

    private int position;
    private CurrentIncome item;

    @BindView(R.id.txParkingName)
    TextView parkingName;
    @BindView(R.id.tvIncome)
    TextView tvIncome;

    private OnIncomeViewholderListener mListener;


    public interface OnIncomeViewholderListener {
        void onClick(int position);

        void onLongClick(int position);
    }

    public IncomeViewholder(View view, OnIncomeViewholderListener listener) {
        super(view);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        ButterKnife.bind(this, view);
        mListener = listener;

    }

    public void renderUi(int position, Parking entity) {
        this.position = position;
        parkingName.setText(entity.getName());
    }

    public void setIncome(Long aLong) {
        // todo

        if (aLong != null) {
            Integer price = aLong.intValue();

            tvIncome.setText(MyUtils.fomatVND(price));
        }

    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onClick(position);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mListener != null) {
            mListener.onLongClick(position);
        }
        return false;
    }

    public static final String TAG = IncomeViewholder.class.getSimpleName();

}