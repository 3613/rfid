package com.theredbean.rfid.presentation.login.signin;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.hnam.uibutton.MyTextView;
import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class LoginFragment extends BaseFragment implements LoginFragmentContract.View {
    public static final String TAG = LoginFragment.class.getSimpleName();


    @Inject
    LoginFragmentContract.Presenter presenter;
    FirebaseAuth mAuth;

    Context context;
    LoginListenner mListenner;
    MyTextView btnRegisterNow;

    @Override
    public void showOwnerScreen() {
        Timber.tag(TAG).e("showOwnerScreen");
        mListenner.showOwnerScreen();
        mListenner.onSignOut();
    }

    @Override
    public void showEmployeeScreen() {
        Timber.tag(TAG).e("showEmployeeScreen");
        mListenner.showEmployeeScree();

    }

    @Override
    public void showAlertError(String msg) {
//        RB_Warning_MSg(msg);
//        Timber.tag(TAG).e(msg);

    }

    @Override
    public void showAlertNoExistingUser() {
        RB_Error_MSg("showAlertNoExistingUser");

    }

    @Override
    public void showInitOwnerMessage() {
        // todo what the fuck?
    }

    @Override
    public void onLoginSuccess() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        assert user != null;
        Toast.makeText(context, "onLoginSuccess line 68" + user.getEmail(), Toast.LENGTH_SHORT).show();
        String userid = user.getUid();

    }

    @Override
    public void onLoginFailed(String msg) {
        RB_Error_MSg(msg);
    }

    @Override
    public void showErrorUserScreen() {

    }

    @Override
    public void showErrorUserScreen(String msg) {
        mListenner.showErrorScreen(msg);
    }


    public interface LoginListenner {
        void onGotoRegister();

        void onGotoForgotPassWord();

        void showOwnerScreen(); // result, report page

        void showEmployeeScree(); // camera in home Activity

        void openRegisterEmployeee();

        void openRegisterOwner();

        void openNewLogin();

        void onSignOut();

        void showErrorScreen(String msg);
    }


    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.edtpass)
    EditText edtPass;
    @BindView(R.id.edtUserName)
    EditText edtUser;


//    @BindView(R.id.btnGotoRegister)
//    TextView btnGotoRegister;

    @BindView(R.id.btnLostPass)
    TextView btnLostpass;


    @OnClick(R.id.btnRegEmployee)
    public void RegisterEmployee() {
        mListenner.openRegisterEmployeee();
    }


    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof LoginFragment.LoginListenner) {
            mListenner = (LoginFragment.LoginListenner) context;
        } else {
            throw new ClassCastException(getActivity().getLocalClassName() + "is not cast CameraResult listener");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_login;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInjection();

    }

    @Override
    protected void onMyCreateView(View view) {
        mAuth = FirebaseAuth.getInstance();

        presenter.takeView(this);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {
//        pbloading.setVisibility(View.VISIBLE);
        RB_Loading_MSg("");

    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }

    @Override
    public void showAlertDialog(String msg) {

    }

    @OnClick(R.id.btnLogin)
    public void doLogin() {

        if (TextUtils.isEmpty(edtPass.getText()) || TextUtils.isEmpty(edtUser.getText())) {
            RB_Warning_MSg("Không thể để trống ");
            return;
        }
        Timber.tag(TAG).e("dologin ne");
        String email = edtUser.getText().toString().trim();
        String password = edtPass.getText().toString().trim();
        presenter.login(email, password);
    }

    @OnClick(R.id.btnLostPass)
    public void onbtnGotoRegister() {
        mListenner.onGotoForgotPassWord();
    }


    private void updateUI(FirebaseUser user) {
        if (user != null) {
            Toast.makeText(getActivity(), "login success", Toast.LENGTH_SHORT).show();
        }
    }

    public static LoginFragment getInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle data = new Bundle();
        fragment.setArguments(data);
        return fragment;
    }


    public void initInjection() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build()
                .inject(this);

    }


    @OnClick(R.id.btnRegOwner)
    public void openNewRegister() {
        Timber.tag(TAG).e("btnRegOwner : openNewRegister");
        mListenner.openNewLogin();
    }


}
