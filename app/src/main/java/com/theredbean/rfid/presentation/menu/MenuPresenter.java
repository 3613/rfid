package com.theredbean.rfid.presentation.menu;


import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.di.ActivityScope;

import javax.inject.Inject;
import javax.inject.Named;

@ActivityScope

public class MenuPresenter implements MenuContract.Presenter {

    MenuContract.View mView;


    IDatabase iDatabase;

    @Inject
    public MenuPresenter(@Named("realm") IDatabase iDatabase) {
        this.iDatabase = iDatabase;
    }

    @Override
    public void takeView(MenuContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }
}
