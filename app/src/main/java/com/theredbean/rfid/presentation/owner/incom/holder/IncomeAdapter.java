package com.theredbean.rfid.presentation.owner.incom.holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theredbean.rfid.R;
import com.theredbean.rfid.business.model.CurrentIncome;
import com.theredbean.rfid.models.Parking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by hoang on 15/04/2018 nhe.
 */
public class IncomeAdapter extends RecyclerView.Adapter<IncomeViewholder> {
    private static final String TAG = IncomeAdapter.class.getSimpleName();
    private Activity mActivity;
    private List<Parking> mData;
    private LayoutInflater mLayoutInflater;
    private HashMap<String, Long> mIncome; //parking key, income

    public IncomeAdapter(Activity activity) {
        mLayoutInflater = LayoutInflater.from(activity);
        mActivity = activity;
        mData = null;
        mIncome = new HashMap<>();
        mData = new ArrayList<>();
    }

    public void setData(List<Parking> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public void setIncome(CurrentIncome income) {
        mIncome.put(income.getParkingUuid(), income.getIncome());
        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i).getUuid().equalsIgnoreCase(income.getParkingUuid())) {
                notifyItemChanged(i);
                break;
            }
        }

    }


    @Override
    public IncomeViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_icome, parent, false);
        return new IncomeViewholder(view, listener);
    }

    @Override
    public void onBindViewHolder(IncomeViewholder holder, int position) {
        Parking entity = mData.get(position);
        holder.renderUi(position, entity);
        holder.setIncome(mIncome.get(entity.getUuid()));
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        } else {
            return mData.size();
        }
    }

    private final IncomeViewholder.OnIncomeViewholderListener listener = new IncomeViewholder.OnIncomeViewholderListener() {
        @Override
        public void onClick(int position) {

        }

        @Override
        public void onLongClick(int position) {

        }
    };
}
