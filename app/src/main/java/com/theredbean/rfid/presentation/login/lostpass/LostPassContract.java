package com.theredbean.rfid.presentation.login.lostpass;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface LostPassContract {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter<LostPassContract.View> {

    }
}
