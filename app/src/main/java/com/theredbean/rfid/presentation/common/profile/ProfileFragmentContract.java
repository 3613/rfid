package com.theredbean.rfid.presentation.common.profile;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface ProfileFragmentContract {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter<ProfileFragmentContract.View> {

    }
}
