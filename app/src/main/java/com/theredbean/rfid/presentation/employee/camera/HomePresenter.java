package com.theredbean.rfid.presentation.employee.camera;


import android.os.Bundle;

import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.checkcard.CheckCardUsecase;
import com.theredbean.rfid.business.income.IncomeUsecase;
import com.theredbean.rfid.business.login.LoginUsecase;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.rxbus.RxBus;
import com.theredbean.rfid.rxbus.RxBusEvent;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;
import utils.EzConstants;

@ActivityScope
public class HomePresenter implements HomeContract.Presenter {

    private static final String TAG = HomePresenter.class.getSimpleName();
    HomeContract.View mView;
    IDatabase iDatabase;
    LoginUsecase loginUsecase;
    CheckCardUsecase checkCardUsecase;

    EzParkManager usermanager;
    RxBus rxBus;
    CompositeDisposable compositeDisposable;
    IncomeUsecase incomeUsecase;

    @Inject
    public HomePresenter(@Named("realm") IDatabase iDatabase, IncomeUsecase incomeUsecase, EzParkManager userManager, LoginUsecase luc, CheckCardUsecase checkCardUsecase, RxBus rxBus) {
        loginUsecase = luc;
        this.checkCardUsecase = checkCardUsecase;
        this.usermanager = userManager;
        this.incomeUsecase = incomeUsecase;
        this.iDatabase = iDatabase;
        this.rxBus = rxBus;
        this.compositeDisposable = new CompositeDisposable();
        Timber.tag(TAG).e("isDatabase %s", this.iDatabase.toString());
    }

    @Override
    public void takeView(HomeContract.View view) {
        this.mView = view;
        registerBus();
    }


    @Override
    public void dropView() {
        mView = null;
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
            compositeDisposable.clear();
        }
    }

    private void registerBus() {
        Disposable d = rxBus.toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> handleRxEvent((RxBusEvent) o));
        compositeDisposable.add(d);
    }

    private void handleRxEvent(RxBusEvent event) {
        switch (event.what) {
            case INTERNET_STATUS:
                if (mView == null) {
                    return;
                }
                Bundle data = event.data;
                boolean status = data.getBoolean(EzConstants.STATUS_INTERNET, false);
                if (status) {
                    mView.showOnlineView();
                } else {
                    mView.showOfflineView();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void checkTag(String id) {

//        // kiểm tra xem thẻ có đúng prefix hay không
//
//        int myType = MyUtils.typeOfCard(id);
//        Timber.tag(TAG).e("checktag %d \t %s", myType, id);
//
//        // không đúng thì báo lỗi
//        if (myType == EzConstants.BikeType.XE_TAO_LAO) {
//            mView.onCardNotValid(" Thẻ này không phải là thẻ của nhà xe. Vui lòng kiểm tra lại! \n Xin cảm ơn");
//            return;
//        } else {
//
//            // đúng thì check coi là xe đang vô hay ra
//            Timber.tag(TAG).e("checkTag: " + iDatabase.toString());
////            List<CurrentRecord> records = iDatabase.getCurrentRecordByEpc(id);
//            int recordSize = records.size();
//            // ok ne
//            if (recordSize == 0) {
//                mView.openCheckIn(id);
//                return;
//            }
//            if (recordSize > 0) {
//                mView.openCheckOut(id);
//            }
//        }


    }

    @Override
    public void signOut() {
        Timber.tag(TAG).e("signout button");
        if (mView != null) mView.showLoading();
        loginUsecase.signOut(new EzParkingCallback.SignOutCallback() {
            @Override
            public void onSignOutSuccess() {
                if (mView != null) {
                    mView.hideLoading();
                    mView.onSignoutSuccess();
                }
                Timber.tag(TAG).e("onSignOutSuccess");
            }

            @Override
            public void onExceptionError(String msg) {
                if (mView != null) mView.showAlertDialog(msg);
                Timber.tag(TAG).e(msg + "onExceptionAuthentication ");

            }

            @Override
            public void onExceptionAuthentication() {
                if (mView != null) mView.showAlertDialog("onExceptionAuthentication");
                Timber.tag(TAG).e("onExceptionAuthentication");

            }
        });
    }

    @Override
    public void checkTagOnline(String cardid) {
        Timber.tag(TAG).e("checkTagOnline");
        if (mView != null) {
            mView.showLoading("Dang kiem tra the");
        }


        checkCardUsecase.isCardExisting(usermanager.getEmployee(), cardid, new EzParkingCallback.CheckCardExisting() {
            @Override
            public void onIsCardExist(FCurrentRecord record) {
                assert mView != null;
                if (mView != null) {
                    mView.hideLoading();
                    mView.openOnlineCheckout(cardid, record);
                }
                Timber.tag(TAG).e("onIsCardExist");
//                mView.openCheckOut(cardid)

            }

            @Override
            public void onNoCard() {
                
                if (mView == null) return;
                Timber.tag(TAG).e("onNoCard onNoCard");
                
                mView.hideLoading();
                mView.openCheckIn(cardid);
            }

            @Override
            public void onExceptionError(String msg) {
                if (mView == null) return;
                mView.hideLoading();
                mView.onCardNotValid(msg);
                Timber.tag(TAG).e("onExceptionError %s", msg);

            }

            @Override
            public void onExceptionAuthentication() {
                if (mView == null) return;
                mView.hideLoading();
                mView.onCardNotValid("onExceptionAuthentication");
                Timber.tag(TAG).e("onExceptionAuthentication");

            }
        });
    }

    @Override
    public void setNameInLeftSidebar() {
        if (usermanager == null) {
            // nothing to do
        } else {
            if (mView == null) return;
            mView.setNameLeftSidebar(usermanager);

        }
    }


}

