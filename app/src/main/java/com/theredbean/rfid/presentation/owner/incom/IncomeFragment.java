package com.theredbean.rfid.presentation.owner.incom;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hnam.uibutton.MyTextView;
import com.theredbean.rfid.R;
import com.theredbean.rfid.business.model.CurrentIncome;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.Parking;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;
import com.theredbean.rfid.presentation.login.signin.LoginFragment;
import com.theredbean.rfid.presentation.owner.incom.holder.IncomeAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;
import utils.MyUtils;

public class IncomeFragment extends BaseFragment
        implements IncomeContract.View, SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = IncomeFragment.class.getSimpleName();


    @Inject
    IncomeContract.Presenter presenter;

    Context context;

    Mylistener mLMylistener;
    IncomeAdapter adapter2;
    @BindView(R.id.rvIncome)
    RecyclerView rvParking;

    @BindView(R.id.txincome)
    TextView txincome;
    @BindView(R.id.justTitle)
    LinearLayout justTitle;
    @BindView(R.id.txtNoParking)
    MyTextView txtNoParking;
    @BindView(R.id.tvIncomeT)
    MyTextView tvIncomeT;
    SwipeRefreshLayout swipeRefreshLayout;
    Unbinder unbinder;


    @SuppressLint("SetTextI18n")
    @Override
    public void onGetIncomeSuccess(CurrentIncome currentIncome, long total) {
        Timber.tag(TAG).e("onGetIncomeSuccess");
        adapter2.setIncome(currentIncome);
        txincome.setText(MyUtils.fomatVND((int) total) + " VND");
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onGetIncomeFalied(String msg) {

    }

    @Override
    public void ongetParkingsSuccess(List<Parking> parkingList) {
        adapter2.setData(parkingList);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onGetParkingsFailed(String msg) {

    }

    @Override
    public void disPlayNoParking() {
        txtNoParking.setVisibility(View.VISIBLE);
        rvParking.setVisibility(View.INVISIBLE);
        txincome.setText("0 VN");

    }

    @Override
    public void displayHaveData() {
        txtNoParking.setVisibility(View.INVISIBLE);
        rvParking.setVisibility(View.VISIBLE);
    }

    public static Fragment getIntance() {
        IncomeFragment fragment = new IncomeFragment();
        Bundle data = new Bundle();
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    public void onRefresh() {
        Timber.tag(TAG).e("refreshed");

        presenter.getParkings();
    }

    public interface Mylistener {

    }


    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof Mylistener) {
            mLMylistener = (Mylistener) context;
        } else {
            throw new ClassCastException(TAG + " -- can not cast Mylistener interface");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_income;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInjection();

//        Timber.tag(TAG).e("%s : null or not", swipeContainer.toString());


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, super.onCreateView(inflater, container, savedInstanceState));
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    protected void onMyCreateView(View view) {
        setupRV2();
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.takeView(this);
        presenter.getParkings();

    }

    @Override
    public void onResume() {
        super.onResume();
        // todo check parking list empty or not
        presenter.registerIncomeListener();

    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.unregisterIncomListener();

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.dropView();
        unbinder.unbind();
    }

    @Override
    public void showMessage(String message) {
        RB_Succeess_MSg(message);
    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {
        RB_Loading_MSg(msg);

    }

    @Override
    public void hideLoading() {
        Timber.tag(TAG).e("---- zo hideloading");

        RB_hideDialog();
    }

    @Override
    public void showAlertDialog(String msg) {
        RB_Warning_MSg(msg);
    }

    private void initInjection() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build()
                .inject(this);

    }

    private void setupRV2() {
        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        rvParking.setLayoutManager(lm);
        rvParking.setHasFixedSize(true);
        adapter2 = new IncomeAdapter(getActivity());
        rvParking.setAdapter(adapter2);
    }
}
