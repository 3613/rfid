package com.theredbean.rfid.presentation.login.signin;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface LoginFragmentContract {

    interface View extends BaseView {
        void showOwnerScreen();
        void showEmployeeScreen();
        void showAlertError(String msg);
        void showAlertNoExistingUser();
        void showInitOwnerMessage(); //show toast
        void onLoginSuccess();
        void onLoginFailed(String msg);

        void showErrorUserScreen();

        void showErrorUserScreen(String msg);
    }

    interface Presenter extends BasePresenter<LoginFragmentContract.View> {
        void login(String email, String password);

    }
}
