package com.theredbean.rfid.presentation.employee.camera.fragmentcheckin;

import android.graphics.Bitmap;

import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

import java.io.File;

import io.reactivex.disposables.Disposable;

public interface CheckinContract {

    interface View extends BaseView {
        void loadImage(String url);

        void oncheckInSuccess();

        void onCheckInFalied(String msg);

        void oncheckInSuccessV2(String localPathImage, String targetPathImage);


        void disableButton();

        void onNextTimer(Long aLong);

        void onCompleteTimer();
    }

    interface Presenter extends BasePresenter<CheckinContract.View> {

        void showImage(String photourl);

        void processCheckin(String id, String path, int bikeType);


        // onOnline version

        void onlineCheckin(File file, String fileName);

        void onlineCheckin(Bitmap bitmap, String card_NFC_id);

        void onlineCheckin(String path, String card_id);

        void startCounter();
    }
}
