package com.theredbean.rfid.presentation.menu.revenue;


import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.data.db.record.HistoryRecord;
import com.theredbean.rfid.di.ActivityScope;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import timber.log.Timber;
import utils.EzConstants;
import utils.MyUtils;

@ActivityScope

public class RevenuePresenter implements RevenueContract.Presenter {
    public static final String TAG = RevenuePresenter.class.getSimpleName();
    RevenueContract.View view;

    IDatabase realm;

    @Inject
    public RevenuePresenter(@Named("realm") IDatabase realm) {
        this.realm = realm;
    }

    @Override
    public void takeView(RevenueContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        view = null;
    }


    @Override
    public void getListParkedByDuration(long from, long to) {
        List<HistoryRecord> listssss = realm.getHistoryRecordInDurationCopy(from, to);
        Timber.tag(TAG).e("from  %d , \t to %d \n list %s \n %s", from, to, String.valueOf(listssss.size()), listssss.toString());


        int totalXeSo = 0;
        int totalXeGa = 0;
        int totalXeDap = 0;
        int sumMoney = 0;

        for (HistoryRecord record : listssss) {
            if (record.getType() == EzConstants.BikeType.XE_SO) {
                totalXeSo += 1;

            }
            if (record.getType() == EzConstants.BikeType.XE_DAP) {
                totalXeDap += 1;
            }
            if (record.getType() == EzConstants.BikeType.XE_GA) {
                totalXeGa += 1;
            }
        }

        sumMoney = (totalXeDap * EzConstants.PRICE_C_XE_DAP_D) + (totalXeGa * EzConstants.PRICE_A_XE_GA_D) + (totalXeSo * EzConstants.PRICE_B_XE_SO_D);
        view.onGetListSuccess(String.valueOf(totalXeGa), String.valueOf(totalXeSo), String.valueOf(totalXeDap), MyUtils.fomatVND(sumMoney));

    }
}
