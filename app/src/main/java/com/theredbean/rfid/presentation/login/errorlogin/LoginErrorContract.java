package com.theredbean.rfid.presentation.login.errorlogin;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface LoginErrorContract {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter<LoginErrorContract.View> {

    }
}
