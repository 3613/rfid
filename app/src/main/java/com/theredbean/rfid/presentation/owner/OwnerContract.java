package com.theredbean.rfid.presentation.owner;

import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface OwnerContract {

    interface View extends BaseView {
        void onSignoutSuccess();

        void onSignoutFalied(String msg);

        void setLeftProfiles(EzParkManager userManager);
    }

    interface Presenter extends BasePresenter<OwnerContract.View> {

        void signOut();

        void setLeftProfile();
    }
}
