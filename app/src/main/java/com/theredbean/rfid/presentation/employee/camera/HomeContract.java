package com.theredbean.rfid.presentation.employee.camera;

import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface HomeContract {

    interface View extends BaseView {

        void openCheckIn(String id);

        void openCheckOut(String id);

        void onCardNotValid(String msg);

        void onSignoutSuccess();
        void onSignOutFalied(String msg);

        void openOnlineCheckout(String cardid, FCurrentRecord record);

        void setNameLeftSidebar(EzParkManager userManager);

        void showOnlineView();

        void showOfflineView();
    }

    interface Presenter extends BasePresenter<HomeContract.View> {

        void checkTag(String id);

        void signOut();

        // onOnline version bellow

        void checkTagOnline(String cardid);

        void setNameInLeftSidebar();

    }
}
