package com.theredbean.rfid.presentation.menu.incomedetail;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.hnam.uibutton.MyTextView;
import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.IncomModel;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;
import com.theredbean.rfid.presentation.menu.incomedetail.holder.IncomDetalAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;
import utils.MyUtils;
import utils.TimeUtils;

public class IncomeDetailFragment extends BaseFragment implements IncomeDetailContract.View {

    public static final String TAG = IncomeDetailFragment.class.getSimpleName();


    @Inject
    IncomeDetailContract.Presenter presenter;

    IncomDetalAdapter adapter2;
    @BindView(R.id.rvIncome)
    RecyclerView rvParking;
    @BindView(R.id.txtNoParking)
    MyTextView txtNoParking;
    Context context;
    @BindView(R.id.timeStart)
    MyTextView txtStart;
    @BindView(R.id.timeEnd)
    MyTextView txtEnd;


    @BindView(R.id.txincome)
    MyTextView txincome;
    Unbinder unbinder;
    @BindView(R.id.fromToContainer)
    LinearLayout fromToContainer;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;
    @BindView(R.id.line)
    View line;
    @BindView(R.id.tvIncomeT)
    MyTextView tvIncomeT;

    @Override
    public void onloadIncomSuccess(List<IncomModel> incomModels, int sumPrice) {
        adapter2.setData(incomModels);
        txtNoParking.setVisibility(View.INVISIBLE);
        rvParking.setVisibility(View.VISIBLE);
        txincome.setText(MyUtils.fomatVND(sumPrice));


    }

    public Mylistener mylistener;

    public void setFilter(long from, long to) {

        txtEnd.setText(context.getResources().getString(R.string.time_end, String.valueOf(TimeUtils.getTimeText(to))));                //
        txtStart.setText(context.getResources().getString(R.string.time_start, String.valueOf(TimeUtils.getTimeText(from))));                //
        Timber.tag(TAG).e("from setFilter hehe %d,---%d: ", from, to);
        presenter.loadFilterIncome(from, to);

    }

    public interface Mylistener {

        void openFilterIncom();
    }


    public static Fragment getIntance() {
        return new IncomeDetailFragment();
    }


    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof Mylistener) {
            mylistener = (Mylistener) context;
        } else {
            throw new ClassCastException(TAG + " -- can not cast Mylistener interface");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_income_detail;
    }

    long from = 0;
    long to = 0;
    public static String KEY_FROM = "FROM";
    public static String KEY_TO = "TO";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInjection();
        setHasOptionsMenu(true);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, super.onCreateView(inflater, container, savedInstanceState));
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    protected void onMyCreateView(View view) {
        setupRV2();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        txtStart.setText(TimeUtils.getTimeText(TimeUtils.plusOneWeek()));
        txtEnd.setText(TimeUtils.getTimeText(TimeUtils.getCurrentInMiliseconds()));
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.takeView(this);
        presenter.loadFilterIncome(TimeUtils.plusOneWeek(), TimeUtils.getCurrentInMiliseconds());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.dropView();
        unbinder.unbind();
    }

    @Override
    public void showMessage(String message) {
        RB_Succeess_MSg(message);
    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {
        RB_Loading_MSg(msg);

    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }

    @Override
    public void showAlertDialog(String msg) {
        RB_Warning_MSg(msg);
    }

    private void initInjection() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build()
                .inject(this);

    }


    private void setupRV2() {
        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        rvParking.setLayoutManager(lm);
        rvParking.setHasFixedSize(true);
        adapter2 = new IncomDetalAdapter(getActivity());
        rvParking.setAdapter(adapter2);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.mnIncomDetail) {
            mylistener.openFilterIncom();
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_income_detail, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @OnClick(R.id.fromToContainer)
    public void onShowTimeSelected() {
        mylistener.openFilterIncom();
    }
}
