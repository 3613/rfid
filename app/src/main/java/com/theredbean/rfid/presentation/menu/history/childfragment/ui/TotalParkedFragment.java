package com.theredbean.rfid.presentation.menu.history.childfragment.ui;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.dialog.ImageDialogFragment;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.models.FHistoryRecord;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;
import com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.TotalViewholder;
import com.theredbean.rfid.presentation.menu.history.HistoryContract;
import com.theredbean.rfid.presentation.menu.history.childfragment.test.TotalAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;
import utils.ProgressBarUtils;

public class TotalParkedFragment extends BaseFragment implements HistoryContract.View, TotalViewholder.OnTTViewholderListener {
    public static final String TAG = TotalParkedFragment.class.getSimpleName();
    Context context;
    @Inject
    HistoryContract.Presenter presenter;
    TotalAdapter adapter2;

    List<FHistoryRecord> mdata;
    @BindView(R.id.rvHistory)
    RecyclerView rvHistory;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDependency();

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showAlertDialog(String msg) {

    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_history_list_recycler;
    }


    private void initDependency() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(((BaseActivity) getActivity())
                        .getApplicationComponent()).ezParkingModule(new EzParkingModule()).build().inject(this);
    }

    @Override
    protected void onMyCreateView(View view) {
        Timber.tag(TAG).e("life onMyCreateView");

        mdata = new ArrayList<>();
        setupRV2();
    }


    @Override
    public void onStart() {
        super.onStart();
        presenter.getlistHistory();
        Timber.tag(TAG).e("life onMyCreateView");

    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.tag(TAG).e("life onResume");

        presenter.takeView(this);
//        presenter.getCurrentParkingRecord();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.dropView();
    }

    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        Timber.tag(TAG).e("appcontextInstance1 %s", context.getApplicationContext().hashCode());
        Timber.tag(TAG).e("appcontextInstance2 %s", this.context.hashCode());
        Timber.tag(TAG).e("appcontextInstance3 %s", context.hashCode());
        Timber.tag(TAG).e("appcontextInstance4 %s", this.context.getApplicationContext().hashCode());


    }


    private void setupRV2() {

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        rvHistory.setLayoutManager(lm);
        rvHistory.setHasFixedSize(true);
        adapter2 = new TotalAdapter(getActivity(), this);
        adapter2.setData(mdata);
        rvHistory.setAdapter(adapter2);
        adapter2.notifyDataSetChanged();


    }

    @Override
    public void onClick(int position) {
        Timber.tag(TAG).e("kaka: %d", position);

        Bundle args = new Bundle();
        args.putString("imgPath", mdata.get(position).getPathImage());
        DialogFragment newFragment = new ImageDialogFragment();
        newFragment.setArguments(args);
        newFragment.show(getFragmentManager(), TAG);

        //TODO call dialog and put path to the imageView
    }

    @Override
    public void onLongClick(int position) {
        Timber.tag(TAG).e("onLongClickkaka: %d", position);

    }

    @Override
    public void onGotListCurrent(List<FCurrentRecord> currentRecords) {

    }

    @Override
    public void onGotListHistory(List<FHistoryRecord> historyRecords) {
        mdata = historyRecords;
        adapter2.setData(mdata);
    }

    public void setFilter(long start, long end) {
        Timber.tag(TAG).e("Start %d --- end %d", start, end);

        presenter.getTotalParckedRecord(start, end);
    }
}
