package com.theredbean.rfid.presentation.menu.employeelist.holderAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.theredbean.rfid.R;
import com.theredbean.rfid.models.Employee;

import timber.log.Timber;


public class EmployeeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
        View.OnLongClickListener {

    private int position;
    private Employee item;
    OnEmployeeViewHolderListener mListener;


    TextView txtUser, txtJobStatus;

    public interface OnEmployeeViewHolderListener {
        void onClick(int position);

        void onLongClick(int position);
    }

    public EmployeeViewHolder(View view, OnEmployeeViewHolderListener listener) {
        super(view);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        mListener = listener;

        txtUser = itemView.findViewById(R.id.txtUser);
        txtJobStatus = itemView.findViewById(R.id.txtJobStatus);

    }

    public void renderUi(int position, Employee entity) {
        this.position = position;
        txtUser.setText(String.valueOf(entity.getName()));
//        if (entity.getParkingKey() == null || entity.getParkingKey().equals("")) {
//            txtJobStatus.setText("chua co viec");
//        } else {
//            txtJobStatus.setText(entity.getParkingKey());
//        }

    }


    public void setWorking(String parkingName) {
        Timber.tag(TAG).e("setWorking "+parkingName);

        txtJobStatus.setText(parkingName);
    }

    public static final String TAG = EmployeeViewHolder.class.getSimpleName();


    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onClick(position);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mListener != null) {
            mListener.onLongClick(position);
        }
        return false;
    }
}