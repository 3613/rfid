package com.theredbean.rfid.presentation.menu.summary;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface SummaryContract {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter<SummaryContract.View> {

    }
}
