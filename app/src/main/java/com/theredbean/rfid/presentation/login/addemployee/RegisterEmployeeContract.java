package com.theredbean.rfid.presentation.login.addemployee;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface RegisterEmployeeContract {

    interface View extends BaseView {
        void showRegisterEmployeeSuccess();

        void showRegisterEmployeeFailed(String msg);

    }

    interface Presenter extends BasePresenter<RegisterEmployeeContract.View> {
        void registerEmployee(String email, String password, String description);


        void registerEmployeeV2(String email,
                                String password,
                                String name,
                                String description,
                                String ownerUUID);
    }
}
