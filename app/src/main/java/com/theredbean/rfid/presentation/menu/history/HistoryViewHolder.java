package com.theredbean.rfid.presentation.employee.menu.history;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.theredbean.rfid.data.db.record.CurrentRecord;


public class HistoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
        View.OnLongClickListener {

    private int position;
    private CurrentRecord item;

    private OnHistoryViewHolderListener mListener;

    public interface OnHistoryViewHolderListener {
        void onClick(int position);

        void onLongClick(int position);
    }

    public HistoryViewHolder(View view, OnHistoryViewHolderListener listener) {
        super(view);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        mListener = listener;

    }

    public void renderUi(int position, CurrentRecord entity) {
        this.position = position;
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onClick(position);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mListener != null) {
            mListener.onLongClick(position);
        }
        return false;
    }
}