package com.theredbean.rfid.presentation.employee.camera;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nouslogic.nfcmodule.Constant;
import com.nouslogic.nfcmodule.NfcHelper;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
//import com.redbean.rfid.RFIDModule;
import com.redbean.rfid.RFIDStatus;
import com.theredbean.rfid.R;
import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.cameramodule.AutoFitTextureView;
import com.theredbean.rfid.cameramodule.CameraController;
import com.theredbean.rfid.cameramodule.OpenCameraException;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.Navigator;
import com.theredbean.rfid.presentation.employee.camera.fragmentcheckin.CheckinFragment;
import com.theredbean.rfid.presentation.employee.camera.fragmentcheckout.CheckoutFragment;
import com.theredbean.rfid.presentation.menu.MenuActivity;
import com.theredbean.rfid.presentation.login.LoginActivity;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;
import utils.EzConstants;
import utils.FragmentConstants;
import utils.MyUtils;
import utils.TimeUtils;


public class HomeActivity extends BaseActivity implements HomeContract.View,
        CheckinFragment.FragmentCheckInListener,
        CheckoutFragment.FragmetCheckOutListener {

    private static final String TAG = HomeActivity.class.getSimpleName();
    private CameraController mRxCameraController21;
    private View mFocusIndicator;
    private String deviceid, path;
    private FragmentManager fm = getFragmentManager();


    @BindView(R.id.btnOpenCheckIn)
    TextView btnCheckin;
    @BindView(R.id.btnOpenCheckout)
    TextView btnCheckout;
    @BindView(R.id.btnCapture)
    TextView btnCapture;
    TextView leftname, leftDescription;

    /**/


    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;

    @BindView(R.id.offlineNoteContainer)
    RelativeLayout offlineNoteContainer;

    @Inject
    HomeContract.Presenter presenter;


    @Inject
    Navigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializingInjection();
        presenter.takeView(this);
        ButterKnife.bind(this);
        setUpNFCz();
        File outputDir = getCacheDir(); // context being the Activity pointer
        File outputFile = null;
        try {
            outputFile = File.createTempFile("prefix", ".jpg", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        findViewById(R.id.customCameraActivity_switchCamera).setOnClickListener(view -> mRxCameraController21.switchCamera());
        mFocusIndicator = findViewById(R.id.customCameraActivity_focusIndicator);

        mRxCameraController21 = new CameraController(this, mRxCamerController21Callback, outputFile.getAbsolutePath(), (AutoFitTextureView) findViewById(R.id.customCameraActivity_textureView), Configuration.ORIENTATION_PORTRAIT);
        mRxCameraController21.getLifecycle().onCreate(savedInstanceState);


        findViews();
        setupToolbar();
        setUpDrawer();


    }


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private NavigationView.OnNavigationItemSelectedListener itemSelectedListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if (item.getItemId() == R.id.nav_signout) {
                presenter.signOut();
                return true;
            }
            Bundle bundle = new Bundle();
            switch (item.getItemId()) {
                case R.id.nav_historyList:
                    bundle.putInt(EzConstants.KEY_BUNDLE_FRAGMENT_TAG, FragmentConstants.FRAGMENT_HISTORY);
                    break;
                case R.id.nav_summary:
                    bundle.putInt(EzConstants.KEY_BUNDLE_FRAGMENT_TAG, FragmentConstants.FRAGMENT_SUMMARY);
                    break;
                case R.id.nav_revenue:
                    bundle.putInt(EzConstants.KEY_BUNDLE_FRAGMENT_TAG, FragmentConstants.FRAGMENT_REVENUE);
                    break;
                case R.id.nav_config:
                    bundle.putInt(EzConstants.KEY_BUNDLE_FRAGMENT_TAG, FragmentConstants.FRAGMENT_CONFIG);
                    break;
                default:
                    return false;
            }


            Timber.tag(TAG).e("returned");

            drawerLayout.closeDrawer(Gravity.LEFT);
            mRxCameraController21.getLifecycle().onPause();
//            HomeActivity.super.showLoadingDialog("wwwww");
            alertError("", SweetAlertDialog.PROGRESS_TYPE, getString(R.string.please_wait));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dissmissSweetDialog();
                    navigator.startActivity(HomeActivity.this, MenuActivity.class, bundle);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

                }
            }, 2000);


            return true;
        }
    };

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {
        RB_Loading_MSg(msg);
    }

    @Override
    public void hideLoading() {
        dissmissSweetDialog();
    }


    /************************************
     *         START  NFC AREA           *
     *                                   *
     * **********************************/

    NfcHelper nfcHelper;
    private int nfcSupportedStatus = 1;
    private final static int NFC_AVAILABLE = 0;
    private final static int NFC_DISABLE = 1;
    private final static int NFC_NOT_SUPPORT = 2;

    private void setUpNFCz() {
        nfcHelper = new NfcHelper(this);
        nfcHelper.onCreate();
        nfcHelper.setOnlistenerNfc(listener);
        nfcHelper.startReadNFCNoShowDialog();

    }


    private NfcHelper.NfcHelperListener listener = new NfcHelper.NfcHelperListener() {
        @Override
        public void nfcIsDisable() {
            nfcSupportedStatus = NFC_DISABLE;

            Timber.tag(TAG).e("nfcIsDisable");

        }

        @Override
        public void nfcIsEnable() {
            nfcSupportedStatus = NFC_AVAILABLE;
            Timber.tag(TAG).e("nfcIsEnable");

        }

        @Override
        public void nfcIsNotSupport() {
            nfcSupportedStatus = NFC_NOT_SUPPORT;
            Timber.tag(TAG).e("nfcIsNotSupport");


        }

        @Override
        public void nfcTurnOn() {
            nfcSupportedStatus = NFC_AVAILABLE;
            Timber.tag(TAG).e("nfcTurnOn");


        }

        @Override
        public void nfcTurnOff() {
            nfcSupportedStatus = NFC_DISABLE;
            Timber.tag(TAG).e("nfcTurnOff");


        }

        @Override
        public void nfcReadModeFail(int i) {
            Timber.tag(TAG).e("nfcReadModeFail: " + i);
            if (i == Constant.ERROR_READ_TAG_NO_MESSAGE) {
                RB_Error_MSg(getResources().getString(R.string.card_not_value));

            }
//            RB_Error_MSg(getResources().getString(R.string.card_not_in_parking));


        }

        @Override
        public void nfcWriteModeFail(int i) {
            Toast.makeText(HomeActivity.this, "The nfc is" + i, Toast.LENGTH_SHORT).show();
            Timber.tag(TAG).e("nfcWriteModeFail: " + i);
//            RB_Error_MSg(getResources().getString(R.string.card_not_in_parking));

        }

        @Override
        public void nfcReadModeReadMessage(String link) {
            deviceid = link;
            //todo got content
            Timber.tag(TAG).e("nfcReadModeReadMessage linkz: \t " + link);
//            mRxCameraController21.takePhoto();

            presenter.checkTagOnline(link);


        }

        @Override
        public void nfcWriteModeFinished(String s) {
            Timber.tag(TAG).e("nfcWriteModeFinished:" + s);

        }
    };


    /************************************
     *         END  NFC AREA             *
     *                                   *
     * **********************************/

    /*******************************************
     *  Start CALL BACK OF CAMERA COTROLLER    *
     *                                         *
     * ******************************************/
    private final CameraController.Callback mRxCamerController21Callback = new CameraController.Callback() {
        @Override
        public void onFocusStarted() {
            mFocusIndicator.setVisibility(View.VISIBLE);
            mFocusIndicator.setScaleX(1f);
            mFocusIndicator.setScaleY(1f);
            mFocusIndicator.animate().scaleX(2f).scaleY(2f).setDuration(500).start();
        }

        @Override
        public void onFocusFinished() {
            mFocusIndicator.setVisibility(View.GONE);
        }

        @Override
        public void onPhotoTaken(@NonNull String photoUrl, @NonNull Integer photoSourceType) {
//
            replaceFragment(CheckinFragment.getInstance(deviceid, photoUrl), CheckinFragment.TAG);
        }

        @Override
        public void onCameraAccessException() {
            setResult(Activity.RESULT_CANCELED);
            finish();
        }

        @Override
        public void onCameraOpenException(@Nullable OpenCameraException.Reason reason) {
            setResult(Activity.RESULT_CANCELED);
            finish();
        }

        @Override
        public void onException(Throwable throwable) {
            throwable.printStackTrace();
            setResult(Activity.RESULT_CANCELED);
            finish();
        }
    };
    /*******************************************
     *  END CALL BACK OF CAMERA COTROLLER      *
     *                                         *
     * ****************************************/

    /*******************************************
     *  Start Fragment navigator controll      *
     *                                         *
     * ****************************************/
    public void addFragment(Fragment fragment, String tag) {
        if (fm == null) {
            fm = getFragmentManager();

        }


        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.ct, fragment, tag).commit();
    }


    public void hideFragment(int resId) {
        if (fm == null) {
            return;
        } else {

            fm.beginTransaction().setCustomAnimations(R.animator.slide_up, R.animator.slide_down, R.animator.slide_up, R.animator.slide_down).remove(fm.findFragmentById(resId)).commit();
        }

    }


    @SuppressLint("ResourceType")
    private void replaceFragment(Fragment fragment, String tag) {
        if (fm == null) {
            fm = getFragmentManager();
            Timber.tag(TAG).e("replaceFragment null");

        }
        Timber.tag(TAG).e("replaceFragment Ok");
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.slide_up, R.animator.slide_down, R.animator.slide_up, R.animator.slide_down);
        fragmentTransaction.replace(R.id.ct, fragment, tag).commit();
    }

    /*******************************************
     *     END Fragment navigator controll     *
     *                                         *
     * ****************************************/
    /************************************
     *        START LIFECYELE AREA      *
     *                                   *
     * **********************************/
    @Override
    protected void onStart() {
        super.onStart();
        Timber.tag(TAG).e("onStart");
        mRxCameraController21.getLifecycle().onStart();
        mRxCameraController21.getLifecycle().onResume();
        presenter.setNameInLeftSidebar();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.tag(TAG).e("onResume");
//        mRFIDController.startScan();
//        registerListner();
        nfcHelper.onResume(getClass());


    }

    @Override
    protected void onPause() {
        super.onPause();
        Timber.tag(TAG).e("onPause");
//        mRFIDController.stopScan();
//        unregister();

        nfcHelper.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Timber.tag(TAG).e("onStop");

        mRxCameraController21.getLifecycle().onStop();
        mRxCameraController21.getLifecycle().onPause();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mRxCameraController21.getLifecycle().onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        presenter.dropView();
        mRxCameraController21.getLifecycle().onDestroy();
        nfcHelper.stopReadNFCNoShowDialog();

    }

    boolean isNFCDetected;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Timber.tag(TAG).e("on new intent");
        isNFCDetected = true;
        nfcHelper.onNewIntent(intent);
    }

    /************************************
     *   END  LIFECYCLE AREA             *
     *                                   *
     * **********************************/


    /************************************
     *      START  RFID AREA             *
     *                                   *
     * **********************************/
//    @Inject
//    RFIDModule.Controller mRFIDController;

//    private void registerListner() {
//        mRFIDController.setListener(new RFIDModule.Listener() {
//            @Override
//            public void onNewDevice(String cardId) {
//                deviceid = MyUtils.formatEpc(cardId);
//
//                presenter.checkTag(deviceid);
//                //todo chup hinh
////                new Handler().postDelayed(new Runnable() {
////                    @Override
////                    public void run() {
////                        mRxCameraController21.takePhoto();
////
////                    }
////                }, 3000);
//
//
//            }
//
//            @Override
//            public void onConnect() {
//                Timber.tag(TAG).e("onConnect");
//            }
//
//            @Override
//            public void onDisconnect() {
//                Timber.tag(TAG).e("ondisconnect");
//            }
//
//            @Override
//            public void onChangeStatus(RFIDStatus status) {
//                Timber.tag(TAG).e("status %s", status);
//            }
//
//            @Override
//            public void onStopScanSuccess() {
//                Timber.tag(TAG).e("onStopScan");
//            }
//
//            @Override
//            public void onStartScanSuccess() {
//                Timber.tag(TAG).e("start scan success");
//            }
//
//            @Override
//            public void onStartScanFailed() {
//                Timber.tag(TAG).e("start scan failed");
//            }
//        });
//    }
//
//    private void unregister() {
//        mRFIDController.setListener(null);
//    }
    /************************************
     *      END  RFID AREA             *
     *                                   *
     * **********************************/

//
//    @Override
//    public void startCamera() {
//        mRxCameraController21.getLifecycle().onStart();
//    }

    /****************************************************
     *    START COMUNICATE FRAGMENT AND ACTIVITY         *
     *                                                  *
     * **************************************************/


    @Override
    public void onCancelCheckOutButton() {
        hideFragment(R.id.ct);
    }

    @Override
    public void onCancelCheckInButton() {
        hideFragment(R.id.ct);

    }

    @Override
    public void onCheckinFaled(String msg) {
        alertError(" ko thanh Cong", SweetAlertDialog.SUCCESS_TYPE, "ko nice");

    }

    @Override
    public void onCheckinSuccessV2(String localPathImage, String targetPathImage) {
        hideFragment(R.id.ct);
    }

    @Override
    public void onCheckinSuccess() {
        hideFragment(R.id.ct);
        setAppTitle(getResources().getString(R.string.my_app_name));
//        alertError("thanh Cong", SweetAlertDialog.SUCCESS_TYPE, "nice");
        customToast("Xe vào thành công");

    }

    @Override
    public void onCheckOutSuccess() {
        setAppTitle(getResources().getString(R.string.my_app_name));

        hideFragment(R.id.ct);
//        alertError("Checkout", SweetAlertDialog.SUCCESS_TYPE, "Checkout thanh cong");
        customToast("Xe ra thành công");

    }

    @Override
    public void onCheckoutFaled() {
        alertError("Checkout", SweetAlertDialog.ERROR_TYPE, "Checkout ko thanh cong");
        hideFragment(R.id.ct);


    }

    @Override
    public void onCheckOutSuccessV2(String localpath, String onlineTarget) {
        hideFragment(R.id.ct);


    }

    @Override
    public void onConfirmCheckInButton() {
        setAppTitle(getResources().getString(R.string.my_app_name));
        Timber.tag(TAG).e("onConfirmCheckInButton");

    }

    /***************************************************
     *    END  COMUNICATE FRAGMENT AND ACTIVITY       *
     *                                                  *
     * **************************************************/


    /**
     * Fake button to call functional
     */

    public void onBtnFakeCheckOut(View view) {
        replaceFragment(CheckoutFragment.getInstance(deviceid, "fakePatchOut", null), CheckoutFragment.TAG);

    }

    public void onBtnFakeCapture(View view) {
        mRxCameraController21.takePhoto();
    }

    public void onBtnFakeCheckin(View view) {

        deviceid = MyUtils.formatEpc(String.valueOf(Calendar.getInstance().getTimeInMillis()));
        replaceFragment(CheckinFragment.getInstance(deviceid, "fakePatchIn"), CheckinFragment.TAG);
    }


    /***************************************************
     *    VIEW CALLBACK       *
     *                                                  *
     * **************************************************/

    @Override

    public void openCheckIn(String id) {

        setAppTitle(getResources().getString(R.string.bike_in));
        Timber.tag(TAG).e("openCheckIn: bat dau chup hinh");

        Timber.tag(TAG + "timing").e(" \tSTART-CAPTURE------------ \t\t\t\t\t\t %s %s", TimeUtils.get_HH_MM_SS(), "-----start----");
        mRxCameraController21.takePhoto();
    }

    @Override
    public void openCheckOut(String id) {
        Timber.tag(TAG).e("openCheckOut()");
        setAppTitle(getResources().getString(R.string.bike_out));
        replaceFragment(CheckoutFragment.getInstance(deviceid, "1213", null), CheckoutFragment.TAG);

    }

    @Override
    public void onCardNotValid(String msg) {
        RB_Error_MSg(msg);
    }

    @Override
    public void onSignoutSuccess() {
        this.finish();
        navigator.startActivity(HomeActivity.this, LoginActivity.class
                , null);
    }

    @Override
    public void onSignOutFalied(String msg) {
        RB_Error_MSg(msg);
    }

    @Override
    public void openOnlineCheckout(String cardid, FCurrentRecord record) {
        addFragment(CheckoutFragment.getInstance(deviceid, "null", record), CheckoutFragment.TAG);
    }

    @Override
    public void setNameLeftSidebar(EzParkManager userManager) {
        if (userManager.getEmployee().getName() == null ||
                userManager.getEmployee().getDescription() == null) {
            // kemeno
            return;
        }
        leftname.setText(userManager.getEmployee().getName());
        leftDescription.setText(userManager.getEmployee().getDescription());
    }


    private void initializingInjection() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build().inject(this);
    }

    private void setUpDrawer() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_close, R.string.drawer_open);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        actionBarDrawerToggle.syncState();

        // menu listener
        navigationView.setNavigationItemSelectedListener(itemSelectedListener);

        // findview view of header
        View header = navigationView.getHeaderView(0);
        leftDescription = header.findViewById(R.id.txtNavAdress);
        leftname = header.findViewById(R.id.txtNavName);

    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        setAppTitle(getString(R.string.my_app_name));

    }


    private void setAppTitle(String title) {
        try {
            getSupportActionBar().setTitle("");

        } catch (Exception e) {
            Timber.tag(TAG).e(e.getMessage());
        }
    }

    private void findViews() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

    }


    int a = 0;

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        a++;
        if (a == 2) {
            this.finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);
        } else {
            customToast(" Bấm thêm lần nữa để thoát ứng dụng");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    a = 0;
                }
            }, 2000);
        }

    }


    @Override
    public void showOnlineView() {
        Timber.tag(TAG).e("onOnline-status-huuhoang-on");
        try {
            offlineNoteContainer.setVisibility(View.INVISIBLE);

        } catch (Exception e) {
            Timber.tag(TAG).e("showOnlineView %s", e.getMessage());

        }

    }

    @Override
    public void showOfflineView() {
        Timber.tag(TAG).e("onOnline-status-huuhoang-of");

        try {
            offlineNoteContainer.setVisibility(View.VISIBLE);
            RB_Warning_MSg(getString(R.string.offline_mode_notice));
        } catch (Exception e) {
            Timber.tag(TAG).e("showOfflineView %s", e.getMessage());

        }
    }


    @OnClick(R.id.offlineNoteContainer)
    public void onNoticeLick() {
        RB_Warning_MSg(getString(R.string.internet_benefit));
    }

    @OnClick(R.id.btnCloseNotice)
    public void closeOfflineNotice() {
        offlineNoteContainer.setVisibility(View.GONE);
    }
}