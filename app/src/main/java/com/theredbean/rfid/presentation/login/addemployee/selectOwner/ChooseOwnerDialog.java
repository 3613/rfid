package com.theredbean.rfid.presentation.login.addemployee.selectOwner;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.login.ownerlist.viewholder.PickOwnerAdapter;
import com.theredbean.rfid.presentation.login.addemployee.selectOwner.viewholder.ListOwnerAdapter;
import com.theredbean.rfid.presentation.login.addemployee.selectOwner.viewholder.ListOwnerViewHolder;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by hoang on 28/03/2018 nhe.
 */

public class ChooseOwnerDialog extends DialogFragment implements
        ChooseOwnerContract.DialogView, ListOwnerViewHolder.OnOwnerListener {
    @Inject
    ChooseOwnerContract.Presenter presenter;

    // rv

    List<Owner> mdata;
    ListOwnerAdapter ownerListAdapter;

    PickOwnerAdapter adapter;
    @BindView(R.id.rvOwner)
    RecyclerView rvOwner;
//    rv

    Listener mListener;


    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        null.unbind();
        presenter.dropView();
    }

    @Override
    public void displayTest(String msg) {
        Timber.tag(TAG).e("testedzzzzzzzzzzzzzz");

    }

    @Override
    public void onGetlistOwnerSuccess(List<Owner> ownerList) {
        mdata = ownerList;
        setupRV2();
    }

    private void setupRV2() {
        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        rvOwner.setLayoutManager(lm);
        rvOwner.setHasFixedSize(true);
        ownerListAdapter = new ListOwnerAdapter(getActivity(), this);
        ownerListAdapter.setData(mdata);
        rvOwner.setAdapter(ownerListAdapter);
    }

    @Override
    public void onGetlistOwnerFalied() {

    }

    @Override
    public void showLoading() {
        Timber.tag(TAG).e("show loading");

    }

    @Override
    public void hideLoading() {
        Timber.tag(TAG).e("show hideLoading ");

    }

    @Override
    public void onItemClick(int position) {
        Timber.tag(TAG).e("clicked zzzzzzzzzzzzzzzzzzzzzzz %s \t %s", mdata.get(position).getUuid(), mdata.get(position).getName());
        Owner owner = mdata.get(position);
        mListener.onOwnerSelected(mdata.get(position).getUuid(), mdata.get(position).getName());
    }

    @Override
    public void onItemLongClick(int position) {

    }

    public interface Listener {
        void onOwnerSelected(String UU_id, String ownwerName);
    }


    public static final String TAG = ChooseOwnerDialog.class.getSimpleName();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_select_owner, container, true);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        presenter = new ChooseOwnerPrenter()
        setupDI();
        presenter.takeView(this);
        presenter.getListOwner();


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Objects.requireNonNull(getDialog().getWindow()).getAttributes().windowAnimations = R.style.Dialog_NoTitle;
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();

        if (d != null) {
            Objects.requireNonNull(d.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (Listener) getTargetFragment();
        } catch (ClassCastException e) {
            Log.e(TAG, "onAttach: ClassCastException : " + e.getMessage());
        }
    }

    public void setupDI() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build()
                .inject(this);
    }

}
