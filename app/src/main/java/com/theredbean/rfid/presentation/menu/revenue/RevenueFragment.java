package com.theredbean.rfid.presentation.menu.revenue;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.theredbean.rfid.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.dialog.DialogPickTtime;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;
import utils.EzConstants;
import utils.MyUtils;
import utils.TimeUtils;

import static utils.EzConstants.PRICE_C_XE_DAP_D;

public class RevenueFragment extends BaseFragment
        implements RevenueContract.View
        , DialogPickTtime.Listener {
    public static final String TAG = RevenueFragment.class.getSimpleName();
    Context context;
    @Inject
    RevenueContract.Presenter presenter;

    @BindView(R.id.txtTotalSo)
    TextView txtTotalSo;
    @BindView(R.id.txtTotalDap)
    TextView txtTotalDap;
    @BindView(R.id.txtTotaGa)
    TextView txtTotalGa;

    @BindView(R.id.txtSumMoney)
    TextView txtSumMoney;
    @BindView(R.id.txtPriceDap)
    TextView txtPriceDap;
    @BindView(R.id.txtPriceGa)
    TextView txtPriceGa;
    @BindView(R.id.txtPriceSo)
    TextView txtPriceSo;
    @BindView(R.id.txtNameDap)
    TextView txtNameDap;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDependency();
        setHasOptionsMenu(true);
//        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);


    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
        presenter.getListParkedByDuration(TimeUtils.plusOneDay(), TimeUtils.getCurrentInMiliseconds());

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showAlertDialog(String msg) {

    }

    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_revenue;
    }

    @Override
    protected void onMyCreateView(View view) {
    }

    private void initDependency() {
        DaggerEzParkingComponent.builder().applicationComponent(((BaseActivity) getActivity()).getApplicationComponent()).ezParkingModule(new EzParkingModule()).build().inject(this);
    }


    @Override
    public void onStart() {
        super.onStart();
        presenter.takeView(this);

    }

    @Override
    public void onGetListSuccess(String xega, String xeso, String xedap, String doanhthu) {


        txtTotalGa.setText(xega + " chiếc ");
        txtTotalDap.setText(xedap + " chiếc ");
        txtTotalSo.setText(xeso + " chiếc ");

//        txtNameDap.setText("Xe đạp \n " + MyUtils.fomatVND(PRICE_C_XE_DAP_D));


        txtPriceDap.setText(MyUtils.fomatVND(PRICE_C_XE_DAP_D));
        txtPriceGa.setText(MyUtils.fomatVND(EzConstants.PRICE_A_XE_GA_D));
        txtPriceSo.setText(MyUtils.fomatVND(EzConstants.PRICE_B_XE_SO_D));


        String html = "<font color='#ffffff'> Tổng Doanh Thu: </font> " + "<font color='#ffc802'>" + doanhthu + "</font>";
        String vcl = "  <font color='red'>Vào:: &nbsp; </font>";
        txtSumMoney.setText(Html.fromHtml(html));

        Timber.tag(TAG).e("set View OK");
        RB_hideDialog();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.mnFilter) {
            showSelectTimeDialog();
        }
        ;
        return super.onOptionsItemSelected(item);

    }

    private void showSelectTimeDialog() {
        DialogPickTtime dialogPickTtime = new DialogPickTtime();
        dialogPickTtime.setTargetFragment(RevenueFragment.this, 1);
        dialogPickTtime.show(getFragmentManager(), RevenueFragment.TAG);
    }

    public void onLoadData(long from, long to) {
        alertErrors(getString(R.string.loading), "", SweetAlertDialog.PROGRESS_TYPE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter.getListParkedByDuration(from, to);


            }
        }, 1000);

    }

    @Override
    public void onSelected(long howManyTime) {
        onLoadData(howManyTime, TimeUtils.getCurrentInMiliseconds());
        Timber.tag(TAG).e("onSelected : " + howManyTime);
    }

    static RevenueFragment revenueFragment;

    public static Fragment getInstance() {
        if (revenueFragment == null) {
            revenueFragment = new RevenueFragment();
        }
        return revenueFragment;

    }
}
