package com.theredbean.rfid.presentation.login.register;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.theredbean.rfid.R;
import com.theredbean.rfid.business.register.RegisterUsecase;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class RegisterOwnerFragment extends BaseFragment implements RegisterOwnerContract.View {
    public static final String TAG = RegisterOwnerFragment.class.getSimpleName();
    FirebaseAuth mAuth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference referenceRoot, ownerRef;

    RegisterUsecase registerUsecase;

    @Inject
    RegisterOwnerContract.Presenter presenter;

    RegisterListener mlistenner;
    Context context;

    @Override
    public void onRegisterSuccess() {
        mlistenner.showToast("dang ky thang cong");
        RB_Succeess_MSg("dang ky thang cong");

    }

    @Override
    public void onRegisterFalied(String msg) {
        mlistenner.showToast(msg);


    }

    @Override
    public void openLoginFragment() {
        onbtnGotoLogin();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInjection();
    }

    public interface RegisterListener {
        void onGotoLogin();

        void onGotoLostPass();

        void showToast(String msg);
    }

    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof RegisterListener) {
            mlistenner = (RegisterListener) context;
        } else {
            throw new ClassCastException(getActivity().getLocalClassName() + "is not cast RegisterListener listener");
        }
    }

    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtdescription)
    EditText edtDescription;
    @BindView(R.id.btnRegister)
    View btnLogin;
    @BindView(R.id.edtpass)
    EditText edtpass;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.pbLoading)
    ProgressBar pbloading;


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_register_owner;
    }

    @Override
    protected void onMyCreateView(View view) {

//        mAuth = FirebaseAuth.getInstanceLoadingDrawable();
//        firebaseDatabase = FirebaseDatabase.getInstanceLoadingDrawable();
//        referenceRoot = firebaseDatabase.getReference();
//        ownerRef = referenceRoot.child(FirebaseConstant.KEY_OWNER);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            presenter.takeView(this);
        } catch (Exception e) {
            Timber.tag(TAG).e("takeview sai" + e.toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            presenter.takeView(this);
        } catch (Exception e) {
            Timber.tag(TAG).e("takeview sai" + e.toString());
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        try {
            presenter.takeView(this);
        } catch (Exception e) {
            Timber.tag(TAG).e("takeview sai" + e.toString());
        }

    }

    @OnClick(R.id.btnRegister)
    public void register() {
        /**
         * String user, String pass,String name,String description
         */
        String uerEmail = edtEmail.getText().toString().trim();
        String password = edtpass.getText().toString().trim();
        String description = edtDescription.getText().toString().trim();
        String name = edtName.getText().toString().trim();

        if (TextUtils.isEmpty(uerEmail) || TextUtils.isEmpty(password)
                || TextUtils.isEmpty(description) || TextUtils.isEmpty(name)) {
            RB_Warning_MSg(" Không thể bỏ trống");
            return;
        }

        pbloading.setVisibility(View.VISIBLE);
        Timber.tag(TAG).e("btn register: %s, %s ,%s, %s ", uerEmail, password, name, description);
        presenter.register(uerEmail, password, name, description);

    }


    @OnClick(R.id.btnLostPass)
    public void gotoLostPass() {
        mlistenner.onGotoLostPass();
        ;
    }

    @OnClick(R.id.btnGotoLogin)
    public void onbtnGotoLogin() {
        mlistenner.onGotoLogin();
        ;
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            alertErrors(getString(R.string.success), "dang ky thanh cong", SweetAlertDialog.SUCCESS_TYPE);
        }
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("Đang đăng ký");
    }

    @Override
    public void showLoading(String msg) {


    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
        Timber.tag(TAG).e("hideLoading");
        pbloading.setVisibility(View.GONE);

    }

    @Override
    public void showAlertDialog(String msg) {

    }


    public static RegisterOwnerFragment getInstance() {
//        Bundle data = new Bundle();
//        data.putString("DEVICE_ID", deviceId);
//        data.putString("PATH", path);
//        fragment.setArguments(data);
        return new RegisterOwnerFragment();
    }

    private void initInjection() {
        DaggerEzParkingComponent.builder().applicationComponent(((BaseActivity) getActivity()).getApplicationComponent()).ezParkingModule(new EzParkingModule()).build().inject(this);
    }
}
