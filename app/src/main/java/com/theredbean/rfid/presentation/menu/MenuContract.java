package com.theredbean.rfid.presentation.menu;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface MenuContract {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter<MenuContract.View> {

    }
}
