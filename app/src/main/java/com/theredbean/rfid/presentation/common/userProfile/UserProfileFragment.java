package com.theredbean.rfid.presentation.common.userProfile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;

import javax.inject.Inject;

public class UserProfileFragment extends BaseFragment implements UserProfileContract.View {

    public static final String TAG = UserProfileFragment.class.getSimpleName();


    @Inject
    UserProfileContract.Presenter presenter;

    Context context;

    Mylistener mLMylistener;


    /*view implementation*/
    @Override
    public void onLoadCurrentProfileSuccess() {

    }

    @Override
    public void onLoadCurrentProfileFailed(String whatHappend) {

    }

    @Override
    public void onUpdateProfileSuccess() {

    }

    @Override
    public void onUpdateProfileFailed(String whatHappend) {

    }

    /*view implementation*/


    interface Mylistener {

    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_user_profile;
    }


    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof Mylistener) {
            mLMylistener = (Mylistener) context;
        } else {
            throw new ClassCastException(TAG + " -- can not cast Mylistener interface");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInjection();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    protected void onMyCreateView(View view) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.dropView();
    }

    @Override
    public void showMessage(String message) {
        RB_Succeess_MSg(message);
    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {
        RB_Loading_MSg(msg);

    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }

    @Override
    public void showAlertDialog(String msg) {
        RB_Warning_MSg(msg);
    }

    private void initInjection() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build()
                .inject(this);

    }
}
