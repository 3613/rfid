package com.theredbean.rfid.presentation.menu.addParking;


import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.model.OwnerModel;
import com.theredbean.rfid.business.parking.ParkingUsercase;

import javax.inject.Inject;

import timber.log.Timber;

public class AddParkingPresenter implements AddParkingContract.Presenter {


    AddParkingContract.View mView;
    ParkingUsercase parkingUsercase;
    EzParkManager userManager;

    @Inject
    public AddParkingPresenter(ParkingUsercase parkingUsercase, EzParkManager userManager) {
        this.parkingUsercase = parkingUsercase;
        this.userManager = userManager;
    }

    public AddParkingPresenter() {
    }


    @Override
    public void takeView(AddParkingContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void addParking(String name, String address, String description) {
        mView.showLoading();
        parkingUsercase.addParking(name, address, description, new EzParkingCallback.AddParkingCallback() {


            @Override
            public void onAddParkingSuccess(OwnerModel map, String newAutoGeneratedKey) {
                Timber.tag(TAG).e("onAddParkingSuccess(OwnerModel map, String newAutoGeneratedKey");

                mView.hideLoading();
                userManager.setParks(map.getParks());
                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // becau we call onother instance of SweeetAlert dialog,
                        // I need to close that instance after create new
                        mView.onAddparkingSuccess(newAutoGeneratedKey);

                    }
                }, 500);
            }

            @Override
            public void onAddParingFailed(String msg) {
                mView.hideLoading();
                mView.onAddParkingFalied(msg);
            }


            @Override
            public void onExceptionError(String msg) {
                mView.hideLoading();
                mView.onAddParkingFalied(msg);


            }

            @Override
            public void onExceptionAuthentication() {
                mView.hideLoading();
                mView.onAddParkingFalied("onExceptionAuthentication");

            }
        });
    }

    public static final String TAG = AddParkingPresenter.class.getSimpleName();

}
