package com.theredbean.rfid.presentation.menu.revenue;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface RevenueContract {

    interface View extends BaseView {
        void onGetListSuccess(String xega, String xeso, String xedap, String doanhthu);
    }

    interface Presenter extends BasePresenter<RevenueContract.View> {
        void getListParkedByDuration(long from, long to);
    }
}
