package com.theredbean.rfid.presentation.menu.parkinglist.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.theredbean.rfid.R;
import com.theredbean.rfid.models.Parking;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ParkingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
        View.OnLongClickListener {
    public static final String TAG = ParkingViewHolder.class.getSimpleName();

    private int position;
    private Parking item;

    @BindView(R.id.txParkingName)
    TextView txParkingName;
    @BindView(R.id.txtParkingAdress)
    TextView txtParkingAdress;
    TextView txtUser, txtDes;
    @BindView(R.id.ivMarked)
    ImageView imageView;


    private OnParkingViewHolderListener mListener;

    public void setSticker(boolean isSticker) {
        imageView.setVisibility(isSticker ? View.VISIBLE : View.INVISIBLE);
    }

    public interface OnParkingViewHolderListener {
        void onClick(int position);

        void onLongClick(int position);
    }

    public ParkingViewHolder(View view, OnParkingViewHolderListener listener) {
        super(view);
        ButterKnife.bind(this, view);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        mListener = listener;

    }

    public void renderUi(int position, Parking entity) {
        this.position = position;

        txParkingName.setText(entity.getName());
        txtParkingAdress.setText(entity.getAddress());
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onClick(position);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mListener != null) {
            mListener.onLongClick(position);
        }
        return false;
    }
}