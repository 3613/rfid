package com.theredbean.rfid.presentation.login.newlogin;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface RegisterBaseContract {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter<RegisterBaseContract.View> {

    }
}
