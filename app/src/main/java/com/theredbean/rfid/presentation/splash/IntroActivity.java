package com.theredbean.rfid.presentation.splash;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerDatabaseComponent;
import com.theredbean.rfid.di.modules.DatabaseModule;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.Navigator;
import com.theredbean.rfid.presentation.firstconfig.FirstConfigActivity;
import com.theredbean.rfid.presentation.login.LoginActivity;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

public class IntroActivity extends BaseActivity implements IntroContract.View, ActivityCompat.OnRequestPermissionsResultCallback {
    @Inject
    IntroContract.Presenter presenter;
    @Inject
    Navigator navigator;
    @BindView(R.id.txtSettingNote)
    TextView txtSettingNote;
    @BindView(R.id.btnSetting)
    View btnSetting;

    private static final int RC_CAMERA_PERM = 123;
    private static final int RC_STORAGE_PERM = 921;
    public static final String TAG = IntroActivity.class.getSimpleName();


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        initzInjection();
        presenter.takeView(this);
        Timber.tag(TAG).e("onCreate");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checame();
            }
        }, 1500);

    }

    private void initzInjection() {
        DaggerDatabaseComponent.builder().applicationComponent(getApplicationComponent()).databaseModule(new DatabaseModule()).build().inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.takeView(this);
        Timber.tag(TAG).e("onResume");



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.intro_activity;
    }

    @Override
    public void showMessage(String message) {
        RB_Warning_MSg(message);
    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {
        RB_Loading_MSg(msg);

    }

    @Override
    public void hideLoading() {
        dissmissSweetDialog();
    }

    @Override
    public void onLoadConfigSuccess() {
        Timber.tag(TAG).e("onLoadConfigSuccess ------->>");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
        navigator.startActivity2(IntroActivity.this, LoginActivity.class    );
            }
        },2000);
    }

    @Override
    public void onLoadConFigFailed() {
        Timber.tag(TAG).e("onLoadConFigFaailediled: falied:");
        txtSettingNote.setVisibility(View.VISIBLE);
        btnSetting.setVisibility(View.VISIBLE);

    }


    public void gotoFirstConfig(View view) {
        navigator.startActivity(IntroActivity.this, FirstConfigActivity.class, null);
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);

    }

    // check funcion
    public void checame() {
        if (ContextCompat.checkSelfPermission(IntroActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            Timber.tag(TAG).e("            // Permission checame is not granted\n");
            ActivityCompat.
                    requestPermissions(IntroActivity.this, new String[]{Manifest.permission.CAMERA}, RC_CAMERA_PERM);


        } else {
            checkRead();
        }


    }


    public void checkRead() {
        if (ContextCompat.checkSelfPermission(IntroActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Timber.tag(TAG).e("            // Permission checkRead is not granted\n");
            ActivityCompat.
                    requestPermissions(IntroActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, RC_STORAGE_PERM);


        } else {
            checkWrite();
        }
    }

    public void checkWrite() {
        if (ContextCompat.checkSelfPermission(IntroActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Timber.tag(TAG).e("            // Permission checkWrite is not granted\n");
            ActivityCompat.
                    requestPermissions(IntroActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_STORAGE_PERM);


        } else {
            Timber.tag(TAG).e("passmiesion ok");
            presenter.loadConfig();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RC_CAMERA_PERM: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Timber.tag(TAG).e("RC_CAMERA_PERM granted");
                    checkRead();
                } else {
                    Timber.tag(TAG).e("RC_CAMERA_PERM denied");

                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            case RC_STORAGE_PERM: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Timber.tag(TAG).e("RC_STORAGE_PERM granted");
                    checkWrite();
                } else {
                    Timber.tag(TAG).e("RC_STORAGE_PERM denied");
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }


}
