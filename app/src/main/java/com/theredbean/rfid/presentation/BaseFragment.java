package com.theredbean.rfid.presentation;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import utils.AlertDialogListener;

/**
 * Created by hnam on 8/2/2016.
 */
public abstract class BaseFragment extends Fragment {
    public static final String TAG = BaseFragment.class.getSimpleName();
    public static final int DELAY = 250; //ms
    private Unbinder mUnbinder;

    @TargetApi(23)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onAttachToContext(context);
    }

    /**
     * deprecated on API 23
     * use onAttachToContext instead
     *
     * @param activity
     */
    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < 23) {
            onAttachToContext(activity);
        }
    }

    protected abstract void onAttachToContext(Context context);

    protected abstract int getLayoutId();

    protected abstract void onMyCreateView(View view);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        mUnbinder = ButterKnife.bind(this, view);
        onMyCreateView(view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    public void showFWarningMessage(String message) {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).showToastMessage(message);
        }
    }

    public void showFLoadingView(String msg) {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).showLoadingDialog(msg);
        }
    }

    public void hideFLoadingView() {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).hideLoadingDialog();
        }
    }

    public void showFWarningDialog(String msg) {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).showAlertDialog(msg);
        }
    }

    public void alertErrors(String title, String msg, int type) {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).alertError(msg, type, title);
        }
    }

    public void RB_hideDialog() {
        Activity act = getActivity();

        if (act instanceof BaseActivity) {
            ((BaseActivity) act).dissmissSweetDialog();

        }
    }
    // theredbean


    public void RB_Succeess_MSg(String msg) {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).RB_Succeess_MSg(msg);
        }
    }

    public void RB_Error_MSg(String msg) {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).RB_Error_MSg(msg);
        }
    }

    public void RB_Loading_MSg(String msg) {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).RB_Loading_MSg(msg);
        }
    }

    public void RB_Warning_MSg(String msg) {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).RB_Warning_MSg(msg);
        }
    }

    public void RB_Confirm_Dialog(String title,
                                  String contentText,
                                  String cancelText,
                                  String confirmtext,
                                  AlertDialogListener alertListener) {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).RB_Confirm_Dialog(title, contentText, cancelText, confirmtext, alertListener);
        }
    }
}
