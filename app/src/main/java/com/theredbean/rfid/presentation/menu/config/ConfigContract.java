package com.theredbean.rfid.presentation.menu.config;

import com.theredbean.rfid.models.Pricez.FPrice;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface ConfigContract {

    interface View extends BaseView {

        void onWriteSuccess();

        void onWritepriceFalied(String msg);

        void showAlertEditextEmpty(String msg);

        void showLoadConfigSuccess(FPrice fPrice);
    }

    interface Presenter extends BasePresenter<ConfigContract.View> {

        void savePriceConfig(String parkingKey, String a, String b, String c, String d, String e, String f);

        void loadConfig(String parkingKey);
    }
}
