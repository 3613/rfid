package com.theredbean.rfid.presentation.login.addemployee.selectOwner;

import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.presentation.BasePresenter;

import java.util.List;

public interface ChooseOwnerContract {

    interface DialogView {
        void displayTest(String msg);
        void onGetlistOwnerSuccess(List<Owner> ownerList);

        void onGetlistOwnerFalied();

        void showLoading();

        void hideLoading();
    }

    interface Presenter extends BasePresenter<ChooseOwnerContract.DialogView> {
        void testhaha();
        void getListOwner();
    }
}
