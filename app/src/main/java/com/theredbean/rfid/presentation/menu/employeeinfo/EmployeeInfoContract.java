package com.theredbean.rfid.presentation.menu.employeeinfo;

import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Parking;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

import java.util.List;

public interface EmployeeInfoContract {

    interface View extends BaseView {
        void onGetlistParkingSuccess(List<Parking> parkings);

        void ongetListFailed(String msg);

        void displayUpdateParkingForEmployeeSuccess();

        void onUpdateParkingForEmployeeFailed();
    }

    interface Presenter extends BasePresenter<EmployeeInfoContract.View> {
        void getListEmployeee();

        void updateParkingForEmployee(Employee employee, Parking parking);
    }
}
