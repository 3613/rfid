package com.theredbean.rfid.presentation.employee.camera.fragmentcheckout;

import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.checkcard.CheckCardUsecase;
import com.theredbean.rfid.business.income.IncomeUsecase;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.data.db.record.CurrentRecord;
import com.theredbean.rfid.models.FCurrentRecord;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import timber.log.Timber;
import utils.EzConstants;
import utils.PriceUtils;

/**
 * Created by hoang on 20/03/2018.
 */

public class CheckoutPresenter implements CheckoutContract.Presenter {

    private static final String TAG = CheckoutPresenter.class.getSimpleName();
    CheckoutContract.View view;
    CheckCardUsecase checkCardUsecase;
    IDatabase database;
    private List<CurrentRecord> currentRecords;
    IncomeUsecase incomeUsecase;

    EzParkManager userManager;


    @Inject
    public CheckoutPresenter(@Named("realm") IDatabase database,
                             CheckCardUsecase checkcardUsercase,
                             EzParkManager userManager, IncomeUsecase incomeUsecase) {
        this.database = database;
        this.checkCardUsecase = checkcardUsercase;
        this.userManager = userManager;
        this.incomeUsecase = incomeUsecase;
    }


    @Override
    public void takeView(CheckoutContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        view = null;
    }


    @Override
    public void getCurrentCheckoutRecord(String id) {
//        currentRecords = new ArrayList<>();
//        currentRecords = database.getCurrentRecordByEpc(id);
//        if (currentRecords.size() > 0 && currentRecords.size() >= 1) {
//            CurrentRecord record = currentRecords.get(0);
//            Timber.tag(TAG).e(record.getPath());
//            // todo on ok
//            view.onShowCheckoutDetails(PriceUtils.priceMustPay(record), record.getPath());
//        } else {
//            // todo ok failed
//        }
    }

    @Override
    public void processCheckout(String deviceId) {
        Timber.tag(TAG).e("processCheckout   %s", deviceId);
        int status = database.saveHistoryRecordCopy(deviceId);
        switch (status) {
            case EzConstants.CHECKOUT_FALIED:
                view.onCheckoutFalied();

                break;
            case EzConstants.CHECKOUT_SUCCESS:
                view.onCheckoutSuccess();
                break;

            case 3:
                Timber.tag(TAG).e("processCheckout case equal 3");
                break;
        }
    }

    @Override
    public void getCurrentCheckoutRecordOnline(String card_id, FCurrentRecord fCurrentRecord) {
        Timber.tag(TAG).e("getCurrentCheckoutRecordOnline %s", fCurrentRecord.getPathImage());

    }

    @Override
    public void processCheckOutOnline(String card_id, FCurrentRecord currentRecord) {


        if (userManager.getPrices() == null
                || userManager.getPrices().getXe_dap() == null
                || userManager.getPrices().getXe_ga() == null
                || userManager.getPrices().getXe_so() == null) {
            Timber.tag(TAG).e("Bãi xe bạn làm việc chưa được cấu hình gía");
            view.showMessage("Bãi xe bạn làm việc chưa được cấu hình gía");
            return;
        }


        view.showLoading("outing");
        checkCardUsecase.checkOut(userManager.getPrices(),
                userManager.getEmployee(),
                currentRecord, card_id,
                new EzParkingCallback.CheckOutCardCallback() {
            @Override
            public void onCheckOutCardSuccess() {
                view.hideLoading();
                view.onCheckoutSuccess();
                Timber.tag(TAG).e("my object " + userManager.getEmployee().toString());

//                incomeUsecase.saveFee(userManager.getEmployee(),    currentRecord.getType());
                Timber.tag(TAG).e("onCheckoutSuccess");

            }

            @Override
            public void onCheckOutCardFailed(String msg) {
                view.onCheckoutFalied();
                view.showLoading(msg);
            }

            @Override
            public void onCheckOutCardSuccessV2(String localpath, String onlineTarget) {
                Timber.tag(TAG).e("onCheckOutCardSuccessV2");

                view.hideLoading();
                view.onCheckoutSuccessV2(localpath, onlineTarget);
                incomeUsecase.saveFeeV2(userManager.getEmployee(),currentRecord.getPrice() );
            }

            @Override
            public void onExceptionError(String msg) {
                view.onCheckoutSuccess();
                view.showLoading(msg);
            }

            @Override
            public void onExceptionAuthentication() {
                view.onCheckoutSuccess();
                view.showLoading("onExceptionAuthentication");
            }
        });
    }


}
