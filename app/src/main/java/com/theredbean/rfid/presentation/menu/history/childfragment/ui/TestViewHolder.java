package com.theredbean.rfid.presentation.employee.menu.history.childfragment.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.theredbean.rfid.data.db.record.HistoryRecord;


public class TestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private int position;
    private HistoryRecord item;

    private OnTestViewHolderListener mListener;

    public interface OnTestViewHolderListener {
        void onClick(int position);

        void onLongClick(int position);
    }



    public TestViewHolder(View view, OnTestViewHolderListener listener) {
        super(view);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        mListener = listener;

    }

    public void renderUi(int position, HistoryRecord entity) {
        this.position = position;
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onClick(position);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mListener != null) {
            mListener.onLongClick(position);
        }
        return false;
    }
}