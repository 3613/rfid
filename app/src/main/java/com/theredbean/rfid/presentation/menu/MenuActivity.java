package com.theredbean.rfid.presentation.menu;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Parking;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.Navigator;
import com.theredbean.rfid.presentation.menu.config.ConfigFragment;
import com.theredbean.rfid.presentation.employee.menu.history.FilterFragment;
import com.theredbean.rfid.presentation.employee.menu.history.HistoryFragment;
import com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.TotalViewholder;
import com.theredbean.rfid.presentation.menu.revenue.RevenueFragment;
import com.theredbean.rfid.presentation.menu.addParking.AddParkingFragment;
import com.theredbean.rfid.presentation.menu.employeeinfo.EmployeeInfoFragment;
import com.theredbean.rfid.presentation.menu.employeelist.EmployeeListFragment;
import com.theredbean.rfid.presentation.menu.incomedetail.IncomeDetailFragment;
import com.theredbean.rfid.presentation.menu.parkinglist.ParkingFragment;
import com.theredbean.rfid.presentation.menu.summary.SummaryFragment;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;
import utils.EzConstants;
import utils.FragmentConstants;
import utils.ProgressBarUtils;

import static utils.FragmentConstants.FRAGMENT_ADD_PARKING;
import static utils.FragmentConstants.FRAGMENT_CONFIG;
import static utils.FragmentConstants.FRAGMENT_EMPLOYEE_LIST;
import static utils.FragmentConstants.FRAGMENT_HISTORY;
import static utils.FragmentConstants.FRAGMENT_INCOME_DETAIL;
import static utils.FragmentConstants.FRAGMENT_PARKING_LIST;
import static utils.FragmentConstants.FRAGMENT_REVENUE;
import static utils.FragmentConstants.FRAGMENT_SUMMARY;

public class MenuActivity extends BaseActivity
        implements MenuContract.View,
        TotalViewholder.OnTTViewholderListener,
        FilterFragment.Mylistener,
        HistoryFragment.Mylistener,
        AddParkingFragment.Mylistener,
        ConfigFragment.Mylistener,
        IncomeDetailFragment.Mylistener,
        ParkingFragment.MyListener,
        EmployeeListFragment.EmployeeFragmentListener,
        EmployeeInfoFragment.Mylistener {
    private FragmentManager fm;

    public static final String TAG = MenuActivity.class.getSimpleName();
    @Inject
    MenuContract.Presenter presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    int position = -1;

    @Inject
    Navigator navigator;
    String addedParkingUUID = "x";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initializingInjection();
//
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        // Display icon in the toolbar
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
//        getSupportActionBar().setDisplayUseLogoEnabled(true);


        Bundle extras = getIntent().getExtras();
        int fragTag = -1;
        int position = -1;
        String parkingUid;
        if (extras != null) {
            fragTag = extras.getInt(EzConstants.KEY_BUNDLE_FRAGMENT_TAG);
            position = extras.getInt(EzConstants.KEY_BUNDLE_POSITION_ITEM);
            parkingUid = extras.getString(EzConstants.KEY_BUNDLE_PARKING_UUID, "default");
            this.position = position;
            openFragment(fragTag, position, parkingUid);
            Timber.tag(TAG).e("openFragment %d", fragTag);
            Timber.tag(TAG).e("id id %d", this.position);


//            Toast.makeText(this, (String.valueOf(fragTag)), Toast.LENGTH_SHORT).show();
        } else {
            Timber.tag(TAG).e("nullllllllllllllllllllllllllllllll");

        }


    }

    Fragment fragment = null;

    private void openFragment(int fragTag, int position, String parkingUUID) {

        String title = "";
        fm = getFragmentManager();
        // todo using singleton instead of new

        switch (fragTag) {
            case FRAGMENT_HISTORY:

                Timber.tag(TAG).e("->>>>>>>>>>FRAGMENT_HISTORY");

                fragment = HistoryFragment.getInstance();
                title = getString(R.string.parking_history);
                break;
            case FRAGMENT_SUMMARY:
                fragment = SummaryFragment.getInstance();
                title = getString(R.string.summary);
                break;
            case FRAGMENT_REVENUE:
                fragment = RevenueFragment.getInstance();

                title = getString(R.string.revenuez);
                break;
            case FRAGMENT_CONFIG:
                // todo create singleton
                Timber.tag(TAG).e("FRAGMENT_CONFIG");

                fragment = ConfigFragment.getInstance(position, parkingUUID);
                title = getString(R.string.config);
                break;
            case FRAGMENT_ADD_PARKING:
                Timber.tag(TAG).e("FRAGMENT_ADD_PARKING");
                fragment = AddParkingFragment.getInstance();
                title = getString(R.string.add_parking);
                break;
            case FRAGMENT_INCOME_DETAIL:
                Timber.tag(TAG).e("FRAGMENT_INCOME_DETAIL");
                fragment = IncomeDetailFragment.getIntance();
                title = getString(R.string.income_detail);
                break;

            case FRAGMENT_PARKING_LIST:
                Timber.tag(TAG).e("FRAGMENT_PARKING_LIST");
                fragment = ParkingFragment.getInstance();
                title = getString(R.string.parking_list);
                break;

            case FRAGMENT_EMPLOYEE_LIST:
                Timber.tag(TAG).e("FRAGMENT_EMPLOYEE_LIST");
                fragment = EmployeeListFragment.getInstance("x");
                title = getString(R.string.employee_list);
                break;

            default:
                break;

        }

        if (fragment != null) {
            replaceFragment(fragment, fragment.getTag(), title);

        } else {
            return;
        }

    }


    @Override
    protected void onStart() {
        super.onStart();
        presenter.takeView(this);

    }


    @Override
    protected void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_menu;
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {

    }

    private void initializingInjection() {
        DaggerEzParkingComponent.builder().applicationComponent(getApplicationComponent())
                .ezParkingModule(new EzParkingModule()).build().inject(this);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(MenuActivity.this, HomeActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);
//        finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);

    }


    /*******************************************
     *  Start Fragment navigator controll      *
     *                                         *
     * ****************************************/
    public void addFragment(Fragment fragment, String tag) {
        if (fm == null) {
            fm = getFragmentManager();

        }


        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction
                .setCustomAnimations(R.animator.slide_enter, R.animator.slide_exit, R.animator.slide_pop_enter, R.animator.slide_pop_exit);
        fragmentTransaction
                .add(R.id.content_fragment_menu, fragment, tag)
                .addToBackStack(tag).commit();
    }


    public void hideFragment(int resId) {
        if (fm == null) {
            return;
        } else {

            fm.beginTransaction().setCustomAnimations(R.animator.slide_enter, R.animator.slide_exit, R.animator.slide_enter, R.animator.slide_exit).remove(fm.findFragmentById(resId)).commit();
        }

    }


    @SuppressLint("ResourceType")
    private void replaceFragment(Fragment fragment, String tag, String title) {
        if (fm == null) {
            fm = getFragmentManager();
            Timber.tag(TAG).e("replaceFragment null");

        }
        Timber.tag(TAG).e("replaceFragment Ok");
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.slide_enter, R.animator.slide_exit, R.animator.slide_enter, R.animator.slide_exit);
        fragmentTransaction.replace(R.id.content_fragment_menu, fragment, tag).commit();
        if (title != null) {
            setAppTitle(title);
        }

    }

    /*******************************************
     *     END Fragment navigator controll     *
     *                                         *
     * ****************************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //
        Fragment f = getFragmentManager().findFragmentById(R.id.content_fragment_menu);
        if (f instanceof RevenueFragment) {
            getMenuInflater().inflate(R.menu.menu_revenue, menu);
        }
        if (f instanceof ConfigFragment) {
            getMenuInflater().inflate(R.menu.menu_config, menu);
        }
        if (f instanceof HistoryFragment) {
            getMenuInflater().inflate(R.menu.menu_filter_history, menu);
        }
//        if (f instanceof IncomeDetailFragment) {
//            getMenuInflater().inflate(R.menu.menu_income_detail, menu);
//        }

        return true;
    }

    private void setAppTitle(String title) {
        try {
            getSupportActionBar().setTitle(title);

        } catch (Exception e) {
            Timber.tag(TAG).e(e.getMessage());
        }
    }


    @Override
    public void onClick(int position) {
        Timber.tag(TAG).e("menuactivity, %d", position);
    }

    @Override
    public void onLongClick(int position) {
        Timber.tag(TAG).e("menuactivity, %d", position);

    }


    @Override
    public void openFilter() {
        addFragment(FilterFragment.getInstance(), FilterFragment.TAG);
    }

    @Override
    public void onApplyFilter(long start, long end) {
        Timber.tag(TAG).e("Start %d --- end %d", start, end);
        if (fragment instanceof HistoryFragment) {
            //truyen filter param
            ((HistoryFragment) fragment).setFilter(start, end);
        }
        if (fragment instanceof IncomeDetailFragment) {
            //truyen filter param
            ((IncomeDetailFragment) fragment).setFilter(start, end);
        }
    }

    @Override
    public void goBack() {
        super.onBackPressed();
    }

    @Override
    public void openSettingPriceFragment(String newParkingUUIDParked) {
        Bundle bundle = new Bundle();
        bundle.putInt(EzConstants.KEY_BUNDLE_FRAGMENT_TAG, FragmentConstants.FRAGMENT_CONFIG);
        bundle.putString(EzConstants.KEY_BUNDLE_PARKING_UUID, newParkingUUIDParked);
        finish();
        navigator.startActivity(MenuActivity.this, MenuActivity.class, bundle);
    }

    @Override
    public void onBackButton() {
        super.onBackPressed();

    }

    // listener to call ProgressBarUtils
    @Override
    public void showProgressbarUtils() {
        ProgressBarUtils.showWaitingDialog(this);
    }
    // listener to call ProgressBarUtils
    @Override
    public void hideProgressBarUtils() {
        ProgressBarUtils.hideWaitingDialog();
    }

    @Override
    public void openFilterIncom() {
        addFragment(FilterFragment.getInstance(), FilterFragment.TAG);

    }

    // listener of ParkingListFragment
    @Override
    public void onItemPackingSelected(int position, String parkingUUID) {

        Bundle bundle = new Bundle();
        bundle.putInt(EzConstants.KEY_BUNDLE_FRAGMENT_TAG, FRAGMENT_CONFIG);
        bundle.putInt(EzConstants.KEY_BUNDLE_POSITION_ITEM, position);
        bundle.putString(EzConstants.KEY_BUNDLE_PARKING_UUID, parkingUUID);
        Timber.tag(TAG).e("kakakakakak %d", position);

        navigator.startActivity(MenuActivity.this, MenuActivity.class, bundle);
//        navigator.startActivity(OwnerActivity.this, MenuActivity.class, bundle);
    }

    // listener of ParkingListFragment
    @Override
    public void onAddNewPacking() {
        // todo open new Activity instead of fragment over here
//        addFragment(AddParkingFragment.getInstance(), AddParkingFragment.TAG);
        Bundle bundle = new Bundle();
        bundle.putInt(EzConstants.KEY_BUNDLE_FRAGMENT_TAG, FragmentConstants.FRAGMENT_ADD_PARKING);

        navigator.startActivity(MenuActivity.this, MenuActivity.class, bundle);
    }


    // listener of @EmployeeListFragment

    @Override
    public void onFloatbuttonClick() {

    }

    // listener of @EmployeeListFragment
    @Override
    public void onSelectUser(Employee e) {
        replaceFragment(EmployeeInfoFragment.getInstance(e), EmployeeInfoFragment.TAG, "");
    }

    // listener of @IncomeDetailFragment bellow
    @Override
    public void onAddparkingForEmployee(Employee employee, Parking parking) {

    }

    // listener of @IncomeDetailFragment bellow
    @Override
    public void restartApp() {
        navigator.restartApp(this);

    }

    // listener of @IncomeDetailFragment bellow
    @Override
    public void restartFragment(Employee e) {
        replaceFragment(EmployeeInfoFragment.getInstance(e), EmployeeInfoFragment.TAG, "");

    }
}
