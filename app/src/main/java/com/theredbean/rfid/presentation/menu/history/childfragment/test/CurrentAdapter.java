package com.theredbean.rfid.presentation.menu.history.childfragment.test;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theredbean.rfid.R;
import com.theredbean.rfid.models.FCurrentRecord;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by hoang on 02/04/2018 nhe.
 */
public class CurrentAdapter extends RecyclerView.Adapter<com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.CurrentViewHolder> {
    private static final String TAG = CurrentAdapter.class.getSimpleName();
    private List<FCurrentRecord> mData;
    private LayoutInflater mLayoutInflater;
    private Context context;

    private com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.CurrentViewHolder.OnCurrentViewHolderListener mListener;

    public CurrentAdapter(
            Context context,
            com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.CurrentViewHolder.OnCurrentViewHolderListener mListener) {
        mLayoutInflater = LayoutInflater.from(context);
        mData = null;
        this.mListener = mListener;
        this.context = context;
    }

    public void setData(List<FCurrentRecord> data) {
        if (mData == null) {
            mData = new ArrayList<>();
        }
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.CurrentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_history_current_bike, parent, false);
        return new com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.CurrentViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.CurrentViewHolder holder, int position) {
        FCurrentRecord entity = mData.get(position);
        holder.renderUi(context,position, entity);
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }


    private final com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.CurrentViewHolder.OnCurrentViewHolderListener listener = new com.theredbean.rfid.presentation.employee.menu.history.childfragment.test.CurrentViewHolder.OnCurrentViewHolderListener() {
        @Override
        public void onClick(int position) {
            mListener.onClick(position);
        }

        @Override
        public void onLongClick(int position) {
            mListener.onLongClick(position);
        }
    };
}
