package com.theredbean.rfid.presentation.menu.summary;


import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.di.ActivityScope;

import javax.inject.Inject;
import javax.inject.Named;

@ActivityScope
public class SummaryPresenter implements SummaryContract.Presenter {
    public static final String TAG = SummaryPresenter.class.getSimpleName();
    SummaryContract.View mView;
    IDatabase realm;

    @Inject
    public SummaryPresenter(@Named("realm") IDatabase realm) {
        this.realm = realm;
    }

    @Override
    public void takeView(SummaryContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }
}
