package com.theredbean.rfid.presentation;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import javax.inject.Inject;
import javax.inject.Singleton;

import timber.log.Timber;

/**
 * Created by nampham on 9/15/17.
 */
@Singleton
public class Navigator {
    public static final String TAG = Navigator.class.getSimpleName();

    private static Navigator mInstance = null;
    public static final String FRAGMENT_CLASS_NAME_START = "FRAGMENT_CLASS_NAME_START";

    @Inject
    Navigator() {
    }

//    public static Navigator getInstanceLoadingDrawable(){
//        if(mInstance == null)
//        {
//            mInstance = new Navigator();
//        }
//        return mInstance;
//    }


    public void startActivity(Context context, Class<?> activityClass, Bundle data) {
        if (data == null) data = new Bundle();
        if (activityClass != null) {

            Intent intent = new Intent(context, activityClass);
            intent.putExtras(data);
            context.startActivity(intent);
        }
    }

    public void startActivity2(Context context, Class<?> activityClass) {
        if (activityClass != null) {
            Intent intent = new Intent(context, activityClass);
            context.startActivity(intent);
        }
    }


    public void restartApp(Context context) {
        Timber.tag(TAG).e("restart Application");

        Intent i = context.getPackageManager()
                .getLaunchIntentForPackage(context.getPackageName());
        assert i != null;
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(i);
    }


}
