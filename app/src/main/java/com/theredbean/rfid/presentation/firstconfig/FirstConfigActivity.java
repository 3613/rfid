package com.theredbean.rfid.presentation.firstconfig;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerDatabaseComponent;
import com.theredbean.rfid.di.modules.DatabaseModule;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.Navigator;
import com.theredbean.rfid.presentation.login.LoginActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;
import utils.EzConstants;

public class FirstConfigActivity extends BaseActivity implements FirstConfigContract.View {
    public static final String TAG = FirstConfigActivity.class.getSimpleName();

    int a, b, c, d, e, f;

    @Inject
    Navigator navigator;
    @BindView(R.id.edtA)
    EditText edtA;
    @BindView(R.id.edtB)
    EditText edtB;

    @BindView(R.id.edtC)
    EditText edtC;

    @BindView(R.id.edtD)
    EditText edtD;

    @BindView(R.id.edtE)
    EditText edtE;

    @BindView(R.id.edtF)
    EditText edtF;


    @Inject
    FirstConfigContract.Presenter presenter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initzInjection();

        presenter.takeView(this);

        try {
            setPriceEditText();
        } catch (Exception e) {
            Timber.tag(TAG).e(e.getMessage());
        }

        try {
            if (TextUtils.isEmpty(edtA.getText().toString())
                    || TextUtils.isEmpty(edtB.getText().toString())
                    || TextUtils.isEmpty(edtC.getText().toString())
                    || TextUtils.isEmpty(edtD.getText().toString())
                    || TextUtils.isEmpty(edtE.getText().toString())
                    || TextUtils.isEmpty(edtF.getText().toString())) {
                RB_Error_MSg(getString(R.string.please_fill_all));
            } else {
                getPriceFromEditText();

            }
        } catch (Exception e) {
            Timber.tag(TAG).e(e.getMessage());
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_first_config;
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {

    }

    private void initzInjection() {
        DaggerDatabaseComponent.builder()
                .applicationComponent(getApplicationComponent())
                .databaseModule(new DatabaseModule())
                .build()
                .inject(this);
    }


    private void setPriceEditText() {
        edtA.setText(String.valueOf(EzConstants.PRICE_A_XE_GA_D +10));
        edtB.setText(String.valueOf(EzConstants.PRICE_B_XE_SO_D +10));
        edtC.setText(String.valueOf(EzConstants.PRICE_C_XE_DAP_D +10 ));
        edtD.setText(String.valueOf(EzConstants.PRICE_D_XE_GA_N +10));
        edtE.setText(String.valueOf(EzConstants.PRICE_E_XE_SO_N +10));
        edtF.setText(String.valueOf(EzConstants.PRICE_F_XE_DAP_N +10));
    }

    private void getPriceFromEditText() {
        a = Integer.parseInt(edtA.getText().toString());
        b = Integer.parseInt(edtB.getText().toString());
        c = Integer.parseInt(edtC.getText().toString());
        d = Integer.parseInt(edtD.getText().toString());
        e = Integer.parseInt(edtE.getText().toString());
        f = Integer.parseInt(edtF.getText().toString());
    }


    @OnClick(R.id.btnSaveConfig)
    public void saveConfig() {

        if (TextUtils.isEmpty(edtA.getText().toString())
                || TextUtils.isEmpty(edtB.getText().toString())
                || TextUtils.isEmpty(edtC.getText().toString())
                || TextUtils.isEmpty(edtD.getText().toString())
                || TextUtils.isEmpty(edtE.getText().toString())
                || TextUtils.isEmpty(edtF.getText().toString())) {
            RB_Error_MSg(getString(R.string.have_error));

        } else {
            getPriceFromEditText();
            presenter.writeAllPriceConfig(a, b, c, d, e, f);

        }

        Timber.tag(TAG).e("saveConfig: %d, %d, %d, %d, %d, %d --- %d", a, b, c, d, e, f, EzConstants.PRICE_A_XE_GA_D);
    }

    @Override
    public void onWriteSuccess() {
        customToast("Ghi dữ liệu lần đầu thành cong");
        Timber.tag(TAG).e("onWriteSuccess");
        navigator.startActivity(FirstConfigActivity.this, LoginActivity.class, null);
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void onWritepriceFalied() {
        RB_Error_MSg("Vui lòng nhập đầy đủ thông tin \n không được để tr");

    }


}
