package com.theredbean.rfid.presentation.employee.camera.fragmentcheckout;

import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface CheckoutContract {

    interface View extends BaseView {

        void onCheckoutSuccess();

        void onCheckoutFalied();

        void onShowCheckoutDetails(int totalPrice, String photoUrl);

        void onCheckoutSuccessV2(String localpath, String onlineTarget);
    }

    interface Presenter extends BasePresenter<CheckoutContract.View> {


        void getCurrentCheckoutRecord(String id);

        void processCheckout(String deviceId);

        /// onOnline verrsion


        void getCurrentCheckoutRecordOnline(String card_id, FCurrentRecord fCurrentRecord);

        void processCheckOutOnline(String card_id, FCurrentRecord  currentRecord);
    }
}
