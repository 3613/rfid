package com.theredbean.rfid.presentation.menu.parkinglist.viewholder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theredbean.rfid.R;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Parking;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;


/**
 * Created by hoang on 09/04/2018 nhe.
 */
public class ParkingAdapter extends RecyclerView.Adapter<ParkingViewHolder> {
    private static final String TAG = ParkingAdapter.class.getSimpleName();
    private Activity mActivity;
    private List<Parking> mData;
    private LayoutInflater mLayoutInflater;
    int x = -1;

    ParkingViewHolder.OnParkingViewHolderListener mlistenner;

    public ParkingAdapter(Activity activity, ParkingViewHolder.OnParkingViewHolderListener ltn) {
        mLayoutInflater = LayoutInflater.from(activity);
        mActivity = activity;
        mData = null;
        this.mlistenner = ltn;
        mData = new ArrayList<>();

    }

    public void setData(List<Parking> data) {
        notifyDataSetChanged();
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public void setWorkingAt(Employee employee) {
        for (int i = 0; i < mData.size(); i++) {
            if (employee.getParkingKey() == null || employee.getParkingKey() == "") {
                return;
            }
            if (employee.getParkingKey().equalsIgnoreCase(mData.get(i).getUuid())) {
                Timber.tag(TAG).e("adapter %s, %s", employee.getParkingKey(), mData.get(i).getUuid());
                x = i;
                notifyItemInserted(i);
                break;
            }
        }
    }

    @Override
    public ParkingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_list_parking, parent, false);
        return new ParkingViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(ParkingViewHolder holder, int position) {
        Parking entity = mData.get(position);
        holder.renderUi(position, entity);
        if (x == position) {
            holder.setSticker(true);
        } else {
            holder.setSticker(false);
        }
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    private final ParkingViewHolder.OnParkingViewHolderListener listener = new ParkingViewHolder.OnParkingViewHolderListener() {
        @Override
        public void onClick(int position) {
            mlistenner.onClick(position);
        }

        @Override
        public void onLongClick(int position) {
            mlistenner.onLongClick(position);
        }
    };
}
