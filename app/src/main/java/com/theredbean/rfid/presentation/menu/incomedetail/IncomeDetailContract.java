package com.theredbean.rfid.presentation.menu.incomedetail;

import com.theredbean.rfid.models.IncomModel;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

import java.util.List;

public interface IncomeDetailContract {

    interface View extends BaseView {
        void onloadIncomSuccess(List<IncomModel> incomModels, int resultPrice);
    }

    interface Presenter extends BasePresenter<IncomeDetailContract.View> {
        void loadFilterIncome(long from, long to);
    }
}
