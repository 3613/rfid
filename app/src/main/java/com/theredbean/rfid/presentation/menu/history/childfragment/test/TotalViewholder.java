package com.theredbean.rfid.presentation.employee.menu.history.childfragment.test;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.theredbean.rfid.R;
import com.theredbean.rfid.models.FHistoryRecord;

import utils.GlideApp;
import utils.ProgressBarUtils;
import utils.MyUtils;
import utils.TimeUtils;


public class TotalViewholder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private int position;
    private FHistoryRecord item;

    TextView txtEpc, txtTimeIn, txtPrice;
    private ImageView iv;

    private OnTTViewholderListener mListener;

    public interface OnTTViewholderListener {
        void onClick(int position);

        void onLongClick(int position);
    }

    public TotalViewholder(View view, OnTTViewholderListener listener) {
        super(view);
        txtPrice = itemView.findViewById(R.id.txtPrice);
        txtTimeIn = view.findViewById(R.id.txtTimeIn);
        iv = itemView.findViewById(R.id.iv);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        mListener = listener;
    }

    public void renderUi(Context mContext, int position, FHistoryRecord historyRecord) {
        this.position = position;
        String timeHtml = " <font color='red'>Vào:: &nbsp; </font>" + TimeUtils.getTimeText(historyRecord.getTimestampCheckIn())
                + " <br /><font color='red'>Ra  : &nbsp; &nbsp; &nbsp;</font>" +
                TimeUtils.getTimeText(historyRecord.getTimestampCheckOut());

        txtTimeIn.setText(Html.fromHtml(timeHtml));

        txtPrice.setText(itemView.getContext().getResources()
                .getString(R.string.txt_price_bike, MyUtils.fomatVND(historyRecord.getPrice())));

        GlideApp.with(mContext)
                .load(historyRecord.getPathImage())
                .placeholder(ProgressBarUtils.getInstanceLoadingDrawable(mContext))
                .fallback(R.drawable.error_image)
                .error(R.drawable.error_image)
                .into(iv);
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onClick(position);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mListener != null) {
            mListener.onLongClick(position);
        }
        return false;
    }
}