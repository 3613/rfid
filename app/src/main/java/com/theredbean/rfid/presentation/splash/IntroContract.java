package com.theredbean.rfid.presentation.splash;

import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface IntroContract {

    interface View extends BaseView {
        void onLoadConfigSuccess();

        void onLoadConFigFailed();



    }

    interface Presenter extends BasePresenter<IntroContract.View> {

        void loadConfig();


        void cameraPerrmission(Boolean status);


    }
}
