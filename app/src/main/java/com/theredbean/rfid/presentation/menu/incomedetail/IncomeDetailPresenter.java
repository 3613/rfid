package com.theredbean.rfid.presentation.menu.incomedetail;


import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.income.IncomeUsecase;
import com.theredbean.rfid.models.IncomModel;

import java.util.List;

import javax.inject.Inject;

public class IncomeDetailPresenter implements IncomeDetailContract.Presenter {
    private IncomeDetailContract.View view;
    private IncomeUsecase incomeUsecase;
    private EzParkManager ezParkManager;

    @Inject
    IncomeDetailPresenter(IncomeUsecase incomeUsecase, EzParkManager ezParkManager) {
        this.incomeUsecase = incomeUsecase;
        this.ezParkManager = ezParkManager;
    }

    @Override
    public void takeView(IncomeDetailContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        view = null;
    }

    @Override
    public void loadFilterIncome(long from, long to) {
        incomeUsecase.getIncomeWithTimeMilis(from, to, ezParkManager.getOwner().getUuid(),
                ezParkManager.getParks(),
                new EzParkingCallback.IncomeDetailCallBack() {
                    @Override
                    public void onLoadPriceSuccesss(List<IncomModel> incomModels, int resultPrice) {
                        view.onloadIncomSuccess(incomModels,resultPrice);
                    }

                    @Override
                    public void onloadPriceFaied(String msg) {
                        view.showMessage(msg);
                    }
                });

    }
}
