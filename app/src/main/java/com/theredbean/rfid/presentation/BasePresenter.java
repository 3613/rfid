package com.theredbean.rfid.presentation;

/**
 * Created by nampham on 9/5/17.
 */

public interface BasePresenter<T> {

    void takeView(T t);

    void dropView();
}
