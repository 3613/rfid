package com.theredbean.rfid.presentation.employee.menu.history;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;
import com.theredbean.rfid.presentation.employee.menu.history.childfragment.adapter.ViewPagerHistoryAdapter;
import com.theredbean.rfid.presentation.menu.history.childfragment.ui.TotalParkedFragment;
import com.theredbean.rfid.presentation.menu.summary.SummaryContract;

import butterknife.BindView;
import timber.log.Timber;

public class HistoryFragment extends BaseFragment implements SummaryContract.View {
    public static final String TAG = HistoryFragment.class.getSimpleName();
    Context context;

    public Mylistener mylistener;

    public void setFilter(long start, long end) {
        Timber.tag(TAG).e("Start %d --- end %d", start, end);

        int pos = viewPager.getCurrentItem();
        if (pos == 1) {
            TotalParkedFragment fragment = (TotalParkedFragment) mViewPagerAdapter.getItem(pos);
            fragment.setFilter(start, end);
        }
    }

    public interface Mylistener {
        void openFilter();
    }

    @BindView(R.id.vpPager)
    ViewPager viewPager;
    ViewPagerHistoryAdapter mViewPagerAdapter;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showAlertDialog(String msg) {

    }

    @Override
    public void onPause() {
        super.onPause();
        Timber.tag(TAG).e("life onPause");
    }

    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        Timber.tag(TAG).e("life onAttachToContext");

        if (context instanceof HistoryFragment.Mylistener) {
            mylistener = (HistoryFragment.Mylistener) context;
        } else {
            throw new ClassCastException(getActivity().getLocalClassName() + "is not cast HistoryFragment listener");

        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_history_base;
    }

    @Override
    protected void onMyCreateView(View view) {
        setUpTablayout();
        Timber.tag(TAG).e("life onMyCreateView");

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDependency();
        setHasOptionsMenu(true);
        Timber.tag(TAG).e("life onCreate");


    }

    @Override
    public void onStart() {
        super.onStart();
        Timber.tag(TAG).e("life onStart");

    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.tag(TAG).e("life onResume");

    }

    private void initDependency() {
        DaggerEzParkingComponent.builder().applicationComponent(((BaseActivity) getActivity()).getApplicationComponent()).ezParkingModule(new EzParkingModule()).build().inject(this);


    }

    private void setUpTablayout() {
        mViewPagerAdapter = new ViewPagerHistoryAdapter(getChildFragmentManager(), context.getApplicationContext());
        viewPager.setAdapter(mViewPagerAdapter);
        viewPager.setOffscreenPageLimit(4);
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            RelativeLayout relativeLayout = (RelativeLayout)
                    LayoutInflater.from(context).inflate(R.layout.tab_layout, tabLayout, false);

            TextView tabTextView = (TextView) relativeLayout.findViewById(R.id.tab_title);
            if (i == 0) {
                View tabline = (View) relativeLayout.findViewById(R.id.doc);
                tabline.setVisibility(View.GONE);

            }
            tabTextView.setText(tab.getText());
            tab.setCustomView(relativeLayout);
//            tab.select();
        }
//        setIconTab(); //// unComment to registerEmployee Icon


    }


    public static Fragment getInstance() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle data = new Bundle();
        fragment.setArguments(data);
        return fragment;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.mnFilterHisory) {
            Toast.makeText(context, "open fillter", Toast.LENGTH_SHORT).show();
            mylistener.openFilter();

        }
        ;
        return super.onOptionsItemSelected(item);

    }

}

