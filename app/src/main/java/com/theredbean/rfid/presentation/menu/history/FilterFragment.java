package com.theredbean.rfid.presentation.employee.menu.history;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Toast;


import com.hnam.uibutton.MyTextView;
import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;
import utils.TimeUtils;


public class FilterFragment extends BaseFragment {
    public static final String TAG = FilterFragment.class.getSimpleName();
    Context context;

    public Mylistener mylistener;
    @BindView(R.id.oneWeek)
    Button oneWeek;
    @BindView(R.id.oneMonth)
    Button oneMonth;
    @BindView(R.id.ThreeMonth)
    Button ThreeMonth;
    Unbinder unbinder;
    @BindView(R.id.timeStart)
    MyTextView txtStart;
    @BindView(R.id.timeEnd)
    MyTextView txtEnd;
    @BindView(R.id.title)
    MyTextView title;
    @BindView(R.id.btnApplyFilter)
    Button btnApplyFilter;
    private Calendar calendar;
    private int year, month, day;
    long timeStart = TimeUtils.getCurrentInMiliseconds();
    long timeEnd = TimeUtils.getCurrentInMiliseconds();

    List<Button> viewList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        assert rootView != null;
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface Mylistener {
        void onApplyFilter(long start, long end);
    }


    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof Mylistener) {
            mylistener = (Mylistener) context;
        } else {
            throw new ClassCastException(getActivity().getLocalClassName() + "is not cast HistoryFragment listener");

        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_filter_history;
    }

    @Override
    protected void onMyCreateView(View view) {
        setTime();
        viewList.add(oneMonth);
        viewList.add(oneWeek);
        viewList.add(ThreeMonth);


    }

    public void reset() {
        for (int i = 0; i < viewList.size(); i++) {
            viewList.get(i).setBackground(context.getDrawable(R.drawable.button_filter_selector_default));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void setSelectIcon(Button btn) {
        Timber.tag(TAG).e("viewlist %d", viewList.size());
        for (int i = 0; i < viewList.size(); i++) {
            Button z = viewList.get(i);
            if (btn.getId() == z.getId()) {
                Timber.tag(TAG).e("zzzz zoz oz");

                z.setBackground(context.getDrawable(R.drawable.button_selector_selected));
                z.setTextColor(context.getColor(R.color.white));

            } else {
                z.setBackground(context.getDrawable(R.drawable.button_filter_selector_default));
                z.setTextColor(context.getColor(R.color.grey));


            }


        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDependency();
        setHasOptionsMenu(true);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

//        setTime();

        Timber.tag(TAG).e("tren---year: %d ---- month:%d ---- day: %d", year, month + 1, day);


    }

    private void setTime() {
        try {
            // try catch because null
            txtEnd.setText(context.getResources().getString(R.string.time_end, String.valueOf(TimeUtils.getTimeText(timeEnd))));                //
            txtStart.setText(context.getResources().getString(R.string.time_start, String.valueOf(TimeUtils.getTimeText(timeStart))));                //
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initDependency() {
        DaggerEzParkingComponent
                .builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build().inject(this);


    }


    public static Fragment getInstance() {
        FilterFragment fragment = new FilterFragment();
        Bundle data = new Bundle();
        fragment.setArguments(data);
        return fragment;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.mnFilterHisory) {
            Toast.makeText(context, "open fillter", Toast.LENGTH_SHORT).show();

        }
        ;
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.timeStart)
    public void selectStartTime() {
        datePicker
                = new DatePickerDialog(
                context,
                datePickerListener, year, month, day);
        datePicker.show();

    }

    @OnClick(R.id.timeEnd)
    public void selectTimeEnd() {
        datePicker
                = new DatePickerDialog(
                context,
                datePickerEndListener, year, month, day);
        datePicker.show();

    }

    @OnClick(R.id.oneWeek)
    public void onSelectOneWeek() {
        timeEnd = TimeUtils.getCurrentInMiliseconds();
        timeStart = TimeUtils.plusOneWeek();
        Timber.tag(TAG).e(" time start %d --- time end %d", timeStart, timeEnd);
        Timber.tag(TAG).e("end %s", TimeUtils.getTimeText(timeEnd));
        Timber.tag(TAG).e("start %s", TimeUtils.getTimeText(timeStart));
        setTime();
        setSelectIcon(oneWeek);

    }

    @OnClick(R.id.oneMonth)
    public void onSelectOneMonth() {
        timeStart = TimeUtils.plusOneMonth();
        timeEnd = TimeUtils.getCurrentInMiliseconds();
        Timber.tag(TAG).e(" time start %d --- time end %d", timeStart, timeEnd);
        Timber.tag(TAG).e("end %s", TimeUtils.getTimeText(timeEnd));
        Timber.tag(TAG).e("start %s", TimeUtils.getTimeText(timeStart));
        setTime();
        ;
        setSelectIcon(oneMonth);

    }

    @OnClick(R.id.ThreeMonth)
    public void onSelect03Month() {
        timeEnd = TimeUtils.getCurrentInMiliseconds();
        timeStart = TimeUtils.plusTherreMonth();
        Timber.tag(TAG).e(" time start %d --- time end %d", timeStart, timeEnd);
        Timber.tag(TAG).e("end %s", TimeUtils.getTimeText(timeEnd));
        Timber.tag(TAG).e("start %s", TimeUtils.getTimeText(timeStart));
        setTime();


        setSelectIcon(ThreeMonth);
    }

    @OnClick(R.id.btnApplyFilter)
    public void onApplyFiller() {
        getActivity().onBackPressed();
        mylistener.onApplyFilter(timeStart, timeEnd);
    }

    @OnClick(R.id.btnReset)
    public void resetViewMarker() {
        reset();
    }


    DatePickerDialog datePicker;


    public static final int TIME_START = 2;
    public static final int TIME_END = 1;


    DatePickerDialog.OnDateSetListener datePickerListener = (view, year, month, dayOfMonth) -> {
        Timber.tag(TAG).e("year: %d ---- month:%d ---- day: %d", year, month + 1, dayOfMonth);
        openTimePicker(year, month + 1, dayOfMonth, TIME_START);
    };
    DatePickerDialog.OnDateSetListener datePickerEndListener = (view, year, month, dayOfMonth) -> {
        Timber.tag(TAG).e("year: %d ---- month:%d ---- day: %d", year, month + 1, dayOfMonth);
        openTimePicker(year, month + 1, dayOfMonth, TIME_END);
    };

    private void openTimePicker(int year, int month, int date, int usedFor) {
        Timber.tag(TAG).e("usedfor %d", usedFor);

        TimePickerDialog timePickerDialog = new TimePickerDialog(context, (view, hourOfDay, minute) -> {
            // todo get milisecond from year, month dayOfMonth, houofDay, minute to startTime
            if (usedFor == TIME_START) {
//                    timeStart = convert gi do
                timeStart = new DateTime().withYear(year)
                        .withMonthOfYear(month)
                        .withDayOfMonth(date)
                        .withHourOfDay(hourOfDay)
                        .withMinuteOfHour(minute).withSecondOfMinute(0).getMillis();
//                txtStart.setText(context.getResources().getString(R.string.time_from, String.valueOf(TimeUtils.getTimeText(timeStart))));                //
                Timber.tag(TAG).e("start miliseconds %d", timeStart);
                setTime();


            }
            if (usedFor == TIME_END) {
                //timeEnd = convert gi do
                timeEnd = new DateTime().withYear(year)
                        .withMonthOfYear(month)
                        .withDayOfMonth(date)
                        .withHourOfDay(hourOfDay)
                        .withMinuteOfHour(minute).withSecondOfMinute(0).getMillis();//
                Timber.tag(TAG).e("end  miliseconds %d", timeEnd);


                setTime();

            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }


}

