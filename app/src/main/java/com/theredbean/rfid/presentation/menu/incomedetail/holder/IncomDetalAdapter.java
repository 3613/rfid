package com.theredbean.rfid.presentation.menu.incomedetail.holder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.theredbean.rfid.R;
import com.theredbean.rfid.models.IncomModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by hoang on 29/05/2018 nhe.
 */
public class IncomDetalAdapter extends RecyclerView.Adapter<IncomeDetailViewholder> {
    private static final String TAG = IncomDetalAdapter.class.getSimpleName();
    private Activity mActivity;
    private List<IncomModel> mData;
    private LayoutInflater mLayoutInflater;
    private HashMap<String, Integer> mIncome; //parking key, income


    public IncomDetalAdapter(Activity activity) {
        mLayoutInflater = LayoutInflater.from(activity);
        mActivity = activity;
        mIncome = new HashMap<String, Integer>();

        mData = null;
    }

    public void setIncome(IncomModel income) {
        mIncome.put(income.getParkingUUid(), income.getPrice());
        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i).getParkingUUid().equalsIgnoreCase(income.getParkingUUid())) {
                notifyItemChanged(i);
                break;
            }
        }

    }

    public void setData(List<IncomModel> data) {
        if (mData == null) {
            mData = new ArrayList<>();
        }
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public IncomeDetailViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_icome, parent, false);
        return new IncomeDetailViewholder(view, listener);
    }

    @Override
    public void onBindViewHolder(IncomeDetailViewholder holder, int position) {
        IncomModel entity = mData.get(position);
        holder.renderUi(position, entity);
        holder.setName(mIncome.get(entity.getParkingUUid()));
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    private final IncomeDetailViewholder.OnIncomeDetailViewholderListener listener = new IncomeDetailViewholder.OnIncomeDetailViewholderListener() {
        @Override
        public void onClick(int position) {

        }

        @Override
        public void onLongClick(int position) {

        }
    };
}
