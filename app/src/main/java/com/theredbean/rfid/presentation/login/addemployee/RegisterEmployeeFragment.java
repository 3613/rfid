package com.theredbean.rfid.presentation.login.addemployee;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.hnam.uibutton.MyButton;
import com.theredbean.rfid.R;
import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;
import com.theredbean.rfid.presentation.login.addemployee.selectOwner.ChooseOwnerDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;


public class RegisterEmployeeFragment extends BaseFragment implements
        RegisterEmployeeContract.View, ChooseOwnerDialog.Listener {
    public static final String TAG = RegisterEmployeeFragment.class.getSimpleName();
    private static String oId = "";
    ChooseOwnerDialog chooseOwnerDialog;

    @Inject
    EzParkManager userManager;

    @Inject
    RegisterEmployeeContract.Presenter presenter;


    AddEmployeeListener mListener;
    @BindView(R.id.btnshowDialog)
    MyButton btnshowDialog;
    Unbinder unbinder;

    @Override
    public void showRegisterEmployeeSuccess() {
        mListener.openSelectOwner();
        mListener.gotoLoginActivity();
        mListener.onRegisterEmployeeSucess();
    }

    @Override
    public void showRegisterEmployeeFailed(String msg) {
        RB_Error_MSg(msg);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    boolean isSelected = false;

    @Override
    public void onOwnerSelected(String UU_id, String ownwerName) {
        isSelected = true;
        Timber.tag(TAG).e("receved  %s, %s", UU_id, ownwerName);

        // todo insert info to text View, get
        btnshowDialog.setText(ownwerName);
        chooseOwnerDialog.dismiss();
        this.ownerUuid = UU_id;


    }


    public interface AddEmployeeListener {
        void onAddEmployeeSuccess(String id);

        void onSelectOwnerButton();

        void openSelectOwner();

        void gotoLoginActivity();

        void onRegisterEmployeeSucess();
    }


    Context context;
    @BindView(R.id.edtUserName)
    EditText edtUser;
    @BindView(R.id.edtpass)
    EditText edtpass;
    @BindView(R.id.edtName)
    EditText edtName;

    @BindView(R.id.edtDescription)
    EditText edtDescription;


    @OnClick(R.id.btnRegister)
    public void addEmployees() {

        String username = edtUser.getText().toString().trim();
        String pass = edtpass.getText().toString().trim();
        String name = edtName.getText().toString().trim();
        String description = edtDescription.getText().toString().trim();

        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(pass) || TextUtils.isEmpty(description)
                || TextUtils.isEmpty(name) || !isSelected) {
            RB_Warning_MSg(" Không được để trống");

            return;
        }
        if (!isSelected) {
            RB_Warning_MSg(" Vui long chon bai xe");
        }

        Employee employee = new Employee();
        employee.setName(name);

        employee.setDescription(description);
        userManager.setEmployee(employee);


        Timber.tag(TAG).e("keykeykey : %s", userManager.getEmployee().getName());
//        presenter.registerEmployee(username, pass, description);
        presenter.registerEmployeeV2(username, pass, name, description, ownerUuid);
    }

    String ownerUuid = "huuhoang";

    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof AddEmployeeListener) {
            mListener = (AddEmployeeListener) context;
        } else {
            throw new ClassCastException(getActivity().getLocalClassName() + "can not cast AddEmployeeListener ");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_register_employeeeeeeeee;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    protected void onMyCreateView(View view) {

        initInjection();
        presenter.takeView(this);

    }

    private void initInjection() {
        DaggerEzParkingComponent
                .builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule()).build().inject(this);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.dropView();
        unbinder.unbind();
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }

    @Override
    public void showAlertDialog(String msg) {

    }

    public static RegisterEmployeeFragment getInstance(String owner_UID) {
        RegisterEmployeeFragment fragment = new RegisterEmployeeFragment();
        Timber.tag(TAG).e(owner_UID + " uid");
        oId = owner_UID;
//        Bundle data = new Bundle();
//        fragment.setArguments(data);
        return fragment;
    }

    @OnClick(R.id.btnshowDialog)
    public void onShowDialog() {
        chooseOwnerDialog = new ChooseOwnerDialog();
        chooseOwnerDialog.setTargetFragment(RegisterEmployeeFragment.this, 1);
        chooseOwnerDialog.show(getFragmentManager(), ChooseOwnerDialog.TAG);
    }
}
