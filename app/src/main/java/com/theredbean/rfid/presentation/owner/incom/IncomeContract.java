package com.theredbean.rfid.presentation.owner.incom;

import com.theredbean.rfid.business.model.CurrentIncome;
import com.theredbean.rfid.models.Parking;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

import java.util.List;

public interface IncomeContract {

    interface View extends BaseView {

        void onGetIncomeSuccess(CurrentIncome currentIncome, long total);

        void onGetIncomeFalied(String msg);


        void ongetParkingsSuccess(List<Parking> parkingList);

        void onGetParkingsFailed(String msg);

        void disPlayNoParking();

        void displayHaveData();
    }

    interface Presenter extends BasePresenter<IncomeContract.View> {

        void getParkings();

        void registerIncomeListener();

        void unregisterIncomListener();
    }
}
