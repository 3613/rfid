package com.theredbean.rfid.presentation.menu.employeelist;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.hnam.uibutton.MyTextView;
import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Parking;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;
import com.theredbean.rfid.presentation.menu.employeelist.holderAdapter.EmployeeAdapter;
import com.theredbean.rfid.presentation.menu.employeelist.holderAdapter.EmployeeViewHolder;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

public class EmployeeListFragment extends BaseFragment
        implements EmployeeListContract.View
        , EmployeeViewHolder.OnEmployeeViewHolderListener {
    public static final String TAG = EmployeeListFragment.class.getSimpleName();
    EmployeeFragmentListener mlistener;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.txtNoParking)
    MyTextView txtNoParking;
    @BindView(R.id.flAddEmployee)
    FloatingActionButton flAddEmployee;
    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    public interface EmployeeFragmentListener {
        void onFloatbuttonClick();

        void onSelectUser(Employee e);
    }

    Context context;

    List<Employee> mdata;


    EmployeeAdapter adapter2;

    @BindView(R.id.rvListEmployeee)
    RecyclerView rvHistory;

    @Inject
    EmployeeListContract.Presenter presenter;


    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof EmployeeFragmentListener) {
            mlistener = (EmployeeFragmentListener) context;
        } else {
            throw new ClassCastException("canot cast EmployeeListFragment");
        }
    }

    @OnClick(R.id.flAddEmployee)
    public void onFloatAddClick() {
        mlistener.onFloatbuttonClick();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list_employee;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInjection();
        presenter.takeView(this);


//        Bundle bundle = getArguments();
//        if (bundle != null) {
//            currentOwner_UID = bundle.getString(FirebaseConstant.KEY_OWNER);
//        }
    }


    @Override
    protected void onMyCreateView(View view) {
        presenter.takeView(this);
        setupRV2();


    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getListEmployeee("ko can paranmay");
        presenter.getParkingsKey();

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.dropView();
        unbinder.unbind();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {

        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {
        RB_Loading_MSg(msg);

    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }


    @Override
    public void showAlertDialog(String msg) {

    }

    public void initInjection() {
        DaggerEzParkingComponent.builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build()
                .inject(this);

    }


    public static EmployeeListFragment getInstance(String current_Owner_UID) {
        EmployeeListFragment fragment = new EmployeeListFragment();
        Bundle data = new Bundle();
        data.putString("current_Owner_UID", current_Owner_UID);
        fragment.setArguments(data);

        return fragment;
    }


    @Override
    public void onGetListEmployeesOfOwnerSuccess(List<Employee> employees) {
        mdata = employees;
        adapter2.setData(mdata);
        presenter.getParkingsKey();


    }

    @Override
    public void onGetListEmployeesOfOwnerFalied(String msg) {
        Timber.tag(TAG).e(msg);

    }

    @Override
    public void displayEmployee() {
        rvHistory.setVisibility(View.INVISIBLE);
        txtNoParking.setVisibility(View.VISIBLE);
    }


    @Override
    public void gotParkings(List<Parking> parking) {
        adapter2.setParkingLustKey(parking);
    }


    private void setupRV2() {
        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        rvHistory.setLayoutManager(lm);
        rvHistory.setHasFixedSize(true);
        adapter2 = new EmployeeAdapter(getActivity(), this);
        rvHistory.setAdapter(adapter2);
    }

    @Override
    public void onClick(int position) {
        Employee e = mdata.get(position);
        mlistener.onSelectUser(e);
    }

    @Override
    public void onLongClick(int position) {
        Employee e = mdata.get(position);
        mlistener.onSelectUser(e);

    }


}
