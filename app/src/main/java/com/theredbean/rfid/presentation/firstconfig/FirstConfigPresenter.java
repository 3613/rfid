package com.theredbean.rfid.presentation.firstconfig;


import com.theredbean.rfid.data.sharedpreference.ISharedPreferences;

import javax.inject.Inject;

import utils.EzConstants;

public class FirstConfigPresenter implements FirstConfigContract.Presenter {

    FirstConfigContract.View mView;
    ISharedPreferences sharedPreferences;

    @Inject

    public FirstConfigPresenter(ISharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }


    @Override
    public void takeView(FirstConfigContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        this.mView = null;
    }

    @Override
    public void writeAllPriceConfig(int a, int b, int c, int d, int e, int f) {
        if (sharedPreferences.saveAllPriceConfig(a, b, c, d, e, f)) {

            EzConstants.PRICE_A_XE_GA_D = a;
            EzConstants.PRICE_B_XE_SO_D = b;
            EzConstants.PRICE_C_XE_DAP_D = c;
            EzConstants.PRICE_D_XE_GA_N = d;
            EzConstants.PRICE_E_XE_SO_N = e;
            EzConstants.PRICE_F_XE_DAP_N = f;

            mView.onWriteSuccess();

        } else {
            mView.onWritepriceFalied();
        }
    }
}
