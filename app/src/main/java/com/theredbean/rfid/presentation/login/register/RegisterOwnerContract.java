package com.theredbean.rfid.presentation.login.register;

import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

public interface RegisterOwnerContract {

    interface View extends BaseView {

        void onRegisterSuccess();

        void onRegisterFalied(String msg);

        void openLoginFragment();
    }

    interface Presenter extends BasePresenter<RegisterOwnerContract.View> {
        public void register(String user, String pass,String name,String description);

        public void writeOnwerUserRegistered(Owner owner);
    }
}