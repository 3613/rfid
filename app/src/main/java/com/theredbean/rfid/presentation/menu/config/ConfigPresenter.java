package com.theredbean.rfid.presentation.menu.config;


import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.pricing.PriceUsecase;
import com.theredbean.rfid.models.Pricez.FPrice;
import com.theredbean.rfid.models.Pricez.PriceXeGa;
import com.theredbean.rfid.models.Pricez.PriceXedap;
import com.theredbean.rfid.models.Pricez.PriceXesSo;

import javax.inject.Inject;

import timber.log.Timber;

public class ConfigPresenter implements ConfigContract.Presenter {
    public static final String TAG = ConfigPresenter.class.getSimpleName();

    private ConfigContract.View view;
    PriceUsecase priceUsecase;
    EzParkManager userManaager;

    @Inject
    public ConfigPresenter(PriceUsecase priceUsecase, EzParkManager userManager) {
        this.userManaager = userManager;
        this.priceUsecase = priceUsecase;
    }

    @Override
    public void takeView(ConfigContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        view = null;
    }


    @Override
    public void savePriceConfig(String parkingKey, String ga_day, String so_day, String dap_day, String ga_night, String so_night, String dap_night) {
//        view.showLoading();

        PriceXeGa pGa = new PriceXeGa(Integer.parseInt(ga_day), Integer.parseInt(ga_night));
        PriceXesSo pSo = new PriceXesSo(Integer.parseInt(so_day), Integer.parseInt(so_night));
        PriceXedap pDap = new PriceXedap(Integer.parseInt(dap_day), Integer.parseInt(dap_night));

        FPrice fPrice = new FPrice();


        Timber.tag(TAG).e(fPrice.toString());


        fPrice.setXe_so(pSo);
        fPrice.setXe_ga(pGa);
        fPrice.setXe_dap(pDap);

        priceUsecase.savePrice(userManaager.getOwner(), parkingKey, fPrice, new EzParkingCallback.PriceCallback() {

            @Override
            public void onAddPriceSuccess() {
                view.hideLoading();
                view.onWriteSuccess();
                Timber.tag(TAG).e("onAddPriceSuccess");

            }

            @Override
            public void onAddPriceFailed(String msg) {
                view.hideLoading();
                view.onWritepriceFalied(msg);
                Timber.tag(TAG).e("onAddPriceFailed %s", msg);

            }

            @Override
            public void onExceptionError(String msg) {
                view.hideLoading();
                view.onWritepriceFalied(msg);
                Timber.tag(TAG).e("onExceptionError %s", msg);

            }

            @Override
            public void onExceptionAuthentication() {
                view.hideLoading();
                view.onWritepriceFalied("onExceptionAuthentication");

                Timber.tag(TAG).e("onExceptionAuthentication %s", "kaka");

            }
        });
    }

    @Override
    public void loadConfig(String parkingKey) {
        Timber.tag(TAG).e("loadconfig---------price");

        priceUsecase.loadPriceConfig2(parkingKey, userManaager, new EzParkingCallback.LoadPriceCallBack() {
            @Override
            public void onLoadPriceSuccess(FPrice price) {

                view.hideLoading();
                view.showLoadConfigSuccess(price);
            }

            @Override
            public void onLoadPrieceFaied(String msg) {
                view.hideLoading();
            }
        });
    }
}
