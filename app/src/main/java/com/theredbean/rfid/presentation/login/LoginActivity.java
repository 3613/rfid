package com.theredbean.rfid.presentation.login;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.Navigator;
import com.theredbean.rfid.presentation.employee.camera.HomeActivity;
import com.theredbean.rfid.presentation.login.addemployee.RegisterEmployeeFragment;
import com.theredbean.rfid.presentation.login.errorlogin.LoginErrorFragment;
import com.theredbean.rfid.presentation.login.newlogin.RegisterFragment;
import com.theredbean.rfid.presentation.login.ownerlist.PickOwnerFragment;
import com.theredbean.rfid.presentation.login.register.RegisterOwnerFragment;
import com.theredbean.rfid.presentation.login.signin.LoginFragment;
import com.theredbean.rfid.presentation.owner.OwnerActivity;
import com.theredbean.rfid.presentation.menu.employeelist.EmployeeListFragment;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public class LoginActivity extends BaseActivity
        implements LoginFragment.LoginListenner,
        RegisterOwnerFragment.RegisterListener,
        RegisterEmployeeFragment.AddEmployeeListener,
        EmployeeListFragment.EmployeeFragmentListener,
        PickOwnerFragment.PickOwnerFragmentListener, LoginContract.View
        , RegisterFragment.Mylistener, LoginErrorFragment.Mylistener {

    private FragmentManager fm = getFragmentManager();
    int a = 0;
    public static final String TAG = LoginActivity.class.getSimpleName();
    @Inject
    Navigator navigator;

    @Inject
    LoginContract.Presenter mPresenter;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDagger();
        Fabric.with(this, new Crashlytics());
    }


    private void initDagger() {
        //DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(getApplication())).build().inject(this);
        DaggerEzParkingComponent.builder()
                .applicationComponent(getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_user_login;
    }

    private void initzInjection() {
        DaggerEzParkingComponent.builder().applicationComponent(getApplicationComponent()).ezParkingModule(new EzParkingModule()).build().inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.takeView(this);
        mPresenter.checkUserLogin();
    }


    // todo what is thís?
//    @Override
//    public void onBackPressed() {
//        if (getFragmentManager().getBackStackEntryCount() == 1) {
//
//            finish();
//        } else {
//            getFragmentManager().popBackStack();
//        }
//    }

    /*******************************************
     *  Start Fragment navigator controll      *
     *                                         *
     * ****************************************/
    public void addFragment(Fragment fragment, String tag) {
        if (fm == null) {
            fm = getFragmentManager();

        }


        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction
                .setCustomAnimations(R.animator.slide_enter, R.animator.slide_exit, R.animator.slide_pop_enter, R.animator.slide_pop_exit);
        fragmentTransaction.add(R.id.content_fragment_login, fragment, tag)
                .addToBackStack(tag).commit();
    }

    @Override
    public void showAlertDialog(String message) {
        RB_Error_MSg(message);
    }

    public void hideFragment(int resId) {
        if (fm == null) {
            return;
        } else {

            fm.beginTransaction().setCustomAnimations(R.animator.slide_enter, R.animator.slide_exit, R.animator.slide_enter, R.animator.slide_exit).remove(fm.findFragmentById(resId)).commit();
        }

    }


    @SuppressLint("ResourceType")
    private void replaceFragment(Fragment fragment, String tag) {
        if (fm == null) {
            fm = getFragmentManager();
            Timber.tag(TAG).e("replaceFragment null");

        }
        Timber.tag(TAG).e("replaceFragment Ok");
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction
                .setCustomAnimations(R.animator.slide_enter, R.animator.slide_exit, R.animator.slide_enter, R.animator.slide_exit);
        fragmentTransaction.replace(R.id.content_fragment_login, fragment, tag).commit();
    }


    /*******************************************
     *     END Fragment navigator controll     *
     *                                         *
     * ****************************************/


    @Override
    public void onGotoRegister() {
        Toast.makeText(this, "onGotoRegister", Toast.LENGTH_SHORT).show();
        addFragment(RegisterOwnerFragment.getInstance(), RegisterOwnerFragment.TAG);
    }

    @Override
    public void onGotoForgotPassWord() {
        Toast.makeText(this, "onGotoForgotPassWord", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showOwnerScreen() {
        // todo replace  this
        //replaceFragment(new RegisterOwnerFragment(), RegisterOwnerFragment.TAG);
        this.finish();
        navigator.startActivity(LoginActivity.this, OwnerActivity.class, null);
    }

    @Override
    public void showEmployeeScree() {
//        addFragment(new );
        // todo do it
        this.finish();
        navigator.startActivity(LoginActivity.this, HomeActivity.class, null);
    }

    @Override
    public void openRegisterEmployeee() {
        // todo
        addFragment(RegisterEmployeeFragment.getInstance("x"), RegisterEmployeeFragment.TAG);
    }

    @Override
    public void openRegisterOwner() {
        addFragment(RegisterOwnerFragment.getInstance(), RegisterOwnerFragment.TAG);
    }

    @Override
    public void openNewLogin() {
        addFragment(new RegisterFragment(), RegisterFragment.TAG);
    }

    @Override
    public void onGotoLogin() {
        //replaceFragment(LoginFragment.getInstanceLoadingDrawable("1", "1"), LoginFragment.TAG);
    }

    @Override
    public void onGotoLostPass() {
        Toast.makeText(this, "onGotoForgotPassWord", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showToast(String msg) {
        customToast(msg);
    }


    @Override
    public void onAddEmployeeSuccess(String uid) {
        Toast.makeText(this, "them nhan vien thanh cong " + uid, Toast.LENGTH_SHORT).show();
//        replaceFragment(EmployeeListFragment.getInstanceLoadingDrawable(uid), EmployeeListFragment.TAG);
    }

    @Override
    public void onSelectOwnerButton() {
        // todo remove this line later
        addFragment(PickOwnerFragment.getInstance(), PickOwnerFragment.TAG);
    }

    @Override
    public void openSelectOwner() {
        addFragment(PickOwnerFragment.getInstance(), PickOwnerFragment.TAG);
    }

    @Override
    public void onFloatbuttonClick() {
        //replaceFragment(new RegisterEmployeeFragment(), RegisterEmployeeFragment.TAG);
    }

    @Override
    public void onSelectUser(Employee e) {

    }


    @Override
    public void openActivityEmployeeee() {
        navigator.startActivity(LoginActivity.this, HomeActivity.class, null);
    }

    @Override
    public void openActivityOwner() {
        navigator.startActivity(LoginActivity.this, OwnerActivity.class, null);

    }

    @Override
    public void openLoginScreen() {
        addFragment(LoginFragment.getInstance(), LoginFragment.TAG);
    }

    @Override
    public void onSignoutSuccess() {
        showToast(" dang ky thanh cong vui long dang nhap de su dung");
        navigator.startActivity(LoginActivity.this, LoginActivity.class, null);
        RB_Succeess_MSg("");
    }

    @Override
    public void onSignOutFalied(String msg) {

    }

    @Override
    public void showErrorUserScreen() {
        addFragment(LoginErrorFragment.getInstance(), LoginErrorFragment.TAG);
    }

    @Override
    public void showErrorUserScreen(String msg) {
        addFragment(LoginErrorFragment.getInstance(msg), LoginErrorFragment.TAG);
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {
        RB_Loading_MSg(msg);
    }

    @Override
    public void hideLoading() {
        dissmissSweetDialog();
    }

    @Override
    public void gotoLoginActivity() {
        Timber.tag(TAG).e("line 306");

    }

    @Override
    public void onRegisterEmployeeSucess() {
        mPresenter.signOut();
    }


//    @Override
//    public void onBackPressed() {
//       //super.onBackPressed();
//        a++;
//        if (a == 2) {
//
//            this.finish();
//            finish();
//            System.exit(0);
//        } else {
//            customToast(" Bấm thêm lần nữa để thoát ứng dụng ");
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    a = 0;
//                }
//            }, 2000);
//        }
//
//    }

    @Override
    public void ontest() {

    }

    @Override
    public void onSignOut() {
        replaceFragment(LoginFragment.getInstance(), LoginFragment.TAG);
    }

    @Override
    public void showErrorScreen(String msg) {
//        addFragment(LoginErrorFragment.getInstanceLoadingDrawable(msg), LoginErrorFragment.TAG);

        showAlertDialog(msg);
    }

}