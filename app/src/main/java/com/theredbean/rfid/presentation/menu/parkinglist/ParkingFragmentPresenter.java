package com.theredbean.rfid.presentation.menu.parkinglist;


import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.parking.ParkingUsercase;
import com.theredbean.rfid.models.Parking;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class ParkingFragmentPresenter implements ParkingFragmentContract.Presenter {
    public static final String TAG = ParkingFragmentPresenter.class.getSimpleName();


    ParkingFragmentContract.View mView;

    ParkingUsercase parkingUsercase;

    @Inject
    public ParkingFragmentPresenter(ParkingUsercase parkingUsercase) {
        this.parkingUsercase = parkingUsercase;
    }

    @Override
    public void takeView(ParkingFragmentContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        this.mView = null;
    }

    @Override
    public void getParkingList() {
        mView.showLoading();
        parkingUsercase.getParkings(new EzParkingCallback.GetParkingCallback() {

            @Override
            public void onGetParkings(List<Parking> parkings) {
                mView.ongetListParkingSuccessful(parkings);
                Timber.tag(TAG).e("parkings.toString()");

                mView.hideLoading();

            }

            @Override
            public void onGetParkingsEmpty() {
                Timber.tag(TAG).e("onGetParkingsEmpty");
                mView.hideLoading();
                mView.displayNoparking();

            }

            @Override
            public void onExceptionError(String msg) {
                Timber.tag(TAG).e("onExceptionError %s", msg);

                mView.hideLoading();
                mView.ongetListParkingFalied(msg);
            }

            @Override
            public void onExceptionAuthentication() {
                mView.ongetListParkingFalied(TAG + "onExceptionAuthentication");
            }
        });
    }
}
