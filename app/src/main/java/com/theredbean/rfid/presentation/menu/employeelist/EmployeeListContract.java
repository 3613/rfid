package com.theredbean.rfid.presentation.menu.employeelist;

import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Parking;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

import java.util.List;

public interface EmployeeListContract {

    interface View extends BaseView {
        void onGetListEmployeesOfOwnerSuccess(List<Employee> employees);

        void onGetListEmployeesOfOwnerFalied(String msg);

        void displayEmployee();

        void gotParkings(List<Parking> parking);
    }

    interface Presenter extends BasePresenter<EmployeeListContract.View> {
        void getListEmployeee(String udi);

        void getParkingsKey();
    }
}
