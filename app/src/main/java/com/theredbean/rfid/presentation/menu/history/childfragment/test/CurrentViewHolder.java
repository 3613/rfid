package com.theredbean.rfid.presentation.employee.menu.history.childfragment.test;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.theredbean.rfid.R;
import com.theredbean.rfid.data.db.record.CurrentRecord;
import com.theredbean.rfid.models.FCurrentRecord;

import timber.log.Timber;
import utils.GlideApp;
import utils.ProgressBarUtils;
import utils.MyUtils;
import utils.TimeUtils;


public class CurrentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private static final String TAG = CurrentViewHolder.class.getSimpleName();
    private int position;
    private CurrentRecord item;

    private OnCurrentViewHolderListener mListener;


    TextView txtEpc, txtTimeIn, txtPrice;
    ImageView iv;

    public interface OnCurrentViewHolderListener {
        void onClick(int position);

        void onLongClick(int position);
    }

    public CurrentViewHolder(View view, OnCurrentViewHolderListener listener) {
        super(view);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        mListener = listener;


        txtTimeIn = itemView.findViewById(R.id.txtTimeIn);
        iv = itemView.findViewById(R.id.iv);
        txtPrice = itemView.findViewById(R.id.txtPrice);


    }

    public void renderUi(Context context, int position, FCurrentRecord currentRecord) {
        this.position = position;

        txtTimeIn.setText(TimeUtils.getTimeText(currentRecord.getTimestampCheckIn()));

        Timber.tag(TAG).e(currentRecord.getPathImage());


        try {
            txtPrice.setText(itemView.getContext().getResources()
                    .getString(R.string.txt_price_bike, MyUtils.fomatVND(currentRecord.getPrice())));
        } catch (Exception e) {
            Timber.tag(TAG).e("nullll cmnr adapter roi");

        }

        Timber.tag(TAG).e("appcontextInstance1 %s", itemView.getContext().hashCode());
        Timber.tag(TAG).e("appcontextInstance---2 %s", context.hashCode());


        GlideApp.with(context)
                .load(currentRecord.getPathImage())
                .placeholder(ProgressBarUtils.getInstanceLoadingDrawable(context))
                .fallback(R.drawable.error_image)
                .error(R.drawable.error_image)
                .into(iv);

        Timber.tag(TAG).e("path la cai gi %s", currentRecord.getPathImage());

    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onClick(position);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mListener != null) {
            mListener.onLongClick(position);
        }
        return false;
    }
}