package com.theredbean.rfid.presentation.splash;

import com.theredbean.rfid.data.sharedpreference.ISharedPreferences;
import com.theredbean.rfid.di.ActivityScope;

import javax.inject.Inject;

import timber.log.Timber;
import utils.EzConstants;

@ActivityScope
public class IntroPresenter implements IntroContract.Presenter {
    public static final String TAG = IntroPresenter.class.getSimpleName();
    IntroContract.View view;
    ISharedPreferences sharedPreferences;

    @Inject
    public IntroPresenter(ISharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }


    @Override

    public void takeView(IntroContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        view = null;
    }

    @Override
    public void loadConfig() {
        view.onLoadConfigSuccess();
//        try {
//            int a = Integer.parseInt(sharedPreferences.readPreferences(EzConstants.KEY_PRICE_A_XE_GA_D, "-2"));
//            int b = Integer.parseInt(sharedPreferences.readPreferences(EzConstants.KEY_PRICE_B_XE_SO_D, "-2"));
//            int c = Integer.parseInt(sharedPreferences.readPreferences(EzConstants.KEY_PRICE_C_XE_DAP_D, "-2"));
//
//            int d = Integer.parseInt(sharedPreferences.readPreferences(EzConstants.KEY_PRICE_D_XE_GA_N, "-2"));
//            int e = Integer.parseInt(sharedPreferences.readPreferences(EzConstants.KEY_PRICE_E_XE_SO_N, "-2"));
//            int f = Integer.parseInt(sharedPreferences.readPreferences(EzConstants.KEY_PRICE_F_XE_DAP_N, "-2"));
//
//            EzConstants.PRICE_A_XE_GA_D = a;
//            EzConstants.PRICE_B_XE_SO_D = b;
//            EzConstants.PRICE_C_XE_DAP_D = c;
//            EzConstants.PRICE_D_XE_GA_N = d;
//            EzConstants.PRICE_E_XE_SO_N = e;
//            EzConstants.PRICE_F_XE_DAP_N = f;
//
//            // todo code xau, clean sau
//            // mac gia tri mac dinh thi bat ghi lai
//            if (a == -1 || b == -1 || c == -1 || d == -1 || e == -1 || f == -1 || a == -2 || b == -2 || c == -2 || d == -2 || e == -2 || f == -2) {
//                view.onLoadConFigFailed();
//            } else {
//                view.onLoadConfigSuccess();
//
//            }
//        } catch (Exception e) {
//            Timber.tag(TAG).e(e.getMessage());
//            view.onLoadConFigFailed();
//        }


    }


    @Override
    public void cameraPerrmission(Boolean status) {
    }


}
