package com.theredbean.rfid.presentation.login.lostpass;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;

public class LostPassFragment extends BaseFragment implements LostPassContract.View {

    LostPassContract.Presenter presenter;

    @Override
    protected void onAttachToContext(Context context) {

    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected void onMyCreateView(View view) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showLoading(String msg) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showAlertDialog(String msg) {

    }
}
