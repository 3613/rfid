package com.theredbean.rfid.presentation.menu.parkinglist;

import com.theredbean.rfid.models.Parking;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

import java.util.List;

public interface ParkingFragmentContract {

    interface View extends BaseView {
        void ongetListParkingSuccessful(List<Parking> parkings);

        void ongetListParkingFalied(String msg);

        void displayNoparking();
    }

    interface Presenter extends BasePresenter<ParkingFragmentContract.View> {
        void getParkingList();
    }
}
