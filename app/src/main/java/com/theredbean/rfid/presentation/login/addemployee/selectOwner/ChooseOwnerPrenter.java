package com.theredbean.rfid.presentation.login.addemployee.selectOwner;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.employee.EmployeeUsecase;
import com.theredbean.rfid.business.login.LoginUsecase;
import com.theredbean.rfid.business.owner.OwnerUsecase;
import com.theredbean.rfid.models.Owner;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class ChooseOwnerPrenter implements ChooseOwnerContract.Presenter {
    public static final String TAG = ChooseOwnerPrenter.class.getSimpleName();
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();
    ChooseOwnerContract.DialogView mView;

    EzParkManager userManager;
    OwnerUsecase ownerUsecase;
    EmployeeUsecase employeeUsecase;
    LoginUsecase loginUsecase;

    @Inject
    public ChooseOwnerPrenter(OwnerUsecase ownerUsecase, EmployeeUsecase employeeUsecase,
                              LoginUsecase loginUsecase, EzParkManager userManager) {

        Timber.tag(TAG).e("get Injjected ----- ");

        this.ownerUsecase = ownerUsecase;
        this.employeeUsecase = employeeUsecase;
        this.loginUsecase = loginUsecase;
        this.userManager = userManager;

//        Timber.tag(TAG).e(userManager.getEmployee().getName());
    }

    @Override
    public void takeView(ChooseOwnerContract.DialogView view) {
        mView = view;
    }


    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void testhaha() {

    }

    @Override
    public void getListOwner() {
        mView.showLoading();
        ownerUsecase.getOwners(new EzParkingCallback.GetOwnerCallback() {
            @Override
            public void onGetOwners(List<Owner> owners) {
                Timber.tag(TAG).e("numbers: %d - %s", owners.size() , owners.get(1).getName());
                mView.hideLoading();
                mView.onGetlistOwnerSuccess(owners);
            }

            @Override
            public void onGetOwnersFailed() {
                mView.hideLoading();
                mView.onGetlistOwnerFalied();
            }
        });
    }


}
