package com.theredbean.rfid.presentation.login.ownerlist.viewholder;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theredbean.rfid.R;
import com.theredbean.rfid.models.Owner;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by hoang on 09/04/2018 nhe.
 */
public class PickOwnerAdapter extends RecyclerView.Adapter<OwnerViewHolder> {
    private static final String TAG = PickOwnerAdapter.class.getSimpleName();
    private Activity mActivity;
    private List<Owner> mData;
    private LayoutInflater mLayoutInflater;
    OwnerViewHolder.OnOwnerListener mListener;


    public PickOwnerAdapter(Activity activity, OwnerViewHolder.OnOwnerListener mListener) {
        mLayoutInflater = LayoutInflater.from(activity);
        mActivity = activity;
        mData = null;
        this.mListener = mListener;
    }

    public void setData(List<Owner> data) {
        if (mData == null) {
            mData = new ArrayList<>();
        }
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OwnerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_list_owner, parent, false);
        return new OwnerViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(OwnerViewHolder holder, int position) {
        Owner entity = mData.get(position);
        holder.renderUi(position, entity);
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }


    private final OwnerViewHolder.OnOwnerListener listener = new OwnerViewHolder.OnOwnerListener() {
        @Override
        public void onItemClick(int position) {
            mListener.onItemClick(position);
        }

        @Override
        public void onItemLongClick(int position) {
            mListener.onItemLongClick(position);
        }

    };
}
