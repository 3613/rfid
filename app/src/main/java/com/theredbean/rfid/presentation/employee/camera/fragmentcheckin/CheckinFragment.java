package com.theredbean.rfid.presentation.employee.camera.fragmentcheckin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.hnam.uibutton.MyButton;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.theredbean.rfid.R;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.presentation.BaseActivity;
import com.theredbean.rfid.presentation.BaseFragment;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;
import utils.MyUtils;
import utils.RxUltils;

/**
 * Created by hoang on 20/03/2018.
 */

public class CheckinFragment extends BaseFragment implements CheckinContract.View {
    String path = "/data/user/0/com.theredbean.rfid/cache/prefix7637493726113363520.jpg";
    String deviceId = "1994";
    private CompositeDisposable compositeDisposable;


    public FragmentCheckInListener mListener;

    public interface FragmentCheckInListener {
        void onCancelCheckInButton();

        void onConfirmCheckInButton();

        void onCheckinSuccess();

        void onCheckinFaled(String msg);

        void onCheckinSuccessV2(String localPathImage, String targetPathImage);
    }

    @Inject
    CheckinContract.Presenter presenter;

    Context context;

    @BindView(R.id.ivResult)
    ImageView ivResult;
    @BindView(R.id.btnConfirm)
    MyButton btnconfirm;
    @BindView(R.id.btnCancel)
    MyButton btnCancel;


    public static final String TAG = CheckinFragment.class.getSimpleName();

    public static CheckinFragment getInstance(String deviceId, String path) {
        CheckinFragment fragment = new CheckinFragment();
        Bundle data = new Bundle();
        data.putString("DEVICE_ID", deviceId);
        data.putString("PATH", path);
        fragment.setArguments(data);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInjection();
        Timber.tag(TAG).e("onCreate");

        compositeDisposable = new CompositeDisposable();


        /// get path from Activity camera
        Bundle bundle = getArguments();
        if (bundle != null) {
            deviceId = bundle.getString("DEVICE_ID");
            path = bundle.getString("PATH");
        }
        Timber.tag(TAG).e("1-- " + deviceId);
        Timber.tag(TAG).e("1-- " + path);
//        presenter.showImage(path);
    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
        if (context instanceof FragmentCheckInListener) {
            mListener = (FragmentCheckInListener) context;
        } else {
            throw new ClassCastException(getActivity().getLocalClassName() + "is not cast CameraResult listener");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_check_in;
    }

    @Override
    protected void onMyCreateView(View view) {
        presenter.takeView(this);
        Timber.tag(TAG).e("onMyCreateView");
        Timber.tag(TAG).e(presenter.toString());

        try {
            presenter.showImage(path);

        } catch (Exception e) {
            Timber.tag(TAG).e("errors: %s", e);
        }


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void showFWarningMessage(String message) {
        super.showFWarningMessage(message);
    }

    @Override
    public void showFLoadingView(String msg) {
        super.showFLoadingView(msg);
    }

    @Override
    public void hideFLoadingView() {
        super.hideFLoadingView();
    }

    @Override
    public void showFWarningDialog(String msg) {
        super.showFWarningDialog(msg);
    }

    @OnClick(R.id.btnConfirm)
    public void onConfirmCheckin() {
        Timber.tag(TAG).e("kakaka %s, \n %s", path, deviceId);

        presenter.onlineCheckin(path, deviceId);
    }

    private int getBikeType() {
        // get later
        return MyUtils.typeOfCard(deviceId);
    }

    @OnClick(R.id.btnCancel)
    public void onCancelCheckin() {
        mListener.onCancelCheckInButton();
    }


    private void initInjection() {
        DaggerEzParkingComponent
                .builder()
                .applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
                .ezParkingModule(new EzParkingModule())
                .build()
                .inject(this);
    }

    @Override
    public void showMessage(String message) {
        RB_Error_MSg(message);
    }

    @Override
    public void showLoading() {
        RB_Loading_MSg("");
    }

    @Override
    public void showLoading(String msg) {
        RB_Loading_MSg(msg);
    }

    @Override
    public void hideLoading() {
        RB_hideDialog();
    }

    @Override
    public void showAlertDialog(String msg) {

    }

    @SuppressLint("CheckResult")
    @Override
    public void loadImage(String url) {
        //String my = "https://firebasestorage.googleapis.com/v0/b/ezpark-bike.appspot.com/o/0Pdk1jqJtMfSa6NWKjQBIQmET6E3%2Fparkingkey%2Fdata%2Fuser%2F0%2Fcom.theredbean.rfid%2Fcache%2Fprefix70005362.jpg?alt=media&token=d0b040e4-22df-4819-83b8-9b8a0bbc0830";
        Picasso.get()
                .load(new File(url))
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .placeholder(R.drawable.ic_adb_black_24dp).error(R.drawable.ic_error_outline_black_24dp)
                .into(ivResult);
        presenter.startCounter();

    }

    @Override
    public void oncheckInSuccess() {
        Timber.tag(TAG).e("oncheckInSuccess");
        mListener.onCheckinSuccess();
    }

    @Override
    public void onCheckInFalied(String msg) {
        Timber.tag(TAG).e("onCheckInFalied: " + msg);

        mListener.onCheckinFaled(msg);

    }

    @Override
    public void oncheckInSuccessV2(String localPathImage, String targetPathImage) {
        mListener.onCheckinSuccessV2(localPathImage, targetPathImage);
    }


    @Override
    public void disableButton() {
        btnconfirm.setClickable(false);

    }

    @Override
    public void onNextTimer(Long aLong) {
        Timber.tag(TAG).e(" %s", String.valueOf(aLong));

        btnconfirm.setText(String.format("Xe vào (%s)", String.valueOf(aLong)));

    }

    @Override
    public void onCompleteTimer() {
        onConfirmCheckin();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.takeView(this);
    }


    @Override
    public void onStop() {
        super.onStop();
        Timber.tag(TAG).e("onStop");

        if (compositeDisposable != null) {
            Timber.tag(TAG).e("onStop no null");

            compositeDisposable.dispose();
            compositeDisposable.clear();
        }
        presenter.dropView();
    }

}
