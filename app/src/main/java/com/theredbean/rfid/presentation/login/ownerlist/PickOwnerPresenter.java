package com.theredbean.rfid.presentation.login.ownerlist;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.employee.EmployeeUsecase;
import com.theredbean.rfid.business.login.LoginUsecase;
import com.theredbean.rfid.business.owner.OwnerUsecase;
import com.theredbean.rfid.models.Owner;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import timber.log.Timber;

public class PickOwnerPresenter implements PickOwnerContract.Presenter {
    public static final String TAG = PickOwnerFragment.class.getSimpleName();
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();
    PickOwnerContract.View mView;

    EzParkManager userManager;
    OwnerUsecase ownerUsecase;
    EmployeeUsecase employeeUsecase;
    LoginUsecase loginUsecase;

    @Inject
    public PickOwnerPresenter(OwnerUsecase ownerUsecase, EmployeeUsecase employeeUsecase,
                              LoginUsecase loginUsecase, EzParkManager userManager) {
        this.ownerUsecase = ownerUsecase;
        this.employeeUsecase = employeeUsecase;
        this.loginUsecase = loginUsecase;
        this.userManager = userManager;

        Timber.tag(TAG).e(userManager.getEmployee().getName());
    }

    @Override
    public void takeView(PickOwnerContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void getListOwner() {
        mView.showLoading();
        ownerUsecase.getOwners(new EzParkingCallback.GetOwnerCallback() {
            @Override
            public void onGetOwners(List<Owner> owners) {
                Timber.tag(TAG).e("numbers: %d", owners.size());
                mView.hideLoading();
                mView.onGetlistOwnerSuccess(owners);
            }

            @Override
            public void onGetOwnersFailed() {
                mView.hideLoading();
                mView.onGetlistOwnerFalied();
            }
        });
    }

    @Override
    public void saveEmployee(String owner_UID) {
        Timber.tag(TAG).e("owner_ID %s", owner_UID);
        mView.showLoading();
        Timber.tag(TAG ).e( "got user: %s: ",userManager.getEmployee().getName());

        if (mAuth == null || user == null) {
            mView.onSaveEmployeeFailed("null mAuthentication");
            return;
        } else {
            String employee_UID = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();
            employeeUsecase.saveEmployee(
                    employee_UID,
                    userManager.getEmployee().getName(),
                    user.getEmail(),
                    userManager.getEmployee().getDescription(),
                    owner_UID,
                    new
                            EzParkingCallback.SaveEmployeeCallback() {
                                @Override
                                public void onSaveEmployeeSuccess() {
                                    mView.hideLoading();
                                    mView.onSaveEmployeeSuccess();
                                }

                                @Override
                                public void onSaveEmployeeFailed(String msg) {
                                    mView.hideLoading();
                                    mView.onSaveEmployeeFailed(msg);
                                }

                                @Override
                                public void onExceptionError(String msg) {
                                    mView.hideLoading();
                                    mView.onSaveEmployeeFailed(msg);
                                }

                                @Override
                                public void onExceptionAuthentication() {
                                    mView.hideLoading();
                                    mView.onSaveEmployeeFailed("onExceptionAuthentication");
                                }
                            });
        }
    }

    @Override
    public void signOut() {
        loginUsecase.signOut(new EzParkingCallback.SignOutCallback() {
            @Override
            public void onSignOutSuccess() {

            }

            @Override
            public void onExceptionError(String msg) {

            }

            @Override
            public void onExceptionAuthentication() {

            }
        });
    }
}
