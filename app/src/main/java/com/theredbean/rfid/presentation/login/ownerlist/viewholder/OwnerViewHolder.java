package com.theredbean.rfid.presentation.login.ownerlist.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.theredbean.rfid.R;
import com.theredbean.rfid.models.Owner;


public class OwnerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
        View.OnLongClickListener {

    private int position;
    private Owner item;
    OwnerViewHolder.OnOwnerListener mListener;


    TextView txtUser;

    public interface OnOwnerListener {
        void onItemClick(int position);

        void onItemLongClick(int position);
    }

    public OwnerViewHolder(View view, OwnerViewHolder.OnOwnerListener listener) {
        super(view);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        mListener = listener;

        txtUser = itemView.findViewById(R.id.txtUser);

    }

    public void renderUi(int position, Owner entity) {
        this.position = position;
        txtUser.setText(String.valueOf(entity.getName()));
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onItemClick(position);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mListener != null) {
            mListener.onItemLongClick(position);
        }
        return false;
    }

}