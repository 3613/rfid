package com.theredbean.rfid.presentation.menu.history;

import com.theredbean.rfid.data.db.record.CurrentRecord;
import com.theredbean.rfid.data.db.record.HistoryRecord;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.models.FHistoryRecord;
import com.theredbean.rfid.presentation.BasePresenter;
import com.theredbean.rfid.presentation.BaseView;

import java.util.List;

public interface HistoryContract {

    interface View extends BaseView {
        void onGotListCurrent(List<FCurrentRecord> currentRecords);

        void onGotListHistory(List<FHistoryRecord> historyRecords);
    }

    interface Presenter extends BasePresenter<HistoryContract.View> {
        List<CurrentRecord> getCurrentParkingRecord();

        List<HistoryRecord> getTotalParckedRecord();

        // onOnline version


        void getListCurrent();

        void getlistHistory();

        void getTotalParckedRecord(long start, long end);
    }
}
