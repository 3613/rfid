package com.theredbean.rfid.business.image;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.models.Employee;

import java.io.File;

/**
 * Created by nampham on 4/9/18.
 */
public interface ImageUsecase {

    /**
     * upload image
     *
     * @param employee: object
     * @param file:     File object
     * @param fileName: file name with extension
     * @param callback
     */
    void uploadImage(Employee employee, File file, String fileName, EzParkingCallback.UploadCallback callback);


    /**
     * upload image
     *
     * @param image
     * @param callback
     */
    void uploadImage(@NonNull ImageModel image, EzParkingCallback.UploadCallback callback);

    /**
     * upload image
     *
     * @param employee: object
     * @param bitmap:   bitmap
     * @param callback
     */
    void uploadImage(Employee employee, Bitmap bitmap, EzParkingCallback.UploadCallback callback);

    /**
     * @param
     * @param
     * @param
     */

    void saveLocalPath(ImageModel image, EzParkingCallback.NewLocalPathCallBack callBack);

    /**
     * @param imageModel --> getLocal ---> getPathz
     */

    void updatelocalPath(ImageModel imageModel);

}
