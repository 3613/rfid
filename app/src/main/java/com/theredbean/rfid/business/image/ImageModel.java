package com.theredbean.rfid.business.image;

import org.parceler.Parcel;

/**
 * Created by nampham on 4/15/18.
 */
@Parcel
public class ImageModel {
    String employeeUuid;
    String parkingUuid;
    String ownerUuid;
    String path;
    String cardId;
    long timeIn;
    long timeOut;

    public long getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(long timeIn) {
        this.timeIn = timeIn;
    }

    public long getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(long timeOut) {
        this.timeOut = timeOut;
    }

    String localPath;

    public ImageModel() {
    }

    public String getEmployeeUuid() {
        return employeeUuid;
    }

    public void setEmployeeUuid(String employeeUuid) {
        this.employeeUuid = employeeUuid;
    }

    public String getParkingUuid() {
        return parkingUuid;
    }

    public void setParkingUuid(String parkingUuid) {
        this.parkingUuid = parkingUuid;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getOwnerUuid() {
        return ownerUuid;
    }

    public void setOwnerUuid(String ownerUuid) {
        this.ownerUuid = ownerUuid;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    @Override
    public String toString() {
        return "ImageModel{" +
                "employeeUuid='" + employeeUuid + '\'' +
                ", parkingUuid='" + parkingUuid + '\'' +
                ", ownerUuid='" + ownerUuid + '\'' +
                ", path='" + path + '\'' +
                ", cardId='" + cardId + '\'' +
                '}';
    }
}
