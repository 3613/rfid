package com.theredbean.rfid.business.income;

import android.annotation.SuppressLint;
import android.util.Log;
import android.util.TimeUtils;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.theredbean.rfid.business.BaseUsecase;
import com.theredbean.rfid.business.EzParkUtils;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.model.CurrentIncome;
import com.theredbean.rfid.configure.FirebaseConstant;
import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.IncomModel;
import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.models.Parking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import durdinapps.rxfirebase2.RxFirebaseChildEvent;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.functions.Consumer;
import timber.log.Timber;

/**
 * Created by nampham on 4/12/18.
 */
@ActivityScope
public class IncomeUsecaseImpl extends BaseUsecase implements IncomeUsecase {
    private static final String TAG = IncomeUsecaseImpl.class.getSimpleName();

    @Inject
    public IncomeUsecaseImpl() {

    }

    @Override
    public void getTotayIncome(Owner owner, List<Parking> parkings, EzParkingCallback.IncomeCallback callback) {

        String todayTimeStampe = String.valueOf(EzParkUtils.generateKeyTimeStampe());
        mRootDB.child(FirebaseConstant.KEY_OWNER)
                .child(String.valueOf(todayTimeStampe))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<CurrentIncome> result = new ArrayList<>();
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            long income = (long) ds.getValue();
                            String parkingUuid = ds.getKey();
                            result.add(new CurrentIncome(parkingUuid, income));
                        }
                        callback.onIncomeTodayForAllParks(result);


                    }


                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        callback.onExceptionError(databaseError.getMessage());
                    }
                });
    }

    @Override
    public void saveFee(Employee employee, int type) {
        String ownerUuid = employee.getOwnerKey();
        String parking = employee.getParkingKey();
        String todayTimeStampe = String.valueOf(EzParkUtils.generateKeyTimeStampe());
        DatabaseReference mRef = mRootDB.child(FirebaseConstant.KEY_INCOME)
                .child(ownerUuid)
                .child(todayTimeStampe)
                .child(parking);
        mRef.keepSynced(true);
        mRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue() != null) {
                    long currentTotal = (long) mutableData.getValue();
                    currentTotal += type * 1000;
                    Timber.tag(TAG).e("currentTotal %d --type %d,", currentTotal, type);

                    mutableData.setValue(currentTotal);
                } else {
                    long value = type * 1000;
                    mutableData.setValue(value);
                    Timber.tag(TAG).e("currentTotal value %d--type %d,", value, type);


                }
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    long total = (long) dataSnapshot.getValue();
                    Timber.tag(TAG).e("final value: %d", total);
                }
            }
        });
    }

    @Override
    public void saveFeeV2(Employee employee, int price) {
        String ownerUuid = employee.getOwnerKey();
        String parking = employee.getParkingKey();
        String todayTimeStampe = String.valueOf(EzParkUtils.generateKeyTimeStampe());
        DatabaseReference mRef = mRootDB.child(FirebaseConstant.KEY_INCOME)
                .child(ownerUuid)
                .child(todayTimeStampe)
                .child(parking);
        mRef.keepSynced(true);
        mRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.getValue() != null) {
                    long currentTotal = (long) mutableData.getValue();
                    currentTotal += price;
                    Timber.tag(TAG).e("currentTotal %d --type %d,", currentTotal, price);

                    mutableData.setValue(currentTotal);
                } else {
                    long value = price;
                    mutableData.setValue(value);
                    Timber.tag(TAG).e("currentTotal value %d--type %d,", value, price);


                }
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    long total = (long) dataSnapshot.getValue();
                    Timber.tag(TAG).e("final value: %d", total);
                }
            }
        });
    }

    int i = 0;
    int j = 0;
    int z = 0;
    int totalPrice = 0;
    int sumPrice = 0;
    int x = 0;
    int y = 0;

    @SuppressLint("CheckResult")
    @Override
    public void getIncomeWithTimeMilis(long from, long to, String ownerKey,
                                       List<Parking> parks,
                                       EzParkingCallback.IncomeDetailCallBack callBack) {
        Query query = mRootDB
                .child(FirebaseConstant.KEY_INCOME)
                .child(ownerKey)
                .orderByKey().startAt(String.valueOf(from))
                .endAt(String.valueOf(String.valueOf(to)));
        RxFirebaseDatabase.observeChildEvent(query)
                .subscribe(new Consumer<RxFirebaseChildEvent<DataSnapshot>>() {
                    @Override
                    public void accept(RxFirebaseChildEvent<DataSnapshot> dataSnapshotRxFirebaseChildEvent) throws Exception {
                        Timber.tag(TAG).e("myHIHI1111 %d--> %s", x++, dataSnapshotRxFirebaseChildEvent.getKey());

                    }
                });
        // Income--> [OwnerKey]
        mRootDB
                .child(FirebaseConstant.KEY_INCOME)
                .child(ownerKey)
                .orderByKey().startAt(String.valueOf(from))
                .endAt(String.valueOf(String.valueOf(to)))
                .addValueEventListener(new ValueEventListener() {
                    @SuppressLint("LogNotTimber")
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        HashMap<String, Integer> priceMap = new HashMap<>();
                        if (!dataSnapshot.exists()) {
                            Timber.tag(TAG).e("DATA else");
                            String notify = String.format("Không có dữ liệu từ \n %s  \nđến %s ",
                                    utils.TimeUtils.getTimeText(from),
                                    utils.TimeUtils.getTimeText(to));
                            callBack.onloadPriceFaied(notify);
                            return;
                        }
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            for (DataSnapshot ds2 : ds.getChildren()) {
                                if (!ds2.exists()) return;
                                int price = ds2.getValue(Integer.class);
                                if (priceMap.containsKey(ds2.getKey())) {
                                    int p = priceMap.get(ds2.getKey());
                                    p += ds2.getValue(Integer.class);
                                    // update
                                    priceMap.put(ds2.getKey(), p);

                                } else {
                                    int p2 = ds2.getValue(Integer.class);
                                    priceMap.put(ds2.getKey(), p2);
                                }
                                i++;
                                Timber.tag(TAG).e("DATA2244----key so %d %s %d", i, ds2.getKey(), price);
                                totalPrice += price;
                            }

                        }
                        Timber.tag(TAG).e("total price %d: ", totalPrice);
                        Timber.tag(TAG).e("total price %s: ", priceMap.toString());
                        // result :
                            /*
                            * total price 848012:
                                total price {-LByog73CligakaBsmxQ=2000,
                                -LDLTKsWDBMcE3NrRQOE=24000,
                                -LD2KC_4JxOQQ6l1-XHW=93000,
                                -LCZt2Hnt19vhwwWlrxw=57000,
                                -LDLnhE8HajWOQw53yf1=0, -
                                LBvD1oN1jXCvwNeYFj_=672012}: */

                        // todo: map lai thanh 1 object, truyen qua adapter lam viec
                        List<IncomModel> incomModelList = new ArrayList<>();
                        Set mapSet = (Set) priceMap.entrySet();
                        for (Object aMapSet : mapSet) {

                            Map.Entry mapEntry = (Map.Entry) aMapSet;
                            String parkingKey = (String) mapEntry.getKey();
                            int parkingPrice = (int) mapEntry.getValue();
                            Timber.tag(TAG).e("myKey %s  = myvalue %d", parkingKey, parkingPrice);
                            for (Parking park : parks) {
                                // chỉ thống kê bãi nào đang tồn tại
                                if (park.getUuid().equals(parkingKey)) {
                                    String parkingName = park.getName();
                                    incomModelList.add(new IncomModel(parkingPrice, parkingName, parkingKey));
                                    sumPrice += parkingPrice;
                                }  // những bãi không tồn tại sẽ không có parkingName //                                        incomModelList.add(new IncomModel(parkingPrice, parkingKey));


                            }
                        }
                        callBack.onLoadPriceSuccesss(incomModelList, sumPrice);
                        Timber.tag(TAG).e("myArrays = %s", incomModelList.toString());
                        Timber.tag(TAG).e("myArrays size = %d", incomModelList.size());
                        Timber.tag(TAG).e("total price result %d: ", sumPrice);
                        sumPrice = 0;
                        Timber.tag(TAG).e("total price result22 %d: ", sumPrice);


                    }

                    @SuppressLint("LogNotTimber")
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e("TESTZ2", "loi");
                        callBack.onloadPriceFaied(databaseError.getMessage());
                    }
                });


    }

}
