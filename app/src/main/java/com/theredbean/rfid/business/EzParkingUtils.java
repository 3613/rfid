package com.theredbean.rfid.business;

import java.util.Calendar;

/**
 * Created by nampham on 4/10/18.
 */
public class EzParkingUtils {

    public static String generaterImageName(String userUuid) {
        String prefix = userUuid.substring(0, 5);
        String suffix = String.valueOf(Calendar.getInstance().getTimeInMillis());
        String extension = "jpg";
        return String.format("%s_%s.%s", prefix, suffix, extension);
    }
}
