package com.theredbean.rfid.business.owner;

import com.theredbean.rfid.business.EzParkingCallback;

/**
 * Created by nampham on 4/8/18.
 */
public interface OwnerUsecase {

    void getOwners(EzParkingCallback.GetOwnerCallback callback);
}
