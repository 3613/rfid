package com.theredbean.rfid.business;

/**
 * Created by nampham on 4/7/18.
 */
public interface BaseEzCallback {
    void onExceptionError(String msg);
    void onExceptionAuthentication();
}
