package com.theredbean.rfid.business.checkcard;

import android.support.annotation.NonNull;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.theredbean.rfid.business.BaseUsecase;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.configure.FirebaseConstant;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.models.FHistoryRecord;
import com.theredbean.rfid.models.Pricez.FPrice;
import com.theredbean.rfid.rxbus.RxBus;

import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;

import timber.log.Timber;
import utils.EzConstants;
import utils.MyUtils;
import utils.PriceCalculator;

/**
 * Created by nampham on 4/10/18.
 */
@ActivityScope
public class CheckCardUsecaseImpl extends BaseUsecase implements CheckCardUsecase {
    public static final String TAG = CheckCardUsecaseImpl.class.getSimpleName();
    public static final String TAG_CHECKIN = CheckCardUsecaseImpl.class.getSimpleName() + "--Checkin---222";
    public static final String TAG_CHECKIN_CD = CheckCardUsecaseImpl.class.getSimpleName() + "--Checkin---111";
    public static final String TAG_CHECKOUT = CheckCardUsecaseImpl.class.getSimpleName() + "--Checkout";
    public static final String TAG_CHECKOUT_REMOVE = CheckCardUsecaseImpl.class.getSimpleName() + "--Checkout---remove";

    int aaa = 0;
    int bbb = 0;
    int ccc = 0;
    int ddd = 0;
    int eee = 0;
    int fff = 0;
    int ggg = 0;
    int hhh = 0;
    int reset = 9;

    private IDatabase iDatabase;
    RxBus rxBus;

    @Inject
    public CheckCardUsecaseImpl(@Named("realm") IDatabase iDatabase, RxBus rxBus) {
        this.iDatabase = iDatabase;
        Timber.tag(TAG).e("Idatabase %s", iDatabase.toString());
        this.rxBus = rxBus;

    }


    @Override
    public void isCardExisting(Employee employee, String cardId, EzParkingCallback.CheckCardExisting callback) {
        if (mAuth == null || mAuth.getCurrentUser() == null) {
            callback.onExceptionAuthentication();
        }
        if (MyUtils.typeOfCard(cardId)  == EzConstants.BikeType.XE_TAO_LAO){
            callback.onExceptionError("Thẻ này không phải của bãi xe");
            return;
        }
        String ownerUuid = employee.getOwnerKey();
        String parkingUuid = employee.getParkingKey();
        mRootDB.keepSynced(true);


        DatabaseReference r =
                mRootDB.child(FirebaseConstant.KEY_CURRENT)
                        .child(ownerUuid)
                        .child(parkingUuid)
                        .child(cardId);

        r.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    FCurrentRecord currentRecord = dataSnapshot.getValue(FCurrentRecord.class);
                    callback.onIsCardExist(currentRecord);
                    Timber.tag(TAG).e("onDataChange-----tren");

                } else {
                    Timber.tag(TAG).e("-----duoi");
                    r.removeEventListener(this);
                    r.removeEventListener(this);
                    callback.onNoCard();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Timber.tag(TAG).e("onCancelled ");

            }
        });

    }

    @Override
    public void checkIn(FPrice price, Employee employee, String cardId, String path, EzParkingCallback.CheckInCardCallback callback) {
        if (mAuth == null || mAuth.getCurrentUser() == null) {
            callback.onExceptionAuthentication();
        }

        String ownerUuid = employee.getOwnerKey();
        String parkingUuid = employee.getParkingKey();

        FCurrentRecord record = new FCurrentRecord();
        record.setCardId(cardId);
        record.setEmployeeKey(employee.getUuid());
        record.setPathImage("x");
        record.setCachePath(path);
        record.setType(MyUtils.typeOfCard(cardId));
        record.setPrice(PriceCalculator.priceOf(record.getType(), price));

        Timber.tag(TAG).e("myPricezzz %d",PriceCalculator.priceOf(record.getType(), price));


        Timber.tag(TAG).e("setLocalpath@checkIn %s,", path);

        long checkinTimez = Calendar.getInstance().getTimeInMillis();
        Timber.tag(TAG).e("checkinTimez tren %d,", checkinTimez);

        record.setTimestampCheckIn(checkinTimez); // time is current in milisecons
        record.setType(MyUtils.typeOfCard(cardId));

        Timber.tag(TAG).e("check type %d", MyUtils.typeOfCard(cardId));


        String checkinPath = String.format("%s/%s/%s/%s/",
                FirebaseConstant.KEY_CURRENT,
                ownerUuid, parkingUuid, cardId);

        Timber.tag(TAG).e("xemxem checkinPath %s", checkinPath);


        mRootDB.child(checkinPath).addChildEventListener(checkInListener(record, checkinPath, callback));
        mRootDB.child(FirebaseConstant.KEY_CURRENT)
                .child(ownerUuid)
                .child(parkingUuid)
                .child(cardId)
                .setValue(record);
    }


    private ChildEventListener checkInListener(
            FCurrentRecord currentRecord, String target,
            EzParkingCallback.CheckInCardCallback callback) {
        return new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (aaa == 0) {
                    aaa++;
                    try {
                        callback.onCheckInCardSuccessV2(currentRecord);

                    } catch (Exception e) {
                        Timber.tag(TAG_CHECKIN).e("==== exxeption");
                        callback.onCheckInCardFailed(e.getMessage());

                    }
                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Timber.tag(TAG_CHECKIN).e("onChildChanged %s, \t %s\t %s", dataSnapshot.getKey(), dataSnapshot.getValue(), s);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                bbb++;
                Timber.tag(TAG_CHECKIN).e("onChildRemoved number- bbb: %d \t %s, \t", bbb, dataSnapshot.getKey());

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//                Timber.tag(TAG_CHECKIN).e("onChildMoved %s, \t %s \t", dataSnapshot.getKey(), s, dataSnapshot.getValue());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Timber.tag(TAG_CHECKIN).e("onCancelled" + databaseError.getMessage());

            }
        };

    }


    @Override
    public void checkOut(@NonNull FPrice price, @NonNull Employee employee,
                         @NonNull FCurrentRecord record, String cardId,
                         EzParkingCallback.CheckOutCardCallback callback) {
        if (mAuth == null || mAuth.getCurrentUser() == null) {
            callback.onExceptionAuthentication();
        }

        String ownerUuid = employee.getOwnerKey();
        String parkingUuid = employee.getParkingKey();
        //save history
        FHistoryRecord history = new FHistoryRecord();
        long checkinTimez = record.getTimestampCheckIn();
        Timber.tag(TAG).e("checkinTimez tren %d,", checkinTimez);

        history.setEmployeeKeyCheckIn(record.getEmployeeKey());
        history.setTimestampCheckIn(checkinTimez);
        history.setPathImage(record.getPathImage());
        history.setType(record.getType());
        //check out info
        //todo
        history.setPrice(PriceCalculator.priceOf(record.getType(), price)); //configure again
        history.setEmployeeKeyCheckOut(employee.getUuid());
        long timeCheckout = Calendar.getInstance().getTimeInMillis();
        history.setTimestampCheckOut(timeCheckout);

        String checkoutCardName = String.format(Locale.US, "%s_%d", cardId, timeCheckout
        );
        history.setCardId(checkoutCardName);

        String keyUuid = String.format(Locale.US,
                "%s_%d", cardId,
                history.getTimestampCheckOut());

        String outpath = String.format("%s/%s/%s/%s",
                FirebaseConstant.KEY_HISTORY,
                ownerUuid,
                parkingUuid,
                "");

        String historyPath = String.format("%s/%s/%s/%s",
                FirebaseConstant.KEY_HISTORY,
                ownerUuid,
                parkingUuid,
                keyUuid);


        String pathCardDelete = String.format("%s/%s/%s/%s",
                FirebaseConstant.KEY_CURRENT,
                ownerUuid,
                parkingUuid,
                cardId);

        String onlinePathtarget = String.format("%s/%s"
                , historyPath,
                FirebaseConstant.PARAM_PATH_IMAGE);


        if (iDatabase.isPendingObjectExist(history, record.cardId)) {

            // todo update and remove
            iDatabase.updateCurrentToHistory(record.getCardId(), history);
            // maybe need callback over here
            mRootDB.child(historyPath)
                    .setValue(history);

            Timber.tag(TAG).e("update ---- remove and update");


        } else {
            // todo remove only
            mRootDB.child(historyPath)
                    .setValue(history);
            Timber.tag(TAG).e("update ----  remove only");

        }


        // this is listener for move Current to History Object
        mRootDB.child(outpath)
                .addChildEventListener(moveCurrentToHistoryListener(cardId, pathCardDelete, onlinePathtarget, history));

        // this is listener for delete action
        mRootDB.child(pathCardDelete).addChildEventListener(deleteCurrentListener(history, cardId, callback));


    }

    private ChildEventListener deleteCurrentListener(FHistoryRecord history, String cardid, EzParkingCallback.CheckOutCardCallback callback) {
        return new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ccc++;
//                Timber.tag(TAG_CHECKOUT).e("onChildAdded number- ccc: %d \t%s, \t %s \t", ccc, dataSnapshot.getKey(), dataSnapshot.getValue(), s);


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Timber.tag(TAG_CHECKOUT_REMOVE).e("onChildChanged %s, \t %s\t %s", dataSnapshot.getKey(), dataSnapshot.getValue(), s);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if (ddd == 0) {
                    ddd++;
                    try {//                        callback.onCheckOutCardSuccess();
                        callback.onCheckOutCardSuccessV2(cardid, "just a path" + ggg++);
                        Timber.tag(TAG_CHECKOUT_REMOVE).e("onChildRemoved number- ddd: %d \t %s, \t %s", ddd, dataSnapshot.getKey(), dataSnapshot.getValue());

                    } catch (Exception e) {
                        Timber.tag(TAG_CHECKOUT_REMOVE).e("---- exeption cmnr");

                    }
                }


            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Timber.tag(TAG_CHECKOUT_REMOVE).e("onChildMoved %s, \t %s \t %s", dataSnapshot.getKey(), s, dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Timber.tag(TAG_CHECKOUT_REMOVE).e("onCancelled" + databaseError.getMessage());
                callback.onCheckOutCardFailed(databaseError.getMessage());

            }
        };
    }

    private ChildEventListener moveCurrentToHistoryListener(String currentCardid, String pathCardDeletez, String onlinePathtarget, FHistoryRecord history) {
        return new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (eee == 0) {
                    eee++;
                    Timber.tag(TAG_CHECKOUT).e("onChildAdded  number- eee: %d t %s, \t %s \t %s" , eee, dataSnapshot.getKey(), dataSnapshot.getValue(), s);
                    Timber.tag(TAG).e("-=----- call delete over here");
                    mRootDB.child(pathCardDeletez).removeValue();
                    Timber.tag(TAG).e("onlinePathtarget %s", onlinePathtarget);


                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Timber.tag(TAG_CHECKOUT).e("onChildChanged %s, \t %s\t %s", dataSnapshot.getKey(), dataSnapshot.getValue(), s);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Timber.tag(TAG_CHECKOUT).e("onChildRemoved %s, \t %s", dataSnapshot.getKey(), dataSnapshot.getValue());

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Timber.tag(TAG_CHECKOUT).e("onChildMoved %s, \t %s \t  %s", dataSnapshot.getKey(), s, dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Timber.tag(TAG_CHECKOUT).e("onCancelled" + databaseError.getMessage());

            }
        };
    }


    @Override
    public void checkinV2(Employee employee, String cardID, String path, EzParkingCallback.CheckInCardCallback cardCallback) {

    }

    @Override
    public void checkOutV2(FPrice price, Employee employee, FCurrentRecord record, String cardId, EzParkingCallback.CheckOutCardCallback callback) {

    }
}
