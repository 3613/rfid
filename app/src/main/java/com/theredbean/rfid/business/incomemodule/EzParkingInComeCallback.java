package com.theredbean.rfid.business.incomemodule;

import com.theredbean.rfid.business.model.CurrentIncome;

/**
 * Created by nampham on 4/12/18.
 */
public interface EzParkingInComeCallback {
    void onInComeToday(CurrentIncome currentIncome);
    void onExeptionError(String msg);
}
