package com.theredbean.rfid.business.image;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hoang on 11/05/2018 nhe.
 */
public class PendingImageModel {
    private String localPath;
    private String pathToCard;
    private Map<String, String> pendingMaps = new HashMap<String, String>();


    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getPathToCard() {
        return pathToCard;
    }

    public void setPathToCard(String pathToCard) {
        this.pathToCard = pathToCard;
    }

    public Map<String, String> getPendingMaps() {
        return pendingMaps;
    }

    public void setPendingMaps(Map<String, String> pendingMaps) {
        this.pendingMaps = pendingMaps;
    }


    public void toMapQueue(String localpat, String pathToCard) {
        this.pendingMaps.put(localpat, pathToCard);
    }

}
