package com.theredbean.rfid.business.login;

import com.theredbean.rfid.business.EzParkingCallback;

/**
 * Created by nampham on 4/7/18.
 */
public interface LoginUsecase {
    //login with email vs pass
    void loginWithEmailAndPass(String email, String password, EzParkingCallback.LoginCallback callback);

    void saveOwnerProfileToDatabase(EzParkingCallback.SaveOwnerCallBack callBack);

    void checkLogged(EzParkingCallback.CheckLoggedCallback callback);

    void signOut(EzParkingCallback.SignOutCallback callback);
}
