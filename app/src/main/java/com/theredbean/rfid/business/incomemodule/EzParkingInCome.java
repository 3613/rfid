package com.theredbean.rfid.business.incomemodule;

import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.models.Parking;

import java.util.List;

/**
 * Created by nampham on 4/12/18.
 */
public interface EzParkingInCome {
    void registerCallback(EzParkingInComeCallback callback);
    void registerListenerChangeFromParks(Owner owner, List<Parking> parkings);
    void unregisterListenerChangeFromParks();
}
