package com.theredbean.rfid.business.imageV2;

import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.image.ImageModel;
import com.theredbean.rfid.rxbus.penz.PendingObject;

/**
 * Created by hoang on 16/05/2018 nhe.
 */
public interface UploadUsecase {

    /**
     * @param pendingObject
     */
    void uploadImgCurrent(PendingObject pendingObject, EzParkingCallback.UploadPendingCallback callback);


    void uploadImgHistory(PendingObject o,EzParkingCallback.UploadPendingCallback callback);
}
