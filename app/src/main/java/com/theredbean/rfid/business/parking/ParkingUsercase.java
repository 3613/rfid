package com.theredbean.rfid.business.parking;

import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Parking;

/**
 * Created by nampham on 4/7/18.
 */
public interface ParkingUsercase {
    //register bai xe
    void addParking(String name, String address,
                    String description, EzParkingCallback.AddParkingCallback callback);

    //get danh sach bai xe
    void getParkings(EzParkingCallback.GetParkingCallback callback);

    //gan nhan vien vao bai gui xe
    void updateParkingForEmployee(Employee employee,
                                  Parking parking,
                                  EzParkingCallback.UpdateParkingForEmployeeCallback callback);

    //kiem tra nhan vien do co thuoc bai xe nao khong
    void checkEmployeeInWork(Employee employee, EzParkingCallback.CheckEmployeeInWorkCallback callback);
}
