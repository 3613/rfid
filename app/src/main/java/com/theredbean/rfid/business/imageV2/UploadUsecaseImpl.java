package com.theredbean.rfid.business.imageV2;

import android.annotation.SuppressLint;
import android.net.Uri;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theredbean.rfid.business.BaseUsecase;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.EzParkingUtils;
import com.theredbean.rfid.configure.FirebaseConstant;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.rxbus.penz.PendingObject;

import java.io.File;

import javax.inject.Inject;

import durdinapps.rxfirebase2.RxFirebaseStorage;
import rx.functions.Action1;
import timber.log.Timber;
import utils.RxUltils;

/**
 * Created by hoang on 16/05/2018 nhe.
 */
@SuppressLint("CheckResult")

public class UploadUsecaseImpl extends BaseUsecase implements UploadUsecase {
    public static final String TAG = UploadUsecaseImpl.class.getSimpleName();

    IDatabase iDatabase;

    @Inject
    public UploadUsecaseImpl(IDatabase iDatabase) {
        this.iDatabase = iDatabase;
    }


    @Override
    public void uploadImgCurrent(PendingObject pendingObject, EzParkingCallback.UploadPendingCallback callback) {
        /// rx
        File file2 = new File(pendingObject.getLocalPath());
        Uri uri2 = Uri.fromFile(file2);
        String name2 = EzParkingUtils.generaterImageName(pendingObject.getEmployeeUuid());

        StorageReference sf2 = getParkingRef(pendingObject.getOwnerUuid(), pendingObject.getParkingUuid(), name2);
        RxFirebaseStorage.putFile(sf2, uri2)
                .subscribe(taskSnapshot -> {
                    Timber.tag(TAG).e("CS : localpath in uploadImgCurrent %s,", pendingObject.getLocalPath());

                    String localPath = pendingObject.getLocalPath();
                    savePathCurrent(pendingObject, taskSnapshot.getDownloadUrl().toString(), localPath);
                    callback.onUploadPendingSuccess(pendingObject);
                }, throwable -> {
                    // nothing to do
                });

        ///manual wy


        //File file = new File(model.getPath());
//        Timber.tag(TAG).e(">>>> : " + pendingObject.getLocalPath());
//        File file = new File(pendingObject.getLocalPath());
//        Uri uri = Uri.fromFile(file);
//
//
//        String name = EzParkingUtils.generaterImageName(pendingObject.getEmployeeUuid());
//        StorageReference sf = getParkingRef(pendingObject.getOwnerUuid(), pendingObject.getParkingUuid(), name);
//        sf.putFile(uri).addOnSuccessListener(taskSnapshot -> {
//            Uri uri1 = taskSnapshot.getDownloadUrl();
//            if (uri1 != null) {
//                //todo
//                Timber.tag(TAG).e("CS : localpath in uploadImgCurrent %s,", pendingObject.getLocalPath());
//
////update localpath and uploadedUrl path to card
//                String localPath = pendingObject.getLocalPath();
//                savePathCurrent(pendingObject, uri1.toString(), localPath);
//
//                callback.onUploadPendingSuccess(pendingObject);
//                // remove Pending Object , callback --> service
//                //callback.onUploadSuccess(uri1.toString());
//            } else {
////                callback.onUploadFailed("Uploaded failed. Please try again");
//            }
//        });
    }


    @Override
    public void uploadImgHistory(PendingObject pendingObject, EzParkingCallback.UploadPendingCallback callback) {
        //File file = new File(model.getPath());
        Timber.tag(TAG).e(">>>> enjoy uploadImgHistory  : " + pendingObject.getLocalPath());
        File file = new File(pendingObject.getLocalPath());
        Uri uri = Uri.fromFile(file);


        String name = EzParkingUtils.generaterImageName(pendingObject.getEmployeeUuid());
        StorageReference sf = getParkingRef(pendingObject.getOwnerUuid(), pendingObject.getParkingUuid(), name);
        sf.putFile(uri).addOnSuccessListener(taskSnapshot -> {
            Uri uri1 = taskSnapshot.getDownloadUrl();
            if (uri1 != null) {
                pendingObject.setCardId(pendingObject.getCardId());
                //todo
                Timber.tag(TAG).e("CS : localpath in uploadImgHistory %s, -- card %s", pendingObject.getLocalPath(), pendingObject.getCardId());

//update localpath and uploadedUrl path to card
                String localPath = pendingObject.getLocalPath();
                savePathHstoryi(pendingObject, uri1.toString(), localPath);

                callback.onUploadPendingSuccess(pendingObject);
                // remove Pending Object , callback --> service
                //callback.onUploadSuccess(uri1.toString());
            } else {
                Timber.tag(TAG).e("dlkjsaldkajsdlkjsadlksajdlkjsa else cmnr");

//                callback.onUploadFailed("Uploaded failed. Please try again");
            }
        });
    }

    private void savePathHstoryi(PendingObject model, String s, String localPath) {
        Timber.tag(TAG).e("dlkjsaldkajsdlkjsadlksajdlkjsa");

        String ownerUuid = model.getOwnerUuid();
        String parkingUuid = model.getParkingUuid();
        String cardId = model.getCardId();
        mRootDB.child(FirebaseConstant.KEY_HISTORY)
                .child(ownerUuid)
                .child(parkingUuid)
                .child(cardId)
                .child(FirebaseConstant.PARAM_PATH_IMAGE)
                .setValue(s);

    }

    private void savePathCurrent(PendingObject model, String s, String localPath) {
        String ownerUuid = model.getOwnerUuid();
        String parkingUuid = model.getParkingUuid();
        String cardId = model.getCardId();
        mRootDB.child(FirebaseConstant.KEY_CURRENT)
                .child(ownerUuid)
                .child(parkingUuid)
                .child(cardId)
                .child(FirebaseConstant.PARAM_PATH_IMAGE)
                .setValue(s);
        mRootDB.child(FirebaseConstant.KEY_CURRENT)
                .child(ownerUuid)
                .child(parkingUuid)
                .child(cardId)
                .child(FirebaseConstant.PARAM_PATH_LOCAL)
                .setValue(localPath);

    }

}
