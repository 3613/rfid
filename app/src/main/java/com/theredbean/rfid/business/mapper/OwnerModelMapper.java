package com.theredbean.rfid.business.mapper;

import com.theredbean.rfid.business.model.OwnerModel;
import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.models.Parking;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by nampham on 4/12/18.
 */
public class OwnerModelMapper {

    public OwnerModel map(Owner input) {
        OwnerModel output = null;
        if (input != null) {
            output = new OwnerModel();
            output.setOwner(input);
            if (input.getParkings() == null){
                output.setParks(new ArrayList<>());
            } else {
                List<Parking> parks = new ArrayList<>();
                for (Map.Entry<String, Parking> entry : input.getParkings().entrySet()){
                    Parking p = entry.getValue();
                    p.setUuid(entry.getKey());
                    parks.add(p);
                }
                output.setParks(parks);
            }

        }
        return output;
    }

    public List<OwnerModel> map(List<Owner> list) {
        List<OwnerModel> result = new ArrayList<>();
        for (Owner input : list) {
            OwnerModel output = map(input);
            if (output != null) {
                result.add(output);
            }
        }
        return result;
    }
}
