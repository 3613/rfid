package com.theredbean.rfid.business.income;

import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.models.Parking;

import java.util.List;

/**
 * Created by nampham on 4/12/18.
 */
public interface IncomeUsecase {

    void getTotayIncome(Owner owner, List<Parking> parkings, EzParkingCallback.IncomeCallback callback);

    //get Income parking with from - to

    //get Income parkings with from - to

    void saveFee(Employee employee, int type);

    void saveFeeV2(Employee employee, int type);

    // summary income


    void getIncomeWithTimeMilis(long from, long to,
                                String ownerkey, List<Parking> parks,
                                EzParkingCallback.IncomeDetailCallBack callBack);

}
