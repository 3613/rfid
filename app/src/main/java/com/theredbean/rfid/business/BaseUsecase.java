package com.theredbean.rfid.business;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Created by nampham on 4/7/18.
 */
public class BaseUsecase {
    private static final String TAG = BaseUsecase.class.getSimpleName();


    protected final FirebaseAuth mAuth;
    protected final FirebaseDatabase mFireDB;
    protected final DatabaseReference mRootDB;
    protected final FirebaseUser mUser;
    protected final FirebaseStorage mStorage;

    // Create a storage reference from our app
    private final StorageReference mStorageRef;


    public BaseUsecase() {
        mAuth = FirebaseAuth.getInstance();
        mFireDB = FirebaseDatabase.getInstance();
        mRootDB = mFireDB.getReference();
        this.mUser = FirebaseAuth.getInstance().getCurrentUser();
        this.mStorage = FirebaseStorage.getInstance();
        this.mStorageRef = mStorage.getReference();

        mRootDB.keepSynced(true);
    }


    protected StorageReference getParkingRef(String ownerUuid, String parkingUuid) {
        return mStorageRef.child(ownerUuid).child(parkingUuid);
    }

    protected StorageReference getParkingRef(String ownerUuid, String parkingUuid, String fileName) {
        return mStorageRef.child(ownerUuid).child(parkingUuid).child(fileName);
    }
    protected StorageReference getRootStorageRef() {
        return mStorageRef;
    }

}
