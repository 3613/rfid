package com.theredbean.rfid.business.owner;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.theredbean.rfid.business.BaseUsecase;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.configure.FirebaseConstant;
import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.models.Owner;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nampham on 4/8/18.
 */
@ActivityScope
public class OwnerUsecaseImpl extends BaseUsecase implements OwnerUsecase {

    @Inject
    public OwnerUsecaseImpl(){

    }

    @Override
    public void getOwners(EzParkingCallback.GetOwnerCallback callback) {

        mRootDB.child(FirebaseConstant.KEY_OWNER).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Owner> result = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Owner owner = ds.getValue(Owner.class);
                    if (owner != null) {
                        owner.setUuid(ds.getKey());
                        result.add(owner);
                    }
                }
                callback.onGetOwners(result);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onGetOwnersFailed();
            }
        });
    }
}
