package com.theredbean.rfid.business.image;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theredbean.rfid.business.BaseUsecase;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.EzParkingUtils;
import com.theredbean.rfid.configure.FirebaseConstant;
import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.models.Employee;

import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nampham on 4/9/18.
 */
@ActivityScope
public class ImageUsecaseImpl extends BaseUsecase implements ImageUsecase {


    @Inject
    public ImageUsecaseImpl() {

    }

    @Override
    public void uploadImage(Employee employee, File file, String fileName, EzParkingCallback.UploadCallback callback) {
        Uri uri = Uri.fromFile(file);
        StorageReference sf = getParkingRef(employee.getOwnerKey(), employee.getParkingKey(), fileName);
        sf.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri uri = taskSnapshot.getDownloadUrl();
                if (uri != null) {
                    callback.onUploadSuccess(uri.getPath());
                } else {
                    callback.onUploadFailed("Uploaded failed. Please try again");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onExceptionError(e.getMessage());
            }
        });
    }

    @Override
    public void uploadImage(@NonNull Employee employee, @NonNull Bitmap bitmap, EzParkingCallback.UploadCallback callback) {
        String name = EzParkingUtils.generaterImageName(employee.getUuid());
        StorageReference sf = getParkingRef(employee.getOwnerKey(), employee.getParkingKey(), name);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = sf.putBytes(data);
        uploadTask.addOnFailureListener(exception -> {
            // Handle unsuccessful uploads
            callback.onExceptionError(exception.getMessage());
        }).addOnSuccessListener(taskSnapshot -> {
            Uri uri = taskSnapshot.getDownloadUrl();
            if (uri != null) {
                callback.onUploadSuccess(uri.toString());
            } else {
                callback.onUploadFailed("Uploaded failed. Please try again");
            }
        });
    }

    @Override
    public void saveLocalPath(ImageModel model, EzParkingCallback.NewLocalPathCallBack callBack) {
        Timber.tag(TAG).e("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");

        String ownerUuid = model.getOwnerUuid();
        String parkingUuid = model.getParkingUuid();
        String cardId = model.getCardId();
        String localpath = model.getLocalPath();
        mRootDB.child(FirebaseConstant.KEY_CURRENT)
                .child(ownerUuid)
                .child(parkingUuid)
                .child(cardId)
                .child(FirebaseConstant.PARAM_PATH_LOCAL)
                .setValue(localpath).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()) {
                    callBack.onUpdateFalied(task.getException().getMessage());
                } else {
                    callBack.updateSuccees();
                }
            }
        });
    }

    @Override
    public void updatelocalPath(ImageModel imageModel) {
        mRootDB.child(FirebaseConstant.KEY_CURRENT)
                .child(imageModel.getOwnerUuid())
                .child(imageModel.getParkingUuid())
                .child(imageModel.getCardId())
                .child(FirebaseConstant.PARAM_PATH_LOCAL)
                .setValue(imageModel.getLocalPath()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Timber.tag(TAG).e(">>>> Update localPath offline successs<<<<");

            }
        });
    }


    private static final String TAG = ImageUsecaseImpl.class.getSimpleName();

    @Override
    public void uploadImage(@NonNull ImageModel model, EzParkingCallback.UploadCallback callback) {
        //File file = new File(model.getPath());
        Timber.tag(TAG).e(">>>> : %s", model.getLocalPath());
        File file = new File(model.getLocalPath());
        Uri uri = Uri.fromFile(file);
        String name = EzParkingUtils.generaterImageName(model.getEmployeeUuid());
        StorageReference sf = getParkingRef(model.getOwnerUuid(), model.getParkingUuid(), name);
        sf.putFile(uri).addOnSuccessListener((OnSuccessListener<UploadTask.TaskSnapshot>) taskSnapshot -> {
            Uri uri1 = taskSnapshot.getDownloadUrl();
            if (uri1 != null) {
                //todo
                //kiem hash map luu
                //dung model.getCardId() kiem object trong map  => card do current hay history
                //if current => savePathToCurrentRecord(model, uri1.toString(), callback);
                // if history => savePathToHistory(....)
                savePathToCurrentRecord(model, uri1.toString(), callback);

                //callback.onUploadSuccess(uri1.toString());
            } else {
                callback.onUploadFailed("Uploaded failed. Please try again");
            }
        }).addOnFailureListener((OnFailureListener) e -> callback.onExceptionError(e.getMessage()));
    }

    private void savePathToCurrentRecord(ImageModel model, String path, EzParkingCallback.UploadCallback callback) {
        String ownerUuid = model.getOwnerUuid();
        String parkingUuid = model.getParkingUuid();
        String cardId = model.getCardId();
        mRootDB.child(FirebaseConstant.KEY_CURRENT)
                .child(ownerUuid)
                .child(parkingUuid)
                .child(cardId)
                .child(FirebaseConstant.PARAM_PATH_IMAGE)
                .setValue(path).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callback.onUploadSuccess(path);
                } else {
                    callback.onUploadFailed("upload failed. try again");
                }
            }
        });
    }
}
