package com.theredbean.rfid.business.register;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.theredbean.rfid.business.BaseUsecase;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.mapper.UserProfileMapper;
import com.theredbean.rfid.business.model.UserProfile;
import com.theredbean.rfid.configure.FirebaseConstant;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Owner;

import java.util.Objects;

import javax.inject.Inject;

import timber.log.Timber;


/**
 * Created by hoang on 07/04/2018 nhe.
 */
public class RegisterUsecaseImpl extends BaseUsecase implements RegisterUsecase {
    public static final String TAG = RegisterUsecaseImpl.class.getSimpleName();

    @Inject
    public RegisterUsecaseImpl() {
    }


    @Override
    public void register(String email, String password, EzParkingCallback.RegisterCallback callback) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            callback.onRegisterFailed(Objects.requireNonNull(task.getException()).getMessage());
                        } else {
                            callback.onRegisterSuccess();
                        }

                    }
                });
    }

    @Override
    public void registerV1(String email, String password, String name, String description, EzParkingCallback.RegisterCallbackV1 callback) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            callback.onRegisterFailed(Objects.requireNonNull(task.getException()).getMessage());
                        } else {
                            UserProfileMapper mapper = new UserProfileMapper();
                            UserProfile profile = mapper.map(task.getResult().getUser());
                            if (profile != null) {
                                profile.setName(name);
                                profile.setDescription(description);
                                saveOwner(profile, callback);
                            } else {
                                callback.onRegisterFailed("User profile is empty");
                            }

                        }
                    }
                });
    }

    @Override
    public void registerEmployee(String email,
                                 String password,
                                 String name,
                                 String description,
                                 String ownerUUID,
                                 EzParkingCallback.RegisterEmployeeCallback callbackV3) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            callbackV3.onRegisterFailed(Objects.requireNonNull(task.getException()).getMessage());
                        } else {
                            // todo submit to firebase
                            if (mAuth == null || mAuth.getCurrentUser() == null) {
                                callbackV3.onRegisterFailed("Authentication is null");
                                return;
                            }
                            String employeeUuid = mAuth.getCurrentUser().getUid();
                            Employee employee = new Employee();
                            employee.setName(name);
                            employee.setDescription(description);
                            employee.setOwnerKey(ownerUUID);
                            mRootDB.child(FirebaseConstant.KEY_EMPLOYEES)
                                    .child(employeeUuid)
                                    .setValue(employee)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Timber.tag(TAG).e("-----just log onSuccess method");

                                        }
                                    })
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            callbackV3.onRegisterSuccess();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            callbackV3.onRegisterFailed(e.getMessage());
                                        }
                                    });
                        }

                    }
                });

    }


    private void saveOwner(UserProfile userProfile, EzParkingCallback.RegisterCallbackV1 callback) {
        if (mAuth == null || mAuth.getCurrentUser() == null) {
            return;
        }

        Owner owner = new Owner();
        owner.setName(userProfile.getName());
        owner.setEmail(userProfile.getEmail());
        owner.setDescription(userProfile.getDescription());
        owner.setActivated(false);

        mRootDB.child(FirebaseConstant.KEY_OWNER)
                .child(userProfile.getUserUuid())
                .setValue(owner).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callback.onRegisterSuccess(userProfile);
                } else {
                    callback.onRegisterFailed(Objects.requireNonNull(task.getException()).getMessage());
                }
            }
        });
    }


}
