package com.theredbean.rfid.business;

import java.util.Calendar;

/**
 * Created by nampham on 4/13/18.
 */
public class EzParkUtils {

    public static long generateKeyTimeStampe(){
        Calendar today = Calendar.getInstance();


        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        return (today.getTimeInMillis() / 1000) * 1000 ;
    }
}
