package com.theredbean.rfid.business.model;

import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.models.Parking;

import java.util.List;

/**
 * Created by nampham on 4/12/18.
 */
public class OwnerModel {

    private Owner owner;
    private List<Parking> parks;

    public OwnerModel() {
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public List<Parking> getParks() {
        return parks;
    }

    public void setParks(List<Parking> parks) {
        this.parks = parks;
    }

    @Override
    public String toString() {
        return "OwnerModel{" +
                "owner=" + owner +
                ", parks=" + parks +
                '}';
    }
}
