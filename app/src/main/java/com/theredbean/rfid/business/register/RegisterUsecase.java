package com.theredbean.rfid.business.register;

import com.theredbean.rfid.business.EzParkingCallback;

/**
 * Created by nampham on 4/7/18.
 */
public interface RegisterUsecase {
    //register owner
    void register(String email, String password, EzParkingCallback.RegisterCallback callback);

    void registerV1(String email, String password, String name, String description, EzParkingCallback.RegisterCallbackV1 callback);

    void registerEmployee(String email,
                          String password,
                          String name,
                          String description,
                          String ownerUUID,
                          EzParkingCallback.RegisterEmployeeCallback callbackV3);



}
