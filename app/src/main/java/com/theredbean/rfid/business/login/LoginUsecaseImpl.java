package com.theredbean.rfid.business.login;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.theredbean.rfid.business.BaseUsecase;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.mapper.OwnerModelMapper;
import com.theredbean.rfid.business.mapper.UserProfileMapper;
import com.theredbean.rfid.business.model.UserProfile;
import com.theredbean.rfid.configure.FirebaseConstant;
import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Owner;

import java.util.Objects;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nampham on 4/7/18.
 */
@ActivityScope
public class LoginUsecaseImpl extends BaseUsecase implements LoginUsecase {


    private static final String TAG = LoginUsecase.class.getSimpleName();

    @Inject
    public LoginUsecaseImpl() {

    }

    @Override
    public void loginWithEmailAndPass(String email, String password, EzParkingCallback.LoginCallback callback) {
        Timber.tag(TAG).e("login with email and pass");
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    callback.onExceptionError(task.getException().getMessage());
                } else {
                    String uuid = task.getResult().getUser().getUid();
                    mRootDB.child(FirebaseConstant.KEY_EMPLOYEES).child(uuid).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                Employee employee = dataSnapshot.getValue(Employee.class);
                                employee.setUuid(dataSnapshot.getKey());
                                callback.onEmployeeLogged(employee);
                            } else {
                                //check owner is exisintgs or not
                                mRootDB.child(FirebaseConstant.KEY_OWNER).child(uuid).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            Owner owner = dataSnapshot.getValue(Owner.class);
                                            owner.setUuid(dataSnapshot.getKey());
                                            OwnerModelMapper mapper = new OwnerModelMapper();
                                            callback.onOwnerLogged(mapper.map(owner));
                                        } else {
                                            callback.onOwnerFirstLogin();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        callback.onExceptionError(databaseError.getMessage());
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            callback.onExceptionError(databaseError.getMessage());
                        }
                    });

                }
            }
        });
    }


    public void saveOwnerProfileToDatabase(EzParkingCallback.SaveOwnerCallBack callBack) {
        if (mAuth == null || mAuth.getCurrentUser() == null){
            callBack.onSaveOwnerFailed("Authentication is null");
            return;
        }
        String uid = mAuth.getCurrentUser().getUid();
        Owner owner = new Owner();
        owner.setName(mAuth.getCurrentUser().getDisplayName());
        owner.setEmail(mAuth.getCurrentUser().getEmail());
        owner.setActivated(false);
        // Owners --> userUID
        //              ---> Object prop1
        //              ---> Object prop2
        //              ---> Object prop3
        mRootDB.child(FirebaseConstant.KEY_OWNER)
                .child(uid)
                .setValue(owner).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callBack.onSaveOwnerSuccess();
                } else {
                    callBack.onSaveOwnerFailed(Objects.requireNonNull(task.getException()).getMessage());
                }
            }
        });
    }

    @Override
    public void checkLogged(EzParkingCallback.CheckLoggedCallback callback) {
        if (mAuth == null || mAuth.getCurrentUser() == null){
            callback.onNoUserLogged();
            return;
        }
        String uuid = mAuth.getCurrentUser().getUid();
        UserProfileMapper mapper = new UserProfileMapper();
        mRootDB.child(FirebaseConstant.KEY_EMPLOYEES).child(uuid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    UserProfile userProfile = mapper.map(mAuth.getCurrentUser());
                    Employee employee = dataSnapshot.getValue(Employee.class);
                    employee.setUuid(dataSnapshot.getKey());
                    callback.onEmployeeLogged(userProfile, employee);
                } else {
                    //check owner is exisintgs or not
                    mRootDB.child(FirebaseConstant.KEY_OWNER).child(uuid).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                UserProfile userProfile = mapper.map(mAuth.getCurrentUser());
                                Owner owner = dataSnapshot.getValue(Owner.class);
                                owner.setUuid(dataSnapshot.getKey());
                                OwnerModelMapper mapper = new OwnerModelMapper();
                                callback.onOwnerLogged(userProfile, mapper.map(owner));
                            } else {
                                callback.onNoUserLogged();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            callback.onExceptionError(databaseError.getMessage());
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onExceptionError(databaseError.getMessage());
            }
        });
    }

    @Override
    public void signOut(EzParkingCallback.SignOutCallback callback) {
        if (mAuth == null){
            callback.onExceptionAuthentication();
            return;
        }
        mAuth.signOut();
        callback.onSignOutSuccess();
    }
}
