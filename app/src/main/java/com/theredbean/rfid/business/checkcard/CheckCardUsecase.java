package com.theredbean.rfid.business.checkcard;

import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.models.Pricez.FPrice;

/**
 * Created by nampham on 4/10/18.
 */
public interface CheckCardUsecase {

    void isCardExisting(Employee employee, String cardId, EzParkingCallback.CheckCardExisting callback);

    void checkIn(FPrice prices, Employee employee, String cardId, String path, EzParkingCallback.CheckInCardCallback callback);

    void checkOut(FPrice price, Employee employee, FCurrentRecord record, String cardId, EzParkingCallback.CheckOutCardCallback callback);

    // offline version of firebase


    void checkinV2(Employee employee, String cardID, String path, EzParkingCallback.CheckInCardCallback cardCallback);

    void checkOutV2(FPrice price, Employee employee, FCurrentRecord record, String cardId, EzParkingCallback.CheckOutCardCallback callback);

}
