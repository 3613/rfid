package com.theredbean.rfid.business.employee;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.theredbean.rfid.business.BaseUsecase;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.configure.FirebaseConstant;
import com.theredbean.rfid.di.ActivityScope;
import com.theredbean.rfid.models.Employee;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by nampham on 4/8/18.
 */
@ActivityScope
public class EmployeeUsecaseImpl extends BaseUsecase implements EmployeeUsecase {

    @Inject
    public EmployeeUsecaseImpl() {

    }

    @Override
    public void saveEmployee(String employeeUuid, String name, String email, String description, String ownerKey, EzParkingCallback.SaveEmployeeCallback callback) {
        if (mAuth == null || mAuth.getCurrentUser() == null) {
            callback.onSaveEmployeeFailed("Authentication is null");
            return;
        }
        Employee employee = new Employee();
        employee.setName(name);
        employee.setDescription(description);
        employee.setOwnerKey(ownerKey);
        mRootDB.child(FirebaseConstant.KEY_EMPLOYEES)
                .child(employeeUuid)
                .setValue(employee)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        callback.onSaveEmployeeSuccess();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        callback.onSaveEmployeeFailed(e.getMessage());
                    }
                });
    }

    @Override
    public void getEmployees(EzParkingCallback.EmployeeCallback callback) {
        if (mAuth == null || mAuth.getCurrentUser() == null) {
            callback.onGetEmployeeFailed("Authentication is null");
            return;
        }
        String ownerUuid = mAuth.getCurrentUser().getUid();
        mRootDB.child(FirebaseConstant.KEY_EMPLOYEES)
                .orderByChild(FirebaseConstant.PARAM_EMPLOYEE_OWNER_UUID)
                .equalTo(ownerUuid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Employee> results = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Employee e = ds.getValue(Employee.class);
                    if (e != null) {
                        e.setUuid(ds.getKey());
                        results.add(e);
                    }
                }
                callback.onGetEmployeeSuccess(results);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onGetEmployeeFailed(databaseError.getMessage());
            }
        });
    }
}
