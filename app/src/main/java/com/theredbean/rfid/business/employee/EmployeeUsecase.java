package com.theredbean.rfid.business.employee;

import com.theredbean.rfid.business.EzParkingCallback;

/**
 * Created by nampham on 4/7/18.
 */
public interface EmployeeUsecase {

    void saveEmployee(String employeeUuid, String name, String email, String description,
                      String ownerKey, EzParkingCallback.SaveEmployeeCallback callback);

    //get employee list
    void getEmployees(EzParkingCallback.EmployeeCallback callback);
}
