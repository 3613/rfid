package com.theredbean.rfid.business;

import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.models.Parking;
import com.theredbean.rfid.models.Pricez.FPrice;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by nampham on 4/10/18.
 */
//@Singleton
public class EzParkManager {

    private Employee employee;
    private Parking parking;

    private Owner owner;
    private List<Parking> parks;
    private FPrice prices;

    public FPrice getPrices() {
        return prices;
    }

    public void setPrices(FPrice prices) {
        this.prices = prices;
    }


    @Inject
    public EzParkManager() {

    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }


    public Parking getParking() {
        return parking;
    }

    public void setParking(Parking parking) {
        this.parking = parking;
    }

    public List<Parking> getParks() {
        return parks;
    }

    public void setParks(List<Parking> parks) {
        this.parks = parks;
    }

    @Override
    public String toString() {
        return "EzParkManager{" +
                "employee=" + employee +
                ", parking=" + parking +
                ", owner=" + owner +
                ", parks=" + parks +
                ", prices=" + prices +
                '}';
    }
}

