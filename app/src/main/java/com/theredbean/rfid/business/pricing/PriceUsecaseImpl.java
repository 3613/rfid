package com.theredbean.rfid.business.pricing;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.theredbean.rfid.business.BaseUsecase;
import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.configure.FirebaseConstant;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.models.Parking;
import com.theredbean.rfid.models.Pricez.FPrice;
import com.theredbean.rfid.models.Pricez.PriceXeGa;
import com.theredbean.rfid.models.Pricez.PriceXedap;
import com.theredbean.rfid.models.Pricez.PriceXesSo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by hoang on 19/04/2018 nhe.
 */
public class PriceUsecaseImpl extends BaseUsecase implements PriceUsecase {
    public static final String TAG = PriceUsecaseImpl.class.getSimpleName();

    @Inject
    public PriceUsecaseImpl() {
    }

    @Override
    public void savePrice(Owner owner, String parkingKey, FPrice fPrice, EzParkingCallback.PriceCallback callback) {
        if (mAuth == null || mAuth.getCurrentUser() == null) {
            callback.onExceptionError("Authentication is null");
            return;
        }
        String owners = FirebaseConstant.KEY_OWNER;
        String ownerUuid = owner.getUuid();
        String parkings = FirebaseConstant.KEY_PARKINGS;
        String prices = FirebaseConstant.KEY_PRICES;

        String path = String.format("%s/%s/%s/%s/%s",
                owners,
                ownerUuid,
                parkings,
                parkingKey,
                prices);

        Timber.tag(TAG).e("urlne %s", path);
        mRootDB.child(path).setValue(fPrice).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()) {

                    callback.onAddPriceFailed(Objects.requireNonNull(task.getException()).getLocalizedMessage());
                } else {
                    callback.onAddPriceSuccess();
                }
            }
        });
    }


    @Override
    public void loadPriceConfig(Employee employee, EzParkingCallback.CheckPriceCallBack callback) {
        Timber.tag(TAG).e("start provideParking");
        String parkingkey = employee.getParkingKey();
        String ownerKye = employee.getOwnerKey();

        String path = String.format("%s/%s/%s/%s/%s",
                FirebaseConstant.KEY_OWNER,
                ownerKye,
                FirebaseConstant.KEY_PARKINGS,
                parkingkey,
                FirebaseConstant.KEY_PRICES);
        Timber.tag(TAG).e("mypath %s", path);


        mRootDB.child(path)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Parking> parkings = new ArrayList<>();
                        if (!dataSnapshot.exists()) {
                            Timber.tag(TAG).e("provide data is not exist");
                            callback.onPriceCheckFalied("Chủ bãi xe bạn đang làm việc chưa cấu hình giá \n Vui lòng thông báo với chủ bãi xe.");
                        } else {
                            try {
                                FPrice price = dataSnapshot.getValue(FPrice.class);

                                price.setXe_dap(new PriceXedap(Objects.requireNonNull(price).getXe_dap().getDay(), price.getXe_dap().getNight()));
                                price.setXe_ga(new PriceXeGa(price.getXe_ga().getDay(), price.getXe_ga().getNight()));
                                price.setXe_so(new PriceXesSo(price.getXe_so().getDay(), price.getXe_so().getNight()));
                                callback.onPriceCheckSuccess(price);

                            } catch (Exception e) {
                                Timber.tag(TAG).e("null %s", e.getMessage());
                                callback.onPriceCheckFalied(e.getLocalizedMessage());
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Timber.tag(TAG).e(databaseError.getMessage());
                        callback.onPriceCheckFalied(databaseError.getMessage());
                    }
                });


    }

    @Override
    public void loadPriceConfig2(String parkingKey, EzParkManager userManaager, EzParkingCallback.LoadPriceCallBack callBack) {
        Timber.tag(TAG).e("start load config");

//        String path = String.format("%s/%s/%s/%s/%s",
//                FirebaseConstant.KEY_OWNER,
//                ownerKye,
//                FirebaseConstant.KEY_PARKINGS,
//                parkingkey,
//                FirebaseConstant.KEY_PRICES);

        String path = String.format("%s/%s/%s/%s/%s",
                FirebaseConstant.KEY_OWNER,
                userManaager.getOwner().getUuid(), // this ownerkey
                FirebaseConstant.KEY_PARKINGS,
                parkingKey,
                FirebaseConstant.KEY_PRICES);

        mRootDB.child(path).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Timber.tag(TAG).e("provide data is not exist");
                } else {
                    try {
                        FPrice price = dataSnapshot.getValue(FPrice.class);

                        price.setXe_dap(new PriceXedap(Objects.requireNonNull(price).getXe_dap().getDay(), price.getXe_dap().getNight()));
                        price.setXe_ga(new PriceXeGa(price.getXe_ga().getDay(), price.getXe_ga().getNight()));
                        price.setXe_so(new PriceXesSo(price.getXe_so().getDay(), price.getXe_so().getNight()));
                        Timber.tag(TAG).e("prz %s \n %s \n %s \n %s \n %s \n %s \n "
                                , price.getXe_dap().getDay(), price.getXe_dap().getNight()
                                , price.getXe_ga().getDay(), price.getXe_ga().getNight()
                                , price.getXe_so().getDay(), price.getXe_so().getNight());
                        callBack.onLoadPriceSuccess(price);

                    } catch (Exception e) {
                        Timber.tag(TAG).e("null %s", e.getMessage());
                        callBack.onLoadPrieceFaied(e.getMessage());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
