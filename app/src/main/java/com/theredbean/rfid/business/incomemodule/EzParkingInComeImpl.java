package com.theredbean.rfid.business.incomemodule;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.theredbean.rfid.business.EzParkUtils;
import com.theredbean.rfid.business.model.CurrentIncome;
import com.theredbean.rfid.configure.FirebaseConstant;
import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.models.Parking;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by nampham on 4/12/18.
 */
public class EzParkingInComeImpl implements EzParkingInCome {

    private static final String TAG = EzParkingInComeImpl.class.getSimpleName();
    private EzParkingInComeCallback callback;

    protected final FirebaseAuth mAuth;
    protected final FirebaseDatabase mFireDB;
    protected final DatabaseReference mRootDB;

    long todayTimestampe;

    public EzParkingInComeImpl() {
        mAuth = FirebaseAuth.getInstance();
        mFireDB = FirebaseDatabase.getInstance();
        mRootDB = mFireDB.getReference();

        todayTimestampe = EzParkUtils.generateKeyTimeStampe();


        mIncomeHashMap = new HashMap<>();
    }

    @Override
    public void registerCallback(EzParkingInComeCallback callback) {
        this.callback = callback;
    }

    private Owner mOwner;
    private HashMap<String, DatabaseReference> mIncomeHashMap;

    @Override
    public void registerListenerChangeFromParks(Owner owner, List<Parking> parkings) {
        mOwner = owner;
        //create mInComeReference by parking
        Timber.tag(TAG).e(owner.getUuid());
        Timber.tag(TAG).e(String.valueOf(todayTimestampe));

        for (Parking p : parkings) {

            DatabaseReference ref = mRootDB.child(FirebaseConstant.KEY_INCOME)
                    .child(mOwner.getUuid())
                    .child(String.valueOf(todayTimestampe))
                    .child(p.getUuid());
            ref.keepSynced(true);
            ref.addValueEventListener(valueEventListener);
            mIncomeHashMap.put(p.getUuid(), ref);
        }
    }

    @Override
    public void unregisterListenerChangeFromParks() {
        for (Map.Entry<String, DatabaseReference> entry : mIncomeHashMap.entrySet()){
            entry.getValue().removeEventListener(valueEventListener);
        }
        mIncomeHashMap.clear();
    }

    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            String parkUuid = dataSnapshot.getKey(); //parking uuid
            Timber.tag(TAG).e("parkUuid %s", parkUuid);
            if (dataSnapshot.exists()) {
                Timber.tag(TAG).e("parkUuid have data" );
                long total = (long) dataSnapshot.getValue();
                Timber.tag(TAG).e("parkUuid have todal %d", total );

                callback.onInComeToday(new CurrentIncome(parkUuid, total));
            } else {
                dataSnapshot.getRef().setValue(0L);
                callback.onInComeToday(new CurrentIncome(parkUuid, 0L));
                Timber.tag(TAG).e("parkUuid else");


            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            callback.onExeptionError(databaseError.getMessage());
        }
    };


}
