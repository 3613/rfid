package com.theredbean.rfid.business.pricing;

import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Owner;
import com.theredbean.rfid.models.Pricez.FPrice;

/**
 * Created by hoang on 19/04/2018 nhe.
 */
public interface PriceUsecase {
    void savePrice(Owner owner, String parkingKey, FPrice fPrice, EzParkingCallback.PriceCallback callback);

    void loadPriceConfig(Employee employee, EzParkingCallback.CheckPriceCallBack callback);

    void loadPriceConfig2(String parkingKey, EzParkManager userManaager, EzParkingCallback.LoadPriceCallBack callback );
}


