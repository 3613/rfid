package com.theredbean.rfid.business.history;

import android.annotation.SuppressLint;

import com.google.firebase.database.Query;
import com.theredbean.rfid.business.BaseUsecase;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.configure.FirebaseConstant;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.models.FHistoryRecord;
import com.theredbean.rfid.models.Owner;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import durdinapps.rxfirebase2.DataSnapshotMapper;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import rx.functions.Action1;
import timber.log.Timber;

/**
 * Created by hoang on 12/04/2018 nhe.
 */
@SuppressLint("CheckResult")

public class HistoryUsecaseImpl extends BaseUsecase implements HistoryUsecase {
    public static final String TAG = HistoryUsecaseImpl.class.getSimpleName();

    @Inject
    public HistoryUsecaseImpl() {
    }

    @Override
    public void getHistoryList(Owner owner, Employee employee, EzParkingCallback.HistoryCallBack callBack) {
        if (mAuth == null || mAuth.getCurrentUser() == null) {
            callBack.onExceptionError("Authentication is null");
            return;
        }
        String o2 = employee.getOwnerKey();
        String parkingKey = employee.getParkingKey();

        String historyListPath =
                String.format("%s/%s/%s",
                        FirebaseConstant.KEY_HISTORY, o2, parkingKey);
        RxFirebaseDatabase
                .observeSingleValueEvent(
                        mRootDB.child(historyListPath).orderByChild(FirebaseConstant.KEY_TIME_CHECIN)
                        , DataSnapshotMapper.listOf(FHistoryRecord.class))
                .subscribe(historyRecordList -> {
                    if (historyRecordList.isEmpty()) {
                        callBack.onExceptionError(" Khong co du lieu");
                        return;
                    }
                    callBack.success(historyRecordList);
                }, throwable -> callBack.onExceptionError(throwable.getMessage()));
    }

    @Override
    public void getCurrentList(Owner owner, Employee employee, EzParkingCallback.HistoryCallBack callBack) {
        if (mAuth == null || mAuth.getCurrentUser() == null) {
            callBack.onExceptionError("Authentication is null");
            return;
        }
        String o2 = employee.getOwnerKey();
        String parkingKey = employee.getParkingKey();

        String currentListPath =
                String.format("%s/%s/%s",
                        FirebaseConstant.KEY_CURRENT,
                        o2, parkingKey);

        // RX Way
        RxFirebaseDatabase
                .observeSingleValueEvent(
                        mRootDB.child(currentListPath).orderByChild(FirebaseConstant.KEY_TIME_CHECIN),
                        DataSnapshotMapper.listOf(FCurrentRecord.class))
                .subscribe(currentRecordList -> {
                    if (currentRecordList.isEmpty()) {
                        callBack.onExceptionError(" Khong co du lieu");
                        return;
                    }
                    callBack.onGotListCurrent(currentRecordList);

                }, throwable -> callBack.onExceptionError(throwable.getMessage()));
    }

    @Override
    public void getHistoryListWithFilter(Owner owner, Employee employee, long from, long to, EzParkingCallback.HistoryCallBack callBack) {
        if (mAuth == null || mAuth.getCurrentUser() == null) {
            callBack.onExceptionError("Authentication is null");
            return;
        }
        String o2 = employee.getOwnerKey();
        String parkingKey = employee.getParkingKey();
        Timber.tag(TAG).e("%s - %s- %s - \n %d - %d"
                , o2, parkingKey, FirebaseConstant.KEY_TIME_CHECKOUT, from, to);
        Timber.tag(TAG).e("historyyyyy");
        Query query = mRootDB.child(FirebaseConstant.KEY_HISTORY)
                .child(o2)
                .child(parkingKey)
                .orderByChild(FirebaseConstant.KEY_TIME_CHECKOUT)
                .startAt(from)
                .endAt(to);

        RxFirebaseDatabase
                .observeSingleValueEvent(query,
                        DataSnapshotMapper.listOf(FHistoryRecord.class))
                .subscribe(historyRecordList -> {
                    if (historyRecordList.isEmpty()) {
                        callBack.onExceptionError("khong co du lieu");
                        return;
                    }
                    callBack.success(historyRecordList);
                }, throwable -> callBack.onExceptionError(throwable.getMessage()));
        // manual way
//        mRootDB.child(FirebaseConstant.KEY_HISTORY)
//                .child(o2)
//                .child(parkingKey)
//                .orderByChild(FirebaseConstant.KEY_TIME_CHECKOUT)
//                .startAt(from)
//                .endAt(to)
//                .addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        if (dataSnapshot.exists()) {
//                            List<FHistoryRecord> data = new ArrayList<>();
//                            for (DataSnapshot ds : dataSnapshot.getChildren()) {
//                                FHistoryRecord c = ds.getValue(FHistoryRecord.class);
//                                assert c != null;
//                                data.add(c);
//
//                            }
//                            Timber.tag(TAG).e(String.valueOf(data.size()));
//
//                            callBack.success(data);
//                        } else {
//                            callBack.onExceptionError(" Khong co du lieu");
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//                        callBack.onExceptionError(databaseError.getMessage());
//                    }
//                });
    }

    @Override
    public void getCurrentListWithFilter(Owner owner, Employee employee, long from, long to, EzParkingCallback.HistoryCallBack callBack) {
        if (mAuth == null || mAuth.getCurrentUser() == null) {
            callBack.onExceptionError("Authentication is null");
            return;
        }
        Timber.tag(TAG).e("getCurrentListWithFilter");

        String o2 = employee.getOwnerKey();
        String parkingKey = employee.getParkingKey();

        Query query = mRootDB.child(FirebaseConstant.KEY_CURRENT)
                .child(o2)
                .child(parkingKey)
                .orderByChild(FirebaseConstant.KEY_TIME_CHECKOUT)
                .startAt(from)
                .endAt(to);
        RxFirebaseDatabase
                .observeSingleValueEvent(query,
                        DataSnapshotMapper.listOf(FCurrentRecord.class))
                .subscribe(currentLists -> {
                    if (currentLists.isEmpty()) {
                        callBack.onExceptionError("khong co du lieu");
                        return;
                    }
                    callBack.onGotListCurrent(currentLists);
                }, throwable -> callBack.onExceptionError(throwable.getMessage()));

//        mRootDB.child(FirebaseConstant.KEY_CURRENT)
//                .child(o2)
//                .child(parkingKey)
//                .orderByChild(FirebaseConstant.KEY_TIME_CHECKOUT)
//                .startAt(from)
//                .endAt(to)
//                .addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        if (dataSnapshot.exists()) {
//                            List<FCurrentRecord> data = new ArrayList<>();
//                            for (DataSnapshot ds : dataSnapshot.getChildren()) {
//                                FCurrentRecord c = ds.getValue(FCurrentRecord.class);
//                                assert c != null;
//                                data.add(c);
//                            }
//                            callBack.onGotListCurrent(data);
//                        } else {
//                            callBack.onExceptionError(" Khong co du lieu");
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//                        callBack.onExceptionError(databaseError.getMessage());
//                    }
//                });
    }
}
