package com.theredbean.rfid.business.mapper;

import com.google.firebase.auth.FirebaseUser;
import com.theredbean.rfid.business.model.UserProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nampham on 4/9/18.
 */
public class UserProfileMapper {

    public UserProfile map(FirebaseUser input) {
        UserProfile output = null;
        if (input != null) {
            output = new UserProfile();
            output.setEmail(input.getEmail());
            output.setName(input.getDisplayName());
            output.setDescription(input.getPhoneNumber());
            output.setUserUuid(input.getUid());
        }
        return output;
    }

    public List<UserProfile> map(List<FirebaseUser> list) {
        List<UserProfile> result = new ArrayList<>();
        for (FirebaseUser input : list) {
            UserProfile output = map(input);
            if (output != null) {
                result.add(output);
            }
        }
        return result;
    }
}
