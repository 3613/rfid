package com.theredbean.rfid.business.model;

/**
 * Created by nampham on 4/12/18.
 */
public class CurrentIncome {
    private String parkingUuid;
    private long income;

    public CurrentIncome(String parkingUuid, long income) {
        this.parkingUuid = parkingUuid;
        this.income = income;
    }

    public String getParkingUuid() {
        return parkingUuid;
    }

    public void setParkingUuid(String parkingUuid) {
        this.parkingUuid = parkingUuid;
    }

    public long getIncome() {
        return income;
    }

    public void setIncome(long income) {
        this.income = income;
    }

    @Override
    public String toString() {
        return "CurrentIncome{" +
                "parkingUuid='" + parkingUuid + '\'' +
                ", income=" + income +
                '}';
    }
}
