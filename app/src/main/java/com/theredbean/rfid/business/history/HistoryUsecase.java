package com.theredbean.rfid.business.history;

import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.models.Employee;
import com.theredbean.rfid.models.Owner;

/**
 * Created by hoang on 12/04/2018 nhe.
 */
public interface HistoryUsecase {
    void getHistoryList(Owner owner, Employee employee, EzParkingCallback.HistoryCallBack callBack);

    void getCurrentList(Owner owner, Employee employee, EzParkingCallback.HistoryCallBack callBack);

    void getHistoryListWithFilter(Owner owner, Employee employee, long from, long to, EzParkingCallback.HistoryCallBack callBack);

    void getCurrentListWithFilter(Owner owner, Employee employee, long from, long to, EzParkingCallback.HistoryCallBack callBack);

}
