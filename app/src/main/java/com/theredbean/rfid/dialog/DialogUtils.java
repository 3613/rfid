package com.theredbean.rfid.dialog;

import android.app.Dialog;
import android.view.Window;

import com.theredbean.rfid.R;


/**
 * Created by kelvin.nguyen on 8/15/14.
 */
public class DialogUtils {
    /**
     * Không hiện thị titel của dialog và đặt background là 1 bức ảnh null, đồng thời đặt hiệu ứng
     * khi xuất hiện và tắt dialog
     * @param dialog
     */
    public static void iniDialog(Dialog dialog){
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.mipmap.bg_null);
        dialog.setCancelable(true);
        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_Dialog;
    }

    public static void iniDialogNotCancelable(Dialog dialog){
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.mipmap.bg_null);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_Dialog;
    }


}
