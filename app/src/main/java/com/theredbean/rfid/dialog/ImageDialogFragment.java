package com.theredbean.rfid.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.theredbean.rfid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;
import utils.GlideApp;

/**
 * Created by hoang on 28/03/2018 nhe.
 */

public class ImageDialogFragment extends DialogFragment {
    Listener mListener;
    ImageView imageView;
    Button button;
    View view;

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void onclickCloseButton() {
        ImageDialogFragment.this.dismiss();
        button.setBackground(view.getContext().getDrawable(R.drawable.button_selector_selected));
        button.setTextColor(view.getContext().getColor(R.color.white));
    }


    public interface Listener {
        void onSelected(long howManyTime);
    }

    @BindView(R.id.iv)
    ImageView iv;

    public static final String TAG = ImageDialogFragment.class.getSimpleName();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_imageview, container, true);
        ButterKnife.bind(this, view);
//        Bundle mArgs = getArguments();
//        String myValue = mArgs.getString("huuhoang", "detaula");
//
//        Timber.tag(TAG).e("my values tren: %s", myValue);
        imageView = view.findViewById(R.id.btnClose);
        button = view.findViewById(R.id.btnCancel);
        this.view = view;
        return view;


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getDialog().getWindow().getAttributes().windowAnimations = R.style.Dialog_NoTitle;
        super.onActivityCreated(savedInstanceState);
        Bundle mArgs = getArguments();
        String myValue = mArgs.getString("imgPath", "detaula");


//        Glide.with(getActivity()).load(new File(myValue)).into(iv);
//        Picasso.get().load(new File(myValue)).into(iv);
        Timber.tag(TAG).e("appcontextInstance z %s", context.getApplicationContext().hashCode());

        GlideApp.with(context.getApplicationContext()).load(myValue).placeholder(R.mipmap.ic_launcher)
                .error(R.drawable.bg_default)
                .into(iv);

        imageView.setOnClickListener(v -> dismiss());
        button.setOnClickListener(v -> onclickCloseButton());


    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();

        if (d != null) {
            d.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    private Context context;

    @Override
    public void onAttach(Context context) {
        this.context = context;
        super.onAttach(context);
        try {
            mListener = (Listener) getTargetFragment();
        } catch (ClassCastException e) {
            Log.e(TAG, "onAttach: ClassCastException : " + e.getMessage());
        }
    }


}
