package com.theredbean.rfid.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.View;

import com.theredbean.rfid.R;
import com.theredbean.rfid.presentation.BaseActivity;

/**
 * Created by lamthinh on 8/30/17.
 */

public class NotifiDialog extends Dialog implements View.OnClickListener {

    private BaseActivity mBaseActivity;


    public NotifiDialog(@NonNull Context context) {
        super(context);
    }

    public NotifiDialog(@NonNull Context context, BaseActivity baseActivity) {
        super(context);
        mBaseActivity = baseActivity;

    }

    public NotifiDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected NotifiDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DialogUtils.iniDialog(this);
        setContentView(R.layout.dialog_confirm_checkin);



    }




    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSkip:
                break;
            case R.id.btnGotoStore:


        }

    }


}

