package com.theredbean.rfid.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theredbean.rfid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;
import utils.TimeUtils;

/**
 * Created by hoang on 28/03/2018 nhe.
 */

public class DialogPickTtime extends DialogFragment {
    Listener mListener;

    public interface Listener {
        void onSelected(long howManyTime);
    }


    public static final String TAG = DialogPickTtime.class.getSimpleName();
    @BindView(R.id.oneWeek)
    View oneWeek;
    @BindView(R.id.oneMonth)
    View oneMonth;
    @BindView(R.id.ThreeMonth)
    View ThreeMonth;
    @BindView(R.id.oneYear)
    View oneYear;
    long from;
    String path;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_mycustom_dialog, container, true);
        ButterKnife.bind(this, view);


        return view;


    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getDialog().getWindow().getAttributes().windowAnimations = R.style.Dialog_NoTitle;
        super.onActivityCreated(savedInstanceState);

    }

    @OnClick({R.id.oneYear, R.id.oneMonth, R.id.ThreeMonth, R.id.oneWeek})
    public void loaddate(View v) {
        switch (v.getId()) {
            case R.id.oneWeek:
                from = TimeUtils.plusOneWeek();
                Timber.tag(TAG).e("plusOneWeek: %d", from);

                break;
            case R.id.oneMonth:
                from = TimeUtils.plusOneMonth();
                Timber.tag(TAG).e("plusOneMonth: %d", from);
                break;
            case R.id.ThreeMonth:
                from = TimeUtils.plusTherreMonth();
                Timber.tag(TAG).e("plusTherreMonth : %d", from);

                break;
            case R.id.oneYear:
                from = TimeUtils.plusOneYear();
                Timber.tag(TAG).e("plusOneYear: %d", from);
                break;
            default:
                from = TimeUtils.plusOneYear();
                Timber.tag(TAG).e("default : %d", from);

                break;
        }

        mListener.onSelected(from);
//        revenueFragment.onLoadData(from, TimeUtils.getCurrentInMiliseconds());
        getDialog().dismiss();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();

        if (d != null) {
            d.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (Listener) getTargetFragment();
        } catch (ClassCastException e) {
            Log.e(TAG, "onAttach: ClassCastException : " + e.getMessage());
        }
    }


}
