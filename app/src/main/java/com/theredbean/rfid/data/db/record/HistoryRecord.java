package com.theredbean.rfid.data.db.record;

import java.io.File;

import io.realm.RealmObject;

/**
 * Created by nampham on 3/12/18.
 * history parking record
 */

public class HistoryRecord extends RealmObject {
    private String epc;

    private long checkInTime;

    private long checkOutTime;

    private int type;

    private int price;

    private String path;

//    private File file;

//    public File getFile(String path){
//        if (file == null){
//            file = new File(path);
//        }
//        return file;
//    }

//    public void setImgFile(File imgFile) {
//        this.file = imgFile;
//    }

    public String getEpc() {
        return epc;
    }

    public void setEpc(String epc) {
        this.epc = epc;
    }

    public long getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(long checkInTime) {
        this.checkInTime = checkInTime;
    }

    public long getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(long checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "HistoryRecord{" +
                "epc='" + epc + '\'' +
                ", checkInTime=" + checkInTime +
                ", checkOutTime=" + checkOutTime +
                ", type=" + type +
                ", price=" + price +
                ", path='" + path + '\'' +
                '}';
    }
}
