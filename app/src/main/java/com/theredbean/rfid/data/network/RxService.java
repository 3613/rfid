package com.theredbean.rfid.data.network;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.CacheControl;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * Created by hnam on 8/16/2016.
 */
@Singleton
public class RxService {

    private static Retrofit mRetrofit;
    public static String mAccessToken = "";
    public static String mTokenKey = "";

    @Inject
    public RxService(){

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new RequestApiInterceptor())
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        //run asynchronous
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());


        mRetrofit = new Retrofit.Builder()
                .baseUrl(RxApi.API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .addCallAdapterFactory(rxAdapter)
                .build();
    }

    public void setTokenKey(String tokenKey){
        mTokenKey = tokenKey;

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new RequestApiInterceptorWithHeader(tokenKey))
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        //run asynchronous
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());

        mRetrofit = new Retrofit.Builder()
                .baseUrl(RxApi.API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .addCallAdapterFactory(rxAdapter)
                .build();
    }

    private static class RequestApiInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();

            //handle request on url
            HttpUrl originalHttpUrl = request.url();
            HttpUrl url = originalHttpUrl.newBuilder()
                    .build();

            // Request customization: addEmployee request headers
            Request.Builder requestBuilder = request.newBuilder()
                    .url(url)
                    .cacheControl(CacheControl.FORCE_NETWORK);

            Request r = requestBuilder.build();
            return chain.proceed(r);
        }
    }

    private static class RequestApiInterceptorWithHeader implements Interceptor {
        private String token = "";
        public RequestApiInterceptorWithHeader(String token){
            this.token = token;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();



            // Request customization: addEmployee request headers
            Request.Builder requestBuilder = request.newBuilder()
                    .header("Authorization", String.format("BASIC=%s", token))
                    .cacheControl(CacheControl.FORCE_NETWORK);

            Request r = requestBuilder.build();
            return chain.proceed(r);
        }
    }

    public RxApi.Anon getAnonApi(){
        return mRetrofit.create(RxApi.Anon.class);
    }

    public RxApi.Auth getAuthApi(){
        return mRetrofit.create(RxApi.Auth.class);
    }

    public static Retrofit getRetrofit(){return mRetrofit;}
}
