package com.theredbean.rfid.data.db;

import com.theredbean.rfid.business.image.ImageModel;
import com.theredbean.rfid.data.db.record.CurrentRecord;
import com.theredbean.rfid.data.db.record.HistoryRecord;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.models.FHistoryRecord;
import com.theredbean.rfid.rxbus.penz.PendingObject;

import java.util.List;

/**
 * Created by nampham on 3/14/18.
 */

public interface IDatabase {
    void saveCurrentRecord(String epc, String path, int type);


    CurrentRecord getRecord(String epc);

    void saveHistoryRecord(String epc);

    int saveHistoryRecordCopy(String epc);

    List<HistoryRecord> getHistoryRecordInDuration(long from, long to);

    List<HistoryRecord> getHistoryRecordInDurationCopy(long from, long to);

    List<HistoryRecord> getHistoryRecordInDurationWithType(long from, long to, int type);

    List<HistoryRecord> getHistoryRecord();

    List<CurrentRecord> getCurrentRecord();

    List<CurrentRecord> getCurrentRecordByType(int type);

    List<CurrentRecord> getCurrentRecordByEpc(String epc);


    // firebase helper stufff

    List<PendingObject> getAllPendings();

    void addCurrentPending(FCurrentRecord currentRecord, String onlineLinkTarget, RealmCallBack.AddPendingcallBack callback);

    void addCurrentPending(ImageModel imageModel);

    void updateCurrentToHistory(String currentCardid, FHistoryRecord imageModel);

    void removeUploadedObject(String cardID, long timeIn);


    void removeUploadedObject(PendingObject pending);

    boolean isPendingObjectExist(FHistoryRecord history, String curentCardID);
}
