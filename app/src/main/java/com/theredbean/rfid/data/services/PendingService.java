package com.theredbean.rfid.data.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.theredbean.rfid.app.MyApp;
import com.theredbean.rfid.business.imageV2.UploadUsecase;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.di.components.ApplicationComponent;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.rxbus.penz.PendingObject;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nampham on 5/16/18.
 */
public class PendingService extends IntentService {
    public static final String TAG = PendingService.class.getSimpleName();


    @Inject
    IDatabase database;

    @Inject
    UploadUsecase uploadUsecase;

    public PendingService() {
        super("pending_service");
    }

    public PendingService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        initialInject();

        //get du lieu len tu realm -> list<PendingObject>
        // if list.empty => return
        // ! list.empty => upload
        // is currrent => upload current
        // is history => upload history
        Timber.tag(TAG).e(">>>>>>>>>>>>>>>>>>onHandleIntent.");



    }

    private void initialInject() {
        ApplicationComponent component = ((MyApp) getApplication()).getComponent();
        DaggerEzParkingComponent.builder()
                .applicationComponent(component)
                .ezParkingModule(new EzParkingModule())
                .build().inject(this);
    }
}
