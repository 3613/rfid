package com.theredbean.rfid.data.db.record;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import io.realm.RealmObject;

/**
 * Created by nampham on 3/12/18.
 * current record
 */

public class CurrentRecord extends RealmObject {

    private String epc;
    private long checkInTime;
    private int type;
    private String path;


//
//    private File f;
//    FileOutputStream outFile = null;

//
//    public File getFile(String path) throws IOException {
//        if (f == null) {
//            f = new File(path);
//        }
//            f.delete();
//
//        return f;
//    }
;

    public String getEpc() {
        return epc;
    }

    public void setEpc(String epc) {
        this.epc = epc;
    }

    public long getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(long checkInTime) {
        this.checkInTime = checkInTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return " \n CurrentRecord{" +
                "epc='" + epc + '\'' +
                ", checkInTime=" + checkInTime +
                ", type=" + type +
                ", path='" + path + '\'' +
                '}';
    }
}
