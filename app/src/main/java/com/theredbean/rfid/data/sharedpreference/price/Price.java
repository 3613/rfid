package com.theredbean.rfid.data.sharedpreference.price;

/**
 * Created by hoang on 23/03/2018 nhe.
 */

public class Price {
    public int price_ga_day;
    public int price_so_day;
    public int price_dap_day;
    public int price_ga_night;
    public int price_so_night;
    public int price_dap_night;

    public Price(int price_ga_day,
                 int price_so_day,
                 int price_dap_day,
                 int price_ga_night,
                 int price_so_night,
                 int price_dap_night) {
        this.price_ga_day = price_ga_day;
        this.price_so_day = price_so_day;
        this.price_dap_day = price_dap_day;
        this.price_ga_night = price_ga_night;
        this.price_so_night = price_so_night;
        this.price_dap_night = price_dap_night;
    }
}
