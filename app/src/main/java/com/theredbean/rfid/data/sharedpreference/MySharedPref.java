package com.theredbean.rfid.data.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.theredbean.rfid.di.ActivityScope;

import javax.inject.Inject;

import timber.log.Timber;
import utils.EzConstants;

/**
 * Created by hoang on 19/03/2018.
 */
@ActivityScope
public class MySharedPref implements ISharedPreferences {
    public static final String TAG = MySharedPref.class.getSimpleName();

    private static MySharedPref mySharedPref;
    private SharedPreferences preferences;
    private static Context context;

    @Inject
    Gson gson;


    public MySharedPref(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public MySharedPref(Context context) {
        this.context = context;
    }


    public MySharedPref() {
    }

    public MySharedPref(Context app, Gson gson) {
        this.context = app;
        this.gson = gson;
    }


    @Override
    public boolean savePreferences(String key, String value) {

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    @Override
    public boolean saveAllPriceConfig(int a, int b, int c, int d, int e, int f) {

        if (a < 0
                || b < 0
                || c < 0
                || d < 0
                || e < 0
                || f < 0) {
            return false;
        } else {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());

            try {
                EzConstants.PRICE_C_XE_DAP_D = a;
                EzConstants.PRICE_A_XE_GA_D = b;
                EzConstants.PRICE_E_XE_SO_N = c;
                EzConstants.PRICE_D_XE_GA_N = d;
                EzConstants.PRICE_B_XE_SO_D = e;
                EzConstants.PRICE_F_XE_DAP_N = f;

                SharedPreferences.Editor editor = sp.edit();
                editor.putString(EzConstants.KEY_PRICE_A_XE_GA_D, String.valueOf(EzConstants.PRICE_C_XE_DAP_D));
                editor.putString(EzConstants.KEY_PRICE_B_XE_SO_D, String.valueOf(EzConstants.PRICE_A_XE_GA_D));
                editor.putString(EzConstants.KEY_PRICE_C_XE_DAP_D, String.valueOf(EzConstants.PRICE_E_XE_SO_N));
                editor.putString(EzConstants.KEY_PRICE_D_XE_GA_N, String.valueOf(EzConstants.PRICE_D_XE_GA_N));
                editor.putString(EzConstants.KEY_PRICE_E_XE_SO_N, String.valueOf(EzConstants.PRICE_B_XE_SO_D));
                editor.putString(EzConstants.KEY_PRICE_F_XE_DAP_N, String.valueOf(EzConstants.PRICE_F_XE_DAP_N));
                return editor.commit(); // write success return: true
            } catch (Exception z) {

                Timber.tag(TAG).e(z.getMessage());


            }
        }

        return false;  // write falied return: false
    }

    @Override
    public String readPreferences(String key, String defaultValue) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return sp.getString(key, defaultValue);
    }

    @Override
    public String getString(String key, String defaultValue) {
        if (preferences == null) return "";
        return preferences.getString(key, defaultValue);
    }

    @Override
    public String getString(String key) {
        if (preferences == null) return "";
        return preferences.getString(key, "");
    }

    @Override
    public void putString(String key, String value) {
        checkForNullKey(key);
        checkForNullValue(value);
        preferences.edit().putString(key, value).apply();
    }

    @Override
    public void checkForNullKey(String key) {
        if (key == null) {
            throw new NullPointerException();
        }
    }

    @Override
    public void checkForNullValue(String value) {
        if (value == null) {
            throw new NullPointerException();
        }

    }

    public static MySharedPref getInstance() {
        if (mySharedPref == null) {
            mySharedPref = new MySharedPref();
            mySharedPref.preferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return mySharedPref;
    }


}
