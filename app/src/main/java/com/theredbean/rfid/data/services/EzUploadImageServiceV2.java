package com.theredbean.rfid.data.services;

import android.app.Service;
import android.content.Intent;

import android.os.IBinder;
import android.support.annotation.Nullable;

import com.theredbean.rfid.app.MyApp;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.image.ImageUsecase;
import com.theredbean.rfid.business.imageV2.UploadUsecase;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.di.components.ApplicationComponent;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.rxbus.RxBus;
import com.theredbean.rfid.rxbus.penz.PendingObject;


import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by nampham on 4/15/18.
 */
public class EzUploadImageServiceV2 extends Service {
    public static final String TAG = EzUploadImageServiceV2.class.getSimpleName();


    @Inject
    RxBus rxBus;

    @Inject
    ImageUsecase imageUsecase;

    @Inject
    UploadUsecase uploadUsecase;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public void onCreate() {
        super.onCreate();
        Timber.tag(TAG).e("onCreate222222>>>>>>>");
        initInjector();
        List<PendingObject> pendingObjects = iDatabase.getAllPendings();
        if (pendingObjects.isEmpty()) {
            Timber.tag(TAG).e("onCreate222222 ---> if pending Empty");
            stopSelf();

            return;
        } else {
            Timber.tag(TAG).e("onCreate222222 --->  have data");

            for (PendingObject o : pendingObjects) {
                if (o.isCurrentRecord()) {
                    Timber.tag(TAG).e("CS : test %s", o.getLocalPath());

                    uploadUsecase.uploadImgCurrent(o, new EzParkingCallback.UploadPendingCallback() {
                        @Override
                        public void onUploadPendingSuccess(PendingObject pending) {
                            Timber.tag(TAG).e("CS: onUploadPendingSuccess callback service");
                            iDatabase.removeUploadedObject(pending);
                        }

                        @Override
                        public void onUploadPendingFailed(String msg) {

                        }
                    });
                } else {

                    uploadUsecase.uploadImgHistory(o, new EzParkingCallback.UploadPendingCallback() {
                        @Override
                        public void onUploadPendingSuccess(PendingObject pending) {
                            Timber.tag(TAG).e("CS: onUploadPendingSuccess callback duoi");
                            iDatabase.removeUploadedObject(o);

                        }

                        @Override
                        public void onUploadPendingFailed(String msg) {

                        }
                    });

                }

                stopSelf();
            }
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private void initInjector() {
        ApplicationComponent component = ((MyApp) getApplication()).getComponent();
        DaggerEzParkingComponent.builder()
                .applicationComponent(component)
                .ezParkingModule(new EzParkingModule())
                .build().inject(this);
    }


    @Inject
    IDatabase iDatabase;


}
