package com.theredbean.rfid.data.db;

import android.content.Context;
import android.support.annotation.NonNull;


import com.theredbean.rfid.business.image.ImageModel;
import com.theredbean.rfid.data.db.record.CurrentRecord;
import com.theredbean.rfid.data.db.record.HistoryRecord;
import com.theredbean.rfid.models.FCurrentRecord;
import com.theredbean.rfid.models.FHistoryRecord;
import com.theredbean.rfid.rxbus.penz.PendingCons;
import com.theredbean.rfid.rxbus.penz.PendingObject;

import java.util.Calendar;
import java.util.List;


import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import timber.log.Timber;
import utils.EzConstants;
import utils.PriceUtils;

/**
 * Created by nampham on 3/14/18.
 */

public class RealmDatabase implements IDatabase {
    @Inject
    public RealmDatabase() {
    }

    int checkoutstatus = 3;
    private static final String TAG = RealmDatabase.class.getSimpleName();

    private Context context;

    public RealmDatabase(Context context) {
        this.context = context;

        Realm.init(this.context);
        RealmConfiguration config = new RealmConfiguration.Builder().name("mytest.realm").build();
        Realm.setDefaultConfiguration(config);
    }


    @Override
    public void saveCurrentRecord(String epc, String path, int type) {
        Realm realm = Realm.getDefaultInstance(); // opens "myrealm.realm"
        try {
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Timber.tag(TAG).e("00000000000000000000000000");

                    CurrentRecord entry = realm.createObject(CurrentRecord.class);
                    entry.setCheckInTime(Calendar.getInstance().getTimeInMillis());
                    entry.setEpc(epc);
                    entry.setPath(path);
                    entry.setType(type);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    Timber.tag(TAG).e("cucesssssssssss");

                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    Timber.tag(TAG).e(error.getMessage());

                }
            });
        } finally {
            realm.close();
        }

//        Realm realm2 = Realm.getDefaultInstance();
//        final RealmResults<CurrentRecord> results = realm2.where(CurrentRecord.class).findAllAsync();
//        for (CurrentRecord e : results) {
//            Timber.tag(TAG).e("%s", e.toString());
//        }
    }


    @Override
    public CurrentRecord getRecord(String epc) {
        Realm realm = Realm.getDefaultInstance();
        CurrentRecord record = realm.where(CurrentRecord.class).equalTo("epc", epc).findFirst();
        realm.close();
        return record;
    }


    @Override
    public int saveHistoryRecordCopy(String epc) {

        Realm realm = Realm.getDefaultInstance();
        CurrentRecord record = realm.where(CurrentRecord.class).equalTo("epc", epc).findFirst();
        assert record != null;
        Timber.tag(TAG).e("saveHistoryRecordCopy" + record.toString());
        if (record == null) {
            Timber.tag(TAG).e("no current record");
            checkoutstatus = EzConstants.CHECKOUT_FALIED;
            return checkoutstatus;
        }

        HistoryRecord historyRecord = new HistoryRecord();
        historyRecord.setEpc(record.getEpc());
        historyRecord.setPath(record.getPath());
        historyRecord.setType(record.getType());
        historyRecord.setPrice(PriceUtils.priceOf(record));

        Timber.tag(TAG).e("setPrice " + PriceUtils.priceOf(record));
        Timber.tag(TAG).e("urlPath " + record.getPath());

        historyRecord.setCheckInTime(record.getCheckInTime());
        historyRecord.setCheckOutTime(Calendar.getInstance().getTimeInMillis());
        Timber.tag(TAG).e("saveHistoryRecordCopy checkout" + record.toString());
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Timber.tag(TAG).e("insert>>>>>");
                realm.insert(historyRecord);
                checkoutstatus = EzConstants.CHECKOUT_SUCCESS;
                // todo using callback instead that way
            }
        });


        //todo remove current
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                record.deleteFromRealm();
            }
        });


        realm.close();
        return checkoutstatus;

    }

    @Override
    public List<HistoryRecord> getHistoryRecordInDuration(long from, long to) {
        //find in history
        Realm realm = Realm.getDefaultInstance();
        RealmResults<HistoryRecord> records = realm.where(HistoryRecord.class).between("checkInTime", from, to).findAll();
        List<HistoryRecord> results = realm.copyFromRealm(records);
        realm.close();
        return results;
    }

    @Override
    public List<HistoryRecord> getHistoryRecordInDurationCopy(long from, long to) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<HistoryRecord> records = realm.where(HistoryRecord.class).between("checkInTime", from, to).findAll();
        List<HistoryRecord> results = realm.copyFromRealm(records);
        realm.close();
        return results;
    }

    @Override
    public List<HistoryRecord> getHistoryRecordInDurationWithType(long from, long to, int type) {
        return null;
    }

    @Override
    public List<HistoryRecord> getHistoryRecord() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<HistoryRecord> records = realm.where(HistoryRecord.class).findAllAsync();
        List<HistoryRecord> results = realm.copyFromRealm(records);
        realm.close();
        return results;
    }


    @Override
    public List<CurrentRecord> getCurrentRecord() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<CurrentRecord> records = realm.where(CurrentRecord.class).findAllAsync();
        List<CurrentRecord> results = realm.copyFromRealm(records);
        realm.close();
        return results;
    }

    @Override
    public List<CurrentRecord> getCurrentRecordByType(int type) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<CurrentRecord> records = realm.where(CurrentRecord.class).equalTo("type", type).findAllAsync();
        List<CurrentRecord> results = realm.copyFromRealm(records);
        realm.close();
        return results;
    }

    @Override
    public List<CurrentRecord> getCurrentRecordByEpc(String epc) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<CurrentRecord> records = realm.where(CurrentRecord.class).equalTo("epc", epc).findAllAsync();
        List<CurrentRecord> results = realm.copyFromRealm(records);
        realm.close();
        return results;
    }

    @Override
    public List<PendingObject> getAllPendings() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<PendingObject> records = realm.where(PendingObject.class).findAll();
        return realm.copyFromRealm(records);
    }

    @Override
    public void addCurrentPending(FCurrentRecord currentRecord, String onlineLinkTarget, RealmCallBack.AddPendingcallBack callback) {

    }


    @Override
    public void addCurrentPending(ImageModel imageModel) {
        Timber.tag(TAG).e("start add currentpending");

        Realm realm = Realm.getDefaultInstance(); // opens "myrealm.realm"

        try {
            realm.executeTransactionAsync(
                    new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

//                            String localPath;
//                            long checkinTime;
//                            long checkouttime;
//                            String cardId;
//
//                            String employeeUuid;
//                            String parkingUuid;
//                            String ownerUuid;
//                            String path;

                            Timber.tag(TAG).e("start0--->execute");
                            PendingObject entry = realm.createObject(PendingObject.class);
                            entry.setLocalPath(imageModel.getLocalPath());
                            entry.setCardId(imageModel.getCardId());
                            ;
                            entry.setEmployeeUuid(imageModel.getEmployeeUuid());
                            entry.setParkingUuid(imageModel.getParkingUuid());
                            entry.setOwnerUuid(imageModel.getOwnerUuid());
                            Timber.tag(TAG).e("CS: timein %d", imageModel.getTimeIn());

                            entry.setCheckinTime(imageModel.getTimeIn());
                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            Timber.tag(TAG).e("onAddSuccess");
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            Timber.tag(TAG).e(error.getMessage());
                        }
                    });
        } finally {
            realm.close();
        }
    }

    @Override
    public void updateCurrentToHistory(String currentCardid, FHistoryRecord histor) {

        Timber.tag(TAG).e("update history");

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Timber.tag(TAG).e("excute");

                PendingObject object = realm.where(PendingObject.class)
                        .equalTo(PendingCons.KEY_CARD_ID, currentCardid)
                        .findFirst();
                if (object == null) {
                    object = realm.createObject(PendingObject.class);
                }

                object.setCardId(histor.getCardId());
                object.setCheckouttime(histor.getTimestampCheckOut());


                Timber.tag(TAG).e("newcardid %s", histor.getCardId());

            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Timber.tag(TAG).e(" moveCurrentPendingToHistoryPending on success");
                realm.close();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Timber.tag(TAG).e(" moveCurrentPendingToHistoryPending on error %s", error.getMessage());
                realm.close();

            }
        });
    }

    @Override
    public void saveHistoryRecord(String epc) {
        Realm realm = Realm.getDefaultInstance();
        CurrentRecord record = realm.where(CurrentRecord.class).equalTo("epc", epc).findFirst();
        if (record == null) {
            Timber.tag(TAG).e("no current record");
            try {

            } catch (Exception e) {
                Timber.tag(TAG).e(" saveHistoryRecord -->Exception: ", e.getMessage());

            }
            return;
        }


        HistoryRecord historyRecord = new HistoryRecord();
        historyRecord.setEpc(record.getEpc());
        historyRecord.setPath(record.getPath());
        historyRecord.setType(record.getType());
        historyRecord.setPrice(PriceUtils.priceOf(record));
        historyRecord.setCheckInTime(record.getCheckInTime());
        historyRecord.setCheckOutTime(Calendar.getInstance().getTimeInMillis());
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Timber.tag(TAG).e("insert>>>>>");
                realm.insert(historyRecord);
                try {

                } catch (Exception e) {
                    Timber.tag(TAG).e(" saveHistoryRecord -->execute: ", e.getMessage());

                }

            }
        });


        //todo remove current
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                record.deleteFromRealm();
            }
        });

        realm.close();
    }


    @Override
    public void removeUploadedObject(String cardID, long timeIn) {
        Timber.tag(TAG).e("enjoy func dlete");


        Realm r = Realm.getDefaultInstance();
        RealmResults<PendingObject> listObj = r.where(PendingObject.class).findAll();
        PendingObject obj = listObj
                .where()
                .equalTo(PendingCons.KEY_CARD_ID, cardID)
                .and()
                .equalTo(PendingCons.KEY_CHECKIN_TIME, timeIn)
                .findFirst();

        if (obj != null) {
            if (!r.isInTransaction()) {
                r.beginTransaction();
            }
            Timber.tag(TAG).e("dlete %s", obj, toString());

            obj.deleteFromRealm();
            r.commitTransaction();
        }

    }

    @Override
    public void removeUploadedObject(PendingObject pending) {
        Timber.tag(TAG).e("removeUploadedObject >>>>> start");


        Realm r = Realm.getDefaultInstance();
        RealmResults<PendingObject> listObj = r.where(PendingObject.class).findAll();
        PendingObject obj = listObj
                .where()
                .equalTo(PendingCons.KEY_CARD_ID, pending.getCardId())
                .findFirst();

        if (obj != null) {
            Timber.tag(TAG).e("removeUploadedObject >>>>> have object");

            if (!r.isInTransaction()) {
                r.beginTransaction();
            }
            Timber.tag(TAG).e("dlete %s", obj, toString());

            obj.deleteFromRealm();
            r.commitTransaction();
        } else {
            Timber.tag(TAG).e("removeUploadedObject >>>>> found no object");

        }

    }

    @Override
    public boolean isPendingObjectExist(FHistoryRecord history,String curentCardID) {
        Realm realm = Realm.getDefaultInstance();
        PendingObject pendingObject = realm.where(PendingObject.class)
                .equalTo(PendingCons.KEY_CARD_ID, curentCardID)
                .equalTo(PendingCons.KEY_CHECKIN_TIME, history.getTimestampCheckIn())
                .findFirst();
        return pendingObject != null;
    }


}
