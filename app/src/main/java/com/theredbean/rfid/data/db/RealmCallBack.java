package com.theredbean.rfid.data.db;

import com.theredbean.rfid.rxbus.penz.PendingObject;

import java.util.List;

/**
 * Created by hoang on 14/05/2018 nhe.
 */
public interface RealmCallBack {
    public interface AddPendingcallBack {
        void onAddSuccess();

        void onAddFaied(String msg);
    }

    public interface ReadPendingCallBack {
        void onReadSuccess(List<PendingObject> pendings);

        void onReadFailed(String msg);
    }

    public interface MoveCurrentToHistoryPending {
        void onMoveSuccess();

        void onMoveFailed(String msg);
    }

    public interface RemovePending {
        void onRemoveSuccess();

        void onRemoveFailed();
    }
}
