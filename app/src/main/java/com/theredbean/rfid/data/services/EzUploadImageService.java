package com.theredbean.rfid.data.services;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.theredbean.rfid.app.MyApp;
import com.theredbean.rfid.business.EzParkingCallback;
import com.theredbean.rfid.business.image.ImageModel;
import com.theredbean.rfid.business.image.ImageUsecase;
import com.theredbean.rfid.data.db.IDatabase;
import com.theredbean.rfid.di.components.ApplicationComponent;
import com.theredbean.rfid.di.components.DaggerEzParkingComponent;
import com.theredbean.rfid.di.modules.EzParkingModule;
import com.theredbean.rfid.rxbus.RxBus;
import com.theredbean.rfid.rxbus.RxBusConstants;
import com.theredbean.rfid.rxbus.RxBusEvent;

import org.parceler.Parcels;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.Queue;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;
import utils.FileUtils;
import utils.RxUltils;

/**
 * Created by nampham on 4/15/18.
 */
public class EzUploadImageService extends Service {

    @Inject
    RxBus rxBus;

    @Inject
    ImageUsecase imageUsecase;

    @Inject
    IDatabase iDatabase;

    @Inject
    MyApp myApp;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public void onCreate() {
        super.onCreate();
        Timber.tag(TAG).e("onCreate>>>>>>>");
        compositeDisposables = new CompositeDisposable();
        initInjector();
        registerBus();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterBus();
    }

    private void initInjector() {
        ApplicationComponent component = ((MyApp) getApplication()).getComponent();
        DaggerEzParkingComponent.builder()
                .applicationComponent(component)
                .ezParkingModule(new EzParkingModule())
                .build().inject(this);
    }

    private CompositeDisposable compositeDisposables;

    private void registerBus() {
        Disposable d = rxBus.toObservable().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o instanceof RxBusEvent) {
                        handleRxBus((RxBusEvent) o);
                    }
                });
        compositeDisposables.add(d);
    }

    private void unregisterBus() {
        compositeDisposables.clear();
    }

    private void handleRxBus(RxBusEvent event) {
        switch (event.what) {
            case UPLOAD_IMAGE:
                Timber.tag(TAG).e("2222222222222222222222222");
                ImageModel image = Parcels.unwrap(event.data.getParcelable(RxBusConstants.DATA));
//                imageQueue.add(image);
//                imageQueueProcessing();
//                new ConvertToWebpTask(this, image).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                ConvertToWebpTaskWithRx(image);
                break;
        }
    }

    private Queue<ImageModel> imageQueue = new LinkedList<>();
    private static final String TAG = EzUploadImageService.class.getSimpleName();
    private boolean isProcessing = false;

    private void imageQueueProcessing() {
        if (imageQueue.isEmpty()) {
            return;
        }
        if (isProcessing) {
            return;
        }
        isProcessing = true;
        ImageModel model = imageQueue.poll();
        imageUsecase.uploadImage(model, new EzParkingCallback.UploadCallback() {
            @Override
            public void onUploadSuccess(String downloadUrl) {

                // todo remove hash

                imageQueue.peek();
                Timber.tag(TAG).e("continue run");
                isProcessing = false;
                imageQueueProcessing();

            }

            @Override
            public void onUploadFailed(String msg) {
                Timber.tag(TAG).e("onUploadFailed>>>>>>");
                ImageModel model = imageQueue.peek();
                imageQueue.add(model);
                isProcessing = false;
                imageQueueProcessing();
            }

            @Override
            public void onExceptionError(String msg) {
                Timber.tag(TAG).e("onExceptionError>>>>>>");
                ImageModel model = imageQueue.peek();
                imageQueue.add(model);
                isProcessing = false;
                imageQueueProcessing();
            }

            @Override
            public void onExceptionAuthentication() {
                Timber.tag(TAG).e("authentication>>>>>>");
                //todo add save to sharePerferences
                isProcessing = false;
            }
        });
//        imageUsecase.saveLocalPath(model, new EzParkingCallback.NewLocalPathCallBack() {
//            @Override
//            public void onExceptionError(String msg) {
//
//            }
//
//            @Override
//            public void onExceptionAuthentication() {
//
//            }
//
//            @Override
//            public void updateSuccees() {
//                Timber.tag(TAG).e("localpathz updateSuccees");
//
//            }
//
//            @Override
//            public void onUpdateFalied(String msg) {
//                Timber.tag(TAG).e("localpathz onUpdateFalied");
//
//            }
//        });
    }


    private static class ConvertToWebpTask extends AsyncTask<Void, Void, String> {
        private final WeakReference<EzUploadImageService> ref;
        private ImageModel imageModel = null;

        public ConvertToWebpTask(EzUploadImageService service, ImageModel model) {
            this.ref = new WeakReference<>(service);
            this.imageModel = model;
        }


        @Override
        protected String doInBackground(Void... a) {
            String path = imageModel.getPath();
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            //create new file
            File file = FileUtils.createImageFile();
            try {
                FileOutputStream fos = new FileOutputStream(file);
                //compress file
                Matrix matrix = new Matrix();

                matrix.postRotate(90);
                Bitmap bm = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                bm.compress(Bitmap.CompressFormat.WEBP, 10, fos);


                fos.close();
                fos.flush();
                bm.recycle();
                return file.getPath();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null) {
                Timber.tag(TAG).e(">>>>>>>>>>>>>>>>>>>>>>>>.. %s", s);

                EzUploadImageService service = ref.get();
                imageModel.setLocalPath(s);


                if (service != null && ref.get() != null) {
                    Timber.tag(TAG).e("on post execute %s,", s);
                    //todo check internet
                    if (ref.get().myApp.isInternet) {
                        Timber.tag(TAG).e(">>>> have internet");

                        service.imageQueue.add(imageModel);
                        service.imageQueueProcessing();
                    } else {
                        Timber.tag(TAG).e(">>>> have internet without internet");
                        ref.get().imageUsecase.updatelocalPath(imageModel);

                        ref.get().iDatabase.addCurrentPending(imageModel);

                    }


                    //todo no internet => luu hash
                }

            }
        }
    }


    private void ConvertToWebpTaskWithRx(ImageModel imageModel) {
        // todo move to usecase / presenter or not  ????????
        RxUltils.copyfileFromCacheToStorage(imageModel)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(file -> {
                    imageModel.setLocalPath(file.getPath());
                    imageUsecase.updatelocalPath(imageModel);
                    // stuff
                    Timber.tag(TAG).e("RX doonNext");
                    if (myApp.isInternet) {
                        Timber.tag(TAG).e("Rx Onnext Is Internet ");
                        Timber.tag(TAG).e("Rx Onnext file Name :%s", file.getPath());
                        imageQueue.add(imageModel);
                        EzUploadImageService.this.imageQueueProcessing();
                    } else {
                        Timber.tag(TAG).e("Rx Onnext Is no Internet");

                        iDatabase.addCurrentPending(imageModel);
                    }

                })
                .doOnError(e -> Timber.tag(TAG).e("RX doOnError %s", e.getMessage()))
                .doOnComplete(() -> {
                    Timber.tag(TAG).e("RX doonComplete ");

                })
                .doOnSubscribe(d -> Timber.tag(TAG).e("RX doOnSubscribe "))
                .subscribe();

    }

}
