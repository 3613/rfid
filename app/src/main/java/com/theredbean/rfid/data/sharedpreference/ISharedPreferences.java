package com.theredbean.rfid.data.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hoang on 19/03/2018.
 */

public interface ISharedPreferences {

    boolean savePreferences(String key, String value);

    boolean saveAllPriceConfig(int price_ga_day,
                               int price_so_day,
                               int price_dap_day,
                               int price_ga_night,
                               int price_so_night,
                               int price_dap_night);

    String readPreferences(String key, String defauleValue);

    String getString(String key, String defaultValue);

    String getString(String key);

    void putString(String key, String value);

    void checkForNullKey(String key);

    void checkForNullValue(String value);


}
