package com.theredbean.rfid.data.network;


/**
 * Created by hnam on 8/16/2016.
 */
public interface RxApi {
    //String API_ENDPOINT = "http://hr.soincorp.com/api/"; // TESTING
    String API_ENDPOINT = "http://soinsurveillance.com/api/"; // PRODUCT
    String API_KEY = "y1kanskdy1ihaisuydh813ydasasdt187eas";


    //it contains all api before sign-in
    interface Anon {

//        @FormUrlEncoded
//        @POST("users/sign_in?api_token="+API_KEY)
//        Observable<RespSignInEntity> signIn(@Field("username") String username,
//                                            @Field("password") String password);
//
//        @GET("users/check_token/token_key/{token_key}?api_token="+API_KEY)
//        Observable<RespCheckToken> checkToken(@Path("token_key") String token);
//
//        @FormUrlEncoded
//        @POST("users/reset_password?api_token="+API_KEY)
//        Observable<RespResetPassword> resetPassword(@Field("email") String email);
//
//        @FormUrlEncoded
//        @POST("users/sign_in_facebook?api_token="+API_KEY)
//        Observable<RespSignInFacebook> signInFacebook(@Field("email") String email,
//                                                      @Field("display_name") String displayName);
//
//        @FormUrlEncoded
//        @POST("users/sign_in_google?api_token="+API_KEY)
//        Observable<RespSignInFacebook> signInGoogle(@Field("email") String email,
//                                                    @Field("display_name") String displayName);
    }

    //it contains all apis after signed in
    interface Auth {

//        @GET("product_categories/list?&api_token="+API_KEY)
//        Observable<RespCategoryListEntity> getCategories(@Query("parent_id") String parentId, @Query("s") String keySearch);
//
//        @GET("products/list?&api_token="+API_KEY)
//        Observable<RespProductList> getProducts(@Query("sub_category_id") String subCategoryId
//                , @Query("s") String keySearch);
//
//        @GET("products/detail/id/{id}?api_token="+API_KEY)
//        Observable<RespDetailProductEntity> getDetailProduct(@Path("id") String id);
//
//        @GET("quotations/change_status/id/{id}/status/{status}?api_token="+API_KEY)
//        Observable<RespChangeStatusQuotation> changeQuotationStatus(
//                @Path("id") String quotationId,
//                @Path("status") String status,
//                @Query("access_key") String accessToken);
//
//
//
//        @FormUrlEncoded
//        @POST("quotations/list?api_token="+API_KEY)
//        Observable<RespBaoGiaList> getBaoGia(@Field("p") String p,
//                                             @Field("ppp") String ppp,
//                                             @Field("status") String status,
//                                             @Query("access_key") String accessToken);
//
//        @FormUrlEncoded
//        @POST("quotations/list?api_token="+API_KEY)
//        Observable<RespCongNoList> getCongNo(@Field("p") String p,
//                                             @Field("ppp") String ppp,
//                                             @Field("paid_status") String status,
//                                             @Query("access_key") String accessToken);
//
//        @FormUrlEncoded
//        @POST("customers/list?api_token="+API_KEY)
//        Observable<RespGetCustomers> getCustomers(@Field("p") String p,
//                                                  @Field("ppp") String ppp);
//
//        @FormUrlEncoded
//        @POST("quotations/addEmployee?api_token="+API_KEY)
//        Observable<RespCreateQuotation> createQuotation(@Query("access_key") String accessToken,
//                                                        @Field("title") String title,
//                                                        @Field("status") String status,
//                                                        @Field("discount") String discount,
//                                                        @Field("discount_service") String discountService,
//                                                        @Field("vat") String vat,
//                                                        @Field("hikconnect_email") String hikconnectEmail,
//                                                        @Field("hikconnect_username") String hikconnectUsername,
//                                                        @Field("hikconnect_password") String hikconnectPassword,
//                                                        @Field("customer_id") String customerId,
//                                                        @Field("date_added") String createdDate,
//                                                        @Field("date_signed") String approvedDate,
//                                                        @Field("date_acceptance") String DeliveryDate,
//                                                        @Field("date_invoiced") String invoiceDate,
//                                                        @Field("date_paid") String paidDate,
//                                                        @Field("date_warranty_expired") String warrantyDate,
//                                                        @Field("description") String description);
//        @FormUrlEncoded
//        @POST("quotations/addEmployee?api_token="+API_KEY)
//        Observable<RespCreateQuotation> createQuotation(@Query("access_key") String accessToken,
//                                                        @Field("title") String title,
//                                                        @Field("status") String status,
//                                                        @Field("discount") String discount,
//                                                        @Field("discount_service") String discountService,
//                                                        @Field("vat") String vat,
//                                                        @Field("hikconnect_email") String hikconnectEmail,
//                                                        @Field("hikconnect_username") String hikconnectUsername,
//                                                        @Field("hikconnect_password") String hikconnectPassword,
//                                                        @Field("customer_id") String customerId,
//                                                        @Field("customer_title") String customerName,
//                                                        @Field("customer_address") String customerAddress,
//                                                        @Field("customer_phone") String customerPhone,
//                                                        @Field("customer_email") String customerEmail,
//                                                        @Field("date_added") String createdDate,
//                                                        @Field("date_signed") String approvedDate,
//                                                        @Field("date_acceptance") String DeliveryDate,
//                                                        @Field("date_invoiced") String invoiceDate,
//                                                        @Field("date_paid") String paidDate,
//                                                        @Field("date_warranty_expired") String warrantyDate,
//                                                        @Field("description") String description);
//
//        @FormUrlEncoded
//        @POST("quotations/edit/id/{id}?api_token="+API_KEY)
//        Observable<RespCreateQuotation> editQuotation(@Path("id") String quotationId, @Query("access_key") String accessToken,
//                                                      @Field("title") String title,
//                                                      @Field("status") String status,
//                                                      @Field("discount") String discount,
//                                                      @Field("discount_service") String discountService,
//                                                      @Field("vat") String vat,
//                                                      @Field("hikconnect_email") String hikconnectEmail,
//                                                      @Field("hikconnect_username") String hikconnectUsername,
//                                                      @Field("hikconnect_password") String hikconnectPassword,
//                                                      @Field("customer_id") String customerId,
//                                                      @Field("date_added") String createdDate,
//                                                      @Field("date_signed") String approvedDate,
//                                                      @Field("date_acceptance") String DeliveryDate,
//                                                      @Field("date_invoiced") String invoiceDate,
//                                                      @Field("date_paid") String paidDate,
//                                                      @Field("date_warranty_expired") String warrantyDate,
//                                                      @Field("description") String description);
//
//        @FormUrlEncoded
//        @POST("quotations/edit/id/{id}?api_token="+API_KEY)
//        Observable<RespCreateQuotation> editQuotation(@Path("id") String quotationId, @Query("access_key") String accessToken,
//                                                      @Field("title") String title,
//                                                      @Field("status") String status,
//                                                      @Field("discount") String discount,
//                                                      @Field("discount_service") String discountService,
//                                                      @Field("vat") String vat,
//                                                      @Field("hikconnect_email") String hikconnectEmail,
//                                                      @Field("hikconnect_username") String hikconnectUsername,
//                                                      @Field("hikconnect_password") String hikconnectPassword,
//                                                      @Field("customer_id") String customerId,
//                                                      @Field("customer_title") String customerName,
//                                                      @Field("customer_address") String customerAddress,
//                                                      @Field("customer_phone") String customerPhone,
//                                                      @Field("customer_email") String customerEmail,
//                                                      @Field("date_added") String createdDate,
//                                                      @Field("date_signed") String approvedDate,
//                                                      @Field("date_acceptance") String DeliveryDate,
//                                                      @Field("date_invoiced") String invoiceDate,
//                                                      @Field("date_paid") String paidDate,
//                                                      @Field("date_warranty_expired") String warrantyDate,
//                                                      @Field("description") String description);
//
//        @FormUrlEncoded
//        @POST("questions/list?api_token="+API_KEY)
//        Observable<RespQuestionList> getQuestionList(@Query("access_key") String accessToken,
//                                                     @Field("p") String p,
//                                                     @Field("ppp") String ppp);
//
//        @FormUrlEncoded
//        @POST("products/list?api_token="+API_KEY)
//        Observable<RespProductList> getProductList(@Query("access_key") String accessToken,
//                                                   @Field("p") String p,
//                                                   @Field("ppp") String ppp);
//
//        @FormUrlEncoded
//        @POST("packages/list?api_token="+API_KEY)
//        Observable<RespPackageList> getPackageList(@Query("access_key") String accessToken,
//                                                   @Field("p") String p,
//                                                   @Field("ppp") String ppp);
//
//        @POST("quotation_items/addEmployee?api_token="+API_KEY)
//        Observable<RespCreateQuotation> addQuotationItem(@Query("access_key") String accessToken,
//                                                         @Body RequestBody body);
//
//        @POST("quotation_materials/addEmployee?api_token="+API_KEY)
//        Observable<RespCreateQuotation> addQuotationMaterial(@Query("access_key") String accessToken,
//                                                             @Body RequestBody body);
//
//        @POST("quotation_questions/addEmployee?api_token="+API_KEY)
//        Observable<RespAddQuestionQuotation> addQuotationQuestion(@Query("access_key") String accessToken,
//                                                                  @Body RequestBody requestBody);
//
//        @FormUrlEncoded
//        @POST("quotation_questions/edit/id/{id}?api_token="+API_KEY)
//        Observable<RespEditQuestion> editQuotationQuestion(@Query("access_key") String accessToken,
//                                                           @Path("id") String questionId,
//                                                           @Field("answer") String answer);
//
//        @FormUrlEncoded
//        @POST("quotation_payment_progress/addEmployee?api_token="+API_KEY)
//        Observable<RespAddPayment> addQuotationPayment(@Query("access_key") String accessToken,
//                                                       @Field("quotation_id") String quotationId,
//                                                       @Field("title") String title,
//                                                       @Field("price") String price,
//                                                       @Field("price_vnd") String priceVnd,
//                                                       @Field("price_rub") String priceRub,
//                                                       @Field("price_cny") String priceCny,
//                                                       @Field("deadline") String deadline);
//
//        @FormUrlEncoded
//        @POST("quotation_payment_progress/edit/id/{id}?api_token="+API_KEY)
//        Observable<RespAddPayment> editQuotationPayment(@Query("access_key") String accessToken,
//                                                        @Path("id") String paymentId,
//                                                        @Field("title") String title,
//                                                        @Field("price") String price,
//                                                        @Field("price_vnd") String priceVnd,
//                                                        @Field("price_rub") String priceRub,
//                                                        @Field("price_cny") String priceCny,
//                                                        @Field("deadline") String deadline);
//
//
//        @FormUrlEncoded
//        @POST("packages/list?api_token="+API_KEY)
//        Observable<RespDoanhSoList> getDoanhSoList(@Query("access_key") String accessToken,
//                                                   @Field("p") String p,
//                                                   @Field("ppp") String ppp);
//
//        @FormUrlEncoded
//        @POST("salaries/list?api_token="+API_KEY)
//        Observable<RespBangLuongList> getBangLuongList(@Query("access_key") String accessToken,
//                                                       @Field("user_id") String userId,
//                                                       @Field("start_date") String startDate,
//                                                       @Field("end_date") String endDate);
   }
}
