package utils;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.theredbean.rfid.configure.FirebaseConstant;

import timber.log.Timber;

/**
 * Created by hoang on 10/05/2018 nhe.
 */
public class InternetStatusHelper {
    public static final String TAG = InternetStatusHelper.class.getSimpleName();

    public interface InternetStatusHelperListener {
        public void onOnline();

        public void onOffline();
    }

    private InternetStatusHelperListener mListener;

    public void setListener(InternetStatusHelperListener l) {
        mListener = l;
    }

    public void rcheck() {
        Timber.tag(TAG).e("start check internet --->>><<<<");

        DatabaseReference appstatusReference =
                FirebaseDatabase.getInstance().getReference(FirebaseConstant.KEY_ONLINE_STATE);

        appstatusReference.onDisconnect().setValue("offlinez");
        DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    Timber.tag(TAG).e("onstatechange %s", "onOnline");
                    appstatusReference.setValue("online9");
                    mListener.onOnline();
                } else {
                    Timber.tag(TAG).e("onstatechange %s", "offline");
                    appstatusReference.setValue("offline");
                    mListener.onOffline();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Timber.tag(TAG).e("Listener was cancelled");
            }
        });
    }

}
