package utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by hoang on 02/04/2018 nhe.
 */
@GlideModule
public class MyGlide extends AppGlideModule {
    // this class with anotation for create GlideApp class
}
