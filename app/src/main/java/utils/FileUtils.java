package utils;

import android.annotation.SuppressLint;
import android.os.Environment;

import java.io.File;
import java.util.Calendar;

/**
 * Created by nampham on 4/16/18.
 */
public class FileUtils {

    /**
     * create image file in RedBean folder
     * @return
     */
    public static File createImageFile() {
        // Tạo thư mục chứa hình
        File folder = new File(Environment.getExternalStorageDirectory() + EzConstants.IMAGE_FOLDER_NAME);
        if (!folder.exists()) {
            @SuppressLint("SdCardPath")
            File root = new File(EzConstants.IMAGE_PATH);
            root.mkdirs();
        }


        // tên tên hình là thời gian
        @SuppressLint("SdCardPath")
        File file = new File(new File(EzConstants.IMAGE_PATH), Calendar.getInstance().getTimeInMillis() + ".jpg");
        if (file.exists()) {
            file.delete();
        }

        return file;
    }
}
