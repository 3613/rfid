package utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.support.v4.widget.CircularProgressDrawable;

import com.theredbean.rfid.R;

/**
 * Created by hoang on 22/05/2018 nhe.
 */
public class ProgressBarUtils {
    private static CircularProgressDrawable loading;

    public static CircularProgressDrawable getInstanceLoadingDrawable(Context context) {
        if (loading == null) {
            loading = new CircularProgressDrawable(context);
        }
        loading.setCenterRadius(50f);
        loading.setStrokeWidth(5f);
        loading.setColorSchemeColors(R.color.redcenter);
        loading.setColorFilter(Color.RED, PorterDuff.Mode.SCREEN);
//        loading.setBackgroundColor(R.color.default_color); //
        loading.start();
        return loading;
    }


    private static ProgressDialog progressDialog;

    /**
     * This static function can be called anywhere to show the progress dialog, especially while
     * calling an api or execute a heavy processing at background
     *
     * @param context: Application Context
     */
    /**
     * This static function can be called anywhere to show the progress dialog, especially while
     * calling an api or execute a heavy processing at background
     *
     * @param context: Application Context
     */
    public static void showWaitingDialog(Context context) {
        progressDialog = new ProgressDialog(context, R.style.ProgressBar);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        progressDialog.show();
        try {
            Handler mainHandler = new Handler(context.getMainLooper());

            Runnable myRunnable = new Runnable() {
                @Override
                public void run() {
                    try{
                        progressDialog.show();

                    }catch (Exception e2){
                        e2.printStackTrace();
                    }
                }
            };
            mainHandler.post(myRunnable);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
    /**
     * Call this method to close the progress dialog if any
     */
    public static void hideWaitingDialog() {
        if (progressDialog != null) {
            try {
                progressDialog.dismiss();
                progressDialog = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
