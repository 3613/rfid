package utils;

import com.google.gson.Gson;
import com.theredbean.rfid.data.db.record.CurrentRecord;
import com.theredbean.rfid.data.db.record.HistoryRecord;
import com.theredbean.rfid.data.sharedpreference.ISharedPreferences;
import com.theredbean.rfid.models.FHistoryRecord;

import javax.inject.Inject;

/**
 * Created by hoang on 23/03/2018 nhe.
 */

public class PriceUtils {
    public static final String TAG = PriceUtils.class.getSimpleName();

    Gson gson;
    @Inject

    ISharedPreferences sharedPreferences;


    public static int priceOf(CurrentRecord record) {
        int p = 0;


        switch (record.getType()) {
            case EzConstants.BikeType.XE_SO:
                p = EzConstants.PRICE_B_XE_SO_D;
                break;
            case EzConstants.BikeType.XE_GA:
                p = EzConstants.PRICE_A_XE_GA_D;
                break;
            case EzConstants.BikeType.XE_DAP:
                p = EzConstants.PRICE_C_XE_DAP_D;
                break;
        }

        return p;
    }

    public static int priceMustPay(CurrentRecord record) {
        int p = 0;
        int packedDate = Integer.parseInt((TimeUtils.totalDayParked(String.valueOf(record.getCheckInTime()))));

        switch (record.getType()) {
            case EzConstants.BikeType.XE_GA:
                p = EzConstants.PRICE_A_XE_GA_D * packedDate;
                break;
            case EzConstants.BikeType.XE_SO:
                p = EzConstants.PRICE_B_XE_SO_D * packedDate;
                break;
            case EzConstants.BikeType.XE_DAP:
                p = EzConstants.PRICE_C_XE_DAP_D * packedDate;
                break;
        }
        return p;
    }

    public static int priceMustPayHistory(HistoryRecord record) {
        int p = 0;
        long start = record.getCheckInTime();
        long target = record.getCheckOutTime();

        int packedDate = Integer.parseInt(TimeUtils.totalDayParkedHistory(start, target));

        switch (record.getType()) {
            case EzConstants.BikeType.XE_GA:
                p = EzConstants.PRICE_A_XE_GA_D * packedDate;
                break;
            case EzConstants.BikeType.XE_SO:
                p = EzConstants.PRICE_B_XE_SO_D * packedDate;
                break;
            case EzConstants.BikeType.XE_DAP:
                p = EzConstants.PRICE_C_XE_DAP_D * packedDate;
                break;
        }
        return p;
    }

    public static int priceMustPayHistory(FHistoryRecord record) {
        int p = 0;
        long start = record.getTimestampCheckIn();
        long target = record.getTimestampCheckOut();

        int packedDate = Integer.parseInt(TimeUtils.totalDayParkedHistory(start, target));

        switch (record.getType()) {
            case EzConstants.BikeType.XE_GA:
                p = EzConstants.PRICE_A_XE_GA_D * packedDate;
                break;
            case EzConstants.BikeType.XE_SO:
                p = EzConstants.PRICE_B_XE_SO_D * packedDate;
                break;
            case EzConstants.BikeType.XE_DAP:
                p = EzConstants.PRICE_C_XE_DAP_D * packedDate;
                break;
        }
        return p;
    }

}
