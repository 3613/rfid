package utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.theredbean.rfid.business.image.ImageModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import rx.Subscriber;
import rx.functions.Action0;
import rx.subscriptions.Subscriptions;


/**
 * Created by hoang on 22/05/2018 nhe.
 */
public class RxUltils {
    public static Observable<File> copyfileFromCacheToStorage(ImageModel imageModel) {
        return Observable.create(emitter -> {
            String path = imageModel.getPath();
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            //create new file
            File file = FileUtils.createImageFile();
            try {
                FileOutputStream fos = new FileOutputStream(file);
                //compress file
                Matrix matrix = new Matrix();

                matrix.postRotate(90);
                Bitmap bm = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                bm.compress(Bitmap.CompressFormat.WEBP, 10, fos);


                fos.close();
                fos.flush();
                bm.recycle();
                emitter.onNext(file);
            } catch (IOException e) {
                e.printStackTrace();
                emitter.onError(e);
            }
            emitter.onComplete();
        });
    }

    public static Observable<Long> getTimerObservable() {
        return Observable
                .interval(1, TimeUnit.SECONDS)
                .take(4)
                .map(v -> 3 - v);
    }

}
