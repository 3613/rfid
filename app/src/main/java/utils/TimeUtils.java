package utils;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.SimpleDateFormat;
import java.util.Date;

import timber.log.Timber;

/**
 * Created by hoang on 26/03/2018 nhe.
 */

public class TimeUtils {
    public static final String TAG = TimeUtils.class.getSimpleName();

    public static String getDate(String milliSeconds) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd");
        String dateString = formatter.format(new Date(Long.parseLong(milliSeconds)));
        Timber.tag(TAG).e(dateString);
        return dateString;
    }

    public static String totalDayParked(String dateParkedFrom) {
        DateTime start = new DateTime(Long.valueOf(dateParkedFrom));
        DateTime target = new DateTime();  //NOW
        int date = Days.daysBetween(start.toLocalDate(), target.toLocalDate()).getDays();
        String totalDayParked = String.valueOf(date + 1);
        Timber.tag(TAG).e("totalDayParked: %s", totalDayParked);
        return totalDayParked;
    }

    public static String totalDayParkedHistory(long dateCheckIn, long dateCheckout) {
        DateTime start = new DateTime(dateCheckIn);
        DateTime target = new DateTime(dateCheckout);

        int totalDaypacked = Days.daysBetween(start.toLocalDate(), target.toLocalDate()).getDays();
        Timber.tag(TAG).e("totalDayParked %s:  ", String.valueOf(totalDaypacked + 1));
        return String.valueOf(totalDaypacked + 1);

    }

    public static long plusOneDay() {
        return DateTime.now().plusDays(-1).withTimeAtStartOfDay().getMillis();
    }


    public static long plusOneWeek() {
        return DateTime.now().plusWeeks(-1).withDayOfWeek(1).getMillis();
    }

    public static long plusOneMonth() {
        return DateTime.now().plusMonths(-1).withDayOfMonth(1).withTimeAtStartOfDay().getMillis();
    }

    public static long plusTherreMonth() {
        return DateTime.now().plusMonths(-3).withDayOfMonth(1).withTimeAtStartOfDay().getMillis();

    }

    public static long plusOneYear() {
        return DateTime.now().plusYears(-1).withMonthOfYear(1).withDayOfYear(1).withTimeAtStartOfDay().getMillis();
    }

    public static long getCurrentInMiliseconds() {
        return new DateTime().getMillis();
    }


    public static String getTimeText(long m) {
        final DateTime dt = new DateTime(m);


        return dt.toString("dd/MM/YYY - H:mm ");


        // might output "6 October, 2013"

    }

    public static String get_HH_MM_SS() {
        final DateTime dt = new DateTime(getCurrentInMiliseconds());
        return dt.toString("H:mm:ss");

    }

    public static long getStartOfDay() {
        return new DateTime().withTimeAtStartOfDay().getHourOfDay();
    }

}
