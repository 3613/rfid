package utils;

/**
 * Created by hoang on 06/04/2018 nhe.
 */
public class FragmentConstants {
    public static final String FRAGMENT_TAG_KEY = "fragTag";

    public static final int FRAGMENT_BASE = 0;


    public static final int FRAGMENT_PARKING_LIST = FRAGMENT_BASE + 1;
    public static final int FRAGMENT_EMPLOYEE_LIST = FRAGMENT_BASE + 2;
    public static final int FRAGMENT_REVENUE = FRAGMENT_BASE + 3;
    public static final int FRAGMENT_ACCOUNT = FRAGMENT_BASE + 4;
    public static final int FRAGMENT_CONFIG = FRAGMENT_BASE + 5;
    public static final int FRAGMENT_HISTORY = FRAGMENT_BASE + 6;
    public static final int FRAGMENT_SUMMARY = FRAGMENT_BASE + 7;
    public static final int FRAGMENT_USER_PROFILE = FRAGMENT_BASE + 8;
    public static final int FRAGMENT_ADD_PARKING = FRAGMENT_BASE + 9;
    public static final int FRAGMENT_INCOME_DETAIL = FRAGMENT_BASE + 10;


}
