package utils;

import com.theredbean.rfid.business.EzParkManager;
import com.theredbean.rfid.models.Pricez.FPrice;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by hoang on 20/04/2018 nhe.
 */
public class PriceCalculator {
    public static final String TAG = PriceCalculator.class.getSimpleName();


    @Inject
    public PriceCalculator(EzParkManager userManger) {

    }

    public static int priceOf(int type, FPrice price) {
        int p = 0;
        switch (type) {
            case EzConstants.BikeType.XE_SO:
                p = price.getXe_so().getDay();
                Timber.tag(TAG).e("gia la :%d", p);
                break;
            case EzConstants.BikeType.XE_GA:
                p = price.getXe_ga().getDay();
                Timber.tag(TAG).e("gia la :%d", p);
                break;
            case EzConstants.BikeType.XE_DAP:
                p = price.getXe_dap().getDay();
                Timber.tag(TAG).e("gia la :%d", p);
                break;
        }

        return p;
    }
}
