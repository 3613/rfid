package utils;

/**
 * Created by hoang on 16/03/2018.
 */

public class EzConstants {

    public class BikeType {
        public static final int XE_GA = 1;
        public static final int XE_SO = 2;
        public static final int XE_DAP = 3;
        public static final int XE_TAO_LAO = -2;
    }


    public static int PRICE_A_XE_GA_D = -1;
    public static int PRICE_B_XE_SO_D = -1;
    public static int PRICE_C_XE_DAP_D = -1;
    public static int PRICE_D_XE_GA_N = -1;
    public static int PRICE_E_XE_SO_N = -1;
    public static int PRICE_F_XE_DAP_N = -1;


    public static final String IMAGE_FOLDER_NAME = "/RedbeanBike";
    public static final String IMAGE_PATH = "/sdcard" + IMAGE_FOLDER_NAME + "/";

    // shared prefercences constants
    public static final String EXTRA_PRICE = "EXTRA_PRICE";
    public static final String EXTRA_TEST = "EXTRA_TEST";

    public static final String EXTRAS_PHOTO_URL = "EXTRAS_PHOTO_URL";
    public static final String EXTRAS_DEVICE_ID = "EXTRAS_DEVICE_ID";

    public static final String STATUS_INTERNET = "internet";


    // realm sttue

    public static final int CHECKOUT_SUCCESS = 2;
    public static final int CHECKOUT_FALIED = 4;

    /*
    *
    *           EzConstants. = a;
                EzConstants. = b;
                EzConstants. = c;
                EzConstants. = d;
                EzConstants. = e;
                EzConstants. = f;
    * */
    public static final String KEY_PRICE_A_XE_GA_D = "KEY_PRICE_A_XE_GA_D";
    public static final String KEY_PRICE_B_XE_SO_D = "KEY_PRICE_B_XE_SO_D";
    public static final String KEY_PRICE_C_XE_DAP_D = "KEY_PRICE_C_XE_DAP_D";
    public static final String KEY_PRICE_D_XE_GA_N = "KEY_PRICE_D_XE_GA_N";
    public static final String KEY_PRICE_E_XE_SO_N = "KEY_PRICE_E_XE_SO_N";
    public static final String KEY_PRICE_F_XE_DAP_N = "KEY_PRICE_F_XE_DAP_N";


    public static String[] listKeyPref() {

        String[] listKey = {KEY_PRICE_A_XE_GA_D, KEY_PRICE_B_XE_SO_D, KEY_PRICE_C_XE_DAP_D, KEY_PRICE_D_XE_GA_N, KEY_PRICE_E_XE_SO_N, KEY_PRICE_F_XE_DAP_N};

        return listKey;
    }


    /// for bundle

    public static final String KEY_BUNDLE_FRAGMENT_TAG = "fragtag";
    public static final String KEY_BUNDLE_POSITION_ITEM = "position";
    public static final String KEY_BUNDLE_PARKING_UUID = "parkingUUID";
    public static final String BUNDLE_DEFAULT = "default";
    public static final String KEY_FIRST_CONFIG_PRICE = "firtconfig";


}
