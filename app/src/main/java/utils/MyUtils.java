package utils;

import java.text.DecimalFormat;

import timber.log.Timber;

/**
 * Created by hoang on 19/03/2018.
 */

public class MyUtils {
    private static final String TAG = MyUtils.class.getSimpleName();

    public static String formatEpc(String s) {
        return s.trim().replace(' ', '_');
    }

    public static String fomatVND(int number) {
        DecimalFormat formatter = new DecimalFormat("###,###,###");

        return formatter.format(number);
    }

    public static int typeOfCard(String cardId) {
        int type = EzConstants.BikeType.XE_TAO_LAO;
        if (cardId.startsWith("RB_DAP_")) {
            type = EzConstants.BikeType.XE_DAP;

        }
        if (cardId.startsWith("RB_GA_")) {
            type = EzConstants.BikeType.XE_GA;
        }
        if (cardId.startsWith("RB_SO_")) {
            type = EzConstants.BikeType.XE_SO;
        }
        if (cardId.equals("") || cardId.equals("")) {
             type = EzConstants.BikeType.XE_TAO_LAO;
        }

        Timber.tag(TAG).e("my tag type: %d", type);
        return type;
    }


}
