package utils;

/**
 * Created by hoang on 16/04/2018 nhe.
 */
public interface AlertDialogListener {
    void onAlertConfirmClick();

    void onAlertCancelClick();

}

