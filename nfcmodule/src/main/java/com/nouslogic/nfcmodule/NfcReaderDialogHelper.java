package com.nouslogic.nfcmodule;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by tiencao on 10/15/17.
 */

public class NfcReaderDialogHelper {

    private boolean D = false;

    private NfcReaderListener mListener;

    private Activity activity;
    private NfcAdapter nfcAdapter;


    private boolean readyRead = false;


    private AlertDialog alertDialog = null;
    private int nfcStatus = 0;

    public NfcReaderDialogHelper(Activity a ){
        activity = a;

    }

    private void setLog(String str){
        if(D){
            Log.e("NfcHelper", str);
        }
    }

    public void onCreate(){
        nfcAdapter = NfcAdapter.getDefaultAdapter(activity);
        if(nfcAdapter != null){
            if(nfcAdapter.isEnabled()){
                nfcIsEnable();
                nfcStatus = Constant.NFC_ACTIVE;
            }else{

                nfcIsDisable();
                nfcStatus = Constant.NFC_DISABLE;
            }
        }else{

            nfcNotSupport();
            nfcStatus = Constant.NFC_NOT_SUPPORT;
        }
    }

    public void startReadNFC(){
        if(nfcAdapter != null){
            if(nfcAdapter.isEnabled()){
                nfcStatus = Constant.NFC_ACTIVE;
            }else{
                nfcStatus = Constant.NFC_DISABLE;
            }
        }

        if( nfcStatus == Constant.NFC_ACTIVE) {
            readyRead = true;
            readNFCDialog();
        }else if( nfcStatus == Constant.NFC_DISABLE) {
            nfcReadError(Constant.ERROR_NFC_DISABLE);
        }else if( nfcStatus == Constant.NFC_NOT_SUPPORT) {
            nfcReadError(Constant.ERROR_NFC_NOT_SUPPORT);
        }
    }

    /**
     * Call in onNewIntent() <br>
     *  */
    public void onNewIntent(Intent intent){
        if(readyRead){

            Parcelable[] parcelables = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

            if(parcelables != null && parcelables.length > 0){
                NfcReadText nfcReadText = new NfcReadText(activity);
                nfcReadText.setDebug(D);
                nfcReadText.setListener(new NfcReadText.ReadTextListener() {
                    @Override
                    public void noNDERecordFound() {
                        //Have message but not record
                        nfcReadError(Constant.ERROR_READ_TAG_MESSAGE_NO_RECORD);
                    }

                    @Override
                    public void tagContent(String message) {
                        //Have message text content
                        setLog(" Read Mode: NfcHelper tagContent message: " + message);
                        nfcReadMessageContent(message);

                    }
                });
                nfcReadText.readTextFromMessage((NdefMessage) parcelables[0]);
            }else{
                setLog(" Read Mode: NfcHelper No message");
                nfcReadError(Constant.ERROR_READ_TAG_NO_MESSAGE);
            }
        }
    }





    /**
     * Enable foreground dispatch (gui hang) nfc  <br>
     *Call in onResume function <br>
     *@param aclass : The activity class, Class a = getClass;
     *  */
    public void onResume(Class aclass){
        if(nfcStatus != Constant.NFC_NOT_SUPPORT) {
            setLog("startWriteTag");
            IntentFilter filter = new IntentFilter(Constant.ACTION_ADAPTER_STATE_CHANGED);
            activity.registerReceiver(mReceiver, filter);


            Intent intent = new Intent(activity, aclass).addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
            PendingIntent pendingIntent = PendingIntent.getActivity(activity, 0, intent, 0);
            IntentFilter[] intentFilter = new IntentFilter[]{};
            nfcAdapter.enableForegroundDispatch(activity, pendingIntent, intentFilter, null);
        }

    }


    /**
     * Disable foreground dispatch (gui hang) nfc  <br>
     *Call in onPause function <br>
     *  */
    public void onPause(){
        if(nfcStatus != Constant.NFC_NOT_SUPPORT) {
            setLog("stopWriteTag");
            activity.unregisterReceiver(mReceiver);
            nfcAdapter.disableForegroundDispatch(activity);
        }

    }


    public void setOnlistenerNfc(NfcReaderListener l){
        mListener = l;
    }




    //TODO read mode

    private void nfcReadMessageContent(String content){
        if(mListener != null){
            mListener.nfcReadModeReadMessage(content);
        }
        dismissDialog();
        readyRead = false;
    }

    private void nfcReadError(int error){
        if(mListener != null){
            mListener.nfcReadModeFail(error);
        }
        dismissDialog();
        readyRead = false;
    }



    private void nfcIsEnable(){
        if(mListener != null){
            mListener.nfcIsEnable();
        }
    }

    private void nfcIsDisable(){
        if(mListener != null){
            mListener.nfcIsDisable();
        }
    }

    private void nfcNotSupport(){
        if(mListener != null){
            mListener.nfcIsNotSupport();
        }
    }


    private void readNFCDialog(){
        dismissDialog();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.read_dialog, null);
        ImageView img = (ImageView) dialogView.findViewById(R.id.read_tag_imv);
        AnimationDrawable animation = (AnimationDrawable) img.getDrawable();
        animation.start();

        Button btnCancel = (Button) dialogView.findViewById(R.id.read_tag_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
                readyRead = false;
            }
        });

        dialogBuilder.setView(dialogView);
        // EditText editText = (EditText) dialogView.findViewById(R.id.label_field);
        //editText.setText("test label");
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void dismissDialog(){
        if(alertDialog != null){
            alertDialog.dismiss();
            alertDialog = null;
        }
    }


    public interface  NfcReaderListener {
        void nfcIsDisable();
        void nfcIsEnable();
        void nfcIsNotSupport();
        void nfcTurnOn();
        void nfcTurnOff();
        //Read Mode
        void nfcReadModeFail(int error);
        void nfcReadModeReadMessage(String message);
    }


    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(NfcAdapter.ACTION_ADAPTER_STATE_CHANGED)) {
                final int state = intent.getIntExtra(Constant.EXTRA_ADAPTER_STATE, Constant.STATE_OFF);
                setLog("BroadcastReceiver state: " + state);
                switch (state) {
                    case NfcAdapter.STATE_OFF:
                        if(mListener != null){
                            mListener.nfcTurnOff();
                        }
                        break;
                    case NfcAdapter.STATE_ON:
                        if(mListener != null){
                            mListener.nfcTurnOn();
                        }
                        break;
                }
            }
        }
    };


}
