package com.nouslogic.nfcmodule;

/**
 * Created by tiencao on 10/17/17.
 */

public class Constant {

    //Action mode
    public final static int NONE_MODE = 0;
    public final static int READ_MODE = 1;
    public final static int WRITE_MODE = 2;

    //Write error
    public final static int ERROR_WRITE_TAG_NOT_NDEF_FORMATABLE = 1;
    public final static int ERROR_WRITE_TAG_NOT_WRITEABLE = 2;

    //Read error
    public final static int ERROR_READ_TAG_NO_MESSAGE = 3;
    public final static int ERROR_READ_TAG_MESSAGE_NO_RECORD= 4;

    public final static int ERROR_NFC_NOT_SUPPORT = 5;
    public final static int ERROR_NFC_DISABLE = 6;


    //NFC status
    public final static int NFC_ACTIVE = 1;
    public final static int NFC_DISABLE = 2;
    public final static int NFC_NOT_SUPPORT = 3;


    public final static String ACTION_ADAPTER_STATE_CHANGED = "android.nfc.action.ADAPTER_STATE_CHANGED";
    public final static String EXTRA_ADAPTER_STATE = "android.nfc.extra.ADAPTER_STATE";
    public final static int STATE_OFF = 1;





}
