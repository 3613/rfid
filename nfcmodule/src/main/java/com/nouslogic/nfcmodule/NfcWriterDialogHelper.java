package com.nouslogic.nfcmodule;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by tiencao on 10/15/17.
 */

public class NfcWriterDialogHelper {

    public boolean DEBUG = false;

    private NfcWriterListener mListener;
    private Activity activity;
    private NfcAdapter nfcAdapter;

    private String messageWrite = "Hello world";
    private AlertDialog alertDialog = null;

    private boolean readyWrite = false;


    private int nfcStatus = 0;

    public NfcWriterDialogHelper(Activity a ){
        activity = a;
    }

    private void setLog(String str){
        if(DEBUG){
            Log.e("NfcHelper", str);
        }
    }

    public void onCreate(){
        nfcAdapter = NfcAdapter.getDefaultAdapter(activity);
        if(nfcAdapter != null){
            if(nfcAdapter.isEnabled()){
                nfcIsEnable();
                nfcStatus = Constant.NFC_ACTIVE;
            }else{

                nfcIsDisable();
                nfcStatus = Constant.NFC_DISABLE;
            }
        }else{

            nfcNotSupport();
            nfcStatus = Constant.NFC_NOT_SUPPORT;
        }
    }



    public void startWriteDialog(){
        if(nfcAdapter != null){
            if(nfcAdapter.isEnabled()){
                nfcStatus = Constant.NFC_ACTIVE;
            }else{
                nfcStatus = Constant.NFC_DISABLE;
            }
        }



        if( nfcStatus == Constant.NFC_ACTIVE) {
            readyWrite = true;
            waitingWriteDialog();
        }else if( nfcStatus == Constant.NFC_DISABLE) {
            nfcWriteFail(Constant.ERROR_NFC_DISABLE);
        }else if( nfcStatus == Constant.NFC_NOT_SUPPORT) {
            nfcWriteFail(Constant.ERROR_NFC_NOT_SUPPORT);
        }
    }

    /**
     * Call in onNewIntent() <br>
     *  */
    public void onNewIntent(Intent intent){
        if(nfcStatus == Constant.NFC_ACTIVE) {
            if (readyWrite) {
                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                NfcWriteText nfcWriteText = new NfcWriteText(activity);
                nfcWriteText.setListener(new NfcWriteText.WriteTextListener() {
                    @Override
                    public void tagWriteFail(int code) {
                        nfcWriteFail(code);
                        writeErrorDialog();
                        readyWrite = false;
                    }

                    @Override
                    public void writeTextFinsihed() {
                        nfcWriteFinished();
                        writeSuccessDialog();
                        readyWrite = false;
                    }
                });
                nfcWriteText.writeTextMessage(tag, messageWrite);
            }
        }

    }


    /**
     * Set content when write nfc  <br>     *
     *@param message : String message;
     *  */
    public void setTextMessageWrite(String message){
        messageWrite = message;
    }


    /**
    * Enable foreground dispatch (gui hang) nfc  <br>
     *Call in onResume function <br>
     *@param aclass : The activity class, Class a = getClass;
    *  */
    public void onResume(Class aclass){
        if(nfcStatus != Constant.NFC_NOT_SUPPORT) {
            setLog("startWriteTag");
            Intent intent = new Intent(activity, aclass).addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
            PendingIntent pendingIntent = PendingIntent.getActivity(activity, 0, intent, 0);
            IntentFilter[] intentFilter = new IntentFilter[]{};
            nfcAdapter.enableForegroundDispatch(activity, pendingIntent, intentFilter, null);

            IntentFilter filter = new IntentFilter(Constant.ACTION_ADAPTER_STATE_CHANGED);
            activity.registerReceiver(mReceiver, filter);
        }


    }


    /**
     * Disable foreground dispatch (gui hang) nfc  <br>
     *Call in onPause function <br>
     *  */
    public void onPause(){
        if(nfcStatus != Constant.NFC_NOT_SUPPORT) {
            setLog("stopWriteTag");
            activity.unregisterReceiver(mReceiver);
            nfcAdapter.disableForegroundDispatch(activity);

        }

    }


    public void setOnlistenerNfc(NfcWriterListener l){
        mListener = l;
    }


    //TODO write mode
    private void nfcWriteFail(int error){
        if(mListener != null){
            mListener.nfcWriteModeFail(error);
        }


    }
    private void nfcWriteFinished(){

        if(mListener != null){
            mListener.nfcWriteModeFinished();
        }


    }


    private void nfcIsEnable(){
        if(mListener != null){
            mListener.nfcIsEnable();
        }
    }

    private void nfcIsDisable(){
        if(mListener != null){
            mListener.nfcIsDisable();
        }
    }

    private void nfcNotSupport(){
        if(mListener != null){
            mListener.nfcIsNotSupport();
        }
    }

    private void waitingWriteDialog(){

        if(alertDialog != null){
            alertDialog.dismiss();
            alertDialog = null;
        }


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.write_dialog, null);
        ImageView img = (ImageView) dialogView.findViewById(R.id.write_tag_imv);
        AnimationDrawable animation = (AnimationDrawable) img.getDrawable();
        animation.start();

        Button btnCancel = (Button) dialogView.findViewById(R.id.write_tag_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(alertDialog != null){
                    alertDialog.dismiss();
                    alertDialog = null;
                }
                readyWrite = false;
            }
        });
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }


    private void writeErrorDialog(){

        if(alertDialog != null){
            alertDialog.dismiss();
            alertDialog = null;
        }


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.write_error_dialog, null);

        Button btnCancel = (Button) dialogView.findViewById(R.id.write_tag_error_ok);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(alertDialog != null){
                    alertDialog.dismiss();
                    alertDialog = null;
                }
            }
        });
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void writeSuccessDialog(){

        if(alertDialog != null){
            alertDialog.dismiss();
            alertDialog = null;
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.write_success_dialog, null);

        Button btnCancel = (Button) dialogView.findViewById(R.id.write_tag_success_ok);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(alertDialog != null){
                    alertDialog.dismiss();
                    alertDialog = null;
                }
            }
        });
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public interface  NfcWriterListener {
        void nfcIsDisable();
        void nfcIsEnable();
        void nfcIsNotSupport();
        void nfcTurnOn();
        void nfcTurnOff();
        //Write Mode
        void nfcWriteModeFinished();
        void nfcWriteModeFail(int error);
    }


    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(NfcAdapter.ACTION_ADAPTER_STATE_CHANGED)) {
                final int state = intent.getIntExtra(Constant.EXTRA_ADAPTER_STATE, Constant.STATE_OFF);
                setLog("BroadcastReceiver state: " + state);
                switch (state) {
                    case NfcAdapter.STATE_OFF:
                        if(mListener != null){
                            mListener.nfcTurnOff();
                        }
                        break;
                    case NfcAdapter.STATE_ON:
                        if(mListener != null){
                            mListener.nfcTurnOn();
                        }
                        break;
                }
            }
        }
    };

}
