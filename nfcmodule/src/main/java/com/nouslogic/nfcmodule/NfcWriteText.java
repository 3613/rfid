package com.nouslogic.nfcmodule;

import android.app.Activity;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Locale;

/**
 * Created by tiencao on 10/15/17.
 */

public class NfcWriteText {
    private boolean Debug = true;
    private Activity activity;

    public NfcWriteText(Activity a){
        activity = a;
    }

    public void writeTextMessage(Tag tag, String message){
        NdefMessage ndefMessage = createNdefMessage(message);
        writeNdefMessage(tag,ndefMessage);
    }

    public void setDebug(boolean debug){
        Debug = debug;
    }


    private void setLog(String str){
        if(Debug){
            Log.e("NfcHelper", "Write Mode: " + str);
        }
    }

    private void formatTag(Tag tag, NdefMessage ndefMessage) {
        try {
            NdefFormatable ndefFormatable = NdefFormatable.get(tag);
            if (ndefFormatable == null) {
                if(listener != null){
                    listener.tagWriteFail(Constant.ERROR_WRITE_TAG_NOT_NDEF_FORMATABLE);
                }
                return;
            }
            ndefFormatable.connect();
            ndefFormatable.format(ndefMessage);
            ndefFormatable.close();
            if(listener != null){
                listener.writeTextFinsihed();
            }
        } catch (Exception e) {
            Log.e("test", "formatTag Exception: " + e.toString());
        }
    }


    private void writeNdefMessage(Tag tag, NdefMessage ndefMessage){
        try{


            Ndef ndef = Ndef.get(tag);
            if(ndef == null){
                formatTag(tag,ndefMessage);
            }else{
                ndef.connect();
                if(!ndef.isWritable()){
                    if(listener != null){
                        listener.tagWriteFail(Constant.ERROR_WRITE_TAG_NOT_WRITEABLE);
                    }
                    ndef.close();
                    return;
                }
                ndef.writeNdefMessage(ndefMessage);
                ndef.close();
                if(listener != null){
                    listener.writeTextFinsihed();
                }
            }
        }catch (Exception e){
            Log.e("test","writeNdefMessage Exception: " + e.toString());
        }
    }


    private NdefMessage createNdefMessage(String link){
        NdefRecord ndefRecord = NdefRecord.createUri(link);
        NdefMessage ndefMessage = new NdefMessage(new NdefRecord[]{ndefRecord});
        return  ndefMessage;
    }


    private WriteTextListener listener;
    public void setListener(WriteTextListener l){
        listener  = l;
    }



    public interface WriteTextListener {
        void tagWriteFail(int code);
        void writeTextFinsihed();
    }

}
