package com.nouslogic.nfcmodule;

import android.app.Activity;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * Created by tiencao on 10/16/17.
 */

public class NfcReadText {


    private boolean Debug = true;

    Activity activity;

    public NfcReadText(Activity a) {
        activity = a;
    }

    public void setDebug(boolean debug) {
        Debug = debug;
    }


    private void setLog(String str) {
        if (Debug) {
            Log.e("NfcHelper", "Read Mode: " + str);
        }
    }

    public void readTextFromMessage(NdefMessage ndefMessage) {

        setLog("NfcReadText readTextFromMessage");

        NdefRecord[] ndefRecords = ndefMessage.getRecords();
        if (ndefRecords != null && ndefRecords.length > 0) {

            NdefRecord record = ndefRecords[0];
            String tagContent = "no_content";

            byte[] lengthType = record.getType();
             if ((Arrays.equals(lengthType, NdefRecord.RTD_ALTERNATIVE_CARRIER))) {
                setLog("NFC-----------: 01 ");
            } else if ((Arrays.equals(lengthType, NdefRecord.RTD_HANDOVER_CARRIER))) {
                setLog("NFC-----------: 02 ");
            } else if ((Arrays.equals(lengthType, NdefRecord.RTD_HANDOVER_REQUEST))) {
                setLog("NFC-----------: 03 ");
            } else if ((Arrays.equals(lengthType, NdefRecord.RTD_HANDOVER_SELECT))) {
                setLog("NFC-----------: 04 ");
            } else if ((Arrays.equals(lengthType, NdefRecord.RTD_SMART_POSTER))) {
                setLog("NFC-----------: 05 ");
            }
            if (Arrays.equals(lengthType, NdefRecord.RTD_URI)) {
                String str = Arrays.toString(record.getPayload());

                byte[] payload = NdefRecord.RTD_URI;
                StringBuffer pCont = new StringBuffer();
                for (int rn = 0; rn < payload.length; rn++) {
                    pCont.append((char) payload[rn]);
                }
                StringBuffer type = new StringBuffer();
                type.append((char) payload[0]);
                if (type.toString().equals("U")) {
                    if (record.toUri() != null) {
                        tagContent = record.toUri().toString();
                    }
                }
            } else if (Arrays.equals(lengthType, NdefRecord.RTD_TEXT)) {
                String str = Arrays.toString(record.getPayload());

                byte[] payload = record.getPayload();
                StringBuffer pCont = new StringBuffer();
                for (int rn = 3; rn < payload.length; rn++) {
                    pCont.append((char) payload[rn]);
                }
                tagContent = pCont.toString();

            }


            setLog("Read ModeNfcReadText tagContent: " + tagContent);


            if (listener != null) {
                listener.tagContent(tagContent);
            }
        } else {
            setLog("NfcReadText Message no Record");
            if (listener != null) {
                listener.noNDERecordFound();
            }
        }
    }


    public String getTextFromNdefRecord(NdefRecord ndefRecord) {
        String tagContent = null;
        try {
            byte[] payload = ndefRecord.getPayload();
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";
            int languageSize = payload[0] & 0063;
            tagContent = new String(payload, languageSize + 1,
                    payload.length - languageSize - 1, textEncoding);
        } catch (UnsupportedEncodingException e) {
            Log.e("getTextFromNdefRecord", e.getMessage(), e);
        }
        return tagContent;
    }

    ReadTextListener listener;

    public void setListener(ReadTextListener l) {
        listener = l;
    }

    public interface ReadTextListener {
        void noNDERecordFound();

        void tagContent(String message);
    }
}
