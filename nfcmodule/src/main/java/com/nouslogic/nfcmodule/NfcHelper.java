package com.nouslogic.nfcmodule;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by tiencao on 10/15/17.
 */

public class NfcHelper {

    private static final String TAG = NfcHelper.class.getSimpleName();
    private AlertDialog alertDialog = null;
    private int nfcStatus = 0;
    private Activity activity;
    private NfcAdapter nfcAdapter;
    private String messageWrite = "Hello world";
    private int action_mode = Constant.NONE_MODE;


    public boolean DEBUG = true;


    public NfcHelper(Activity a) {
        activity = a;
    }

    //TODO funciton action
    public void openNFCSetting(Activity activity){
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            activity.startActivity(new Intent(android.provider.Settings.ACTION_NFC_SETTINGS));
        } else {
            activity.startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
        }
    }



    public void startReadNFC(){
        setLog("startReadNFC");
        if(nfcAdapter != null){
            if(nfcAdapter.isEnabled()){
                nfcStatus = Constant.NFC_ACTIVE;
            }else{
                nfcStatus = Constant.NFC_DISABLE;
            }
        }

        if( nfcStatus == Constant.NFC_ACTIVE) {
            waittingReadDialog();
        }else if( nfcStatus == Constant.NFC_DISABLE) {
            nfcReadError(Constant.ERROR_NFC_DISABLE);
        }else if( nfcStatus == Constant.NFC_NOT_SUPPORT) {
            nfcReadError(Constant.ERROR_NFC_NOT_SUPPORT);
        }
    }

    public void startReadNFCNoShowDialog(){
        setLog("startReadNFC");
        if(nfcAdapter != null){
            if(nfcAdapter.isEnabled()){
                nfcStatus = Constant.NFC_ACTIVE;
            }else{
                nfcStatus = Constant.NFC_DISABLE;
            }
        }

        if( nfcStatus == Constant.NFC_ACTIVE) {
            action_mode = Constant.READ_MODE;
        }else if( nfcStatus == Constant.NFC_DISABLE) {
            nfcReadError(Constant.ERROR_NFC_DISABLE);
        }else if( nfcStatus == Constant.NFC_NOT_SUPPORT) {
            nfcReadError(Constant.ERROR_NFC_NOT_SUPPORT);
        }
    }

    public void stopReadNFCNoShowDialog(){
        action_mode = Constant.NONE_MODE;
    }

    public void startWriteDialog(){
        setLog("startWriteDialog");
        if(nfcAdapter != null){
            if(nfcAdapter.isEnabled()){
                nfcStatus = Constant.NFC_ACTIVE;
            }else{
                nfcStatus = Constant.NFC_DISABLE;
            }
        }



        if( nfcStatus == Constant.NFC_ACTIVE) {
            waittingWriteDialog();
        }else if( nfcStatus == Constant.NFC_DISABLE) {
            nfcWriteFail(Constant.ERROR_NFC_DISABLE);
        }else if( nfcStatus == Constant.NFC_NOT_SUPPORT) {
            nfcWriteFail(Constant.ERROR_NFC_NOT_SUPPORT);
        }
    }


    public void setOnlistenerNfc(NfcHelperListener l){
        mListener = l;
    }



    public void setTextMessageWrite(String message){
        messageWrite = message;
    }

    //TODO function setting

    /**
     * Call in onNewIntent() <br>
     *  */
    public void onNewIntent(Intent intent){
        if(nfcStatus != Constant.NFC_ACTIVE) {
            return;
        }


        Log.e(TAG, "onNewIntent receive intent ");
        Log.e(TAG, "onNewIntent " + action_mode);
        if(action_mode == Constant.READ_MODE){
            setLog("onNewIntent READ_MODE");
            Parcelable[] parcelables = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if(parcelables != null && parcelables.length > 0){
                NfcReadText nfcReadText = new NfcReadText(activity);
                nfcReadText.setDebug(DEBUG);
                nfcReadText.setListener(new NfcReadText.ReadTextListener() {
                    @Override
                    public void noNDERecordFound() {
                        //Have message but not record
                        nfcReadError(Constant.ERROR_READ_TAG_MESSAGE_NO_RECORD);
                        //todo remove
                        dismissDialog();
                    }

                    @Override
                    public void tagContent(String message) {
                        //Have message text content
                        setLog(" Read Mode: NfcHelper tagContent message: " + message);
                        nfcReadMessageContent(message);
                        dismissDialog();
                    }
                });
                nfcReadText.readTextFromMessage((NdefMessage) parcelables[0]);
            }else{
                setLog(" Read Mode: NfcHelper No message RB");

                nfcReadError(Constant.ERROR_READ_TAG_NO_MESSAGE);
                dismissDialog();
            }
        }else  if (action_mode == Constant.WRITE_MODE) {
            setLog("onNewIntent WRITE_MODE");
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            NfcWriteText nfcWriteText = new NfcWriteText(activity);
            nfcWriteText.setListener(new NfcWriteText.WriteTextListener() {
                @Override
                public void tagWriteFail(int code) {
                    nfcWriteFail(code);
                    writeErrorDialog();

                }

                @Override
                public void writeTextFinsihed() {
                    nfcWriteFinished();
                    writeSuccessDialog();
                }
            });
            nfcWriteText.writeTextMessage(tag, messageWrite);
        }
    }


    public void onCreate(){
        nfcAdapter = NfcAdapter.getDefaultAdapter(activity);
        if(nfcAdapter != null){
            if(nfcAdapter.isEnabled()){
                nfcIsEnable();
                nfcStatus = Constant.NFC_ACTIVE;
            }else{

                nfcIsDisable();
                nfcStatus = Constant.NFC_DISABLE;
            }
        }else{

            nfcNotSupport();
            nfcStatus = Constant.NFC_NOT_SUPPORT;
        }
    }

    /**
     * Enable foreground dispatch (gui hang) nfc  <br>
     *Call in onResume function <br>
     *@param aclass : The activity class, Class a = getClass;
     *  */
    public void onResume(Class aclass){
        if(nfcStatus != Constant.NFC_NOT_SUPPORT) {

            Intent intent = new Intent(activity, aclass).addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
            PendingIntent pendingIntent = PendingIntent.getActivity(activity, 0, intent, 0);
            IntentFilter[] intentFilter = new IntentFilter[]{};
            nfcAdapter.enableForegroundDispatch(activity, pendingIntent, intentFilter, null);

            IntentFilter filter = new IntentFilter(Constant.ACTION_ADAPTER_STATE_CHANGED);
            activity.registerReceiver(mReceiver, filter);
        }


    }


    /**
     * Disable foreground dispatch (gui hang) nfc  <br>
     *Call in onPause function <br>
     *  */
    public void onPause(){
        if(nfcStatus != Constant.NFC_NOT_SUPPORT) {
            activity.unregisterReceiver(mReceiver);
            nfcAdapter.disableForegroundDispatch(activity);

        }

    }

    //TODO private dialog
    private void waittingWriteDialog(){

        dismissDialog();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.write_dialog, null);
        ImageView img = (ImageView) dialogView.findViewById(R.id.write_tag_imv);
        AnimationDrawable animation = (AnimationDrawable) img.getDrawable();
        animation.start();

        Button btnCancel = (Button) dialogView.findViewById(R.id.write_tag_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
            }
        });
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
        action_mode = Constant.WRITE_MODE;
    }

    private void waittingReadDialog(){
        dismissDialog();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.read_dialog, null);
        ImageView img = (ImageView) dialogView.findViewById(R.id.read_tag_imv);
        AnimationDrawable animation = (AnimationDrawable) img.getDrawable();
        animation.start();

        Button btnCancel = (Button) dialogView.findViewById(R.id.read_tag_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
            }
        });

        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
        action_mode = Constant.READ_MODE;
    }


    private void writeErrorDialog(){

        dismissDialog();


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.write_error_dialog, null);

        Button btnCancel = (Button) dialogView.findViewById(R.id.write_tag_error_ok);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
            }
        });
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void writeSuccessDialog(){

        dismissDialog();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.write_success_dialog, null);

        Button btnCancel = (Button) dialogView.findViewById(R.id.write_tag_success_ok);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
            }
        });
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }



    private void dismissDialog(){
        if(alertDialog != null){
            alertDialog.dismiss();
            alertDialog = null;
        }
        //action_mode = Constant.NONE_MODE;
    }


    //TODO private function
    private void setLog(String str){
        if(DEBUG){
            Log.e("NfcHelper", str);
        }
    }

    private void nfcIsEnable(){
        if(mListener != null){
            mListener.nfcIsEnable();
        }
    }

    private void nfcIsDisable(){
        if(mListener != null){
            mListener.nfcIsDisable();
        }
    }

    private void nfcNotSupport(){
        if(mListener != null){
            mListener.nfcIsNotSupport();
        }
    }

    private void nfcReadError(int error){
        if(mListener != null){
            mListener.nfcReadModeFail(error);
        }
    }


    private void nfcReadMessageContent(String content){
        if(mListener != null){
            mListener.nfcReadModeReadMessage(content);
        }
    }

    private void nfcWriteFail(int error){
        if(mListener != null){
            mListener.nfcWriteModeFail(error);
        }
    }

    private void nfcWriteFinished(){
        if(mListener != null){
            mListener.nfcWriteModeFinished(messageWrite);
        }
    }


    private NfcHelperListener mListener;


    public interface  NfcHelperListener {
        void nfcIsDisable();
        void nfcIsEnable();
        void nfcIsNotSupport();
        void nfcTurnOn();
        void nfcTurnOff();
        //Read Mode
        void nfcReadModeFail(int error);
        void nfcWriteModeFail(int error);
        void nfcReadModeReadMessage(String message);
        void nfcWriteModeFinished(String message);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(NfcAdapter.ACTION_ADAPTER_STATE_CHANGED)) {
                final int state = intent.getIntExtra(Constant.EXTRA_ADAPTER_STATE, Constant.STATE_OFF);
                setLog("BroadcastReceiver state: " + state);
                switch (state) {
                    case NfcAdapter.STATE_OFF:
                        if(mListener != null){
                            mListener.nfcTurnOff();
                        }
                        nfcStatus = Constant.NFC_DISABLE;
                        break;
                    case NfcAdapter.STATE_ON:
                        nfcStatus = Constant.NFC_ACTIVE;
                        if(mListener != null){
                            mListener.nfcTurnOn();
                        }
                        break;
                }
            }
        }
    };
}
